/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureMetadata.java
 */

package mlcc.feature.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FeatureMetadata {

    public static final Comparator<FeatureMetadata> VERSION_NEWEST_FIRST_COMPARATOR =
        Comparator.comparingLong(FeatureMetadata::getVersion).reversed();

    private String featureId;
    private String variant;
    private long version;
    private Date creationDate;
    private FeatureStatus status = FeatureStatus.PREPARING_DATA;
    private long recordCount = 0;
    private Date updatedToDate;
    private FeatureDBConfig featureDBConfig = new FeatureDBConfig();
    private String createSQL;
    private List<String> keyColumnNames = new ArrayList<>();
    private List<String> columnNames = new ArrayList<>();
    private List<FeatureColumnType> columnTypes = new ArrayList<>();

    public FeatureMetadata() {}

    @DataFlowConfigurable
    public FeatureMetadata(
            @DataFlowConfigProperty(required = false, description = "TODO") List<HashMap<String, String>> columns,
            @DataFlowConfigProperty(required = false, description = "TODO") List<String> keyColumnNames,
            @DataFlowConfigProperty(required = false, description = "TODO") String featureId,
            @DataFlowConfigProperty(required = false, description = "TODO") String variant,
            @DataFlowConfigProperty(required = false, description = "TODO") long version
    ) {
        this.keyColumnNames = keyColumnNames;
        this.variant = variant;
        this.featureId = featureId;
        this.version = version;

        List<String> colNames = columns.stream().map(col -> col.get("name")).collect(Collectors.toList());
        this.columnNames = colNames;
        List<FeatureColumnType> colTypes = columns.stream().map(col -> FeatureColumnType.forName(col.get("type"))).collect(Collectors.toList());
        this.columnTypes = colTypes;
    }

    public String getFeatureId() {
        return featureId;
    }

    public String getVariant() {
        return variant;
    }

    public long getVersion() {
        return version;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public List<String> getKeyColumnNames() {
        return keyColumnNames;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public List<FeatureColumnType> getColumnTypes() {
        return columnTypes;
    }

    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setKeyColumnNames(List<String> keyColumnNames) {
        this.keyColumnNames = keyColumnNames;
    }

    public void setColumnNames(List<String> columnNames) {
        this.columnNames = columnNames;
    }

    public void setColumnTypes(List<FeatureColumnType> columnTypes) {
        this.columnTypes = columnTypes;
    }

    public FeatureStatus getStatus() {
        return status;
    }

    public void setStatus(FeatureStatus status) {
        this.status = status;
    }

    public Date getUpdatedToDate() {
        return updatedToDate;
    }

    public void setUpdatedToDate(Date updatedToDate) {
        this.updatedToDate = updatedToDate;
    }

    public FeatureDBConfig getFeatureDBConfig() {
        return featureDBConfig;
    }

    public void setFeatureDBConfig(FeatureDBConfig featureDBConfig) {
        this.featureDBConfig = featureDBConfig;
    }

    @JsonIgnore
    public String getFeatureDBTypeId() {
        return featureDBConfig != null ? featureDBConfig.getTypeId() : null;
    }

    @JsonIgnore
    public String getFeatureDBClusterId() {
        return featureDBConfig != null ? featureDBConfig.getClusterId() : null;
    }

    public long getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(long rowCount) {
        this.recordCount = rowCount;
    }

    public String getCreateSQL() {
        return createSQL;
    }

    public void setCreateSQL(String createSQL) {
        this.createSQL = createSQL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeatureMetadata that = (FeatureMetadata) o;
        return version == that.version && recordCount == that.recordCount && Objects.equals(featureId, that.featureId) && Objects.equals(variant, that.variant) && Objects.equals(creationDate, that.creationDate) && status == that.status && Objects.equals(updatedToDate, that.updatedToDate) && Objects.equals(featureDBConfig, that.featureDBConfig) && Objects.equals(createSQL, that.createSQL) && Objects.equals(keyColumnNames, that.keyColumnNames) && Objects.equals(columnNames, that.columnNames) && Objects.equals(columnTypes, that.columnTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(featureId, variant, version, creationDate, status, recordCount, updatedToDate, featureDBConfig, createSQL, keyColumnNames, columnNames, columnTypes);
    }

    @Override
    public String toString() {
        return "FeatureMetadata{" +
                "featureId='" + featureId + '\'' +
                ", variant='" + variant + '\'' +
                ", version=" + version +
                ", creationDate=" + creationDate +
                ", status=" + status +
                ", recordCount=" + recordCount +
                ", updatedToDate=" + updatedToDate +
                ", featureDBConfig=" + featureDBConfig +
                ", createSQL='" + createSQL + '\'' +
                ", keyColumnNames=" + keyColumnNames +
                ", columnNames=" + columnNames +
                ", columnTypes=" + columnTypes +
                '}';
    }
}
