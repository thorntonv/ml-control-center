/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureDBManager.java
 */

package mlcc.feature.api;

import java.io.IOException;
import java.util.List;

public interface FeatureDBManager {

    String DEFAULT_VARIANT = "default";

    List<FeatureMetadata> getFeatureMetadata() throws IOException;

    List<FeatureMetadata> getFeatureMetadata(String featureId) throws IOException;

    FeatureMetadata createFeatureVersion(FeatureMetadata featureMetadata) throws IOException;
    void updateFeatureMetadata(FeatureMetadata featureMetadata) throws IOException;
    void setFeatureActiveVersion(FeatureMetadata featureMetadata) throws IOException;
    FeatureMetadata getFeatureActiveVersionMetadata(String featureId, String variant) throws IOException;
    void deleteFeatureVersion(FeatureMetadata featureMetadata) throws IOException;
    void deleteFeature(String featureId) throws IOException;

    List<FeatureMetadata> cleanup(String featureId, String variant) throws IOException;

    FeatureData getFeatureData(FeatureMetadata featureMetadata) throws IOException;
    FeatureData getFeatureData(String featureId, String variant, Integer version,
        String dbType, String clusterId) throws IOException;

    FeatureData getFeatureData(String featureId, String variant) throws IOException;
}
