/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureDataKey.java
 */

package mlcc.feature.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeatureDataKey extends ArrayList<Object> {

    public static FeatureDataKey create(Object... components) {
        FeatureDataKey key = new FeatureDataKey();
        for (Object component : components) {
            key.add(component);
        }
        return key;
    }

    public static FeatureDataKey create(Iterable<Object> components) {
        FeatureDataKey key = new FeatureDataKey();
        for (Object component : components) {
            key.add(component);
        }
        return key;
    }

}
