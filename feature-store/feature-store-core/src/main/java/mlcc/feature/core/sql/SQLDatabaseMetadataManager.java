/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLDatabaseMetadataManager.java
 */

package mlcc.feature.core.sql;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import javax.sql.DataSource;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.core.FeatureMetadataManager;
import mlcc.feature.core.FeatureMetadataValidator;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * A {@link FeatureMetadataManager} implementation that stores metadata in a SQL database.
 */
public class SQLDatabaseMetadataManager implements FeatureMetadataManager {

    private static final String TABLE_NAME = "featureMetadata";
    private static final String FEATURE_ID_COLUMN_NAME = "featureId";
    private static final String VARIANT_COLUMN_NAME = "variant";
    private static final String VERSION_COLUMN_NAME = "version";
    private static final String METADATA_JSON_COLUMN_NAME = "metadataJson";

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final TransactionTemplate transactionTemplate;
    private static final RowMapper<FeatureMetadata> ROW_MAPPER = (rs, rowNum) -> {
        String metadataJson = rs.getString(METADATA_JSON_COLUMN_NAME);
        try {
            return objectMapper.readValue(metadataJson, FeatureMetadata.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    };

    public SQLDatabaseMetadataManager(DataSource dataSource) {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
        this.jdbcTemplate = new NamedParameterJdbcTemplate(transactionManager.getDataSource());
        this.transactionTemplate = new TransactionTemplate(transactionManager);

        try {
            jdbcTemplate.getJdbcOperations().execute(String.format("CREATE TABLE %s (" +
                "featureId VARCHAR(255), variant VARCHAR(255), version BIGINT, metadataJson TEXT, " +
                "active BOOLEAN, " +
                "PRIMARY KEY (featureId, variant, version))",
                TABLE_NAME));
        } catch (BadSqlGrammarException ex) {
            if (!ex.getMessage().contains("exists")) {
                throw ex;
            }
        }
    }

    @Override
    public List<FeatureMetadata> getFeatureMetadata() throws IOException {
        return jdbcTemplate.query(String.format("SELECT metadataJson FROM %s", TABLE_NAME), ROW_MAPPER);
    }

    @Override
    public List<FeatureMetadata> getFeatureMetadata(String featureId) throws IOException {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("featureId", featureId);
        List<FeatureMetadata> metadataList = jdbcTemplate.query(String.format(
            "SELECT metadataJson FROM %s WHERE featureId = :featureId", TABLE_NAME),
            paramSource, ROW_MAPPER);
        metadataList.sort(FeatureMetadata.VERSION_NEWEST_FIRST_COMPARATOR);
        return metadataList;
    }

    @Override
    public FeatureMetadata getActiveFeatureActiveVersionMetadata(String featureId, String variant) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue(FEATURE_ID_COLUMN_NAME, featureId);
        paramSource.addValue(VARIANT_COLUMN_NAME, variant);
        List<FeatureMetadata> results = jdbcTemplate.query(String.format(
            "SELECT * FROM %s WHERE featureId = :featureId AND " +
                "variant = :variant AND active = TRUE", TABLE_NAME), paramSource, ROW_MAPPER);
        if (results.isEmpty()) {
            return null;
        }
        if (results.size() != 1) {
            throw new IllegalStateException("Found multiple active version for featureId " +
                featureId + " variant " + variant);
        }
        return results.get(0);
    }

    @Override
    public void setActiveFeatureMetadata(FeatureMetadata metadata) throws IOException {
        try {
            transactionTemplate.executeWithoutResult(transactionStatus -> {
                jdbcTemplate.getJdbcTemplate().update(String.format(
                    "UPDATE %s SET active = TRUE WHERE " +
                        "featureId = ? AND variant = ? AND version = ?", TABLE_NAME),
                    metadata.getFeatureId(), metadata.getVariant(), metadata.getVersion());
                jdbcTemplate.getJdbcTemplate().update(String.format(
                    "UPDATE %s SET active = FALSE WHERE " +
                        "featureId = ? AND variant = ? AND version <> ?", TABLE_NAME),
                    metadata.getFeatureId(), metadata.getVariant(), metadata.getVersion());
            });
        } catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void addFeatureMetadata(FeatureMetadata metadata) throws IOException {
        FeatureMetadataValidator.validate(metadata);
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue(FEATURE_ID_COLUMN_NAME, metadata.getFeatureId());
        paramSource.addValue(VARIANT_COLUMN_NAME, metadata.getVariant());
        paramSource.addValue(VERSION_COLUMN_NAME, metadata.getVersion());
        paramSource.addValue(METADATA_JSON_COLUMN_NAME, objectMapper.writeValueAsString(metadata));
        String insertQuery = String.format(
            "INSERT INTO %s (featureId, variant, version, active, metadataJson) " +
            "VALUES(:featureId, :variant, :version, FALSE, :metadataJson)", TABLE_NAME);
        try {
            jdbcTemplate.update(insertQuery, paramSource);
        } catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void updateFeatureMetadata(FeatureMetadata metadata) throws IOException {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue(FEATURE_ID_COLUMN_NAME, metadata.getFeatureId());
        paramSource.addValue(VARIANT_COLUMN_NAME, metadata.getVariant());
        paramSource.addValue(VERSION_COLUMN_NAME, metadata.getVersion());
        paramSource.addValue(METADATA_JSON_COLUMN_NAME, objectMapper.writeValueAsString(metadata));
        String updateQuery = String.format(
            "UPDATE %s SET metadataJson = :metadataJson WHERE " +
            "featureId = :featureId AND variant = :variant AND version = :version", TABLE_NAME);
        try {
            jdbcTemplate.update(updateQuery, paramSource);
        } catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    @Override
    public void deleteFeatureMetadata(FeatureMetadata metadata) throws IOException {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue(FEATURE_ID_COLUMN_NAME, metadata.getFeatureId());
        paramSource.addValue(VARIANT_COLUMN_NAME, metadata.getVariant());
        paramSource.addValue(VERSION_COLUMN_NAME, metadata.getVersion());
        String deleteQuery = String.format("DELETE from %s WHERE " +
            "featureId = :featureId AND variant = :variant AND version = :version", TABLE_NAME);
        jdbcTemplate.update(deleteQuery, paramSource);
    }
}
