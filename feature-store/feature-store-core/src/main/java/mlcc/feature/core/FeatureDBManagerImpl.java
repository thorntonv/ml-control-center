/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureDBManagerImpl.java
 */

package mlcc.feature.core;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import mlcc.feature.api.FeatureDBConfig;
import mlcc.feature.api.FeatureDBManager;
import mlcc.feature.api.FeatureData;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.api.FeatureStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manages feature databases.
 */
public class FeatureDBManagerImpl implements FeatureDBManager {

    private static final Logger logger = LoggerFactory.getLogger(FeatureDBManagerImpl.class);

    private final FeatureMetadataManager metadataManager;
    private final List<FeatureDatabase> featureDBS;

    public FeatureDBManagerImpl(FeatureMetadataManager metadataManager,
        List<FeatureDatabase> featureDBS) {
        this.metadataManager = metadataManager;
        this.featureDBS = featureDBS;
    }

    @Override
    public List<FeatureMetadata> getFeatureMetadata() throws IOException {
        return metadataManager.getFeatureMetadata();
    }

    @Override
    public List<FeatureMetadata> getFeatureMetadata(String featureId) throws IOException {
        return metadataManager.getFeatureMetadata(featureId);
    }

    @Override
    public FeatureMetadata createFeatureVersion(FeatureMetadata featureMetadata)
        throws IOException {
        featureMetadata.setVersion(getVersion());
        FeatureMetadataValidator.validate(featureMetadata);
        FeatureDatabase featureDB = getFeatureDB(featureMetadata);
        metadataManager.addFeatureMetadata(featureMetadata);
        featureDB.createFeatureVersion(featureMetadata);
        logger.info("Created feature version " + featureMetadata);
        cleanUpOldVersions(featureMetadata.getFeatureId(), featureMetadata.getVariant(), featureDB);
        return featureMetadata;
    }

    @Override
    public void updateFeatureMetadata(FeatureMetadata featureMetadata) throws IOException {
        FeatureMetadataValidator.validate(featureMetadata);
        metadataManager.updateFeatureMetadata(featureMetadata);
    }

    @Override
    public void setFeatureActiveVersion(FeatureMetadata featureMetadata) throws IOException {
        logger.info("Setting feature active version: " + featureMetadata);
        FeatureDatabase featureDB = getFeatureDB(featureMetadata);
        metadataManager.setActiveFeatureMetadata(featureMetadata);
        featureDB.setFeatureActiveVersion(featureMetadata);
    }

    @Override
    public FeatureMetadata getFeatureActiveVersionMetadata(String featureId, String variant)
        throws IOException {
        return metadataManager.getActiveFeatureActiveVersionMetadata(featureId, variant);
    }

    @Override
    public void deleteFeatureVersion(FeatureMetadata featureMetadata) throws IOException {
        logger.info("Deleting feature version " + featureMetadata);
        if (isActiveVersion(featureMetadata)) {
            logger.warn("An attempt was made to delete the active feature version for feature " + featureMetadata);
            throw new IllegalArgumentException("Deleting the feature active version is not allowed");
        }

        FeatureDatabase featureDB = getFeatureDB(featureMetadata);
        featureDB.deleteFeatureVersion(featureMetadata);
        metadataManager.deleteFeatureMetadata(featureMetadata);
    }

    @Override
    public void deleteFeature(String featureId) throws IOException {
        logger.info("Deleting feature " + featureId);
        // Use a map to ensure that we only call delete once on each feature db.
        IdentityHashMap<FeatureDatabase, FeatureMetadata> featureDBMetadataMap =
            new IdentityHashMap<>();
        for (FeatureMetadata metadata : getFeatureMetadata(featureId)) {
            FeatureDatabase featureDB = getFeatureDB(metadata);
            if (!featureDBMetadataMap.containsKey(featureDB)) {
                featureDB.deleteFeature(featureId);
                featureDBMetadataMap.put(featureDB, metadata);
            }
            metadataManager.deleteFeatureMetadata(metadata);
        }
    }

    @Override
    public List<FeatureMetadata> cleanup(String featureId, String variant) throws IOException {
        FeatureMetadata featureMetadata = metadataManager.getActiveFeatureActiveVersionMetadata(featureId, variant);
        FeatureDatabase featureDB = getFeatureDB(featureMetadata);
        logger.info("Cleaning up feature " + featureId + ":" + variant);
        return cleanUpOldVersions(featureMetadata.getFeatureId(), featureMetadata.getVariant(), featureDB);
    }


    @Override
    public FeatureData getFeatureData(FeatureMetadata metadata) {
        FeatureDatabase featureDB = getFeatureDB(metadata);
        return featureDB.getFeatureData(metadata);
    }

    @Override
    public FeatureData getFeatureData(String featureId, String variant, Integer version,
        String dbType, String clusterId) throws IOException {
        if (version == null) {
            return getFeatureData(metadataManager.getActiveFeatureActiveVersionMetadata(
                featureId, variant));
        }
        Optional<FeatureMetadata> metadata = metadataManager.getFeatureMetadata(featureId).stream()
            .filter(md -> md.getVariant().equals(variant != null ? variant : DEFAULT_VARIANT))
            .filter(md -> md.getVersion() == version)
            .filter(md -> md.getFeatureDBTypeId().equals(dbType))
            .filter(md -> md.getFeatureDBClusterId().equals(clusterId))
            .findFirst();
        if (!metadata.isPresent()) {
            throw new IOException(String.format(
                "Feature %s, variant=%s, version=%d, dbType=%s, clusterId=%s was not found",
                featureId, variant, version, dbType, clusterId));
        }
        return getFeatureData(metadata.get());
    }

    @Override
    public FeatureData getFeatureData(String featureId, String variant) throws IOException {
        FeatureMetadata metadata = metadataManager.getActiveFeatureActiveVersionMetadata(featureId, variant);
        if (metadata == null) {
            throw new IOException(String.format(
                "There is no active version for feature %s variant %s", featureId, variant));
        }
        FeatureDatabase featureDB = getFeatureDB(metadata);
        return featureDB.getFeatureData(metadata);
    }

    private FeatureDatabase getFeatureDB(FeatureMetadata metadata) {
        Stream<FeatureDatabase> featureDBStream = featureDBS.stream();
        if(metadata.getFeatureDBTypeId() != null && !metadata.getFeatureDBTypeId().isEmpty()) {
            featureDBStream = featureDBStream.filter(fs ->
                fs.getTypeId().equalsIgnoreCase(metadata.getFeatureDBTypeId()));
        }
        if(metadata.getFeatureDBClusterId() != null && !metadata.getFeatureDBClusterId().isEmpty()) {
            featureDBStream = featureDBStream.filter(fs ->
                fs.getClusterId().equalsIgnoreCase(metadata.getFeatureDBClusterId()));
        }
        List<FeatureDatabase> candidates = featureDBStream.collect(Collectors.toList());

        if (candidates.size() != 1) {
            if(candidates.isEmpty()) {
                throw new IllegalArgumentException(
                    "No feature db registered for feature " + metadata.getFeatureId() +
                        " variant " + metadata.getVariant() + ": " + metadata.getFeatureDBConfig());
            } else {
                throw new IllegalArgumentException(
                    "Multiple feature dbs registered for feature " + metadata.getFeatureId() +
                        " variant " + metadata.getVariant() + ": " + metadata.getFeatureDBConfig() +
                        " [" + candidates + "]");
            }
        }
        return candidates.get(0);
    }

    private boolean isActiveVersion(FeatureMetadata featureMetadata) throws IOException {
        FeatureMetadata activeVersion = metadataManager.getActiveFeatureActiveVersionMetadata(
            featureMetadata.getFeatureId(), featureMetadata.getVariant());
        return activeVersion != null && featureMetadata.getVersion() == activeVersion.getVersion();
    }

    private long getVersion() {
        String versionStr = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        return Long.parseLong(versionStr);
    }

    private List<FeatureMetadata> cleanUpOldVersions(String featureId, String variant,
            FeatureDatabase featureDB) throws IOException {
        logger.info(String.format("Checking for old versions of feature %s variant %s in feature db %s to clean up",
            featureId, variant, summaryStr(featureDB)));
        FeatureMetadata activeFeatureMetadata = getFeatureActiveVersionMetadata(featureId, variant);
        if (activeFeatureMetadata == null) {
            logger.info(String.format(
                "Skipping cleanup. There is no active version of feature %s variant %s", featureId, variant));
            return Collections.emptyList();
        }
        List<FeatureMetadata> deleted = new ArrayList<>();
        FeatureDBConfig config = activeFeatureMetadata.getFeatureDBConfig();
        List<FeatureMetadata> metadataList = getFeatureMetadata(featureId).stream()
            .filter(metadata -> metadata.getVariant().equals(variant))
            .sorted(FeatureMetadata.VERSION_NEWEST_FIRST_COMPARATOR)
            .collect(Collectors.toList());
        boolean foundActiveVersion = false;
        int backupsFound = 0;
        for(FeatureMetadata metadata : metadataList) {
            long ageMillis = System.currentTimeMillis() - metadata.getCreationDate().getTime();
            long ageHours = TimeUnit.HOURS.convert(ageMillis, TimeUnit.MILLISECONDS);
            if(metadata.getVersion() == activeFeatureMetadata.getVersion()) {
                foundActiveVersion = true;
                logger.info("Found active version " + metadata);
            } else if (!foundActiveVersion) {
                logger.info("Found version that is newer than the active version " + metadata);
            } else if(metadata.getStatus() == FeatureStatus.READY &&
                    backupsFound < config.getNumOldVersionsToKeep()) {
                backupsFound++;
                logger.info("Found good backup " + metadata);
            } else if(ageHours < config.getMinAgeForOldVersionCleanupHours()) {
                logger.info(String.format("Skipping cleanup of feature %s. " +
                        "It is %d hours old which is less than the minimum of %d hours",
                    metadata, ageHours, config.getMinAgeForOldVersionCleanupHours()));
            } else {
                logger.info(String.format("Deleting feature %s", metadata));
                deleted.add(metadata);
                deleteFeatureVersion(metadata);
            }
        }
        logger.info(String.format("Finished checking for old versions of feature %s variant %s in feature db %s to clean up",
            featureId, variant, summaryStr(featureDB)));
        return deleted;
    }

    private String summaryStr(FeatureDatabase featureDB) {
        return String.format("FeatureDB {typeId=%s, clusterId=%s}",
            featureDB.getTypeId(), featureDB.getClusterId());
    }
}