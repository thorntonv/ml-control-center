/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureWriter.java
 */

package mlcc.feature.core.dataflow;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.exception.DataFlowExecutionException;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import mlcc.feature.api.FeatureDBManager;
import mlcc.feature.api.FeatureData;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.core.FeatureDatabase;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DataFlowComponent(description = "Component for writing feature data. This allows a pipeline to write multiple features")
public class FeatureWriter implements AutoCloseable {

    private final Logger logger = LoggerFactory.getLogger(FeatureWriter.class);

    private final FeatureData featureData;
    private final ExecutorService executor = Executors.newCachedThreadPool();

    @DataFlowConfigurable
    public FeatureWriter(@DataFlowConfigProperty String featureId,
        @DataFlowConfigProperty String variant,
        @DataFlowConfigProperty(required = false) Long version,
        @DataFlowConfigProperty(required = false) String dbType,
        @DataFlowConfigProperty(required = false) String clusterId,
        @DataFlowConfigProperty(required = false,
            description = "The feature database to write to") FeatureDatabase featureDb) {
        FeatureDBManager featureDBManager = DataFlowExecutionContext.getCurrentExecutionContext()
            .getDependencyInjector().getInstance(FeatureDBManager.class);
        try {
            if(featureDb == null) {
                this.featureData = featureDBManager.getFeatureData(
                    featureId, variant, version.intValue(), dbType, clusterId);
            } else {
                FeatureMetadata metadata;
                if(version == null) {
                    metadata = featureDBManager.getFeatureActiveVersionMetadata(featureId, variant);
                } else {
                    metadata = featureDBManager.getFeatureMetadata(featureId).stream()
                        .filter(m -> m.getVariant().equals(variant))
                        .filter(m -> m.getVersion() == version)
                        .findAny().orElse(null);
                }

                this.featureData = featureDb.getFeatureData(metadata);
            }
        } catch (IOException e) {
            throw new DataFlowConfigurationException("Unable to get feature data", e);
        }
    }

    @OutputValue
    public CompletableFuture<Object> getValue(@InputValue Object records) throws DataFlowExecutionException {
        CompletableFuture<Object> result = new CompletableFuture<>();
        executor.submit(() -> {
            try {
                if (records instanceof DataRecords) {
                    featureData.write((DataRecords) records);
                } else if (records instanceof DataRecord) {
                    featureData.write((DataRecord) records);
                } else if (records instanceof Publisher) {
                    featureData.write((Publisher<DataRecord>) records);
                }
                result.complete(records);
            } catch (Throwable e) {
                logger.warn("Error writing records to feature db", e);
                result.completeExceptionally(new DataFlowExecutionException("Error writing records to feature db", e));
            }
        });
        return result;
    }

    @Override
    public void close() throws Exception {
        featureData.close();
    }
}
