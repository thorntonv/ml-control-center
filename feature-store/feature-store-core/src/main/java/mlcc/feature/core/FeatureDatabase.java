/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureDatabase.java
 */

package mlcc.feature.core;

import java.io.IOException;
import mlcc.feature.api.FeatureData;
import mlcc.feature.api.FeatureMetadata;

public interface FeatureDatabase {

    String getTypeId();
    String getClusterId();

    void deleteFeature(String featureId) throws IOException;
    void createFeatureVersion(FeatureMetadata featureMetadata) throws IOException;
    void setFeatureActiveVersion(FeatureMetadata metadata) throws IOException;
    void deleteFeatureVersion(FeatureMetadata metadata) throws IOException;

    FeatureData getFeatureData(FeatureMetadata metadata);
}
