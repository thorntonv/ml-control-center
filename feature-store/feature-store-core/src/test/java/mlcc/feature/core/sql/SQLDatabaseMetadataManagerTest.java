/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLDatabaseMetadataManagerTest.java
 */

package mlcc.feature.core.sql;

import static mlcc.feature.api.FeatureDBManager.DEFAULT_VARIANT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import mlcc.feature.api.FeatureColumnType;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.api.FeatureStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

public class SQLDatabaseMetadataManagerTest {

    private SQLDatabaseMetadataManager metadataManager;

    private static final String TEST_FEATURE_ID_1 = "testFeature1";
    private static final String TEST_FEATURE_ID_2 = "testFeature2";

    private static final List<String> TEST_KEY_COLUMNS_1 = ImmutableList.of("id");

    private static final List<String> TEST_COLUMNS_1 = ImmutableList.of("id", "name", "postalCode");
    private static final List<FeatureColumnType> TEST_COLUMN_TYPES_1 =
        ImmutableList.of(FeatureColumnType.INTEGER, FeatureColumnType.STRING, FeatureColumnType.INTEGER);

    private static final List<String> TEST_COLUMNS_2 = ImmutableList.of("id", "age", "favoriteColor");
    private static final List<FeatureColumnType> TEST_COLUMN_TYPES_2 =
        ImmutableList.of(FeatureColumnType.INTEGER, FeatureColumnType.STRING, FeatureColumnType.INTEGER);

    private static final FeatureMetadata TEST_METADATA_1 = createMetadata(TEST_FEATURE_ID_1, 1,
        TEST_KEY_COLUMNS_1, TEST_COLUMNS_1,TEST_COLUMN_TYPES_1, FeatureStatus.PREPARING_DATA);
    private static final FeatureMetadata TEST_METADATA_1_UPDATED = createMetadata(TEST_FEATURE_ID_1, 1,
        TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, FeatureStatus.READY);
    private static final FeatureMetadata TEST_METADATA_2 = createMetadata(TEST_FEATURE_ID_1, 2,
        TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, FeatureStatus.READY);
    private static final FeatureMetadata TEST_METADATA_3 = createMetadata(TEST_FEATURE_ID_2, 1,
        TEST_KEY_COLUMNS_1, TEST_COLUMNS_2, TEST_COLUMN_TYPES_2, FeatureStatus.READY);

    private EmbeddedDatabase db;

    @Before
    public void setUp() {
        this.db = new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.H2)
            .build();
        this.metadataManager = new SQLDatabaseMetadataManager(db);
    }

    @After
    public void cleanUp() {
        this.db.shutdown();
    }

    @Test
    public void createDB_alreadyExists() throws IOException {
        assertEquals(0, new SQLDatabaseMetadataManager(db).getFeatureMetadata().size());
    }

    @Test
    public void testAddFeatureMetadata_singleFeature_singleVersion() throws IOException {
        assertEquals(0, metadataManager.getFeatureMetadata().size());
        metadataManager.addFeatureMetadata(TEST_METADATA_1);
        List<FeatureMetadata> featureMetadata = metadataManager.getFeatureMetadata();
        assertEquals(1, featureMetadata.size());
        assertEquals(TEST_METADATA_1, featureMetadata.get(0));
    }

    @Test
    public void testAddFeatureMetadata_alreadyExists() throws IOException {
        assertEquals(0, metadataManager.getFeatureMetadata().size());
        metadataManager.addFeatureMetadata(TEST_METADATA_1);
        List<FeatureMetadata> featureMetadata = metadataManager.getFeatureMetadata();
        assertEquals(1, featureMetadata.size());
        assertEquals(TEST_METADATA_1, featureMetadata.get(0));
        try {
            metadataManager.addFeatureMetadata(TEST_METADATA_1);
            fail("expected exception was not thrown");
        } catch (Exception ex) {
            // Expected.
        }
    }

    @Test
    public void testAddFeatureMetadata_singleFeature_multipleVersions() throws IOException {
        assertEquals(0, metadataManager.getFeatureMetadata().size());
        metadataManager.addFeatureMetadata(TEST_METADATA_1);
        assertEquals(TEST_METADATA_1, metadataManager.getFeatureMetadata(TEST_FEATURE_ID_1).get(0));
        metadataManager.addFeatureMetadata(TEST_METADATA_2);
        List<FeatureMetadata> featureMetadata = metadataManager.getFeatureMetadata();
        assertEquals(2, featureMetadata.size());
        assertEquals(TEST_METADATA_1, featureMetadata.get(0));
        assertEquals(TEST_METADATA_2, featureMetadata.get(1));
        assertEquals(TEST_METADATA_2, metadataManager.getFeatureMetadata(TEST_FEATURE_ID_1).get(0));
    }

    @Test
    public void testAddFeatureMetadata_multipleFeatures_multipleVersions() throws IOException {
        assertEquals(0, metadataManager.getFeatureMetadata().size());
        metadataManager.addFeatureMetadata(TEST_METADATA_1);
        assertEquals(TEST_METADATA_1, metadataManager.getFeatureMetadata(TEST_FEATURE_ID_1).get(0));
        metadataManager.addFeatureMetadata(TEST_METADATA_2);
        metadataManager.addFeatureMetadata(TEST_METADATA_3);
        List<FeatureMetadata> featureMetadata = metadataManager.getFeatureMetadata();
        assertEquals(3, featureMetadata.size());
        assertEquals(TEST_METADATA_1, featureMetadata.get(0));
        assertEquals(TEST_METADATA_2, featureMetadata.get(1));
        assertEquals(TEST_METADATA_3, featureMetadata.get(2));
        assertEquals(TEST_METADATA_2, metadataManager.getFeatureMetadata(TEST_FEATURE_ID_1).get(0));
        assertEquals(2, metadataManager.getFeatureMetadata(TEST_FEATURE_ID_1).size());
        assertEquals(1, metadataManager.getFeatureMetadata(TEST_FEATURE_ID_2).size());
    }

    @Test
    public void testGetFeatureMetadata_nonExistentFeature() throws IOException {
        assertEquals(0, metadataManager.getFeatureMetadata("nonExistent").size());
    }

    @Test
    public void testUpdateFeatureMetadata() throws IOException {
        assertEquals(0, metadataManager.getFeatureMetadata().size());
        metadataManager.addFeatureMetadata(TEST_METADATA_1);
        List<FeatureMetadata> featureMetadata = metadataManager.getFeatureMetadata();
        assertEquals(1, featureMetadata.size());
        assertEquals(TEST_METADATA_1, featureMetadata.get(0));
        metadataManager.updateFeatureMetadata(TEST_METADATA_1_UPDATED);
        featureMetadata = metadataManager.getFeatureMetadata();
        assertEquals(1, featureMetadata.size());
        assertEquals(TEST_METADATA_1_UPDATED, featureMetadata.get(0));
    }

    @Test
    public void testUpdateFeatureMetadata_nonExistent() throws IOException {
        assertEquals(0, metadataManager.getFeatureMetadata().size());
        metadataManager.updateFeatureMetadata(TEST_METADATA_1_UPDATED);
        assertEquals(0, metadataManager.getFeatureMetadata().size());
    }

    @Test
    public void testDeleteFeatureMetadata() throws IOException {
        assertEquals(0, metadataManager.getFeatureMetadata().size());
        metadataManager.addFeatureMetadata(TEST_METADATA_1);
        List<FeatureMetadata> featureMetadata = metadataManager.getFeatureMetadata();
        assertEquals(1, featureMetadata.size());
        assertEquals(TEST_METADATA_1, featureMetadata.get(0));
        metadataManager.deleteFeatureMetadata(TEST_METADATA_1);
        assertEquals(0, metadataManager.getFeatureMetadata().size());
    }

    @Test
    public void testSetFeatureActiveVersion() throws IOException {
        metadataManager.addFeatureMetadata(TEST_METADATA_1);
        metadataManager.addFeatureMetadata(TEST_METADATA_2);

        metadataManager.setActiveFeatureMetadata(TEST_METADATA_1);
        assertEquals(TEST_METADATA_1, metadataManager.getActiveFeatureActiveVersionMetadata(TEST_FEATURE_ID_1, DEFAULT_VARIANT));
        metadataManager.setActiveFeatureMetadata(TEST_METADATA_2);
        assertEquals(TEST_METADATA_2, metadataManager.getActiveFeatureActiveVersionMetadata(TEST_FEATURE_ID_1, DEFAULT_VARIANT));
    }

    private static FeatureMetadata createMetadata(String featureId, long version, List<String> keyColumns,
        List<String> columnNames, List<FeatureColumnType> columnTypes, FeatureStatus status) {
        FeatureMetadata metadata = new FeatureMetadata();
        metadata.setFeatureId(featureId);
        metadata.setVariant(DEFAULT_VARIANT);
        metadata.setVersion(version);
        metadata.setStatus(status);
        metadata.getFeatureDBConfig().setTypeId("postgres");
        metadata.getFeatureDBConfig().setClusterId("primary");
        metadata.getFeatureDBConfig().setNumOldVersionsToKeep(2);
        metadata.getFeatureDBConfig().setMinAgeForOldVersionCleanupHours(0);
        metadata.getFeatureDBConfig().setAutoCreateColumns(false);
        metadata.setCreationDate(new Date());
        metadata.setKeyColumnNames(keyColumns);
        metadata.setColumnNames(columnNames);
        metadata.setColumnTypes(columnTypes);
        return metadata;
    }
}