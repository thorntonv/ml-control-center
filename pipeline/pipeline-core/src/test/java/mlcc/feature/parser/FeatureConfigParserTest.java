/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureConfigParserTest.java
 */

package mlcc.feature.parser;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.data.query.DataQueryValueProvider;
import dataflow.sql.SQLDatabaseManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.sql.DataSource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class FeatureConfigParserTest {

    private DataFlowEnvironment env;
    private FeatureConfigParser parser;
    @Mock
    private DataSource mockDataSource;

    @Before
    public void setUp() {
        SQLDatabaseManager databaseManager = new SQLDatabaseManager();
        databaseManager.setDefaultDataSourceId("default");
        databaseManager.registerDataSource("default", mockDataSource);
        env = new DataFlowEnvironment("test", new SimpleDependencyInjector(databaseManager), null);
        parser = new FeatureConfigParser(env.getRegistry());
    }

    @Test
    public void parse_reviews() throws Exception {
        FeatureConfig config = parser.parse(ClassLoader.getSystemResourceAsStream(
            "features/reviews.yaml"));
        DataFlowConfig inputConfig = config.getInputConfigs().get(0);
//        engine.registerDataFlow(inputConfig);
        DataFlowInstance instance = env.getRegistry().getDataFlowRegistration(inputConfig.getId()).getInstanceFactory().newInstance();
        instance.execute();
        assertTrue(instance.getOutput() instanceof DataQueryValueProvider);
        assertEquals("FeatureConfig{id='reviews', variant='default', version=1, featureDBConfig=FeatureDBConfig{typeId='sql', clusterId='null', numOldVersionsToKeep=0, minAgeForOldVersionCleanupHours=0, autoCreateColumns=false, defaultFetchSize=100, extraConfig=null}, featureCreateSQL='null', keyColumnNames=[reviewId], columnNames=[reviewId, languageId, pros, cons, headline, feedback], columnTypes=[INTEGER, STRING, STRING, STRING, STRING, STRING], fullUpdateConfig=FullUpdateConfig{createNewVersion=true} ScheduledTaskConfig{scheduleCron='null', frequency=PT24H} TaskConfig{batchSize=1000, parallelism=30, prefetchCount=null, prefetchAllRecords=false, timeoutSeconds=86400, beforeStartFlow=DataFlowConfig{id='feature_update_pipeline_full_beforeStart_reviews_default', components={featureVariant=ComponentConfig {id='featureVariant', type='null', extendsId='null', properties={}, propertyValueProviders=[], input=[], output=[_placeholderString0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}, _placeholderString0=ComponentConfig {id='_placeholderString0', type='PlaceholderStringValueProvider', extendsId='null', properties={placeholderString=beforeUpdate_$(featureId)_$(featureVariant)_$(featureVersion)}, propertyValueProviders=[], input=[featureVariant, featureVersion, featureId], output=[_TestAction0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}, featureVersion=ComponentConfig {id='featureVersion', type='null', extendsId='null', properties={}, propertyValueProviders=[], input=[], output=[_placeholderString0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}, _TestAction0=ComponentConfig {id='_TestAction0', type='TestAction', extendsId='null', properties={}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}, featureId=ComponentConfig {id='featureId', type='null', extendsId='null', properties={}, propertyValueProviders=[], input=[], output=[_placeholderString0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}}, output=ComponentConfig {id='_TestAction0', type='TestAction', extendsId='null', properties={}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}}, afterCompleteFlow=DataFlowConfig{id='feature_update_pipeline_full_afterComplete_reviews_default', components={featureVariant=ComponentConfig {id='featureVariant', type='null', extendsId='null', properties={}, propertyValueProviders=[], input=[], output=[_placeholderString0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}, _placeholderString0=ComponentConfig {id='_placeholderString0', type='PlaceholderStringValueProvider', extendsId='null', properties={placeholderString=afterComplete_$(featureId)_$(featureVariant)_$(featureVersion)}, propertyValueProviders=[], input=[featureVariant, featureVersion, featureId], output=[_TestAction0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}, featureVersion=ComponentConfig {id='featureVersion', type='null', extendsId='null', properties={}, propertyValueProviders=[], input=[], output=[_placeholderString0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}, _TestAction0=ComponentConfig {id='_TestAction0', type='TestAction', extendsId='null', properties={}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}, featureId=ComponentConfig {id='featureId', type='null', extendsId='null', properties={}, propertyValueProviders=[], input=[], output=[_placeholderString0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}}, output=ComponentConfig {id='_TestAction0', type='TestAction', extendsId='null', properties={}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}}, retry=RetryWithExponentialBackoff{} Retry{maxAttempts=30}, properties={}, dryRunProperties={}}, incrementalUpdateConfig=IncrementalUpdateConfig{} ScheduledTaskConfig{scheduleCron='null', frequency=null} TaskConfig{batchSize=100, parallelism=3, prefetchCount=null, prefetchAllRecords=false, timeoutSeconds=31536000, beforeStartFlow=null, afterCompleteFlow=null, retry=Retry{maxAttempts=3}, properties={}, dryRunProperties={}}, specificKeysUpdateConfig=null, inputConfigs=[DataFlowConfig{id='feature_update_input_reviews_default_input1', components={_ConstValueProvider0=ComponentConfig {id='_ConstValueProvider0', type='ConstValueProvider', extendsId='null', properties={value={id=reviewsDB, baseQuery=SELECT reviewId, languageId, pros, cons, headline, feedback, lastUpdateDate FROM reviews\n"
                + ", keyColumns=[reviewId], lastUpdateTimeColumn=lastUpdateDate, orderByClause=reviewId}}, propertyValueProviders=[], input=[], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}}, output=ComponentConfig {id='_ConstValueProvider0', type='ConstValueProvider', extendsId='null', properties={value={id=reviewsDB, baseQuery=SELECT reviewId, languageId, pros, cons, headline, feedback, lastUpdateDate FROM reviews\n"
                + ", keyColumns=[reviewId], lastUpdateTimeColumn=lastUpdateDate, orderByClause=reviewId}}, propertyValueProviders=[], input=[], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, eventsEnabled=false}}], pipeline=DataFlowConfig{id='feature_update_pipeline_reviews_default', components={inputItems=ComponentConfig {id='inputItems', type='null', extendsId='null', properties={}, propertyValueProviders=[], input=[], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}}, output=ComponentConfig {id='inputItems', type='null', extendsId='null', properties={}, propertyValueProviders=[], input=[], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, eventsEnabled=false}}, metricsConfig=null}",
            config.toString());
    }
}