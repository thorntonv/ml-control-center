/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureManagerTest.java
 */

package mlcc.feature;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.sql.SQLDatabaseManager;
import mlcc.feature.api.*;
import mlcc.feature.core.FeatureDBManagerImpl;
import mlcc.feature.core.FeatureDatabase;
import mlcc.feature.core.FeatureMetadataManager;
import mlcc.feature.core.PostgreSQLFeatureDatabase;
import mlcc.feature.core.sql.SQLDatabaseMetadataManager;
import mlcc.feature.update.FeatureFullUpdateTask;
import mlcc.feature.update.FeatureIncrementalUpdateTask;
import mlcc.feature.update.FeatureSpecificKeysUpdateTask;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static dataflow.core.datasource.ClassPathFileDataSource.getClassPathDataSourcesMatchingGlobPattern;
import static mlcc.feature.api.FeatureDBManager.DEFAULT_VARIANT;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class FeatureManagerTest {

    private FeatureManager featureManager;
    private EmbeddedDatabase db;
    private DataFlowEnvironment env;

    @Rule
    public SingleInstancePostgresRule pg = EmbeddedPostgresRules.singleInstance();

    @Before
    public void setUp() {
        this.db = new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.H2)
            .addScript("reviews-schema.sql")
            .addScript("reviews-data.sql")
            .build();

        SimpleDependencyInjector dependencyInjector = new SimpleDependencyInjector();

        FeatureMetadataManager featureMetadataManager = new SQLDatabaseMetadataManager(db);
        FeatureDatabase postgresFeatureDB = new PostgreSQLFeatureDatabase("test", pg.getEmbeddedPostgres().getPostgresDatabase());
        FeatureDBManager featureDBManager = new FeatureDBManagerImpl(featureMetadataManager,
            ImmutableList.of(postgresFeatureDB));
        env = new DataFlowEnvironment("test", dependencyInjector, null);
        featureManager = new FeatureManager(featureDBManager, env, null, null, null, null, null);
        dependencyInjector.register(featureManager);

        // SQL Database Manager
        SQLDatabaseManager dbManager = new SQLDatabaseManager();
        dbManager.registerDataSource("test", db);
        dependencyInjector.register(dbManager);

        featureManager.registerConfigProvider(() ->
            getClassPathDataSourcesMatchingGlobPattern("/features/*.yaml"), Integer.MAX_VALUE);
    }

    @After
    public void cleanUp() {
        db.shutdown();
        featureManager.close();
    }

    @Test
    public void testLoadFeatureConfigs() {
        assertEquals(7, featureManager.getAllFeatureConfigs().size());
        assertNotNull(featureManager.getFeatureConfig("reviews", DEFAULT_VARIANT));
    }

    @Test
    public void testFeatureFullUpdate() throws Exception {
        FeatureFullUpdateTask task = featureManager.fullUpdate("reviews", DEFAULT_VARIANT);
        FeatureMetadata metadata = task.getFutureResult().get(30, TimeUnit.SECONDS);
        assertEquals(FeatureStatus.READY, metadata.getStatus());
        assertEquals(2, metadata.getRecordCount());
        assertFalse(task.isRunning());
        assertTrue(TestAction.values.contains(String.format("beforeUpdate_reviews_default_%d",
            metadata.getVersion(), metadata.getVersion())));
        assertTrue(TestAction.values.contains(String.format("afterComplete_reviews_default_%d",
            metadata.getVersion(), metadata.getVersion())));
    }

    @Test
    public void testFeatureFullUpdate_cronScheduled() throws Exception {
        FeatureFullUpdateTask task = null;
        // Wait for the task to be registered.
        while (task == null) {
            task = featureManager.getFullUpdateTasks().entrySet().stream()
                .filter(entry -> entry.getKey().getId().equals("reviewsCronScheduled"))
                .map(Entry::getValue).findAny().orElse(null);
            Thread.sleep(1000);
        }

        // Wait for the task to complete.
        FeatureMetadata metadata = task.getFutureResult().get(30, TimeUnit.SECONDS);
        assertEquals(FeatureStatus.READY, metadata.getStatus());
        assertEquals(2, metadata.getRecordCount());
        assertFalse(task.isRunning());
        assertTrue(TestAction.values.contains(String.format("beforeUpdate_reviewsCronScheduled_default_%d",
            metadata.getVersion(), metadata.getVersion())));
        assertTrue(TestAction.values.contains(String.format("afterComplete_reviewsCronScheduled_default_%d",
            metadata.getVersion(), metadata.getVersion())));
    }

    @Test
    public void testFeatureFullUpdate_multiInput() throws Exception {
        FeatureFullUpdateTask task = featureManager.fullUpdate("reviewsMultiInput", DEFAULT_VARIANT);
        FeatureMetadata metadata = task.getFutureResult().get(30, TimeUnit.SECONDS);
        assertEquals(FeatureStatus.READY, metadata.getStatus());
        assertEquals(2, metadata.getRecordCount());
        assertFalse(task.isRunning());
        assertTrue(TestAction.values.contains(String.format("beforeUpdate_reviewsMultiInput_default_%d",
            metadata.getVersion(), metadata.getVersion())));
        assertTrue(TestAction.values.contains(String.format("afterComplete_reviewsMultiInput_default_%d",
            metadata.getVersion(), metadata.getVersion())));
    }

    @Test
    public void testFeatureFullUpdate_inputParam() throws Exception {
        FeatureFullUpdateTask task = featureManager.fullUpdate("reviewsInputParam", DEFAULT_VARIANT);
        FeatureMetadata metadata = task.getFutureResult().get(30, TimeUnit.SECONDS);
        assertEquals(FeatureStatus.READY, metadata.getStatus());
        assertEquals(2, metadata.getRecordCount());
        assertFalse(task.isRunning());
        assertTrue(TestAction.values.contains(String.format("beforeUpdate_reviewsInputParam_default_%d",
            metadata.getVersion(), metadata.getVersion())));
        assertTrue(TestAction.values.contains(String.format("afterComplete_reviewsInputParam_default_%d",
            metadata.getVersion(), metadata.getVersion())));
    }

    @Test
    public void testFeatureFullUpdate_multipleBatches() throws Exception {
        try(Connection conn = db.getConnection(); Statement stmt = conn.createStatement()) {
            for(int cnt = 3; cnt <= 2000; cnt++) {
                stmt.executeUpdate(String.format(
                    "INSERT INTO reviews VALUES (%d, 'eng', 'good health plan', 'rude customers', NULL, NULL, '%s')",
                    cnt, new Timestamp(new Date().getTime())));
            }
        }

        FeatureFullUpdateTask task = featureManager.fullUpdate("reviews", DEFAULT_VARIANT);
        System.out.println("Waiting for task to finish");
        FeatureMetadata metadata = task.getFutureResult().get(60, TimeUnit.SECONDS);
        assertEquals(FeatureStatus.READY, metadata.getStatus());
        assertEquals(2000, metadata.getRecordCount());
        assertFalse(task.isRunning());
    }

    @Test
    public void testFeatureFullUpdate_multipleBatches_multiInput() throws Exception {
        try(Connection conn = db.getConnection(); Statement stmt = conn.createStatement()) {
            for(int cnt = 3; cnt <= 5000; cnt++) {
                stmt.executeUpdate(String.format(
                    "INSERT INTO reviews VALUES (%d, 'eng', 'good health plan', 'rude customers', NULL, NULL, '%s')",
                    cnt, new Timestamp(new Date().getTime())));
            }
        }

        FeatureFullUpdateTask task = featureManager.fullUpdate("reviewsMultiInput", DEFAULT_VARIANT);
        System.out.println("Waiting for task to finish");
        FeatureMetadata metadata = task.getFutureResult().get(60, TimeUnit.SECONDS);
        assertEquals(FeatureStatus.READY, metadata.getStatus());
        assertEquals(5000, metadata.getRecordCount());
        assertFalse(task.isRunning());
    }
    @Test
    public void testFeatureFullUpdate_inputError() {
        FeatureFullUpdateTask task = featureManager.fullUpdate("reviews_inputError", DEFAULT_VARIANT);
        try {
            task.getFutureResult().get(30, TimeUnit.SECONDS);
            fail("expected exception was not thrown");
        } catch(Exception ex) {
            ex.printStackTrace();
            // Expected.
            assertEquals("Table \"NONEXISTENTTABLE\" not found; SQL statement:\n"
                + "SELECT reviewId, languageId, pros, cons, headline, feedback, lastUpdateDate FROM nonExistentTable\n"
                + " ORDER BY reviewId [42102-197]", ex.getCause().getCause().getCause().getMessage());
        }
        assertFalse(task.isRunning());
    }

    @Test
    public void testFeatureFullUpdate_inputError_withRetry() {
        FeatureFullUpdateTask task = featureManager.fullUpdate("reviews_inputError_withRetry", DEFAULT_VARIANT);
        try {
            task.getFutureResult().get(1, TimeUnit.MINUTES);
            fail("expected exception was not thrown");
        } catch(TimeoutException | ExecutionException | InterruptedException ex) {
            assertTrue(task.getRetryCount() > 0);
            assertEquals("mlcc.feature.update.BatchExecutionException: Error writing feature data.", ex.getMessage());
        }
        assertFalse(task.isRunning());
    }


    @Test
    public void testFeatureFullUpdate_pipelineError() {
        FeatureFullUpdateTask task = featureManager.fullUpdate("reviews_pipelineError", DEFAULT_VARIANT);
        try {
            task.getFutureResult().get(30, TimeUnit.SECONDS);
            fail("expected exception was not thrown");
        } catch(Exception ex) {
            ex.printStackTrace();
            // Expected.
            assertEquals("class java.util.HashMap cannot be cast to class dataflow.data.DataRecords (java.util.HashMap is in module java.base of loader 'bootstrap'; dataflow.data.DataRecords is in unnamed module of loader 'app')",
                ex.getCause().getCause().getMessage());
        }
        assertFalse(task.isRunning());
    }
    @Test
    public void testFeatureIncrementalUpdate() throws Exception {
        FeatureFullUpdateTask fullUpdateTask = featureManager.fullUpdate("reviews", DEFAULT_VARIANT);
        FeatureMetadata fullUpdateMetadata = fullUpdateTask.getFutureResult().get(30, TimeUnit.SECONDS);

        // Insert a new record.
        Thread.sleep(2000);
        try(Connection conn = db.getConnection(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(String.format("INSERT INTO reviews VALUES (3, 'eng', 'good health plan', 'rude customers', NULL, NULL, '%s')",
                new Timestamp(new Date().getTime())));
        }

        // Verify that the new feature record doesn't already exist.
        FeatureData featureData = featureManager.getFeatureDBManager().getFeatureData("reviews", DEFAULT_VARIANT);
        assertNull(featureData.read(FeatureDataKey.create(3)));

        FeatureIncrementalUpdateTask task = featureManager.incrementalUpdate("reviews", DEFAULT_VARIANT);
        FeatureMetadata incrementalUpdateMetadata = task.getFutureResult().get();
        assertEquals(FeatureStatus.READY, incrementalUpdateMetadata.getStatus());
        assertTrue(incrementalUpdateMetadata.getUpdatedToDate().getTime() - fullUpdateMetadata.getUpdatedToDate().getTime() > 1000);
        featureData = featureManager.getFeatureDBManager().getFeatureData("reviews", DEFAULT_VARIANT);
        assertEquals("good health plan", featureData.read(FeatureDataKey.create(3)).get("pros"));
    }

    @Test
    public void testSpecificIdsUpdate() throws Exception {
        FeatureFullUpdateTask fullUpdateTask = featureManager.fullUpdate("reviews", DEFAULT_VARIANT);
        FeatureMetadata fullUpdateMetadata = fullUpdateTask.getFutureResult().get(30, TimeUnit.SECONDS);

        // Update an existing record
        Thread.sleep(2000);
        try(Connection conn = db.getConnection(); PreparedStatement stmt = conn.prepareStatement(
            "UPDATE reviews SET pros = ?, lastUpdateDate = ? WHERE reviewId = 2")) {
            stmt.setObject(1, "good people");
            stmt.setObject(2, new Timestamp(new Date().getTime()));
            assertEquals(1, stmt.executeUpdate());
        }

        FeatureSpecificKeysUpdateTask task = featureManager.updateRecordsWithKeys("reviews", DEFAULT_VARIANT,
            ImmutableMap.of("reviewsDB", ImmutableList.of(ImmutableList.of(2))));
        FeatureMetadata updateMetadata = task.getFutureResult().get(30, TimeUnit.SECONDS);
        assertEquals(FeatureStatus.READY, updateMetadata.getStatus());
        assertEquals(fullUpdateMetadata.getUpdatedToDate(), updateMetadata.getUpdatedToDate());
        FeatureData featureData = featureManager.getFeatureDBManager().getFeatureData("reviews", DEFAULT_VARIANT);
        assertEquals("good people", featureData.read(FeatureDataKey.create(2)).get("pros"));
    }
}