/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureConfigParser.java
 */

package mlcc.feature.parser;

import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.parser.DataFlowParser.ConfigurableObjectConstructor;
import java.io.InputStream;
import java.util.Map;
import org.yaml.snakeyaml.Yaml;

public class FeatureConfigParser {

    private final FeatureConfigBuilder configBuilder;

    public FeatureConfigParser(DataFlowRegistry registry) {
        configBuilder = new FeatureConfigBuilder(registry);
    }

    public FeatureConfig parse(InputStream in)
        throws FeatureConfigParseException, FeatureConfigurationException {
        if (in == null) {
            throw new IllegalArgumentException(
                "Attempted to parse feature config with a null input stream");
        }

        Map<String, Object> map;
        try {
            Yaml yaml = new Yaml(new ConfigurableObjectConstructor(configBuilder.getRegistry()));
            map = yaml.load(in);
        } catch (Exception ex) {
            throw new FeatureConfigParseException("Failed to parse feature configuration", ex);
        }
        return configBuilder.build(map);
    }
}
