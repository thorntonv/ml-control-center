/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureConfig.java
 */

package mlcc.feature.parser;

import dataflow.core.config.DataFlowConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import mlcc.feature.api.FeatureColumnType;
import mlcc.feature.api.FeatureDBConfig;

public class FeatureConfig {

    private String id;
    private String variant;
    private int version;
    private FeatureDBConfig featureDBConfig = new FeatureDBConfig();
    private String featureCreateSQL;
    private List<String> keyColumnNames = new ArrayList<>();
    private List<String> columnNames = new ArrayList<>();
    private List<FeatureColumnType> columnTypes = new ArrayList<>();
    private FullUpdateConfig fullUpdateConfig;
    private IncrementalUpdateConfig incrementalUpdateConfig;
    private SpecificKeysUpdateConfig specificKeysUpdateConfig;
    private List<DataFlowConfig> inputConfigs;
    private DataFlowConfig pipeline;
    private MetricsConfig metricsConfig;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public FeatureDBConfig getFeatureDBConfig() {
        return featureDBConfig;
    }

    public void setFeatureDBConfig(FeatureDBConfig featureDBConfig) {
        this.featureDBConfig = featureDBConfig;
    }

    public List<String> getKeyColumnNames() {
        return keyColumnNames;
    }

    public void setKeyColumnNames(List<String> keyColumnNames) {
        this.keyColumnNames = keyColumnNames;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(List<String> columnNames) {
        this.columnNames = columnNames;
    }

    public List<FeatureColumnType> getColumnTypes() {
        return columnTypes;
    }

    public void setColumnTypes(List<FeatureColumnType> columnTypes) {
        this.columnTypes = columnTypes;
    }

    public FullUpdateConfig getFullUpdateConfig() {
        return fullUpdateConfig != null ? fullUpdateConfig : new FullUpdateConfig();
    }

    public void setFullUpdateConfig(FullUpdateConfig fullUpdateConfig) {
        this.fullUpdateConfig = fullUpdateConfig;
    }

    public IncrementalUpdateConfig getIncrementalUpdateConfig() {
        return incrementalUpdateConfig != null ? incrementalUpdateConfig : new IncrementalUpdateConfig();
    }

    public void setIncrementalUpdateConfig(
        IncrementalUpdateConfig incrementalUpdateConfig) {
        this.incrementalUpdateConfig = incrementalUpdateConfig;
    }

    public List<DataFlowConfig> getInputConfigs() {
        return inputConfigs;
    }

    public void setInputConfigs(List<DataFlowConfig> inputConfigs) {
        this.inputConfigs = inputConfigs;
    }

    public DataFlowConfig getPipeline() {
        return pipeline;
    }

    public void setPipeline(DataFlowConfig pipeline) {
        this.pipeline = pipeline;
    }

    public MetricsConfig getMetricsConfig() {
        return metricsConfig;
    }

    public void setMetricsConfig(MetricsConfig metricsConfig) {
        this.metricsConfig = metricsConfig;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public SpecificKeysUpdateConfig getSpecificKeysUpdateConfig() {
        return specificKeysUpdateConfig != null ? specificKeysUpdateConfig : new SpecificKeysUpdateConfig();
    }

    public void setSpecificKeysUpdateConfig(
        SpecificKeysUpdateConfig specificKeysUpdateConfig) {
        this.specificKeysUpdateConfig = specificKeysUpdateConfig;
    }

    public String getFeatureCreateSQL() {
        return featureCreateSQL;
    }

    public void setFeatureCreateSQL(String featureCreateSQL) {
        this.featureCreateSQL = featureCreateSQL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeatureConfig that = (FeatureConfig) o;
        return version == that.version && Objects.equals(id, that.id) && Objects.equals(variant, that.variant) && Objects.equals(featureDBConfig, that.featureDBConfig) && Objects.equals(featureCreateSQL, that.featureCreateSQL) && Objects.equals(keyColumnNames, that.keyColumnNames) && Objects.equals(columnNames, that.columnNames) && Objects.equals(columnTypes, that.columnTypes) && Objects.equals(fullUpdateConfig, that.fullUpdateConfig) && Objects.equals(incrementalUpdateConfig, that.incrementalUpdateConfig) && Objects.equals(specificKeysUpdateConfig, that.specificKeysUpdateConfig) && Objects.equals(inputConfigs, that.inputConfigs) && Objects.equals(pipeline, that.pipeline) && Objects.equals(metricsConfig, that.metricsConfig);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, variant, version, featureDBConfig, featureCreateSQL, keyColumnNames, columnNames, columnTypes, fullUpdateConfig, incrementalUpdateConfig, specificKeysUpdateConfig, inputConfigs, pipeline, metricsConfig);
    }

    @Override
    public String toString() {
        return "FeatureConfig{" +
                "id='" + id + '\'' +
                ", variant='" + variant + '\'' +
                ", version=" + version +
                ", featureDBConfig=" + featureDBConfig +
                ", featureCreateSQL='" + featureCreateSQL + '\'' +
                ", keyColumnNames=" + keyColumnNames +
                ", columnNames=" + columnNames +
                ", columnTypes=" + columnTypes +
                ", fullUpdateConfig=" + fullUpdateConfig +
                ", incrementalUpdateConfig=" + incrementalUpdateConfig +
                ", specificKeysUpdateConfig=" + specificKeysUpdateConfig +
                ", inputConfigs=" + inputConfigs +
                ", pipeline=" + pipeline +
                ", metricsConfig=" + metricsConfig +
                '}';
    }
}
