/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureConfigBuilder.java
 */

package mlcc.feature.parser;

import static dataflow.core.parser.DataFlowConfigBuilder.VALUE_REFERENCE;
import static mlcc.feature.api.FeatureDBManager.DEFAULT_VARIANT;

import dataflow.batch.config.ScheduledTaskConfig;
import dataflow.batch.config.TaskConfig;
import dataflow.core.component.ConstValueProvider;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.RawComponentConfig;
import dataflow.core.config.RawConfigurableObjectConfig;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.parser.DataFlowConfigBuilder;
import dataflow.retry.Retry;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import mlcc.feature.api.FeatureColumnType;
import mlcc.feature.api.FeatureDBConfig;

public class FeatureConfigBuilder {

    public static final String DEFAULT_TIMEOUT = "P365D";

    private static final String PIPELINE_ID_PREFIX = "feature_update_pipeline";
    private static final String INPUT_ID_PREFIX = "feature_update_input";
    private static final String FEATURE = "feature";
    private static final String STORE = "store";

    private final DataFlowRegistry registry;
    private final DataFlowConfigBuilder dataFlowConfigBuilder;

    public FeatureConfigBuilder(DataFlowRegistry registry) {
        this.registry = registry;
        this.dataFlowConfigBuilder = new DataFlowConfigBuilder(registry);
    }

    public FeatureConfig build(Map<String, Object> parsedConfig)
        throws FeatureConfigurationException {
        for (Map.Entry<String, Object> entry : parsedConfig.entrySet()) {
            String key = entry.getKey().toLowerCase();
            if (key.equalsIgnoreCase("Feature")) {
                Object featureConfigObj = entry.getValue();
                if (!(featureConfigObj instanceof Map)) {
                    throw new DataFlowConfigurationException(
                        "Invalid feature configuration: " + featureConfigObj);
                }
                return buildFeatureConfig((Map<String, Object>) featureConfigObj);
            }
        }
        throw new FeatureConfigurationException(
            "Configuration must have Feature as a root element");
    }

    private List<String> getMissingKeys(Map<String, Object> map, String... keys) {
        return Arrays.stream(keys).filter(k -> !map.containsKey(k)).collect(Collectors.toList());
    }

    private FeatureConfig buildFeatureConfig(Map<String, Object> parsedConfig)
        throws FeatureConfigurationException {
        FeatureConfig config = new FeatureConfig();

        List<String> missingKeys = getMissingKeys(parsedConfig, "id", "version",
            "store", "key", "schema", "input", "pipeline");
        if(!missingKeys.isEmpty()) {
            throw new FeatureConfigurationException("Feature config is missing required properties: " + missingKeys);
        }

        config.setId(getRequiredString(FEATURE, "id", parsedConfig));
        config.setVariant(getProperty("variant", parsedConfig, String.class).orElse(DEFAULT_VARIANT));
        config.setVersion(getRequiredProperty(FEATURE, "version", parsedConfig, Integer.class));

        config.setFeatureDBConfig(buildFeatureDBConfig(
            getRequiredMap(FEATURE, "store", parsedConfig)));

        if(parsedConfig.containsKey("fullUpdate")) {
            config.setFullUpdateConfig(buildFullUpdateConfig(
                (Map<String, Object>) parsedConfig.get("fullUpdate"), config));
        }
        if(parsedConfig.containsKey("incrementalUpdate")) {
            config.setIncrementalUpdateConfig(buildIncrementalUpdateConfig(
                (Map<String, Object>) parsedConfig.get("incrementalUpdate"), config));
        }
        if(parsedConfig.containsKey("specificKeysUpdate")) {
            config.setSpecificKeysUpdateConfig(buildSpecificKeysUpdateConfig(
                (Map<String, Object>) parsedConfig.get("specificKeysUpdate"), config));
        }

        config.setKeyColumnNames(buildKeyConfig(parsedConfig.get("key")));

        config.setFeatureCreateSQL((String) parsedConfig.get("createSQL"));
        parseSchema(config, getRequiredString(FEATURE, "schema", parsedConfig));

        String inputDataFlowIdPrefix = String.format("%s_%s_%s", INPUT_ID_PREFIX, config.getId(), config.getVariant());

        config.setInputConfigs(buildInputConfigs(
            getRequiredProperty(FEATURE, "input", parsedConfig, Object.class), inputDataFlowIdPrefix));

        String id = String.format("%s_%s_%s", PIPELINE_ID_PREFIX, config.getId(), config.getVariant());
        config.setPipeline(buildPipelineConfig(parsedConfig.get("pipeline"), id));

        validateConfig(config);

        return config;
    }

    private void validateConfig(FeatureConfig featureConfig) throws FeatureConfigurationException {
    }

    private List<DataFlowConfig> buildInputConfigs(Object parsedConfig, String idPrefix)
        throws FeatureConfigurationException {
        List<DataFlowConfig> inputConfigs = new ArrayList<>();
        if(parsedConfig instanceof Iterable) {
            for(Object parsedInputConfig : (Iterable) parsedConfig) {
                inputConfigs.add(buildInputConfig("Input", (Map<String, Object>) parsedInputConfig));
            }
        } else if(parsedConfig instanceof Map){
            inputConfigs.add(buildInputConfig("Input", (Map<String, Object>) parsedConfig));
        } else {
            throw new FeatureConfigurationException("Invalid input configuration: " + parsedConfig);
        }
        if(inputConfigs.isEmpty()) {
            throw new FeatureConfigurationException("One or more inputs must be configured");
        }
        Set<String> ids = new HashSet<>();
        for(int idx = 0; idx < inputConfigs.size(); idx++) {
            DataFlowConfig inputConfig = inputConfigs.get(idx);
            String id = inputConfig.getId() != null ? inputConfig.getId() : String.format("input%d", idx + 1);
            if(ids.contains(id.toLowerCase())) {
                throw new FeatureConfigurationException("Duplicate input id " + id);
            }
            ids.add(id.toLowerCase());
            inputConfig.setId(idPrefix + "_" + id);
        }
        return inputConfigs;
    }

    private FeatureDBConfig buildFeatureDBConfig(Map<String, Object> parsedConfig)
        throws FeatureConfigurationException {
        FeatureDBConfig config = new FeatureDBConfig();
        config.setTypeId(getRequiredString(STORE, "type", parsedConfig));
        config.setClusterId(
            getProperty("clusterId", parsedConfig, String.class).orElse(null));
        config.setNumOldVersionsToKeep(
            getProperty("numOldVersionsToKeep", parsedConfig, Integer.class).orElse(0));
        config.setMinAgeForOldVersionCleanupHours(
            getProperty("minAgeForOldVersionCleanupHours", parsedConfig, Integer.class).orElse(0));
        config.setAutoCreateColumns(
            getProperty("autoCreateColumns", parsedConfig, Boolean.class).orElse(false));
        config.setDefaultFetchSize(
            getProperty("defaultFetchSize", parsedConfig, Integer.class).orElse(100));
        config.setExtraConfig(
            getProperty("extraConfig", parsedConfig, Map.class).orElse(null));
        return config;
    }

    private FullUpdateConfig buildFullUpdateConfig(Map<String, Object> parsedConfig,
            FeatureConfig featureConfig) throws FeatureConfigurationException {
        FullUpdateConfig config = new FullUpdateConfig();
        buildScheduledTaskConfig(config, parsedConfig, PIPELINE_ID_PREFIX + "_full", featureConfig);
        if (parsedConfig.containsKey("createNewVersion")) {
            config.setCreateNewVersion(getProperty("createNewVersion", parsedConfig,
                Boolean.class).orElse(true));
        }
        return config;
    }

    private IncrementalUpdateConfig buildIncrementalUpdateConfig(Map<String, Object> parsedConfig,
            FeatureConfig featureConfig) throws FeatureConfigurationException {
        IncrementalUpdateConfig config = new IncrementalUpdateConfig();
        buildScheduledTaskConfig(config, parsedConfig, PIPELINE_ID_PREFIX + "_incremental", featureConfig);
        return config;
    }

    private SpecificKeysUpdateConfig buildSpecificKeysUpdateConfig(Map<String, Object> parsedConfig,
            FeatureConfig featureConfig) throws FeatureConfigurationException {
        SpecificKeysUpdateConfig config = new SpecificKeysUpdateConfig();
        buildTaskConfig(config, parsedConfig, PIPELINE_ID_PREFIX + "_specificKeys", featureConfig);
        return config;
    }

    public void buildTaskConfig(TaskConfig taskConfig, Map<String, Object> parsedConfig,
        String pipelineIdPrefix, FeatureConfig featureConfig)
        throws FeatureConfigurationException {
        taskConfig.setBatchSize(getProperty("batchSize", parsedConfig, Integer.class).orElse(null));
        taskConfig.setParallelism(getProperty("parallelism", parsedConfig, Integer.class).orElse(null));
        taskConfig.setPrefetchCount(getProperty("prefetchCount", parsedConfig, Integer.class).orElse(null));
        taskConfig.setPrefetchAllRecords(getProperty("prefetchAllRecords", parsedConfig, Boolean.class).orElse(false));
        taskConfig.setTimeoutSeconds(parseDuration(
            getProperty("timeout", parsedConfig, String.class).orElse(DEFAULT_TIMEOUT)).getSeconds());
        taskConfig.setProperties(getProperty("properties", parsedConfig, Map.class).orElse(new HashMap<>()));
        taskConfig.setDryRunProperties(getProperty("dryRunProperties", parsedConfig, Map.class).orElse(new HashMap<>()));

        if(parsedConfig.containsKey("retry")) {
            Object retryConfig = parsedConfig.get("retry");
            if (retryConfig instanceof RawConfigurableObjectConfig) {
                RawConfigurableObjectConfig objConfig = (RawConfigurableObjectConfig) retryConfig;
                taskConfig.setRetry(buildRetryConfig(objConfig.getType(), objConfig));
            } else if (retryConfig instanceof Map) {
                taskConfig.setRetry(buildRetryConfig(Retry.class.getSimpleName(), (Map) retryConfig));
            } else {
                throw new FeatureConfigurationException("Invalid retry config: " + retryConfig);
            }
        }

        if (parsedConfig.containsKey("beforeStart")) {
            String id = String.format("%s_beforeStart_%s_%s", pipelineIdPrefix,
                featureConfig.getId(), featureConfig.getVariant());
            taskConfig.setBeforeStartFlow(buildPipelineConfig(parsedConfig.get("beforeStart"), id));
        }
        if (parsedConfig.containsKey("afterComplete")) {
            String id = String.format("%s_afterComplete_%s_%s", pipelineIdPrefix,
                featureConfig.getId(), featureConfig.getVariant());
            taskConfig.setAfterCompleteFlow(buildPipelineConfig(parsedConfig.get("afterComplete"), id));
        }
    }

    private Retry buildRetryConfig(String type, Map<String, Object> retryConfig) {
        if (retryConfig == null) {
            return null;
        }

        return (Retry) registry.getComponentRegistry().getPropertyObjectBuilders().get(type)
            .build(retryConfig, Collections.emptyMap());
    }

    private void buildScheduledTaskConfig(ScheduledTaskConfig taskConfig,
        Map<String, Object> parsedConfig, String pipelineIdPrefix, FeatureConfig featureConfig)
            throws FeatureConfigurationException {
        buildTaskConfig(taskConfig, parsedConfig, pipelineIdPrefix, featureConfig);

        taskConfig.setFrequency(parseDuration(
            getProperty("frequency", parsedConfig, String.class).orElse(null)));
        taskConfig.setScheduleCron(parseScheduleCron(
            getProperty("schedule", parsedConfig, String.class).orElse(null)));
    }

    private Duration parseDuration(String str) {
        if (str == null || str.isEmpty()) {
            return null;
        }
        return Duration.parse(str);
    }

    private String parseScheduleCron(String str) throws FeatureConfigurationException {
        if (str == null || str.isEmpty()) {
            return null;
        }
        str = str.trim().toLowerCase();
        if (str.equals("hourly")) {
            return "0 * * * * ?";
        } else if (str.equals("daily")) {
            return "0 0 0 * * ?";
        }
        return str;
    }

    private DataFlowConfig buildInputConfig(String inputType, Map<String, Object> rawInputConfig)
        throws FeatureConfigurationException {

        // Wrap the rawInputConfig in a ConstValueProvider since we want to get a reference to the
        // DataQueryValueProvider component not the value that is obtained from the component.
        Map<String, Object> constValueProviderConfig = new HashMap<>();
        constValueProviderConfig.put("value", rawInputConfig);

        RawComponentConfig constValueProvider = new RawComponentConfig(
            ConstValueProvider.class.getSimpleName(), constValueProviderConfig);

        DataFlowConfig dataFlowConfig = dataFlowConfigBuilder.build(constValueProvider);
        if (dataFlowConfig.getComponents().isEmpty()) {
            throw new FeatureConfigurationException(inputType + " value provider is not defined");
        }
        return dataFlowConfig;
    }

    private void parseSchema(FeatureConfig config, String schema)
        throws FeatureConfigurationException {
        String[] columnsInfo = schema.split(",");
        if(columnsInfo.length == 0) {
            throw new FeatureConfigurationException("Invalid schema: " + schema);
        }
        List<String> columnNames = new ArrayList<>();
        List<FeatureColumnType> columnTypes = new ArrayList<>();
        for(String columnInfoStr : columnsInfo) {
            String[] columnInfo = columnInfoStr.split(":");
            if(columnInfo.length != 2) {
                throw new FeatureConfigurationException("Invalid schema: " + schema);
            }
            if(columnInfo[0].trim().isEmpty()) {
                throw new FeatureConfigurationException("Invalid schema: " + schema);
            }
            columnNames.add(columnInfo[0].trim());
            try {
                columnTypes.add(FeatureColumnType.forName(columnInfo[1].trim()));
            } catch (Exception ex) {
                throw new FeatureConfigurationException("Invalid column type " + columnInfo[1] +
                    " in schema " + schema);
            }
        }
        config.setColumnNames(columnNames);
        config.setColumnTypes(columnTypes);
    }

    private DataFlowConfig buildPipelineConfig(Object rawPipelineConfig, String id)
        throws FeatureConfigurationException {
        if(rawPipelineConfig instanceof List) {
            Map<String, Object> map = new HashMap<>();
            map.put("Components", rawPipelineConfig);
            rawPipelineConfig = map;
        }
        if(!(rawPipelineConfig instanceof Map)) {
            throw new FeatureConfigurationException("Invalid pipeline config: " + rawPipelineConfig);
        }
        DataFlowConfig pipelineConfig = dataFlowConfigBuilder.build((Map<String, Object>) rawPipelineConfig);
        pipelineConfig.setId(id);
        return pipelineConfig;
    }

    private List<String> buildKeyConfig(Object rawConfig)
        throws FeatureConfigurationException {
        if(rawConfig == null) {
            throw new FeatureConfigurationException("Missing value for key property");
        }
        if(rawConfig instanceof String) {
            String str = (String) rawConfig;
            return Arrays.stream(str.split(","))
                .map(String::trim)
                .collect(Collectors.toList());
        } else if(rawConfig instanceof List) {
            List<String> list = (List<String>) rawConfig;
            return list.stream().map(String::trim).collect(Collectors.toList());
        }
        throw new FeatureConfigurationException("Invalid value for key property: " + rawConfig);
    }

    private String getRequiredString(String parentType, String key, Map<String, Object> map)
        throws FeatureConfigurationException {
        return getRequiredProperty(parentType, key, map, String.class);
    }

    private Map<String, Object> getRequiredMap(String parentType, String key, Map<String, Object> map)
        throws FeatureConfigurationException {
        return getRequiredProperty(parentType, key, map, Map.class);
    }

    private <T> T getRequiredProperty(String parentType, String key, Map<String, Object> map, Class<T> typeClass)
        throws FeatureConfigurationException {
        Optional<T> value = getProperty(key, map, typeClass);
        if(!value.isPresent()) {
            Object valueObj = map.get(key);
            if(valueObj != null) {
                throw new FeatureConfigurationException(String.format(
                    "Invalid value for %s property %s. Expected %s but was %s",
                    parentType, key, typeClass, valueObj.getClass()));
            } else {
                throw new FeatureConfigurationException(String.format("Required %s property %s is missing",
                    parentType, key));
            }
        }
        return value.get();
    }
    
    private <T> Optional<T> getProperty(String key, Map<String, Object> map, Class<T> typeClass) {
        Object valueObj = map.get(key);
        if(valueObj instanceof String && VALUE_REFERENCE.matcher((String) valueObj).matches()) {
            throw new DataFlowConfigurationException("Value references are not allowed for property values");
        }
        if (valueObj == null || !typeClass.isAssignableFrom(valueObj.getClass())) {
            return Optional.empty();
        }
        return Optional.of((T) valueObj);
    }

    public DataFlowRegistry getRegistry() {
        return registry;
    }
}
