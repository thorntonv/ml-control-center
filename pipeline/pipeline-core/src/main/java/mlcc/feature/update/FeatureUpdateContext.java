/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureUpdateContext.java
 */

package mlcc.feature.update;

import dataflow.batch.TaskContext;
import dataflow.core.engine.DataFlowInstance;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import mlcc.feature.api.FeatureData;
import mlcc.feature.api.FeatureMetadata;

public class FeatureUpdateContext extends TaskContext {

    private FeatureMetadata featureMetadata;
    private FeatureData featureDataOutput;
    private Date lastMetadataUpdateTime = new Date();
    private AtomicLong retryCount = new AtomicLong();
    private Map<String, AtomicLong> pipelineCompletedItemCount = new ConcurrentHashMap<>();
    private Map<String, Object> configProperties = new HashMap<>();
    private List<DataFlowInstance> allocatedInstances = Collections.synchronizedList(new ArrayList<>());

    public FeatureMetadata getFeatureMetadata() {
        return featureMetadata;
    }

    public void setFeatureMetadata(FeatureMetadata featureMetadata) {
        this.featureMetadata = featureMetadata;
    }

    public AtomicLong getRetryCount() {
        return retryCount;
    }

    public FeatureData getFeatureDataOutput() {
        return featureDataOutput;
    }

    public void setFeatureDataOutput(FeatureData featureDataOutput) {
        this.featureDataOutput = featureDataOutput;
    }

    public Date getLastMetadataUpdateTime() {
        return lastMetadataUpdateTime;
    }

    public void setLastMetadataUpdateTime(Date lastMetadataUpdateTime) {
        this.lastMetadataUpdateTime = lastMetadataUpdateTime;
    }

    public AtomicLong getCompletedItemCount(String pipelineId) {
        return this.pipelineCompletedItemCount.computeIfAbsent(pipelineId, k -> new AtomicLong());
    }

    public Map<String, AtomicLong> getPipelineCompletedItemCounts() {
        return pipelineCompletedItemCount;
    }

    public Map<String, Object> getConfigProperties() {
        return configProperties;
    }

    public void setConfigProperties(Map<String, Object> configProperties) {
        this.configProperties = configProperties;
    }

    public List<DataFlowInstance> getAllocatedInstances() {
        return allocatedInstances;
    }

    public void setAllocatedInstances(
        List<DataFlowInstance> allocatedInstances) {
        this.allocatedInstances = allocatedInstances;
    }
}
