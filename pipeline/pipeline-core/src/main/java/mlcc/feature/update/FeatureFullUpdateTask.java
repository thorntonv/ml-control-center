/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureFullUpdateTask.java
 */

package mlcc.feature.update;

import static mlcc.feature.FeatureManager.DRY_RUN_VARIANT;

import dataflow.batch.config.TaskConfig;
import io.micrometer.core.instrument.MeterRegistry;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import mlcc.feature.FeatureManager;
import mlcc.feature.api.FeatureData;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.api.FeatureStatus;
import mlcc.feature.parser.FeatureConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates a new version of a feature and performs a full update. After the update is complete the
 * active version is switched to this new version.
 */
public class FeatureFullUpdateTask extends AbstractFeatureUpdateTask {

    private static final Logger logger = LoggerFactory.getLogger(FeatureFullUpdateTask.class);

    private final boolean switchActiveVersion;

    public FeatureFullUpdateTask(String featureId, String variant, Boolean switchActiveVersion,
            Boolean dryRun, Integer dryRunRecordCount, FeatureManager featureManager,
            TaskConfig defaultTaskConfig, Map<String, Object> configProperties, String metricNamePrefix,
            MeterRegistry meterRegistry) {
        super(featureId, variant, dryRun, dryRunRecordCount, featureManager, defaultTaskConfig,
            configProperties, metricNamePrefix, meterRegistry);
        this.switchActiveVersion = switchActiveVersion == null || switchActiveVersion;
    }

    @Override
    public String getUpdateType() {
        return "full";
    }

    @Override
    protected void onBatchCompletion(FeatureUpdateContext context, boolean updateMetadata) throws IOException {
        if (updateMetadata) {
            context.getFeatureMetadata().setRecordCount(context.getCompletedItemCount().get());
            featureDBManager.updateFeatureMetadata(context.getFeatureMetadata());
        }
    }

    @Override
    protected void finishUpdate(FeatureUpdateContext context) {
        FeatureMetadata featureMetadata = context.getFeatureMetadata();
        featureMetadata.setStatus(FeatureStatus.READY);
        featureMetadata.setRecordCount(context.getCompletedItemCount().get());
        featureMetadata.setUpdatedToDate(context.getStartTime());
        try {
            if (switchActiveVersion) {
                featureDBManager.setFeatureActiveVersion(featureMetadata);
            }
            featureDBManager.updateFeatureMetadata(featureMetadata);
        } catch (Throwable e) {
            featureMetadata.setStatus(FeatureStatus.FAILED);
            try {
                featureDBManager.updateFeatureMetadata(featureMetadata);
            } catch (IOException ex) {
                logger.warn("Error updating feature metadata", ex);
            }
            throw new RuntimeException(e);
        }
    }

    @Override
    protected TaskConfig getTaskConfig(FeatureConfig featureConfig) {
        return featureConfig.getFullUpdateConfig();
    }

    @Override
    protected FeatureData getOutputFeatureData(FeatureUpdateContext context) throws IOException {
        if (!featureConfig.getFullUpdateConfig().isCreateNewVersion()) {
            return super.getOutputFeatureData(context);
        }
        FeatureMetadata featureMetadata = new FeatureMetadata();
        featureMetadata.setFeatureId(featureConfig.getId());
        featureMetadata.setVariant(context.isDryRun() ? DRY_RUN_VARIANT : featureConfig.getVariant());
        featureMetadata.setKeyColumnNames(featureConfig.getKeyColumnNames());
        featureMetadata.setColumnNames(featureConfig.getColumnNames());
        featureMetadata.setColumnTypes(featureConfig.getColumnTypes());
        featureMetadata.setCreateSQL(featureConfig.getFeatureCreateSQL());
        featureMetadata.setCreationDate(new Date());
        featureMetadata.setStatus(FeatureStatus.PREPARING_DATA);
        featureMetadata.setFeatureDBConfig(featureConfig.getFeatureDBConfig());
        featureMetadata = featureDBManager.createFeatureVersion(featureMetadata);
        logger.info(String.format("Created new version %d of feature %s variant %s",
            featureMetadata.getVersion(), featureMetadata.getFeatureId(),
            featureMetadata.getVariant()));
        return featureDBManager.getFeatureData(featureMetadata);
    }
}
