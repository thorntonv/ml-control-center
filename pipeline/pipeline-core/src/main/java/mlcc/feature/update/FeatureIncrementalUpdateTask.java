/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureIncrementalUpdateTask.java
 */

package mlcc.feature.update;

import dataflow.batch.config.TaskConfig;
import dataflow.data.query.DataFilter;
import dataflow.data.query.LastUpdateTimeFilter;
import io.micrometer.core.instrument.MeterRegistry;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import mlcc.feature.FeatureManager;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.parser.FeatureConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Performs an incremental update of the active version of a feature. The update will include
 * changes to the input data that occurred since the {@link FeatureMetadata#getUpdatedToDate()}
 */
public class FeatureIncrementalUpdateTask extends AbstractFeatureUpdateTask {

    private static final Logger logger = LoggerFactory.getLogger(FeatureIncrementalUpdateTask.class);

    public FeatureIncrementalUpdateTask(String featureId, String variant,
            Boolean dryRun, Integer dryRunRecordCount, FeatureManager featureManager,
            TaskConfig defaultTaskConfig, Map<String, Object> configProperties, String metricNamePrefix,
            MeterRegistry meterRegistry) {
        super(featureId, variant, dryRun, dryRunRecordCount, featureManager, defaultTaskConfig,
            configProperties, metricNamePrefix, meterRegistry);
    }

    @Override
    public String getUpdateType() {
        return "incremental";
    }

    @Override
    protected List<DataFilter> getFilters(String inputId, FeatureMetadata featureMetadata,
        Map<String, Object> queryParams) {
        List<DataFilter> filters = new ArrayList<>();
        filters.add(new LastUpdateTimeFilter(featureMetadata.getUpdatedToDate()));
        // Make last update time available as a query parameter.
        queryParams.put("lastUpdateTime", featureMetadata.getUpdatedToDate());
        return filters;
    }

    @Override
    protected TaskConfig getTaskConfig(FeatureConfig featureConfig) {
        return featureConfig.getIncrementalUpdateConfig();
    }

    @Override
    protected void onUpdateStart(FeatureUpdateContext context) {
        String updatedToDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
            .format(context.getFeatureMetadata().getUpdatedToDate());
        logger.info(String.format("Incremental update will include changes since %s",
            updatedToDate));
    }

    @Override
    protected void finishUpdate(FeatureUpdateContext context) {
        FeatureMetadata featureMetadata = context.getFeatureMetadata();
        featureMetadata.setUpdatedToDate(context.getStartTime());
        try {
            featureDBManager.updateFeatureMetadata(featureMetadata);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
