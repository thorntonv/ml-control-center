/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureSpecificKeysUpdateTask.java
 */

package mlcc.feature.update;

import dataflow.batch.config.TaskConfig;
import dataflow.data.query.DataFilter;
import dataflow.data.query.MultiKeyDataFilter;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import mlcc.feature.FeatureManager;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.parser.FeatureConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Performs an update of the records with the specified keys.
 */
public class FeatureSpecificKeysUpdateTask extends AbstractFeatureUpdateTask {

    private static final Logger logger = LoggerFactory.getLogger(
        FeatureSpecificKeysUpdateTask.class);

    private final Map<String, List<List<Object>>> keysToUpdate;

    public FeatureSpecificKeysUpdateTask(String featureId, String variant,
            Map<String, List<List<Object>>> keysToUpdate, Boolean dryRun, Integer dryRunRecordCount,
            FeatureManager featureManager, TaskConfig defaultTaskConfig,
            Map<String, Object> configProperties, String metricNamePrefix, MeterRegistry meterRegistry) {
        super(featureId, variant, dryRun, dryRunRecordCount, featureManager, defaultTaskConfig,
            configProperties, metricNamePrefix, meterRegistry);
        this.keysToUpdate = keysToUpdate;
    }

    @Override
    public String getUpdateType() {
        return "specific keys";
    }

    @Override
    protected List<DataFilter> getFilters(String inputId, FeatureMetadata featureMetadata,
            Map<String, Object> queryParams) {
        List<DataFilter> filters = new ArrayList<>();
        List<List<Object>> keys = keysToUpdate.get(inputId);
        if (keys != null) {
            filters.add(new MultiKeyDataFilter(keys));
        }
        return filters;
    }

    @Override
    protected boolean isInputEnabled(String inputId) {
        return keysToUpdate.containsKey(inputId);
    }

    @Override
    protected TaskConfig getTaskConfig(FeatureConfig featureConfig) {
        return featureConfig.getSpecificKeysUpdateConfig();
    }

    @Override
    protected void onUpdateStart(FeatureUpdateContext context) {
        logger.info(String.format("%d records to update", keysToUpdate.size()));
    }
}
