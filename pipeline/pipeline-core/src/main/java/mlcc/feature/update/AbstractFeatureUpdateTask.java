/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AbstractFeatureUpdateTask.java
 */

package mlcc.feature.update;

import dataflow.batch.config.TaskConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.util.MetricsUtil;
import dataflow.core.util.StringUtil;
import dataflow.data.DataRecords;
import dataflow.data.query.DataFilter;
import dataflow.data.query.DataQueryValueProvider;
import dataflow.retry.Retry;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import mlcc.feature.FeatureManager;
import mlcc.feature.api.FeatureDBManager;
import mlcc.feature.api.FeatureData;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.parser.FeatureConfig;
import mlcc.feature.parser.FeatureConfigBuilder;
import mlcc.feature.parser.FeatureConfigurationException;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;
import reactor.core.Exceptions;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNullElse;
import static mlcc.feature.FeatureManager.DRY_RUN_VARIANT;
import static mlcc.feature.parser.FeatureConfigBuilder.DEFAULT_TIMEOUT;

/**
 * An abstract base class for feature update tasks.
 */
public abstract class AbstractFeatureUpdateTask {

    // Defaults to use if they have not been set by the caller.
    private static final int DEFAULT_TASK_PARALLELISM = 3;
    private static final int DEFAULT_TASK_PREFETCH_COUNT = 1;
    private static final int DEFAULT_TASK_BATCH_SIZE = 200;

    public static int DEFAULT_DRY_RUN_RECORD_COUNT = 500;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final int DEFAULT_METADATA_UPDATE_INTERVAL_MILLIS = 30 * 1000;

    protected final FeatureManager featureManager;
    protected final FeatureDBManager featureDBManager;
    protected final FeatureConfig featureConfig;
    protected final DataFlowEnvironment env;

    protected final String metricNamePrefix;
    private Timer pipelineExecutionTimer;
    private Timer batchTimer;
    private Counter cancelCounter;
    private AtomicLong taskRuntimeSecondsGauge;
    private AtomicLong recordCountGauge;
    protected final MeterRegistry meterRegistry;
    private final int metadataUpdateIntervalMillis = DEFAULT_METADATA_UPDATE_INTERVAL_MILLIS;
    private final FeatureUpdateContext context = new FeatureUpdateContext();

    private CompletableFuture<FeatureMetadata> futureResult;

    private final AtomicBoolean running = new AtomicBoolean(false);
    private final AtomicBoolean canceled = new AtomicBoolean(false);
    private final TaskConfig taskConfig;
    private ScheduledFuture<?> progressUpdater;
    private final ScheduledExecutorService progressUpdateScheduler =
        Executors.newScheduledThreadPool(1);
    private Date lastMetricsUpdateTime;
    private AtomicLong lastMetricsUpdateCompletedRecordCount = new AtomicLong();

    public AbstractFeatureUpdateTask(String featureId, String variant,
        boolean dryRun, int dryRunRecordCount, FeatureManager featureManager,
        TaskConfig defaultTaskConfig, Map<String, Object> configProperties, String metricNamePrefix,
        MeterRegistry meterRegistry) {
        this.featureManager = featureManager;
        this.featureDBManager = featureManager.getFeatureDBManager();
        Optional<FeatureConfig> featureConfig = featureManager.getFeatureConfig(featureId, variant);
        if (!featureConfig.isPresent()) {
            throw new DataFlowConfigurationException(
                String.format("Feature %s variant %s does not exist", featureId, variant));
        }

        this.featureConfig = featureConfig.get();
        this.env = featureManager.getDataFlowEnvironment();
        context.setTaskId(UUID.randomUUID().toString().replace("-", ""));
        context.setDryRun(dryRun);
        context.setDryRunMaxItemCount(dryRunRecordCount);

        this.metricNamePrefix = StringUtils.isEmpty(metricNamePrefix) ? getClass().getSimpleName() :
            String.format("%s.%s", metricNamePrefix, getClass().getSimpleName());
        this.meterRegistry = meterRegistry;
        this.taskConfig = buildTaskConfig(defaultTaskConfig, dryRun);
        context.setRetry(Retry.clone(this.taskConfig.getRetry()));

        Map<String, Object> mergedConfigProperties = new HashMap<>(getConfigProperties(this.featureConfig));
        mergedConfigProperties.putAll(taskConfig.getProperties());
        if(dryRun) {
            mergedConfigProperties.putAll(taskConfig.getDryRunProperties());
        }
        mergedConfigProperties.putAll(configProperties);
        context.setConfigProperties(mergedConfigProperties);
    }

    private TaskConfig buildTaskConfig(TaskConfig defaultTaskConfig, boolean dryRun) {
        TaskConfig taskConfig = new TaskConfig();
        TaskConfig featureTaskConfig = getTaskConfig(featureConfig);
        if(dryRun) {
            try {
                TaskConfig dryRunTaskConfig = new TaskConfig();
                new FeatureConfigBuilder(env.getRegistry()).buildTaskConfig(
                    dryRunTaskConfig, featureTaskConfig.getDryRunProperties(), "", featureConfig);
                taskConfig.mergePropertiesIfNotSet(dryRunTaskConfig);
            } catch (FeatureConfigurationException e) {
                throw new DataFlowConfigurationException("Unable to parse dry run tsak config", e);
            }
        }
        taskConfig.mergePropertiesIfNotSet(featureTaskConfig);
        taskConfig.mergePropertiesIfNotSet(defaultTaskConfig);
        taskConfig.mergePropertiesIfNotSet(buildDefaultTaskConfig());
        return taskConfig;
    }

    public String getId() {
        return context != null ? context.getTaskId() : null;
    }

    public synchronized CompletableFuture<FeatureMetadata> start() {
        if (futureResult != null) {
            return futureResult;
        }
        running.set(true);
        canceled.set(false);

        futureResult = new CompletableFuture<>();
        context.setStartTime(new Date());
        featureManager.getExecutor().submit(this::execute);
        return futureResult;
    }

    private void execute() {
        String updateType = getUpdateType();
        FeatureMetadata featureMetadata;
        DataFlowExecutionContext existingExecutionContext =
            DataFlowExecutionContext.getCurrentExecutionContext();
        try {
            setMDC(context);
            logger.info(String.format("Starting %s update of feature %s variant %s",
                updateType, featureConfig.getId(), featureConfig.getVariant()));

            logger.info(taskConfig.toString());
            context.setFeatureDataOutput(getOutputFeatureData(context));
            featureMetadata = context.getFeatureDataOutput().getMetadata();
            context.setFeatureMetadata(featureMetadata);

            setMDC(context);

            initMetrics(featureMetadata, context.getRetry());
            addRetryLogging(context.getRetry());

            onUpdateStart(context);

            DataFlowInstance instance = createPipelineInstance(
                featureConfig.getPipeline().getId(), context);
            DataFlowExecutionContext executionContext = DataFlowExecutionContext
                .createExecutionContext(instance.getEnvironment());
            executionContext.setInstance(instance);

            if (progressUpdater != null && !progressUpdater.isDone()) {
                progressUpdater.cancel(true);
            }
            this.progressUpdater = progressUpdateScheduler.scheduleAtFixedRate(() -> {
                if(running.get()) {
                    updateMetrics(context);
                    logProgress(context);
                }
            }, 5, 30, TimeUnit.SECONDS);

            // Run before update pipeline
            runBeforeStartPipeline(context);

            // Define and run the update pipeline.
            Map<String, Flux<FeatureUpdateBatchContext>> inputFluxList = getInputValueProviders(context).entrySet().stream()
                .filter(e -> isInputEnabled(e.getKey()))
                .collect(Collectors.toMap(Entry::getKey, e -> createSingleInputDataFlux(e.getKey(), e.getValue(), context)));
            inputFluxList.forEach((inputId, inputFlux) -> {
                logger.info("Starting update for input {} of {}:{}",
                    inputId, featureConfig.getId(), featureConfig.getVariant());
                inputFlux.subscribeOn(Schedulers.newBoundedElastic(taskConfig.getPrefetchCount(), Integer.MAX_VALUE, "mlccInput"))
                    .parallel(taskConfig.getParallelism(), taskConfig.getPrefetchCount())
                    .runOn(Schedulers.newBoundedElastic(taskConfig.getParallelism(), Integer.MAX_VALUE, "mlccPipeline"), taskConfig.getPrefetchCount())
                    .flatMap(createPipelineFn(inputId))
                    .flatMap(createFinishBatchFn())
                    .sequential()
                    .doOnError(ex -> doOnError(context, ex))
                    .blockLast(Duration.ofSeconds(requireNonNullElse(taskConfig.getTimeoutSeconds(), Duration.parse(DEFAULT_TIMEOUT).getSeconds())));
            });

            // Run after update pipeline.
            runAfterCompletePipeline(context);

            // Update is finished.
            doOnComplete(context);
        } catch (Throwable ex) {
            doOnError(context, ex);
        } finally {
            if(!futureResult.isDone()) {
                logger.warn("Task did not complete the result future " + getId());
            }
            DataFlowExecutionContext.setCurrentExecutionContext(existingExecutionContext);
        }
    }

    public boolean isRunning() {
        return running.get();
    }

    public boolean isCanceled() {
        return canceled.get();
    }

    public void setCanceled(boolean canceled) {
        this.canceled.set(canceled);
    }

    public CompletableFuture<FeatureMetadata> getFutureResult() {
        return futureResult;
    }

    public Date getStartTime() {
        return context.getStartTime();
    }

    public int getRetryCount() {
        return context.getRetryCount().intValue();
    }

    public double getRunTimeHours() {
        if (context.getStartTime() == null) {
            return 0.0;
        }
        return (System.currentTimeMillis() - context.getStartTime().getTime()) /
            1000.0 / 60.0 / 60.0;
    }

    public abstract String getUpdateType();

    protected List<DataFilter> getFilters(String inputId, FeatureMetadata featureMetadata,
        Map<String, Object> queryParams) {
        return Collections.emptyList();
    }

    protected boolean isInputEnabled(String inputId) {
        return true;
    }

    protected void onUpdateStart(FeatureUpdateContext context) {
    }

    protected void onBatchCompletion(FeatureUpdateContext context, boolean updateMetadata)
        throws IOException {
    }

    protected void onUpdateFailure(FeatureUpdateContext context, Throwable ex) {
    }

    protected void onUpdateCanceled(FeatureUpdateContext context) {
    }

    protected void finishUpdate(FeatureUpdateContext context) {
    }

    protected Counter getErrorCounter(String errorType) {
        if (meterRegistry == null) {
            return null;
        }
        List<Tag> tags = getTags(context.getFeatureMetadata()).entrySet().stream()
            .map(entry -> Tag.of(entry.getKey(), entry.getValue()))
            .collect(Collectors.toList());
        if (errorType != null) {
            tags.add(Tag.of("errorType", errorType));
        }
        return meterRegistry.counter(getMetricName("errorCount"), tags);
    }

    protected TaskConfig getTaskConfig(FeatureConfig featureConfig) {
        return new TaskConfig();
    }

    protected Map<String, String> getTags(FeatureMetadata featureMetadata) {
        Map<String, String> tags = new HashMap<>();
        tags.put("featureId", featureConfig.getId());
        tags.put("variant", featureConfig.getVariant());
        tags.put("version", featureMetadata != null ? String.valueOf(featureMetadata.getVersion()) : "unknown");
        tags.put("updateType", getUpdateType());
        tags.put("dryRun", String.valueOf(context.isDryRun()));
        return tags;
    }

    protected Map<String, Object> getConfigProperties(FeatureConfig featureConfig) {
        Map<String, Object> properties = new HashMap<>();
        TaskConfig taskConfig = getTaskConfig(featureConfig);
        if (taskConfig.getProperties() != null) {
            properties.putAll(taskConfig.getProperties());
        }
        if (context.isDryRun() && taskConfig.getDryRunProperties() != null) {
            properties.putAll(taskConfig.getDryRunProperties());
        }
        return properties;
    }

    protected TaskConfig buildDefaultTaskConfig() {
        TaskConfig taskConfig = new TaskConfig();
        taskConfig.setBatchSize(DEFAULT_TASK_BATCH_SIZE);
        taskConfig.setParallelism(DEFAULT_TASK_PARALLELISM);
        taskConfig.setPrefetchCount(DEFAULT_TASK_PREFETCH_COUNT);
        return taskConfig;
    }

    protected void updateMetrics(FeatureUpdateContext context) {
        setMDC(context);
        try {
            long recordCount = context.getCompletedItemCount().get();
            if (lastMetricsUpdateTime != null) {
                int completedRecordsSinceMetricsUpdate =
                    (int) (recordCount - lastMetricsUpdateCompletedRecordCount.get());
                long millisSinceMetricsUpdate =
                    System.currentTimeMillis() - lastMetricsUpdateTime.getTime();
                if (completedRecordsSinceMetricsUpdate > 0) {
                    context.getTimeEstimator().recordExecutionTime(
                        completedRecordsSinceMetricsUpdate, millisSinceMetricsUpdate);
                }
            }
            if (recordCountGauge != null) {
                recordCountGauge.set(recordCount);
            }
            if (taskRuntimeSecondsGauge != null) {
                taskRuntimeSecondsGauge.set(
                    (System.currentTimeMillis() - context.getStartTime().getTime()) / 1000);
            }
            lastMetricsUpdateTime = new Date();
            lastMetricsUpdateCompletedRecordCount.set(recordCount);
        } catch (Throwable ex) {
            logger.warn(getClass().getSimpleName() + " metrics update error", ex);
        }
    }

    protected void logProgress(FeatureUpdateContext context) {
        setMDC(context);
        try {
            FeatureMetadata featureMetadata = context.getFeatureMetadata();
            long recordCount = context.getCompletedItemCount().get();
            double avgItemsPerSecond =
                1000.0 / context.getTimeEstimator().getAverageTimePerItemInMillis();
            String timeEstimateStr = "";
            String totalCountEstimateStr = "";
            if (context.getEstimatedItemCount().get() > 0) {
                long timeEstimateSeconds = context.getTimeEstimator()
                    .getRemainingTimeEstimateSeconds(context.getEstimatedItemCount().get());
                timeEstimateStr = String
                    .format(" - %.2f records/sec. - estimated hours remaining: %.1f",
                        avgItemsPerSecond, timeEstimateSeconds / 60.0 / 60.0);
                totalCountEstimateStr = "/" + context.getEstimatedItemCount().get();
            } else if (avgItemsPerSecond > 0) {
                timeEstimateStr = String.format(" - %.2f records/sec.", avgItemsPerSecond);
            }

            String pipelineCounts = "";
            if (context.getPipelineCompletedItemCounts().size() > 1) {
                pipelineCounts = " (" + StringUtil.join(context.getPipelineCompletedItemCounts().entrySet().stream()
                    .map(entry -> entry.getKey() + "=" + entry.getValue().get())
                    .collect(Collectors.toList()), ", ") + ")";
            }

            double runTimeHours = getRunTimeHours();
            logger.info(String.format(
                "%s update of feature %s variant %s version %d is in progress - %d%s total records completed%s in %.1f hours%s",
                getUpdateType(), featureMetadata.getFeatureId(),
                featureMetadata.getVariant(), featureMetadata.getVersion(), recordCount,
                totalCountEstimateStr, pipelineCounts, runTimeHours, timeEstimateStr));
        } catch (Throwable ex) {
            logger.warn(getClass().getSimpleName() + " progress logger error", ex);
        }
    }

    private void doOnComplete(FeatureUpdateContext context) {
        try {
            context.getFeatureDataOutput().close();
        } catch (Throwable e) {
            doOnError(context, e);
            throw new RuntimeException(e);
        }
        try {
            if (!canceled.get()) {
                updateMetrics(context);
                finishUpdate(context);

                logger.info(String.format("Finished %s update, %d records updated",
                    getUpdateType(), context.getCompletedItemCount().get()));
            } else {
                logger.info(String.format("%s update canceled", getUpdateType()));
            }
        } finally {
            cleanUp();

            if (!canceled.get()) {
                futureResult.complete(context.getFeatureMetadata());
            } else {
                futureResult.completeExceptionally(
                    new FeatureUpdateException("The update was canceled"));
            }
        }
    }

    protected String getErrorType(Throwable ex) {
        if (ex.getCause() != null) {
            return getErrorType(ex.getCause());
        }
        return ex.getClass().getSimpleName();
    }

    private void doOnError(FeatureUpdateContext context, Throwable ex) {
        try {
            if (futureResult.isCompletedExceptionally()) {
                // The task has already completed with an error.
                return;
            }
            onUpdateFailure(context, ex);

            Counter errorCounter = getErrorCounter(getErrorType(ex));
            if (errorCounter != null) {
                errorCounter.increment();
            }
            FeatureMetadata featureMetadata = context.getFeatureMetadata();
            String version = featureMetadata != null ?
                String.format("version %d ", featureMetadata.getVersion()) : "";
            logger.warn(String.format("%s update of feature %s variant %s %s failed",
                getUpdateType(), featureConfig.getId(), featureConfig.getVariant(), version), ex);
        } finally {
            cleanUp();
            futureResult.completeExceptionally(ex);
        }
    }

    private void cleanUp() {
        running.set(false);
        try {
            if(context.getFeatureDataOutput() != null) {
                context.getFeatureDataOutput().close();
                context.setFeatureDataOutput(null);
            }
        } catch (Throwable e) {
            logger.warn("Error closing feature data output", e);
        }
        try {
            logger.info("Performing final clean up");
            context.getAllocatedInstances().forEach(instance -> {
                try {
                    instance.close();
                    logger.info("Closed pipeline instance " +
                        instance.getConfig().getId() + ":" + instance.getInstanceId());
                } catch (Throwable e) {
                    logger.warn("Error closing DataFlow instance " +
                        instance.getConfig().getId() + ":" + instance.getInstanceId(), e);
                }
            });
            context.getAllocatedInstances().clear();
            if (progressUpdater != null && !progressUpdater.isDone()) {
                progressUpdater.cancel(false);
                progressUpdater = null;
            }
        } catch (Throwable ex) {
            logger.warn("Error performing cleanup after feature update", ex);
        }
        MDC.clear();
    }

    private void runBeforeStartPipeline(FeatureUpdateContext context) throws Exception {
        TaskConfig taskConfig = getTaskConfig(featureConfig);
        if (taskConfig.getBeforeStartFlow() != null) {
            DataFlowInstance instance = createPipelineInstance(
                taskConfig.getBeforeStartFlow().getId(), context);
            logger.info("Executing beforeStart actions");
            instance.execute();
            logger.info("Finished executing beforeStart actions");
        }
    }

    private void runAfterCompletePipeline(FeatureUpdateContext context) throws Exception {
        TaskConfig taskConfig = getTaskConfig(featureConfig);
        if (taskConfig.getAfterCompleteFlow() != null) {
            DataFlowInstance instance = createPipelineInstance(
                taskConfig.getAfterCompleteFlow().getId(), context);
            logger.info("Executing afterComplete actions");
            instance.execute();
            logger.info("Finished executing afterComplete actions");
        }
    }

    private Flux<FeatureUpdateBatchContext> createSingleInputDataFlux(
        String id, DataQueryValueProvider inputValueProvider, FeatureUpdateContext context) {
        FeatureMetadata featureMetadata = context.getFeatureMetadata();

        Map<String, Object> queryParams = new HashMap<>();

        // Asynchronously try to get the record count.
        Mono.create((Consumer<MonoSink<Long>>) sink -> {
            setMDC(context);
            MDC.put("inputId", id);
            Optional<Long> estimatedCount = inputValueProvider.getEstimatedResultCount(
                queryParams, getFilters(id, context.getFeatureMetadata(), queryParams));
            logger.info("Estimated record count: " +
                (estimatedCount.isPresent() ? estimatedCount.get() : "unknown"));
            sink.success(estimatedCount.orElse(null));
        }).subscribeOn(Schedulers.newBoundedElastic(1, 1, "getRecordCount"))
            .subscribe(count -> context.getEstimatedItemCount().addAndGet(count != null ? count : 0));

        String updateType = getUpdateType();
        AtomicLong readItemCount = new AtomicLong(0);

        List<DataFilter> filters = new ArrayList<>(getFilters(id, featureMetadata, queryParams));

        logger.info("Starting read for input " + id);

        return Flux.using(() -> inputValueProvider.queryForStream(
                queryParams, filters, taskConfig.getBatchSize()),
            (Function<Stream<DataRecords>, Publisher<FeatureUpdateBatchContext>>) inputRecordsStream -> {
                if(Boolean.TRUE.equals(taskConfig.getPrefetchAllRecords())) {
                    // Preload all record batches into memory.
                    inputRecordsStream = preloadDataRecords(id, inputRecordsStream);
                }
                Iterator<DataRecords> inputRecordsIt = inputRecordsStream.iterator();
                return Flux.generate(sink -> {
                    setMDC(context);
                    if (context.isDryRun() &&
                        readItemCount.get() > context.getDryRunMaxItemCount()) {
                        logger.info("Read all records needed for dry run");
                        sink.complete();
                        return;
                    }
                    if (canceled.get()) {
                        logger.info(
                            String.format("%s update of feature %s variant %s version %d was canceled",
                                updateType, featureMetadata.getFeatureId(), featureMetadata.getVariant(),
                                featureMetadata.getVersion()));

                        if (cancelCounter != null) {
                            cancelCounter.increment();
                        }
                        onUpdateCanceled(context);
                        sink.error(new FeatureUpdateException("The update was canceled"));
                        return;
                    }

                    FeatureUpdateBatchContext batchContext = new FeatureUpdateBatchContext();
                    batchContext.setInputId(id);
                    batchContext.setTaskContext(context);
                    batchContext.setStartTime(new Date());

                    try {
                        if(inputRecordsIt.hasNext()) {
                            DataRecords inputRecords = inputRecordsIt.next();
                            context.getInputItemCount().addAndGet(inputRecords.size());
                            readItemCount.addAndGet(inputRecords.size());
                            batchContext.setRecords(inputRecords);
                            sink.next(batchContext);
                        } else {
                            logger.info("Finished reading input records");
                            sink.complete();
                        }
                    } catch (Throwable e) {
                        logger.warn("Error reading input data", e);
                        sink.error(new BatchExecutionException("Error reading input data.",
                            batchContext, e));
                    }
                });
            }, inputRecordsStream -> {
                try {
                    inputRecordsStream.close();
                } catch (Throwable t) {
                    logger.warn("Error closing input record stream", t);
                    throw Exceptions.propagate(t);
                }
            });
    }

    private Stream<DataRecords> preloadDataRecords(String inputId, Stream<DataRecords> inputRecordsStream) {
        logger.info("Preloading all records for input " + inputId);
        List<DataRecords> allRecordBatches = new ArrayList<>();
        AtomicInteger recordCount = new AtomicInteger();

        java.util.Timer timer = new java.util.Timer();
        try {
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    logger.info("Preloaded " + recordCount.get() + " records for input " + inputId);
                }
            }, 0, TimeUnit.SECONDS.toMillis(60));
            inputRecordsStream.forEach(batch -> {
                allRecordBatches.add(batch);
                recordCount.addAndGet(batch.size());
            });
        } finally {
            timer.cancel();
            logger.info("Finished preloading " + recordCount.get() + " records for input " + inputId);
        }
        return allRecordBatches.stream();
    }

    private Function<FeatureUpdateBatchContext, Publisher<FeatureUpdateBatchContext>> createPipelineFn(String inputId) {
        ThreadLocal<DataFlowInstance> threadLocalPipeline = new ThreadLocal<>();
        return batchContext -> {
            FeatureUpdateContext context = batchContext.getTaskContext();
            try {
                DataRecords inputRecords = batchContext.getRecords();
                setMDC(context, batchContext);
                if (threadLocalPipeline.get() == null) {
                    threadLocalPipeline.set(createPipelineInstance(
                        featureConfig.getPipeline().getId(), inputId, context));
                }
                DataFlowInstance pipeline = threadLocalPipeline.get();
                pipeline.setValue("inputItems", inputRecords);
                pipeline.setValue("inputKeys", inputRecords.getKeys());
                pipeline.setValue("inputId", inputId);
                batchContext.setRecords(recordExecutionTime(pipelineExecutionTimer, () -> {
                    logger.debug("Executing pipeline");
                    pipeline.execute();
                    logger.debug("Finished executing pipeline");
                    return (DataRecords) pipeline.getOutput();
                }));
                return Mono.just(batchContext).subscribeOn(Schedulers.elastic());
            } catch (Throwable e) {
                throw new BatchExecutionException("Error executing pipeline.", batchContext, e);
            }
        };
    }

    private Function<FeatureUpdateBatchContext, Publisher<FeatureUpdateBatchContext>> createFinishBatchFn() {
        return batchContext -> {
            FeatureUpdateContext context = batchContext.getTaskContext();
            DataRecords records = batchContext.getRecords();
            setMDC(context, batchContext);

            try {
                context.getCompletedItemCount().addAndGet(records.size());
                context.getCompletedItemCount(batchContext.getInputId()).addAndGet(records.size());
                long batchExecutionTimeMillis =
                    (System.currentTimeMillis() - batchContext.getStartTime().getTime());
                long millisSinceMetadataUpdate = System.currentTimeMillis() -
                    context.getLastMetadataUpdateTime().getTime();
                if (millisSinceMetadataUpdate > metadataUpdateIntervalMillis) {
                    onBatchCompletion(context, true);
                    context.setLastMetadataUpdateTime(new Date());
                } else {
                    onBatchCompletion(context, false);
                }

                if (batchTimer != null) {
                    batchTimer.record(batchExecutionTimeMillis, TimeUnit.MILLISECONDS);
                }

                logger.debug(String.format("Finished batch in %d sec. %d records completed",
                    batchExecutionTimeMillis / 1000, records.size()));
                return Mono.just(batchContext).subscribeOn(Schedulers.elastic());
            } catch (Throwable e) {
                throw new BatchExecutionException("Error finishing batch", batchContext, e);
            }
        };
    }

    private DataFlowInstance createPipelineInstance(String pipelineId, FeatureUpdateContext context)
            throws Exception {
        return createPipelineInstance(pipelineId, null, context);
    }

    private DataFlowInstance createInputInstance(String inputId, FeatureUpdateContext context) throws Exception {
        DataFlowInstance instance = env.getRegistry().getDataFlowRegistration(inputId).getInstanceFactory().newInstance(newInstance -> {
            //newInstance.setDefaultRetryBehavior(context.getRetry());
            newInstance.setMetricNamePrefix(metricNamePrefix);
            Map<String, String> tags = getTags(context.getFeatureMetadata());
            tags.put("inputId", inputId != null ? inputId : "");
            newInstance.setMetricTags(tags);

            newInstance.setValue("featureId", featureConfig.getId());
            String featureVariant = !context.isDryRun() ? featureConfig.getVariant() : DRY_RUN_VARIANT;
            newInstance.setValue("featureVariant", featureVariant);
            if (context.getFeatureMetadata() != null) {
                newInstance.setValue("featureVersion", context.getFeatureMetadata().getVersion());
            }
            newInstance.setValue("featureUpdateContext", context);
            context.getConfigProperties().forEach(newInstance::setValue);
        });
        logger.info("Created input instance " +
            instance.getConfig().getId() + ":" + instance.getInstanceId());
        context.getAllocatedInstances().add(instance);
        return instance;
    }

    private DataFlowInstance createPipelineInstance(String pipelineId, String inputId,
            FeatureUpdateContext context) throws Exception {
        DataFlowInstance instance = env.getRegistry().getDataFlowRegistration(pipelineId).getInstanceFactory().newInstance(newInstance -> {
            // newInstance.setDefaultRetryBehavior(context.getRetry());
            newInstance.setMetricNamePrefix(metricNamePrefix);
            Map<String, String> tags = getTags(context.getFeatureMetadata());
            tags.put("inputId", inputId != null ? inputId : "");
            tags.put("pipelineId", pipelineId);
            newInstance.setMetricTags(tags);

            newInstance.setValue("featureId", featureConfig.getId());
            String featureVariant = !context.isDryRun() ? featureConfig.getVariant() : DRY_RUN_VARIANT;
            newInstance.setValue("featureVariant", featureVariant);
            if (context.getFeatureMetadata() != null) {
                newInstance.setValue("featureVersion", context.getFeatureMetadata().getVersion());
            }
            newInstance.setValue("featureUpdateContext", context);
            context.getConfigProperties().forEach(newInstance::setValue);
        });
        logger.info("Created pipeline instance " +
            instance.getConfig().getId() + ":" + instance.getInstanceId());
        context.getAllocatedInstances().add(instance);
        return instance;
    }

    protected void setMDC(FeatureUpdateContext context) {
        setMDC(context, null);
    }

    protected void setMDC(FeatureUpdateContext context, FeatureUpdateBatchContext batchContext) {
        MDC.clear();
        MDC.put("taskId", context.getTaskId());
        MDC.put("updateType", getUpdateType());
        MDC.put("feature", getFeatureInfo(context.getFeatureMetadata()));
        if (context.isDryRun()) {
            MDC.put("dryRun", "true");
        }
        if (batchContext != null) {
            MDC.put("inputId", batchContext.getInputId());
        }
    }

    private String getFeatureInfo(FeatureMetadata featureMetadata) {
        StringBuilder featureInfo = new StringBuilder(String.format("%s:%s",
            featureConfig.getId(), featureConfig.getVariant()));
        if (featureMetadata != null) {
            featureInfo.append(":").append(featureMetadata.getVersion());
        }
        return featureInfo.toString();
    }

    protected Map<String, DataQueryValueProvider> getInputValueProviders(FeatureUpdateContext context)
            throws Exception {
        Map<String, DataQueryValueProvider> inputValueProviders = new LinkedHashMap<>();
        int nextGeneratedId = 1;
        for(DataFlowConfig inputConfig : featureConfig.getInputConfigs()) {
            DataFlowInstance inputInstance = createInputInstance(inputConfig.getId(), context);
            inputInstance.execute();
            Object inputValueProviderObj = inputInstance.getOutput();
            if (!(inputValueProviderObj instanceof DataQueryValueProvider)) {
                throw new DataFlowExecutionException(
                    "Input must be a " + DataQueryValueProvider.class.getSimpleName() + " but was " +
                        inputValueProviderObj.getClass().getSimpleName());
            }
            String id = (String) ((Map)inputConfig.getOutput().getProperties().get("value")).get("id");
            if(id == null) {
                id = "input" + nextGeneratedId++;
            }
            inputValueProviders.put(id, (DataQueryValueProvider) inputValueProviderObj);
        }
        return inputValueProviders;
    }

    protected FeatureData getOutputFeatureData(FeatureUpdateContext context) throws IOException {
        return featureDBManager.getFeatureData(featureConfig.getId(), featureConfig.getVariant());
    }

    private void initMetrics(FeatureMetadata featureMetadata, Retry retry) {
        if (meterRegistry != null) {
            Iterable<Tag> tags = getTags(featureMetadata).entrySet().stream()
                .map(entry -> Tag.of(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
            this.pipelineExecutionTimer = meterRegistry
                .timer(getMetricName("pipelineExecutionTimer"), tags);
            this.recordCountGauge = meterRegistry.gauge(getMetricName("recordCount"),
                tags, new AtomicLong());
            this.taskRuntimeSecondsGauge = meterRegistry.gauge(
                getMetricName("updateRunTimeSeconds"), tags, new AtomicLong());
            this.batchTimer = meterRegistry.timer(getMetricName("batchTimer"), tags);
            this.cancelCounter = meterRegistry.counter(getMetricName("cancelCount"), tags);
            if (retry != null) {
                Counter retryCounter = meterRegistry.counter(getMetricName("retryCount"), tags);
                retry.getEventPublisher().onRetry(event -> retryCounter.increment());
            }
        }
    }

    private void addRetryLogging(Retry retry) {
        if (retry != null) {
            retry.getEventPublisher().onRetry(event -> {
                setMDC(context);
                logger.warn(event.toString());
                context.getRetryCount().set(event.getNumberOfRetryAttempts());
            });
            retry.getEventPublisher().onError(event -> {
                setMDC(context);
                logger.error(event.toString());
            });
        }
    }

    private String getMetricName(String name) {
        StringBuilder finalName = new StringBuilder();
        MetricsUtil.appendToMetricName(metricNamePrefix, finalName);
        MetricsUtil.appendToMetricName(name, finalName);
        return finalName.toString();
    }

    private static <T> T recordExecutionTime(Timer timer, Callable<T> callable) throws Exception {
        if (timer != null) {
            return timer.recordCallable(callable);
        }
        return callable.call();
    }
}
