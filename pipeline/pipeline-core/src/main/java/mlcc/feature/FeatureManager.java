/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureManager.java
 */

package mlcc.feature;

import dataflow.batch.config.ScheduledTaskConfig;
import dataflow.batch.config.TaskConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.datasource.DataSource;
import dataflow.core.datasource.DataSourceEventListener;
import dataflow.core.datasource.DataSourceManager;
import dataflow.core.datasource.DataSourceSupplier;
import dataflow.core.environment.DataFlowEnvironment;
import io.micrometer.core.instrument.MeterRegistry;
import mlcc.feature.api.FeatureDBManager;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.parser.*;
import mlcc.feature.update.AbstractFeatureUpdateTask;
import mlcc.feature.update.FeatureFullUpdateTask;
import mlcc.feature.update.FeatureIncrementalUpdateTask;
import mlcc.feature.update.FeatureSpecificKeysUpdateTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.scheduling.support.CronTrigger;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static mlcc.feature.api.FeatureDBManager.DEFAULT_VARIANT;

public class FeatureManager implements AutoCloseable {

    public static final String DRY_RUN_VARIANT = "dryRun";

    private static final DateFormat DATE_FORMAT = SimpleDateFormat.getDateTimeInstance();
    private static final Logger logger = LoggerFactory.getLogger(FeatureManager.class);

    private class EventListener implements DataSourceEventListener<DataSource> {

        @Override
        public void onAdd(DataSource dataSource) {
            try {
                FeatureConfig featureConfig = readConfig(dataSource);
                setFeatureConfig(featureConfig);
                logger.info("Added config for feature " + featureConfig.getId());
            } catch (IOException | FeatureConfigParseException | FeatureConfigurationException e) {
                onError(e);
            }
        }

        @Override
        public void onChange(DataSource dataSource) {
            try {
                setFeatureConfig(readConfig(dataSource));
            } catch (IOException | FeatureConfigParseException | FeatureConfigurationException e) {
                onError(e);
            }
        }

        @Override
        public void onRemove(DataSource dataSource) {
            try {
                removeFeatureConfig(readConfig(dataSource));
            } catch (IOException | FeatureConfigParseException | FeatureConfigurationException e) {
                onError(e);
            }
        }

        @Override
        public void onError(Exception error) {
            logger.warn("Error loading feature config", error);
        }
    }

    private static final class ScheduledTaskRegistration<T extends AbstractFeatureUpdateTask> {
        private ScheduledFuture<?> scheduledFuture;
        private T task;

        public ScheduledFuture<?> getScheduledFuture() {
            return scheduledFuture;
        }

        public void setScheduledFuture(ScheduledFuture<?> scheduledFuture) {
            this.scheduledFuture = scheduledFuture;
        }

        public T getTask() {
            return task;
        }

        public void setTask(T task) {
            this.task = task;
        }
    }

    private final FeatureConfigParser parser;
    private final DataFlowEnvironment env;
    private final DataSourceManager<DataSource> dataSourceManager = new DataSourceManager<>();
    private final FeatureDBManager featureDBManager;
    private final ExecutorService executor = Executors.newCachedThreadPool();
    private final TaskConfig defaultFullUpdateTaskConfig;
    private final TaskConfig defaultIncrementalUpdateTaskConfig;
    private final TaskConfig defaultTaskConfig;
    private final String metricNamePrefix;
    private final MeterRegistry meterRegistry;
    private final Map<String, FeatureConfig> featureConfigs = new ConcurrentHashMap<>();
    private final ThreadPoolTaskScheduler scheduler;

    private final Map<String, ScheduledTaskRegistration<FeatureFullUpdateTask>> fullUpdateTaskRegistrationMap = new ConcurrentHashMap<>();
    private final Map<String, ScheduledTaskRegistration<FeatureIncrementalUpdateTask>> incrementalUpdateTaskRegistrationMap = new ConcurrentHashMap<>();

    public FeatureManager(FeatureDBManager featureDBManager, DataFlowEnvironment env,
        TaskConfig defaultFullUpdateTaskConfig, TaskConfig defaultIncrementalUpdateTaskConfig,
        TaskConfig defaultTaskConfig, String metricNamePrefix, MeterRegistry meterRegistry) {
        this(featureDBManager, new FeatureConfigParser(env.getRegistry()), env, defaultFullUpdateTaskConfig,
            defaultIncrementalUpdateTaskConfig, defaultTaskConfig, metricNamePrefix, meterRegistry);
    }

    public FeatureManager(FeatureDBManager featureDBManager, FeatureConfigParser parser,
        DataFlowEnvironment env, TaskConfig defaultFullUpdateTaskConfig,
        TaskConfig defaultIncrementalUpdateTaskConfig,
        TaskConfig defaultTaskConfig, String metricNamePrefix, MeterRegistry meterRegistry) {
        this.featureDBManager = featureDBManager;
        this.parser = parser;
        this.env = env;
        dataSourceManager.addEventListener(new FeatureManager.EventListener());
        this.defaultFullUpdateTaskConfig = defaultFullUpdateTaskConfig;
        this.defaultIncrementalUpdateTaskConfig = defaultIncrementalUpdateTaskConfig;
        this.defaultTaskConfig = defaultTaskConfig;
        this.metricNamePrefix = metricNamePrefix;
        this.meterRegistry = meterRegistry;

        this.scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(1);
        scheduler.setThreadNamePrefix("FeatureUpdate");
        scheduler.initialize();
    }

    public DataFlowEnvironment getDataFlowEnvironment() {
        return env;
    }

    public FeatureDBManager getFeatureDBManager() {
        return featureDBManager;
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public void registerConfigProvider(DataSourceSupplier<DataSource> configProvider, int refreshIntervalSeconds) {
        dataSourceManager.register(configProvider, refreshIntervalSeconds);
    }

    public List<? extends AbstractFeatureUpdateTask> getTasks() {
        return Stream.concat(getFullUpdateTasks().values().stream(),
            getIncrementalUpdateTasks().values().stream()).collect(Collectors.toList());
    }

    public Map<FeatureConfig, FeatureFullUpdateTask> getFullUpdateTasks() {
        return getTasks(fullUpdateTaskRegistrationMap);
    }

    public Map<FeatureConfig, FeatureIncrementalUpdateTask> getIncrementalUpdateTasks() {
        return getTasks(incrementalUpdateTaskRegistrationMap);
    }

    private <T extends AbstractFeatureUpdateTask> Map<FeatureConfig, T> getTasks(
            Map<String, ScheduledTaskRegistration<T>> registrationMap) {
        Map<FeatureConfig, T> tasksMap = new HashMap<>();
        for (FeatureConfig featureConfig : featureConfigs.values()) {
            ScheduledTaskRegistration<T> registration = registrationMap.get(getFeatureKey(featureConfig));
            if (registration != null && registration.getTask() != null) {
                tasksMap.put(featureConfig, registration.getTask());
            }
        }
        return tasksMap;
    }

    private FeatureConfig readConfig(DataSource dataSource)
        throws IOException, FeatureConfigParseException, FeatureConfigurationException {
        try (InputStream in = dataSource.getInputStream()) {
            return parser.parse(in);
        }
    }

    synchronized void setFeatureConfig(FeatureConfig featureConfig) {
        String featureKey = getFeatureKey(featureConfig);
        FeatureConfig existingConfig = featureConfigs.get(featureKey);

        if(!Objects.equals(featureConfig, existingConfig)) {
            if (existingConfig != null) {
                removeFeatureConfig(existingConfig);
            }
            if (featureConfig.getPipeline() != null) {
                // engine.registerDataFlow(featureConfig.getPipeline());
            }
            for (DataFlowConfig inputConfig : featureConfig.getInputConfigs()) {
                // engine.registerDataFlow(inputConfig);
            }

            registerBeforeAndAfterUpdateActions(featureConfig.getFullUpdateConfig());
            registerBeforeAndAfterUpdateActions(featureConfig.getIncrementalUpdateConfig());

            featureConfigs.put(featureKey, featureConfig);

            scheduleUpdateTasks(featureConfig);

            logger.info("Loaded config for feature {}:{}",
                featureConfig.getId(), featureConfig.getVariant());
        }
    }

    synchronized void removeFeatureConfig(FeatureConfig featureConfig) {
        String featureKey = getFeatureKey(featureConfig);
        FeatureConfig removedConfig = featureConfigs.remove(featureKey);
        if (removedConfig != null) {
            unregisterUpdateTasks(removedConfig);
            if (featureConfig.getPipeline() != null) {
                // engine.unregisterDataFlow(featureConfig.getPipeline().getId());
            }
//            featureConfig.getInputConfigs().forEach(inputConfig ->
//                engine.unregisterDataFlow(inputConfig.getId()));
            unregisterBeforeAndAfterUpdateActions(featureConfig.getFullUpdateConfig());
            unregisterBeforeAndAfterUpdateActions(featureConfig.getIncrementalUpdateConfig());
            logger.info("Removed config for feature: " + featureConfig.getId());
        } else {
            logger.warn("Config for feature has already been removed: " + featureConfig.getId());
        }
    }

    private void registerBeforeAndAfterUpdateActions(TaskConfig taskConfig) {
        if (taskConfig != null && taskConfig.getBeforeStartFlow() != null) {
            // engine.registerDataFlow(taskConfig.getBeforeStartFlow());
        }
        if (taskConfig != null && taskConfig.getAfterCompleteFlow() != null) {
            // engine.registerDataFlow(taskConfig.getAfterCompleteFlow());
        }
    }

    private void unregisterBeforeAndAfterUpdateActions(TaskConfig taskConfig) {
        if (taskConfig != null && taskConfig.getBeforeStartFlow() != null) {
            // engine.unregisterDataFlow(taskConfig.getBeforeStartFlow().getId());
        }
        if (taskConfig != null && taskConfig.getAfterCompleteFlow() != null) {
            // engine.unregisterDataFlow(taskConfig.getAfterCompleteFlow().getId());
        }
    }

    public Optional<FeatureConfig> getFeatureConfig(String featureId, String variant) {
        return Optional.ofNullable(featureConfigs.get(getFeatureKey(featureId, variant)));
    }

    public List<FeatureConfig> getAllFeatureConfigs() {
        return new ArrayList<>(featureConfigs.values());
    }

    public FeatureSpecificKeysUpdateTask updateRecordsWithKeys(
        String featureId, String variant, Map<String, List<List<Object>>> inputIdKeysMap) {
        return updateRecordsWithKeys(featureId, variant, inputIdKeysMap, new HashMap<>());
    }

    public FeatureSpecificKeysUpdateTask updateRecordsWithKeys(
            String featureId, String variant, Map<String, List<List<Object>>> inputIdKeysMap,
            Map<String, Object> configProperties) {
        FeatureSpecificKeysUpdateTask updateTask = new FeatureSpecificKeysUpdateTask(
            featureId, variant, inputIdKeysMap, false, 0, this,
            defaultTaskConfig, configProperties, metricNamePrefix, meterRegistry);
        updateTask.start();
        return updateTask;
    }

    public FeatureIncrementalUpdateTask incrementalUpdate(String featureId, String variant) {
        return incrementalUpdate(featureId, variant, new HashMap<>());
    }

    public FeatureIncrementalUpdateTask incrementalUpdate(String featureId, String variant,
            Map<String, Object> configProperties) {
        String featureKey = getFeatureKey(featureId, variant);
        ScheduledTaskRegistration<FeatureIncrementalUpdateTask> registration =
            incrementalUpdateTaskRegistrationMap.computeIfAbsent(featureKey,
                k -> new ScheduledTaskRegistration<>());
        if (registration.getTask() != null && registration.getTask().isRunning()) {
            return registration.getTask();
        }

        FeatureIncrementalUpdateTask updateTask = new FeatureIncrementalUpdateTask(featureId, variant,
             false, 0, this, defaultIncrementalUpdateTaskConfig,
            configProperties, metricNamePrefix, meterRegistry);
        registration.setTask(updateTask);
        updateTask.start();
        return updateTask;
    }

    public FeatureFullUpdateTask fullUpdate(String featureId, String variant) {
        return fullUpdate(featureId, variant, new HashMap<>(), false, 0);
    }

    public synchronized FeatureFullUpdateTask fullUpdate(String featureId, String variant,
            boolean dryRun, int dryRunRecordCount) {
        return fullUpdate(featureId, variant, new HashMap<>(), dryRun, dryRunRecordCount);
    }

    public synchronized FeatureFullUpdateTask fullUpdate(String featureId, String variant,
            Map<String, Object> configProperties, boolean dryRun, int dryRunRecordCount) {
        String featureKey = getFeatureKey(featureId, variant);
        ScheduledTaskRegistration<FeatureFullUpdateTask> registration = fullUpdateTaskRegistrationMap
            .computeIfAbsent(featureKey, k -> new ScheduledTaskRegistration<>());
        if (registration.getTask() != null && registration.getTask().isRunning()) {
            logger.info("There is already a full update in progress for feature {}:{}",
                featureId, variant);
            return registration.getTask();
        }
        FeatureFullUpdateTask updateTask = new FeatureFullUpdateTask(featureId, variant,
                true, dryRun, dryRunRecordCount, this,
            defaultFullUpdateTaskConfig, configProperties, metricNamePrefix, meterRegistry);
        updateTask.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        if (!updateTask.getFutureResult().isDone() && !updateTask.isRunning()) {
            throw new RuntimeException("The update task failed to start");
        }
        registration.setTask(updateTask);
        return updateTask;
    }

    public List<FeatureMetadata> cleanup(String featureId, String variant) throws IOException {
        return featureDBManager.cleanup(featureId, variant);
    }

    @Override
    public void close() {
        logger.info("Removing all registered feature configurations");
        for (FeatureConfig featureConfig : featureConfigs.values()) {
            removeFeatureConfig(featureConfig);
        }
    }

    private synchronized void scheduleUpdateTasks(FeatureConfig featureConfig) {
        String featureKey = getFeatureKey(featureConfig);

        unregisterUpdateTasks(featureConfig);

        FullUpdateConfig fullUpdateConfig = featureConfig.getFullUpdateConfig();
        if (fullUpdateConfig != null) {
            ScheduledTaskRegistration<FeatureFullUpdateTask> registration =
                fullUpdateTaskRegistrationMap.computeIfAbsent(featureKey,
                    k -> new ScheduledTaskRegistration<>());
            registration.setScheduledFuture(schedule(featureConfig, "full",
                    fullUpdateConfig, () -> {
                logger.info("Starting scheduled full update of feature {}:{}",
                    featureConfig.getId(), featureConfig.getVariant());
                fullUpdate(featureConfig.getId(), featureConfig.getVariant());
            }));
        }

        IncrementalUpdateConfig incrementalUpdateConfig = featureConfig
            .getIncrementalUpdateConfig();
        if (incrementalUpdateConfig != null) {
            ScheduledTaskRegistration<FeatureIncrementalUpdateTask> registration =
                incrementalUpdateTaskRegistrationMap.computeIfAbsent(featureKey,
                    k -> new ScheduledTaskRegistration<>());
            registration.setScheduledFuture(schedule(featureConfig, "incremental",
                    incrementalUpdateConfig, () -> {
                logger.info("Starting scheduled incremental update of feature {}:{}",
                    featureConfig.getId(), featureConfig.getVariant());
                incrementalUpdate(featureConfig.getId(), featureConfig.getVariant());
            }));
        }
    }

    private synchronized void unregisterUpdateTasks(FeatureConfig featureConfig) {
        String featureKey = getFeatureKey(featureConfig);
        unregisterUpdateTask(fullUpdateTaskRegistrationMap.remove(featureKey));
        unregisterUpdateTask(incrementalUpdateTaskRegistrationMap.remove(featureKey));
    }

    private synchronized void unregisterUpdateTask(ScheduledTaskRegistration<?> registration) {
        if (registration != null) {
            if (registration.getTask() != null) {
                registration.getTask().setCanceled(true);
                registration.setTask(null);
            }
            if (registration.getScheduledFuture() != null) {
                registration.getScheduledFuture().cancel(true);
                registration.setScheduledFuture(null);
            }
        }
    }

    private ScheduledFuture<?> schedule(FeatureConfig featureConfig, String updateType,
        ScheduledTaskConfig taskConfig, Runnable runnable) {
        if (taskConfig.getFrequency() != null) {
            long frequencyMillis = taskConfig.getFrequency().toMillis();
            Instant startTime = Instant.now().plusMillis(frequencyMillis);
            logger.info("Scheduling {} update for feature {}:{} every {} hours. Next execution time is {}",
                updateType, featureConfig.getId(), featureConfig.getVariant(), taskConfig.getFrequency().toHours(),
                DATE_FORMAT.format(Date.from(startTime)));
            return scheduler.scheduleAtFixedRate(runnable, startTime, taskConfig.getFrequency());
        } else if (taskConfig.getScheduleCron() != null) {
            Date startTime = new CronSequenceGenerator(taskConfig.getScheduleCron()).next(new Date());
            logger.info("Scheduling {} update for feature {}:{} using cron {}. Next execution time is {}",
                updateType, featureConfig.getId(), featureConfig.getVariant(), taskConfig.getScheduleCron(),
                DATE_FORMAT.format(startTime));
            return scheduler.schedule(runnable, new CronTrigger(taskConfig.getScheduleCron()));
        }
        return null;
    }

    private String getFeatureKey(String id, String variant) {
        if (variant == null) {
            variant = DEFAULT_VARIANT;
        }
        return String.format("%s_%s", id, variant);
    }

    private String getFeatureKey(FeatureConfig featureConfig) {
        return getFeatureKey(featureConfig.getId(), featureConfig.getVariant());
    }
}
