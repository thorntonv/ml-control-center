package mlcc.pipeline.example;

import static dataflow.core.datasource.DataSourceUtil.getClassPathDataSourcesMatchingGlobPattern;

import com.google.common.collect.ImmutableList;
import dataflow.avro.AvroDataRecordSerializationScheme;
import dataflow.batch.config.ScheduledTaskConfig;
import dataflow.core.datasource.LocalFileDataSource;
import dataflow.core.engine.DataFlowEngine;
import dataflow.core.engine.DataFlowEventListener;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.sql.SQLDataFlowExtension;
import dataflow.sql.SQLDatabaseManager;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.sql.DataSource;
import mlcc.feature.FeatureManager;
import mlcc.feature.api.FeatureDBManager;
import mlcc.feature.core.FeatureDBManagerImpl;
import mlcc.feature.core.FeatureDataFlowExtension;
import mlcc.feature.core.FeatureDatabase;
import mlcc.feature.core.FeatureMetadataManager;
import mlcc.feature.core.dataflow.DataSourceFeatureDatabase;
import mlcc.feature.core.dataflow.DataSourceFeatureDatabase.DefaultNameProvider;
import mlcc.feature.core.sql.SQLDatabaseMetadataManager;
import mlcc.feature.parser.FullUpdateConfig;
import mlcc.feature.parser.IncrementalUpdateConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeatureManagerConfig {

    private static final String SPRING_BOOT_FEATURE_CONFIG_PATH = "/features/*/*.yaml";

    @Autowired
    private DataFlowEngine dataFlowEngine;

    @Bean
    public FeatureManager featureManager() {
        registerDataFlowExtensions();
        Set<String> failedConfigs = Collections.synchronizedSet(new HashSet<>());
        dataFlowEngine.getEventManager().addListener(new DataFlowEventListener() {
            @Override
            public void onDataFlowLoadError(String dataFlowId, Throwable error) {
                failedConfigs.add(dataFlowId);
            }
        });
        FeatureManager featureManager = new FeatureManager(featureDBManager(), dataFlowEngine,
            new FullUpdateConfig(), new IncrementalUpdateConfig(), new ScheduledTaskConfig(),
            "pipelineExampleMetrics", new SimpleMeterRegistry());
        featureManager.registerConfigProvider(() ->
            getClassPathDataSourcesMatchingGlobPattern(SPRING_BOOT_FEATURE_CONFIG_PATH),
            Integer.MAX_VALUE);

        if(!failedConfigs.isEmpty()) {
            throw new DataFlowConfigurationException("Failed to load the following dataflow configs: " + failedConfigs);
        }

        if(featureManager.getAllFeatureConfigs().isEmpty()) {
            throw new DataFlowConfigurationException("No pipeline configurations found");
        }

        return featureManager;
    }

    @Bean
    public FeatureDBManager featureDBManager() {
        return new FeatureDBManagerImpl(featureMetadataManager(), featureDBS());
    }

    @Autowired
    private DataSource dataSource;

    private static final String EXAMPLE_SCHEMA = "{\"type\":\"record\",\"namespace\":\"mlcc-example\",\"name\":\"example\",\"fields\":[{\"name\":\"id\",\"type\":\"int\"},{\"name\":\"name\",\"type\":\"string\"}]}";

    @Bean
    public List<FeatureDatabase> featureDBS() {
        // Just write the feature data to the local file system
        return ImmutableList.of(
            new DataSourceFeatureDatabase("example", "example", new LocalFileDataSource(""),
                new AvroDataRecordSerializationScheme(EXAMPLE_SCHEMA), 10000, new DefaultNameProvider())
            );
    }

    @Bean
    public FeatureMetadataManager featureMetadataManager() {
        // Use the embedded h2 database for feature metadata
        return new SQLDatabaseMetadataManager(dataSource);
    }

    @Bean
    public SQLDatabaseManager sqlDatabaseManager() {
        // Make the embedded h2 database available to the pipeline YAML using clusterId=h2
        SQLDatabaseManager dbManager = new SQLDatabaseManager();
        dbManager.registerDataSource("h2", dataSource);
        return dbManager;
    }


    private void registerDataFlowExtensions() {
        SQLDataFlowExtension.register(dataFlowEngine);
    }
}
