package mlcc.pipeline.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(PipelineExampleConfig.class)
public class PipelineExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PipelineExampleApplication.class, args);
	}
}
