package mlcc.pipeline.example;

import mlcc.spring.MLControlCenterConfig;
import org.springframework.context.annotation.Import;

@Import({MLControlCenterConfig.class, FeatureManagerConfig.class})
public class PipelineExampleConfig {
}
