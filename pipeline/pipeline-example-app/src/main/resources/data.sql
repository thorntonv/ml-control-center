CREATE TABLE inputData (id int, name varchar(255));
INSERT INTO inputData (id, name) VALUES (1, 'USA');
INSERT INTO inputData (id, name) VALUES (2, 'France');
INSERT INTO inputData (id, name) VALUES (3, 'Brazil');
INSERT INTO inputData (id, name) VALUES (4, 'Italy');
INSERT INTO inputData (id, name) VALUES (5, 'Canada');