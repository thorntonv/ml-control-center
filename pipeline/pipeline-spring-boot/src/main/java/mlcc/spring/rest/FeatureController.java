/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureController.java
 */

package mlcc.spring.rest;

import com.google.common.base.Joiner;
import dataflow.core.util.DotFileGenerator;
import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import mlcc.feature.FeatureManager;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.parser.FeatureConfig;
import mlcc.feature.update.AbstractFeatureUpdateTask;
import mlcc.feature.update.FeatureSpecificKeysUpdateTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Timed
@Api(tags = "mlccFeatures")
@RequestMapping("/v1/mlcc/feature")
public class FeatureController {

    private static final Logger logger = LoggerFactory.getLogger(FeatureController.class);

    private final FeatureManager featureManager;

    @Autowired
    public FeatureController(FeatureManager featureManager) {
        this.featureManager = featureManager;
    }

    @RequestMapping(path = "/fullUpdate", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "Perform a full update of a feature", nickname = "fullUpdate")
    public String fullUpdate(@RequestParam String featureId, @RequestParam String variant,
        @RequestParam(required = false) Boolean dryRun,
        @RequestParam(required = false) Integer dryRunRecordCount) {
        dryRun = dryRun != null ? dryRun : false;
        dryRunRecordCount = dryRunRecordCount != null ?
            dryRunRecordCount : AbstractFeatureUpdateTask.DEFAULT_DRY_RUN_RECORD_COUNT;
        return featureManager.fullUpdate(featureId, variant, dryRun, dryRunRecordCount).getId();
    }

    @RequestMapping(path = "/incrementalUpdate", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "Perform an incremental update of a feature", nickname = "incrementalUpdate")
    public String incrementalUpdate(@RequestParam String featureId, @RequestParam String variant) {
        return featureManager.incrementalUpdate(featureId, variant).getId();
    }

    @RequestMapping(path = "/updateIds", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "Perform an update of the given ids of a feature", nickname = "specificIdsUpdate")
    public String specificIdsUpdate(@RequestParam String featureId, @RequestParam String variant,
        @RequestBody Map<String, List<List<Object>>> inputIdKeysMap) throws Exception {
        logger.info("Updating feature {}:{} keys: {}", featureId, variant, inputIdKeysMap);
        FeatureSpecificKeysUpdateTask task = featureManager
            .updateRecordsWithKeys(featureId, variant, inputIdKeysMap);
        task.getFutureResult().get();
        return "SUCCESS";
    }

    @RequestMapping(path = "/tasks", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get a list of tasks", nickname = "tasks")
    public List<TaskInfo> getTasks(@RequestParam(required = false) Boolean runningOnly) {
        return featureManager.getTasks().stream().map(task -> {
            TaskInfo taskInfo = new TaskInfo();
            taskInfo.setId(task.getId());
            taskInfo.setType(task.getUpdateType());
            taskInfo.setStartTime(task.getStartTime());
            taskInfo.setRunning(task.isRunning());
            return taskInfo;
        })
        .filter(task -> runningOnly == null || !runningOnly || task.isRunning())
        .sorted(Comparator.comparing(TaskInfo::getStartTime).reversed())
        .collect(Collectors.toList());
    }

    @RequestMapping(path = "/cleanup", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Cleanup old versions", nickname = "cleanup")
    public String cleanup(@RequestParam String featureId, @RequestParam String variant)
            throws IOException {
        return Joiner.on(", ").join(featureManager.cleanup(featureId, variant).stream()
            .map(FeatureMetadata::getVersion).collect(Collectors.toList()));
    }

    @RequestMapping(path = "/dot", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Generate a dot file", nickname = "dotFile")
    public String dotFile(@RequestParam String featureId, @RequestParam String variant) {
        Optional<FeatureConfig> featureConfig = featureManager.getFeatureConfig(featureId, variant);
        if (featureConfig.isPresent()) {
            return DotFileGenerator.generateDOTFile(featureConfig.get().getPipeline());
        } else {
            throw new IllegalArgumentException(
                "Feature " + featureId + ":" + variant + " does not exist");
        }
    }
}
