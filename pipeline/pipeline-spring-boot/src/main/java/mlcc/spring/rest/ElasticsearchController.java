/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ElasticsearchController.java
 */

package mlcc.spring.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dataflow.elasticsearch.IndexClusterManager;
import dataflow.elasticsearch.IndexManager;
import dataflow.elasticsearch.IndexMetadata;
import dataflow.elasticsearch.IndexerException;
import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Timed
@Api(tags = "mlccElasticsearch")
@RequestMapping("/v1/mlcc/elasticsearch")
public class ElasticsearchController {

    private static final Logger logger = LoggerFactory.getLogger(ElasticsearchController.class);

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final IndexClusterManager clusterManager;

    @Autowired
    public ElasticsearchController(IndexClusterManager clusterManager) {
        this.clusterManager = clusterManager;
    }

    @RequestMapping(path = "/metadata", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Make an index active", nickname = "makeIndexActive")
    public String getIndexMetadata(@RequestParam String clusterId) throws IOException {
        IndexManager indexManager = clusterManager.getIndexManager(clusterId);
        return indexManager.getIndexMetadata().stream().map(md -> {
            try {
                return objectMapper.writeValueAsString(md);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.joining("\n"));
    }

    @RequestMapping(path = "/makeIndexActive", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Make an index active", nickname = "makeIndexActive")
    public String makeIndexActive(@RequestParam String clusterId,
            @RequestParam String indexName) throws IOException, IndexerException {
        logger.warn("Manual set active index triggered for {} in cluster {}", indexName, clusterId);
        IndexManager indexManager = clusterManager.getIndexManager(clusterId);
        indexManager.makeActiveIndex(getIndexMetadata(indexName, indexManager));
        return "SUCCESS";
    }

    @RequestMapping(path = "/aliases", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "List aliases", nickname = "aliases")
    public Map<String, String> listAliases(@RequestParam String clusterId)
            throws IOException, IndexerException {
        IndexManager indexManager = clusterManager.getIndexManager(clusterId);
        Set<String> aliases = indexManager.getIndexMetadata().stream()
            .map(md -> indexManager.getAliasName(md.getType(), md.getSchemaVersion()))
            .collect(Collectors.toSet());
        Map<String, String> aliasIndexNameMap = new HashMap<>();
        for(String alias : aliases) {
            indexManager.getIndexMetadataForAlias(alias).ifPresent(metadata ->
                aliasIndexNameMap.put(alias, metadata.getName()));
        }
        return aliasIndexNameMap;
    }

    @RequestMapping(path = "/dropIndex", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Drop index", nickname = "dropIndex")
    public String dropIndex(@RequestParam String clusterId, @RequestParam String indexName,
            @RequestParam Boolean confirmation) throws Exception {
        if(confirmation == null || !confirmation) {
            throw new IllegalArgumentException("The drop operation must be confirmed");
        }

        logger.warn("Manual drop index triggered for {} in cluster {}", indexName, clusterId);
        IndexManager indexManager = clusterManager.getIndexManager(clusterId);
        indexManager.removeIndex(getIndexMetadata(indexName, indexManager));
        return "SUCCESS";
    }

    @RequestMapping(path = "/cleanup", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "Cleanup old indexes", nickname = "cleanup")
    public String cleanup(@RequestParam String clusterId,
            @RequestParam(required = false) String indexType,
            @RequestParam(required = false) Integer numPriorSchemaVersionsToKeep,
            @RequestParam(required = false) Integer numActiveBackupsForCurrentVersion,
            @RequestParam(required = false) Integer minIndexAgeForCleanupHours,
            @RequestParam(required = false) Boolean cleanupFailedIndexes) throws Exception {
        logger.warn("Manual index clean up triggered for {} in cluster {}",
            indexType != null ? indexType : "all index types", clusterId);
        IndexManager indexManager = clusterManager.getIndexManager(clusterId);
        numPriorSchemaVersionsToKeep = numPriorSchemaVersionsToKeep != null ?
            numPriorSchemaVersionsToKeep : indexManager.getDefaultNumPriorSchemaVersionsToKeep();
        numActiveBackupsForCurrentVersion = numActiveBackupsForCurrentVersion != null ?
            numActiveBackupsForCurrentVersion : indexManager.getDefaultNumActiveBackupsForCurrentVersion();
        minIndexAgeForCleanupHours = minIndexAgeForCleanupHours != null ? minIndexAgeForCleanupHours :
            indexManager.getDefaultMinIndexAgeForCleanupHours();
        cleanupFailedIndexes = cleanupFailedIndexes != null ? cleanupFailedIndexes : false;

        Set<String> indexTypes;
        if (indexType != null) {
            indexTypes = Collections.singleton(indexType);
        } else {
            indexTypes = indexManager.getIndexMetadata().stream()
                .map(IndexMetadata::getType)
                .collect(Collectors.toSet());
        }
        List<IndexMetadata> dropped = new ArrayList<>();
        for (String type : indexTypes) {
            dropped.addAll(indexManager.cleanupOldIndexes(type,
                numPriorSchemaVersionsToKeep, numActiveBackupsForCurrentVersion,
                minIndexAgeForCleanupHours, cleanupFailedIndexes));
        }
        return dropped.stream().map(md -> md.getName() + "v" + md.getVersion()).collect(Collectors.joining(","));
    }

    private IndexMetadata getIndexMetadata(String indexName, IndexManager indexManager)
            throws IOException {
        IndexMetadata metadata = indexManager.getIndexMetadata().stream().filter(md ->
            md.getName().equalsIgnoreCase(indexName)).findAny().orElse(null);
        if (metadata == null) {
            throw new IllegalArgumentException("Index " + indexName +
                " does not exist in cluster " + indexManager.getClusterId());
        }
        return metadata;
    }
}