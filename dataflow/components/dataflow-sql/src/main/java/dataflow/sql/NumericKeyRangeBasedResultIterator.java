/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  NumericKeyRangeBasedResultIterator.java
 */

package dataflow.sql;

import static dataflow.sql.SQLDatabaseIterationMode.NUMERIC_KEY_RANGE_BASED;

import dataflow.data.DataRecords;
import dataflow.data.query.DataFilter;
import dataflow.data.query.DataQueryValueProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import dataflow.data.query.KeyRangeDataFilter;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * Used to implement {@link SQLDatabaseIterationMode#NUMERIC_KEY_RANGE_BASED} result iteration.
 */
public class NumericKeyRangeBasedResultIterator implements Iterator<DataRecords> {

    private final Map<String, Object> paramValues;
    private final List<DataFilter> filters;
    private final int batchSize;
    private final DataQueryValueProvider valueProvider;

    private final String maxKeyQuery;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    private Long maxKey;
    private Long fromKey;
    private DataRecords next;

    NumericKeyRangeBasedResultIterator(Map<String, Object> paramValues,
                                       List<DataFilter> filters, Long fromKey, int batchSize, String maxKeyQuery,
                                       NamedParameterJdbcTemplate jdbcTemplate, DataQueryValueProvider valueProvider) {
        this.paramValues = new HashMap<>(paramValues);
        this.filters = filters;
        this.batchSize = batchSize;
        this.valueProvider = valueProvider;
        this.fromKey = fromKey;
        this.jdbcTemplate = jdbcTemplate;
        this.maxKeyQuery = maxKeyQuery;
    }

    @Override
    public boolean hasNext() {
        if (next == null) {
            loadNext();
        }
        return next != null && !next.isEmpty();
    }

    @Override
    public DataRecords next() {
        DataRecords retVal = next;
        next = null;
        return retVal;
    }

    private void loadNext() {
        if(maxKey == null) {
            maxKey = jdbcTemplate.query(maxKeyQuery, rs -> rs.next() ? rs.getLong(1) : null);
            if(maxKey == null) {
                throw new RuntimeException("Unable to determine the max key for " +
                    NUMERIC_KEY_RANGE_BASED + " iteration");
            }
        }

        try {
            while(fromKey < maxKey && (next == null || next.size() < batchSize)) {
                long toKey = fromKey + batchSize;
                List<DataFilter> filtersWithKeyFilter = new ArrayList<>(filters);
                filtersWithKeyFilter.add(new KeyRangeDataFilter(fromKey, true, toKey, false));
                paramValues.put("fromKey", fromKey);
                paramValues.put("toKey", toKey);
                paramValues.put("batchSize", batchSize);

                DataRecords results = valueProvider.query(paramValues, filtersWithKeyFilter);
                if(next == null) {
                    next = results;
                } else {
                    next.addAll(results.getRecords());
                }
                fromKey = toKey;
            }
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
