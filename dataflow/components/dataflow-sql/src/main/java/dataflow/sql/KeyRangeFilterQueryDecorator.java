/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  KeyRangeFilterQueryDecorator.java
 */

package dataflow.sql;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KeyRangeFilterQueryDecorator implements SQLQueryDecorator {

    private static final String FROM_KEY_PARAM_NAME_PREFIX = "__FROM_KEY";
    private static final String TO_KEY_PARAM_NAME_PREFIX = "__TO_KEY";

    private Object fromKey;
    private boolean fromKeyInclusive;
    private Object toKey;
    private boolean toKeyInclusive;

    @DataFlowConfigurable
    public KeyRangeFilterQueryDecorator(
        @DataFlowConfigProperty(required = false) Object fromKey,
        @DataFlowConfigProperty(required = false) Boolean fromKeyInclusive,
        @DataFlowConfigProperty(required = false) Object toKey,
        @DataFlowConfigProperty(required = false) Boolean toKeyInclusive) {
        this.fromKey = fromKey;
        this.fromKeyInclusive = fromKeyInclusive != null ? fromKeyInclusive : false;
        this.toKey = toKey;
        this.toKeyInclusive = toKeyInclusive != null ? toKeyInclusive : false;
    }

    @Override
    public void apply(SQLQueryInfo queryInfo) {
        queryInfo.getWhereClauses().addAll(getWhereClauses(queryInfo));
        queryInfo.getParamValues().putAll(getParamValues(queryInfo));
    }

    public void setFromKey(Object fromKey, boolean inclusive) {
        this.fromKey = fromKey;
        this.fromKeyInclusive = inclusive;
    }

    public void setToKey(Object toKey, boolean inclusive) {
        this.toKey = toKey;
        this.toKeyInclusive = inclusive;
    }

    private List<String> getWhereClauses(SQLQueryInfo queryInfo) {
        List<String> keyColumns = queryInfo.getKeyColumns();
        List<String> whereClauses = new ArrayList<>();
        if (fromKey != null) {
            String comparisonOp = fromKeyInclusive ? ">=" : ">";
            whereClauses.add(buildKeyValueRangeWhereClause(
                keyColumns, comparisonOp, FROM_KEY_PARAM_NAME_PREFIX));
        }

        if (toKey != null) {
            String comparisonOp = toKeyInclusive ? "<=" : "<";
            whereClauses.add(buildKeyValueRangeWhereClause(
                keyColumns, comparisonOp, TO_KEY_PARAM_NAME_PREFIX));
        }

        return whereClauses;
    }

    private Map<String, Object> getParamValues(SQLQueryInfo queryInfo) {
        List<String> keyColumns = queryInfo.getKeyColumns();
        Map<String, Object> params = new HashMap<>();
        if (fromKey != null) {
            for (int keyColNum = 1; keyColNum <= keyColumns.size(); keyColNum++) {
                Object keyValue = SQLQueryValueProvider.getKeyColumnValue(
                    fromKey, keyColNum - 1);
                params.put(FROM_KEY_PARAM_NAME_PREFIX + "_" + keyColNum, keyValue);
            }
        }

        if (toKey != null) {
            for (int keyColNum = 1; keyColNum <= keyColumns.size(); keyColNum++) {
                Object keyValue = SQLQueryValueProvider.getKeyColumnValue(
                    toKey, keyColNum - 1);
                params.put(TO_KEY_PARAM_NAME_PREFIX + "_" + keyColNum, keyValue);
            }
        }
        return params;
    }

    private static String buildKeyValueRangeWhereClause(List<String> keyColumns,
        String comparison,
        String paramNamePrefix) {
        if (keyColumns == null || keyColumns.isEmpty()) {
            throw new DataFlowConfigurationException(
                "Key columns must be specified with fromKey or toKey parameters");
        }

        StringBuilder builder = new StringBuilder();
        builder.append("(");
        for (int keyColNum = 1; keyColNum <= keyColumns.size(); keyColNum++) {
            if (keyColNum > 1) {
                builder.append(" AND ");
            }
            builder.append(String.format("%s %s :%s_%s", keyColumns.get(keyColNum - 1), comparison,
                paramNamePrefix, keyColNum));
        }
        builder.append(")");
        return builder.toString();
    }
}
