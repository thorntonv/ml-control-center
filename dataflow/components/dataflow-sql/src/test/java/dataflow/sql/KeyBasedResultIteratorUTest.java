/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  KeyBasedResultIteratorUTest.java
 */

package dataflow.sql;

import dataflow.data.DataRecords;
import dataflow.data.query.DataFilter;
import dataflow.data.query.DataQueryValueProvider;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class KeyBasedResultIteratorUTest extends AbstractResultIteratorUTest {

    @Override
    protected Iterator<DataRecords> createIterator(Map<String, Object> paramValues,
        List<DataFilter> filters, int batchSize, DataQueryValueProvider valueProvider) {
        return new KeyBasedResultIterator(paramValues, filters, 0, batchSize, valueProvider);
    }
}