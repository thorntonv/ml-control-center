/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLQueryValueProviderITest.java
 */

package dataflow.sql;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.type.IntegerValue;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import dataflow.data.query.KeyRangeDataFilter;
import dataflow.data.query.LastUpdateTimeFilter;
import dataflow.data.query.MaxRowCountDataFilter;
import dataflow.data.query.MultiKeyDataFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * Integration tests for {@link SQLQueryValueProvider} that use an embedded H2 instance.
 */
@RunWith(MockitoJUnitRunner.class)
public class SQLQueryValueProviderITest {

    static final String USER_ID_COLUMN = "user_id";
    static final String NAME_COLUMN = "name";
    static final String POSTAL_CODE_COLUMN = "postalcode";
    static final String LAST_UPDATED_COLUMN = "lastUpdated";

    static final Set<String> ALL_COLUMN_NAMES = ImmutableSet.of(USER_ID_COLUMN, NAME_COLUMN,
        POSTAL_CODE_COLUMN, LAST_UPDATED_COLUMN);

    static final List<String> DEFAULT_KEY_COLUMN_NAMES = ImmutableList.of(USER_ID_COLUMN);

    static final String TEST_NAME = "John";
    static final Integer TEST_POSTAL_CODE = 94941;
    static final String TEST_NAME2 = "Jim";
    static final Integer TEST_POSTAL_CODE2 = 94111;

    static final DataRecord TEST_INPUT1 = new DataRecord(ImmutableMap.<String, Object>builder()
        .put(USER_ID_COLUMN, 1)
        .build());
    static final DataRecord TEST_INPUT2 = new DataRecord(ImmutableMap.<String, Object>builder()
        .put(USER_ID_COLUMN, 2)
        .build());
    static final DataRecord TEST_INPUT_NON_EXISTENT_USER = new DataRecord(ImmutableMap.<String, Object>builder()
        .put(USER_ID_COLUMN, 1001)
        .build());

    private EmbeddedDatabase db;

    @Mock
    private DataFlowInstance mockInstance;

    @Before
    public void setUp() {
        db = new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.H2)
            .addScript("schema.sql")
            .addScript("test-data.sql")
            .build();
        SQLDatabaseManager dbManager = new SQLDatabaseManager();
        dbManager.registerDataSource("test", db);

        DataFlowEnvironment env = new DataFlowEnvironment("test", new SimpleDependencyInjector(dbManager), null);
        DataFlowExecutionContext executionContext =
            DataFlowExecutionContext.createExecutionContext(env);
        executionContext.setInstance(mockInstance);
    }

    @After
    public void cleanUp() {
        db.shutdown();
        DataFlowExecutionContext.setCurrentExecutionContext(null);
    }

    @Test
    public void getValue_multiRow() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
                "SELECT * from users", null, SQLDatabaseOutputMode.MULTI_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(Collections.emptyMap());

        DataRecords result = (DataRecords) futureResult.get();
        assertEquals(15, result.size());
        assertEquals(1, result.getKeyColumnNames().size());
        assertEquals(USER_ID_COLUMN, result.getKeyColumnNames().get(0));
        assertEquals(4, result.getColumnNames().size());
        ALL_COLUMN_NAMES.forEach(colName -> assertTrue("Missing column " + colName, result.hasColumn(colName)));

        assertEquals(TEST_NAME, result.get(0).get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(0).get(POSTAL_CODE_COLUMN));
        assertEquals(TEST_NAME2, result.get(1).get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE2, result.get(1).get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getEstimatedRowCount() {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            "SELECT * from users", null, SQLDatabaseOutputMode.MULTI_ROW);
        Optional<Long> count = valueProvider.getEstimatedResultCount(Collections.emptyMap(),
            ImmutableList.of());
        assertEquals(15, count.get().intValue());
    }

    @Test
    public void getValue_multiRow_withMaxRowCount() throws InvalidDataException, IOException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            "SELECT * from users", null, SQLDatabaseOutputMode.MULTI_ROW);

        DataRecords result = valueProvider.query(Collections.emptyMap(), ImmutableList.of(
            new KeyRangeDataFilter(3, false, null, false),
            new MaxRowCountDataFilter(10)));
        assertEquals(10, result.size());
        // Verify that the results are returned by user id.
        Integer userId = 3;
        for(DataRecord record : result) {
            assertTrue((Integer) record.get(USER_ID_COLUMN) > userId);
            userId = (Integer) record.get(USER_ID_COLUMN);
        }
    }

    @Test
    public void testQueryForStream_multiRow_withMaxRowCount() throws InvalidDataException, IOException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            "SELECT * from users", null, SQLDatabaseOutputMode.MULTI_ROW);

        Stream<DataRecords> stream = valueProvider.queryForStream(Collections.emptyMap(), ImmutableList.of(
            new KeyRangeDataFilter(3, false, null, false),
            new MaxRowCountDataFilter(10)), 3);
        List<DataRecord> results = stream.flatMap(DataRecords::stream).collect(Collectors.toList());
        assertEquals(10, results.size());
        // Verify that the results are returned by user id.
        Integer userId = 3;
        for(DataRecord record : results) {
            assertTrue((Integer) record.get(USER_ID_COLUMN) > userId);
            userId = (Integer) record.get(USER_ID_COLUMN);
        }
    }


    @Test
    public void getValue_batch_full() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            "SELECT * from users", null, "user_id ASC", SQLDatabaseOutputMode.STREAM);

        CompletableFuture<Object> futureResult = valueProvider.getValue(Collections.emptyMap());
        Stream<DataRecords> result = (Stream<DataRecords>) futureResult.get();
        List<DataRecord> rows = result.flatMap((Function<DataRecords, Stream<DataRecord>>) DataRecords::stream)
                .collect(Collectors.toList());

        int fromKey = Integer.MIN_VALUE;
        assertEquals(15, rows.size());
        for(DataRecord row : rows) {
            int userId = (int) row.get(USER_ID_COLUMN);
            assertTrue(fromKey < userId);
            fromKey = userId;
        }
    }

    @Test
    public void getValue_specificIds() throws InvalidDataException, IOException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            "SELECT * from users", null, SQLDatabaseOutputMode.MULTI_ROW);

        DataRecords result = valueProvider.query(Collections.emptyMap(), ImmutableList.of(
            new MultiKeyDataFilter(ImmutableList.of(3, 5, 7, 9))));
        assertEquals(4, result.size());
        assertEquals("Heather", result.get(0).get(NAME_COLUMN));
        assertEquals("Michelle", result.get(1).get(NAME_COLUMN));
        assertEquals("Cynthia", result.get(2).get(NAME_COLUMN));
        assertEquals("Daniel", result.get(3).get(NAME_COLUMN));
    }

    @Test
    public void getValue_incremental() throws ParseException, InvalidDataException, IOException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            "SELECT * from users", null, SQLDatabaseOutputMode.MULTI_ROW);

        Date lastUpdateTime = new SimpleDateFormat("dd-MMM-yyyy").parse("01-jan-2015");
        DataRecords result = valueProvider.query(Collections.emptyMap(), ImmutableList.of(
            new LastUpdateTimeFilter(lastUpdateTime)));
        assertEquals(3, result.size());
        assertEquals("Jeremy", result.get(0).get(NAME_COLUMN));
        assertEquals("Harry", result.get(1).get(NAME_COLUMN));
        assertEquals("Jason", result.get(2).get(NAME_COLUMN));
    }

    @Test
    public void getValue_multiRow_noResult() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
                "SELECT * from users", "user_id = 1001", SQLDatabaseOutputMode.MULTI_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(Collections.emptyMap());
        DataRecords result = (DataRecords) futureResult.get();
        assertEquals(0, result.size());
    }

    @Test
    public void getValue_singleRow() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, SQLDatabaseOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT1);

        DataRecord result = (DataRecord) futureResult.get();
        assertEquals(TEST_NAME, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleRow_query() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
                "SELECT * FROM users", "postalcode = :postalcode", SQLDatabaseOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(ImmutableMap.<String, Object>builder()
                .put("postalcode", 94111)
                .build());

        DataRecord result = (DataRecord) futureResult.get();
        assertEquals(TEST_NAME2, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE2, result.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_missingKeyColumn() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            "SELECT name,postalcode FROM users", null, SQLDatabaseOutputMode.MULTI_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(Collections.emptyMap());

        try {
            futureResult.get();
            fail("expected exception was not thrown");
        } catch(ExecutionException ex) {
            // Expected.
            assertTrue(ex.getCause() instanceof InvalidDataException);
        }
    }

    @Test
    public void getValue_singleRow_concurrentRequests() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, SQLDatabaseOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult1 = valueProvider.getValue(TEST_INPUT1);
        CompletableFuture<Object> futureResult2 = valueProvider.getValue(TEST_INPUT2);

        DataRecord result1 = (DataRecord) futureResult1.get();
        assertEquals(TEST_NAME, result1.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result1.get(POSTAL_CODE_COLUMN));
        DataRecord result2 = (DataRecord) futureResult2.get();
        assertEquals(TEST_NAME2, result2.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE2, result2.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleRow_noResult() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, SQLDatabaseOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT_NON_EXISTENT_USER);
        assertNull(futureResult.get());
    }

    @Test
    public void getValue_singleRow_allColumns() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
                DEFAULT_KEY_COLUMN_NAMES, SQLDatabaseOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT1);
        DataRecord result = (DataRecord) futureResult.get();
        assertEquals(1, result.get(USER_ID_COLUMN));
        assertEquals(TEST_NAME, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleColumn() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            "SELECT postalcode FROM users", "user_id = :user_id",
            SQLDatabaseOutputMode.SINGLE_COLUMN);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT1);
        assertEquals(TEST_POSTAL_CODE, futureResult.get());
    }

    @Test
    public void getValue_singleColumn_noResult() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            "SELECT postalcode FROM users", "user_id = :user_id",
            SQLDatabaseOutputMode.SINGLE_COLUMN);
        CompletableFuture<Object> futureResult = valueProvider.getValue(TEST_INPUT_NON_EXISTENT_USER);
        assertNull(futureResult.get());
    }

    /**
     * Tests that {@link IntegerValue} is converted to integer using the .
     */
    @Test
    public void getValue_integerValueKeyValue() throws ExecutionException, InterruptedException {
        SQLQueryValueProvider valueProvider = buildValueProvider(
            DEFAULT_KEY_COLUMN_NAMES, SQLDatabaseOutputMode.SINGLE_ROW);
        CompletableFuture<Object> futureResult = valueProvider.getValue(ImmutableMap.<String, Object>builder()
                .put(USER_ID_COLUMN, new IntegerValue(1))
                .build());

        DataRecord result = (DataRecord) futureResult.get();
        assertEquals(TEST_NAME, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(POSTAL_CODE_COLUMN));
    }

    static SQLQueryValueProvider buildValueProvider(List<String> keyColumnNames,
        SQLDatabaseOutputMode outputMode) {
        return new SQLQueryValueProvider("SELECT * FROM users", null, "user_id = :user_id", keyColumnNames, null,"user_id ASC",
            LAST_UPDATED_COLUMN, null, outputMode, null, null, null, null, 1, null, null, null, null, null, null);
    }

    static SQLQueryValueProvider buildValueProvider(String baseQuery, String whereClause,
        SQLDatabaseOutputMode outputMode) {
        return new SQLQueryValueProvider(baseQuery, null, whereClause, DEFAULT_KEY_COLUMN_NAMES, null,null,
            LAST_UPDATED_COLUMN, null, outputMode, null, null, null, null, 1, null, null,null, null, null, null);
    }

    static SQLQueryValueProvider buildValueProvider(String baseQuery, String whereClause,
        String orderByClause, SQLDatabaseOutputMode outputMode) {
        return new SQLQueryValueProvider(baseQuery, null, whereClause, DEFAULT_KEY_COLUMN_NAMES, null, orderByClause,
            LAST_UPDATED_COLUMN, null, outputMode, null, null,null, null, 1, null, null,null, null, null, null);
    }
}
