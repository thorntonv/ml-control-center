/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RetryWithExponentialBackoff.java
 */

package dataflow.retry;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import io.github.resilience4j.core.IntervalFunction;
import io.github.resilience4j.retry.RetryConfig.Builder;
import java.util.List;

/**
 * A retry behavior with either fixed or randomized exponential backoff.
 */
public class RetryWithExponentialBackoff extends Retry {

    @DataFlowConfigurable
    public RetryWithExponentialBackoff(
        @DataFlowConfigProperty Integer maxAttempts,
        @DataFlowConfigProperty(required = false) Boolean failAfterMaxAttempts,
        @DataFlowConfigProperty Integer initialIntervalMillis,
        @DataFlowConfigProperty(required = false) Boolean randomizedInterval,
        @DataFlowConfigProperty(required = false) List<String> retryExceptions,
        @DataFlowConfigProperty(required = false) List<String> ignoreExceptions) {
        super(buildRetry(maxAttempts, failAfterMaxAttempts, initialIntervalMillis, randomizedInterval,
            retryExceptions, ignoreExceptions));
    }

    protected static io.github.resilience4j.retry.Retry buildRetry(Integer maxAttempts,
        Boolean failAfterMaxAttempts,
        Integer initialIntervalMillis,
        Boolean randomizedInterval, List<String> retryExceptions,
        List<String> ignoreExceptions) {
        Builder<?> retryBuilder = retryBuilder(maxAttempts, failAfterMaxAttempts, retryExceptions,
            ignoreExceptions);
        if (randomizedInterval == null || !randomizedInterval) {
            retryBuilder.intervalFunction(
                IntervalFunction.ofExponentialBackoff(initialIntervalMillis));
        } else {
            retryBuilder.intervalFunction(
                IntervalFunction.ofExponentialRandomBackoff(initialIntervalMillis));
        }
        return io.github.resilience4j.retry.Retry.of("DataFlowRetryWithExponentialBackoff",
            retryBuilder.build());
    }

    @Override
    public String toString() {
        return "RetryWithExponentialBackoff{} " + super.toString();
    }
}
