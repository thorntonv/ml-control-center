/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  PostgreSQLWriterComponent.java
 */

package dataflow.postgres;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.sql.SQLDatabaseManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DataFlowComponent(description = "Writes the value to a database.")
public class PostgreSQLWriterComponent implements AutoCloseable {

    private static final Logger logger = LoggerFactory.getLogger(PostgreSQLWriterComponent.class);
    private final PostgreSQLData data;

    @DataFlowConfigurable
    public PostgreSQLWriterComponent(
            @DataFlowConfigProperty(required = false, description =
                    "A string that identifies the cluster to use" +
                            "A cluster with the given id should be registered with the cluster manager") String clusterId,
            @DataFlowConfigProperty(description = "Name of destination table to be created") String tableName,
            @DataFlowConfigProperty(description = "Key columns in destination table") List<String> keyColumnNames,
            @DataFlowConfigProperty(description = "List of column definitions. A column definition is a map with key 'name' and 'type'") List<Map<String, String>> columns,
            @DataFlowConfigProperty(required = false, description = "Optional SQL statement to create table") String createSQL
    ) {
        DataFlowExecutionContext context = DataFlowExecutionContext.getCurrentExecutionContext();
        SQLDatabaseManager dbManager = context.getDependencyInjector().getInstance(SQLDatabaseManager.class);
        DataSource dataSource = (clusterId != null) ? dbManager.getDataSource(clusterId) : dbManager.getDefaultDataSource();

        List<String> columnNames = new ArrayList<>();
        List<ColumnType> columnTypes = new ArrayList<>();
        for (Map<String, String> columnDef : columns) {
            if (!columnDef.containsKey("name")) {
                throw new DataFlowConfigurationException("Column defined by " + columnDef + " did not have 'name' key.");
            }
            if (!columnDef.containsKey("type")) {
                throw new DataFlowConfigurationException("Column defined by " + columnDef + " did not have 'type' key.");
            }
            columnNames.add(columnDef.get("name"));
            columnTypes.add(ColumnType.forName(columnDef.get("type")));
        }

        PostgreSQLDatabase database = new PostgreSQLDatabase(dataSource, tableName, keyColumnNames,
                columnNames, columnTypes, createSQL);
        database.createTable();
        this.data = database.getData();
    }

    @OutputValue
    public dataflow.data.DataRecords getValue(@InputValue Object dataRecords) {
        dataflow.data.DataRecords dataRecordsProper = (dataflow.data.DataRecords) dataRecords;
        logger.debug("Writing record...");

        try {
            data.write(dataRecordsProper);
        } catch (IOException | dataflow.data.InvalidDataException e) {
            logger.warn("Error occurred when writing records.", e);
        }

        return dataRecordsProper;
    }

    @Override
    public void close() throws Exception {}
}
