/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  PostgreSQLDatabase.java
 */

package dataflow.postgres;

import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

public class PostgreSQLDatabase {

    public static final String DEFAULT_TYPE_ID = "sql";

    private static final Logger logger = LoggerFactory.getLogger(PostgreSQLDatabase.class);

    private final DataSource dataSource;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final String tableName;
    private final List<String> keyColumnNames;
    private final List<String> columnNames;
    private final List<ColumnType> columnTypes;
    private final String createSQL;
    private final ExecutorService executor = Executors.newCachedThreadPool();

    private static final Pattern NAME_PATTERN = Pattern.compile("[\\w-_]+");

    public PostgreSQLDatabase(DataSource dataSource, String tableName, List<String> keyColumnNames,
                              List<String> columnNames, List<ColumnType> columnTypes, String createSQL) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.tableName = tableName;
        this.keyColumnNames = keyColumnNames;
        this.columnNames = columnNames;
        this.columnTypes = columnTypes;
        this.createSQL = createSQL;

        if (!NAME_PATTERN.matcher(tableName).matches()) {
            throw new DataFlowConfigurationException("Invalid table name " + tableName);
        }
        if (columnNames == null || columnNames.isEmpty()) {
            throw new IllegalArgumentException("At least one column must be specified");
        }
        if (columnNames.size() != columnTypes.size()) {
            throw new IllegalArgumentException("Invalid column arguments. Column names and types lists must be the same length");
        }
    }

    public void createTable() {

        // Create a new table based on the schema.
        StringBuilder sql = new StringBuilder();
        if (createSQL == null) {
            sql.append(String.format("CREATE TABLE IF NOT EXISTS \"%s\" (", tableName));
            for (int idx = 0; idx < columnNames.size(); idx++) {
                String columnName = columnNames.get(idx);
                ColumnType columnType = columnTypes.get(idx);
                if (idx > 0) {
                    sql.append(", ");
                }
                sql.append(String.format("%s %s", columnName, mapToSQLType(columnType)));
            }
            sql.append(", lastUpdateTime TIMESTAMP");
            sql.append(", PRIMARY KEY (");
            StringUtil.join(sql, keyColumnNames, ", ");
            sql.append(")");
            sql.append(")");
        } else {
            String repCreateSQL = createSQL.replace("$(TABLE_NAME)",
                    String.format("\"%s\"", tableName)).replace("$(TABLE_PREFIX)", tableName);
            sql.append(repCreateSQL);
        }

        // Create or replace feature table.
        logger.debug("Creating table cmd: {}", sql);
        jdbcTemplate.getJdbcTemplate().execute(sql.toString());
    }

    public void dropTable() {
        jdbcTemplate.getJdbcTemplate().execute(String.format("DROP TABLE \"%s\"", tableName));
    }

    public PostgreSQLData getData() {
        return new PostgreSQLData(keyColumnNames, columnNames, columnTypes, tableName, jdbcTemplate, dataSource, executor);
    }

    private String mapToSQLType(ColumnType columnType) {
        switch (columnType) {
            case BOOLEAN:
                return "BOOLEAN";
            case INTEGER:
                return "INT";
            case LONG:
                return "BIGINT";
            case FLOAT:
                return "REAL";
            case DOUBLE:
                return "DOUBLE PRECISION";
            case STRING:
                return "TEXT";
            case DATE:
                return "DATE";
            case DATETIME:
                return "TIMESTAMP";
            case JSON:
                return "JSON";
        }
        throw new IllegalArgumentException("Invalid postgres column type " + columnType.name());
    }

    @Override
    public String toString() {
        return "PostgreSQLDatabase{" +
                "tableName='" + tableName + '\'' +
                ", keyColumnNames=" + keyColumnNames +
                ", columnNames=" + columnNames +
                ", columnTypes=" + columnTypes +
                ", createSQL='" + createSQL + '\'' +
                '}';
    }
}
