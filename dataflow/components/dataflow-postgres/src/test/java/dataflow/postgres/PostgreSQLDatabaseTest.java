/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  PostgreSQLDatabaseTest.java
 */

package dataflow.postgres;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import dataflow.data.query.KeyRangeDataFilter;
import dataflow.data.query.MaxRowCountDataFilter;
import dataflow.data.query.MultiKeyDataFilter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class PostgreSQLDatabaseTest {

    private static final String TEST_TABLE_NAME_1 = "testTableName-1";
    private static final String TEST_TABLE_NAME_2 = "testTableName-2";
    private static final String TEST_INVALID_TABLE_NAME = "testTableName@1";

    private static final List<String> TEST_KEY_COLUMNS_1 = ImmutableList.of("id");

    private static final List<String> TEST_COLUMNS_1 = ImmutableList.of("id", "name", "postalCode");
    private static final List<ColumnType> TEST_COLUMN_TYPES_1 =
            ImmutableList.of(ColumnType.INTEGER, ColumnType.STRING, ColumnType.INTEGER);

    private static final List<String> TEST_COLUMNS_2 = ImmutableList.of("id", "userInfo");
    private static final List<ColumnType> TEST_COLUMN_TYPES_2 =
            ImmutableList.of(ColumnType.INTEGER, ColumnType.JSON);

    private static final String TEST_TABLE_COLUMNS_1 =
            "[{column_name=id, data_type=integer}, {column_name=lastupdatetime, data_type=timestamp without time zone}, {column_name=name, data_type=text}, {column_name=postalcode, data_type=integer}]";

    private JdbcTemplate jdbcTemplate;

    @Mock
    private DataFlowInstance mockInstance;

    @Rule
    public SingleInstancePostgresRule pg = EmbeddedPostgresRules.singleInstance();
    private DataSource dataSource;

    @Before
    public void setUp() {
        this.dataSource = pg.getEmbeddedPostgres().getPostgresDatabase();
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        DataFlowExecutionContext executionContext = DataFlowExecutionContext.createExecutionContext(
                new DataFlowEnvironment("test", new SimpleDependencyInjector(), null));
        executionContext.setInstance(mockInstance);
    }

    @Test
    public void testCreateTable() throws IOException {

        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);
        postgresDB.createTable();
        assertEquals(TEST_TABLE_COLUMNS_1, getTableColumns(TEST_TABLE_NAME_1).toString());
    }

    @Test
    public void testCreateTable_invalidTableName() {
        try {
            PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_INVALID_TABLE_NAME, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);
            postgresDB.createTable();
            fail("expected exception was not thrown");
        } catch (Exception ex) {
            // Expected
            assertEquals("Invalid table name testTableName@1", ex.getMessage());
        }
    }

    @Test
    public void testCreateTable_multipleCreates() throws IOException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);
        postgresDB.createTable();
        postgresDB.createTable();
        assertEquals(TEST_TABLE_COLUMNS_1, getTableColumns(TEST_TABLE_NAME_1).toString());
    }

    @Test
    public void testDropTable() throws IOException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);

        postgresDB.createTable();
        assertEquals(TEST_TABLE_COLUMNS_1, getTableColumns(TEST_TABLE_NAME_1).toString());
        postgresDB.dropTable();
        assertTrue(getTableColumns(TEST_TABLE_NAME_1).isEmpty());
    }

    @Test
    public void testWriteRead_singleRow() throws IOException, InvalidDataException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);

        postgresDB.createTable();
        PostgreSQLData data = postgresDB.getData();
        DataRecord record = new DataRecord();
        record.put("id", 1);
        record.put("name", "Robert");
        record.put("postalCode", 94939);
        data.write(record);

        DataRecord readRecord = data.read(DataKey.create(1));
        assertEquals(record, readRecord);
    }

    @Test
    public void testWriteRead_singleRow_withNullColumn() throws IOException, InvalidDataException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);

        postgresDB.createTable();
        PostgreSQLData data = postgresDB.getData();
        DataRecord record = new DataRecord();
        record.put("id", 1);
        record.put("name", "Robert");
        record.put("postalCode", null);
        data.write(record);

        DataRecord readRecord = data.read(DataKey.create(1));
        assertEquals(record, readRecord);
    }

    @Test
    public void testWriteRead_singleRowWithJsonColumn() throws IOException, InvalidDataException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_2, TEST_KEY_COLUMNS_1, TEST_COLUMNS_2, TEST_COLUMN_TYPES_2, null);

        postgresDB.createTable();
        PostgreSQLData data = postgresDB.getData();
        DataRecord record = new DataRecord();
        record.put("id", 1);

        Map<String, Object> user =
                ImmutableMap.<String, Object>builder().put("name", "Ryan").build();
        record.put("userInfo", user);

        data.write(record);

        DataRecord readRecord = data.read(DataKey.create(1));
        assertEquals(2, readRecord.size());
        assertEquals(1, readRecord.get("id"));
        assertEquals("{\"name\":\"Ryan\"}", readRecord.get("userInfo"));
    }

    @Test
    public void testWriteReadMultiKey() throws IOException, InvalidDataException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);

        postgresDB.createTable();
        PostgreSQLData data = postgresDB.getData();
        writeTestData(data);

        DataRecords records = data.read(ImmutableList.of(
                        new MultiKeyDataFilter(ImmutableList.of(DataKey.create(1), DataKey.create(3), DataKey.create(5)))),
                TEST_COLUMNS_1);
        assertEquals(3, records.size());
        assertEquals("Robert", records.get(0).get("name"));
        assertEquals("Harry", records.get(1).get("name"));
        assertEquals("Jeremy", records.get(2).get("name"));
    }

    @Test
    public void testWriteReadStreamMultiKey() throws IOException, InvalidDataException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);
        postgresDB.createTable();
        PostgreSQLData data = postgresDB.getData();
        writeTestData(data);

        Stream<DataRecords> stream = data.readStream(ImmutableList.of(
                        new MultiKeyDataFilter(ImmutableList.of(List.of(1), List.of(3), List.of(5)))),
                TEST_COLUMNS_1, 2);
        List<DataRecord> records = stream.flatMap(DataRecords::stream).collect(Collectors.toList());
        assertEquals(3, records.size());
        assertEquals("Robert", records.get(0).get("name"));
        assertEquals("Harry", records.get(1).get("name"));
        assertEquals("Jeremy", records.get(2).get("name"));
    }

    @Test
    public void testWriteReadKeyRange() throws IOException, InvalidDataException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);
        postgresDB.createTable();
        PostgreSQLData data = postgresDB.getData();
        writeTestData(data);

        DataRecords records = data.read(ImmutableList.of(
                        new KeyRangeDataFilter(DataKey.create(0), false, null, false),
                        new MaxRowCountDataFilter(4)),
                TEST_COLUMNS_1);
        assertEquals(4, records.size());
        assertEquals("Robert", records.get(0).get("name"));
        assertEquals("Cynthia", records.get(1).get("name"));
        assertEquals("Harry", records.get(2).get("name"));
        assertEquals("Denise", records.get(3).get("name"));
    }

    @Test
    public void testWrite_stringWithNullChar() throws IOException, InvalidDataException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);
        postgresDB.createTable();
        PostgreSQLData data = postgresDB.getData();
        DataRecord record = new DataRecord();
        record.put("id", 1);
        record.put("name", "Rob\0er\0t");
        record.put("postalCode", 94939);
        data.write(record);

        DataRecord readRecord = data.read(DataKey.create(1));
        assertEquals("Robert", readRecord.get("name"));
    }

    private void writeTestData(PostgreSQLData data) throws IOException, InvalidDataException {
        DataRecords records = new DataRecords();
        records.setKeyColumnNames(TEST_KEY_COLUMNS_1);
        records.setColumnNames(TEST_COLUMNS_1);
        DataRecord record = new DataRecord();
        record.put("id", 1);
        record.put("name", "Robert");
        record.put("postalCode", 94939);
        records.add(record);

        record = new DataRecord();
        record.put("id", 2);
        record.put("name", "Cynthia");
        record.put("postalCode", 70806);
        records.add(record);

        record = new DataRecord();
        record.put("id", 3);
        record.put("name", "Harry");
        record.put("postalCode", 85365);
        records.add(record);

        record = new DataRecord();
        record.put("id", 4);
        record.put("name", "Denise");
        record.put("postalCode", 19082);
        records.add(record);

        record = new DataRecord();
        record.put("id", 5);
        record.put("name", "Jeremy");
        record.put("postalCode", 17011);
        records.add(record);

        data.write(records);
    }

    @Test
    public void testRead_nonExistentRow() throws IOException {
        PostgreSQLDatabase postgresDB = new PostgreSQLDatabase(dataSource, TEST_TABLE_NAME_1, TEST_KEY_COLUMNS_1, TEST_COLUMNS_1, TEST_COLUMN_TYPES_1, null);
        postgresDB.createTable();
        PostgreSQLData data = postgresDB.getData();
        assertNull(data.read(DataKey.create(1)));
    }

    private List<Map<String, Object>> getTableColumns(String tableName) {
        return jdbcTemplate.queryForList(String.format(
                "SELECT column_name, data_type FROM information_schema.columns " +
                        "WHERE table_name = '%s' ORDER BY column_name;", tableName));
    }
}
