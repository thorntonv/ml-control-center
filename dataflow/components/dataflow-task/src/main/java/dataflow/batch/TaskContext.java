/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TaskContext.java
 */

package dataflow.batch;

import dataflow.batch.util.ExecutionTimeEstimator;
import dataflow.retry.Retry;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class TaskContext {

    private String taskId;
    private Date startTime;

    private final AtomicInteger retryCount = new AtomicInteger(0);
    private Retry retry;

    private boolean dryRun;
    private int dryRunMaxItemCount;

    private AtomicLong inputItemCount = new AtomicLong(0);
    private AtomicLong completedItemCount = new AtomicLong(0);
    private AtomicLong errorItemCount = new AtomicLong(0);
    private AtomicLong estimatedItemCount = new AtomicLong(-1);
    private ExecutionTimeEstimator timeEstimator = new ExecutionTimeEstimator();
    private Map<String, Object> configProperties = new HashMap<>();

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Retry getRetry() {
        return retry;
    }

    public void setRetry(Retry retry) {
        this.retry = retry;
    }

    public boolean isDryRun() {
        return dryRun;
    }

    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }


    public AtomicLong getInputItemCount() {
        return inputItemCount;
    }

    public void setInputItemCount(AtomicLong inputItemCount) {
        this.inputItemCount = inputItemCount;
    }

    public AtomicLong getCompletedItemCount() {
        return completedItemCount;
    }

    public void setCompletedItemCount(AtomicLong completedItemCount) {
        this.completedItemCount = completedItemCount;
    }

    public AtomicLong getErrorItemCount() {
        return errorItemCount;
    }

    public void setErrorItemCount(AtomicLong errorItemCount) {
        this.errorItemCount = errorItemCount;
    }

    public int getDryRunMaxItemCount() {
        return dryRunMaxItemCount;
    }

    public void setDryRunMaxItemCount(int dryRunMaxItemCount) {
        this.dryRunMaxItemCount = dryRunMaxItemCount;
    }

    public ExecutionTimeEstimator getTimeEstimator() {
        return timeEstimator;
    }

    public void setTimeEstimator(ExecutionTimeEstimator timeEstimator) {
        this.timeEstimator = timeEstimator;
    }

    public AtomicLong getEstimatedItemCount() {
        return estimatedItemCount;
    }

    public void setEstimatedItemCount(AtomicLong estimatedItemCount) {
        this.estimatedItemCount = estimatedItemCount;
    }

    public AtomicInteger getRetryCount() {
        return retryCount;
    }

    public void setConfigProperties(Map<String, Object> configProperties) {
        this.configProperties = new HashMap<>(configProperties);
    }

    public Map<String, Object> getConfigProperties() {
        return configProperties;
    }
}
