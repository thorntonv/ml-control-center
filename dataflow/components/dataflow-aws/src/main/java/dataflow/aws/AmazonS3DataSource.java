/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AmazonS3DataSource.java
 */

package dataflow.aws;

import static dataflow.core.datasource.DataSourceUtil.appendPath;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.datasource.DataSource;
import dataflow.core.datasource.DataSourceUtil;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.exception.DataFlowConfigurationException;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A {@link DataSource} implementation that uses Amazon S3.
 */
public class AmazonS3DataSource implements DataSource {

    private static final Logger logger = LoggerFactory.getLogger(AmazonS3DataSource.class);

    private final String bucket;
    private String key;
    private final String path;
    private final String delimiter;
    private final AmazonS3 s3Client;
    private long lastModificationTimeMillis;
    private final Duration ttl;

    @DataFlowConfigurable
    public AmazonS3DataSource(@DataFlowConfigProperty String bucket,
            @DataFlowConfigProperty(name = "path") String path,
            @DataFlowConfigProperty(required = false) String delimiter,
            @DataFlowConfigProperty(required = false) String ttl) {
        this(bucket, path, delimiter, ttl != null ? Duration.parse(ttl) : null,
            getClientForBucket(bucket));
    }

    private static AmazonS3 getClientForBucket(String bucket) {
        S3ClientManager clientManager = DataFlowExecutionContext.getCurrentExecutionContext()
            .getDependencyInjector().getInstance(S3ClientManager.class);
        AmazonS3 client = clientManager.getClientForBucket(bucket).orElse(null);
        if(client == null) {
            throw new DataFlowConfigurationException("No s3 client configured for bucket " + bucket);
        }
        return client;
    }

    public AmazonS3DataSource(String bucket, String path, String delimiter, Duration ttl, AmazonS3 s3Client) {
        this(bucket, path, delimiter, ttl, s3Client, 0);
    }

    private static AmazonS3 buildClient(String profile, String region) {
        AmazonS3ClientBuilder clientBuilder = AmazonS3ClientBuilder.standard();
        if (profile != null) {
            clientBuilder.setCredentials(new ProfileCredentialsProvider(profile));
        }
        if (region != null) {
            clientBuilder.setRegion(region);
        }
        return clientBuilder.build();
    }

    AmazonS3DataSource(String bucket, String path, String delimiter, Duration ttl,  AmazonS3 s3Client, long lastModificationTimeMillis) {
        this.bucket = bucket;
        delimiter = delimiter != null ? delimiter : DataSource.PATH_SEPARATOR;
        this.delimiter = delimiter;
        this.path = path;
        this.key = path != null ?
                DataSourceUtil.normalizePathSeparator(path, DataSource.PATH_SEPARATOR, this.delimiter) : "";
        if (key.startsWith(delimiter)) {
            // Remove leading delimiter.
            key = key.substring(delimiter.length());
        }
        this.s3Client = s3Client;
        this.ttl = ttl;
        this.lastModificationTimeMillis = lastModificationTimeMillis;
    }

    @Override
    public InputStream getInputStream() {
        return s3Client.getObject(bucket, key).getObjectContent();
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        // TODO(thorntonv): Rework this to stream from memory instead of writing to a file.
        final File tmpFile = File.createTempFile(bucket + "_" + key, ".out");
        tmpFile.deleteOnExit();
        return new FilterOutputStream(new BufferedOutputStream(new FileOutputStream(tmpFile))) {

            @Override
            public void write(int b) throws IOException {
                super.write(b);
            }

            @Override
            public void write(byte[] b) throws IOException {
                super.write(b);
            }

            @Override
            public void write(byte[] b, int off, int len) throws IOException {
                super.write(b, off, len);
            }

            @Override
            public void flush() throws IOException {
                super.flush();
            }

            @Override
            public void close() throws IOException {
                super.close();
                ObjectMetadata objectMetadata = new ObjectMetadata();
                if(ttl != null) {
                    objectMetadata.setExpirationTime(Date.from(Instant.now().plus(ttl)));
                }
                try {
                    logger.info("Writing " + tmpFile.length() + " bytes from " + tmpFile.getAbsolutePath() + " to " + bucket + ":" + key);
                    s3Client.putObject(new PutObjectRequest(bucket, key, tmpFile)
                        .withMetadata(objectMetadata));
                    logger.info("Finished writing to " + bucket + ":" + key);
                } catch(Throwable t) {
                    logger.warn("Error writing data to " + key + " in bucket " + bucket, t);
                    throw t;
                } finally {
                    if(!tmpFile.delete()) {
                        logger.warn("Unable to delete temp file " + tmpFile.getAbsolutePath());
                    }
                }
            }
        };
    }

    public void write(InputStream in) {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        s3Client.putObject(bucket, key, in, objectMetadata);
    }

    public Optional<DataSource> getChild(String name) {
        String childPath = appendPath(path, name);
        return Optional.of(new AmazonS3DataSource(bucket, childPath, delimiter, ttl, s3Client));
    }

    @Override
    public DataSource createChild(String name, boolean isDirectory) {
        return new AmazonS3DataSource(bucket, appendPath(path, name), delimiter, ttl, s3Client);
    }

    @Override
    public List<DataSource> getChildren() {
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                .withBucketName(bucket).withPrefix(key).withDelimiter(delimiter);
        List<DataSource> retVal = new ArrayList<>();
        ObjectListing listing = s3Client.listObjects(listObjectsRequest);

        listing.getCommonPrefixes().stream().map(
                childKey -> new AmazonS3DataSource(bucket, childKey, delimiter, ttl, s3Client))
                .forEach(retVal::add);

        List<S3ObjectSummary> summaries;
        boolean truncated;
        do {
            summaries = listing.getObjectSummaries();

            summaries.stream()
                    .map(childSummary -> new AmazonS3DataSource(bucket, childSummary.getKey(),
                        delimiter, ttl, s3Client, childSummary.getLastModified().getTime()))
                    .forEach(retVal::add);
            truncated = listing.isTruncated();
            if (truncated) {
                listing = s3Client.listNextBatchOfObjects(listing);
            }
        } while (truncated);
        return retVal;
    }

    @Override
    public DataSource getParent() {
        return new AmazonS3DataSource(bucket, DataSourceUtil.getParentPath(path), delimiter, ttl, s3Client);
    }

    public boolean exists() {
        try {
            return s3Client.doesObjectExist(bucket, key);
        } catch(Throwable t) {
            logger.warn("Error checking existence of " + key + " in bucket " + bucket, t);
            throw t;
        }
    }

    @Override
    public boolean hasChanged() {
        if (exists()) {
            ObjectMetadata metadata = s3Client.getObjectMetadata(bucket, key);
            if (metadata != null && metadata.getLastModified() != null) {
                long metadataLastModificationTimeMillis = metadata.getLastModified().getTime();
                if (lastModificationTimeMillis != metadataLastModificationTimeMillis) {
                    lastModificationTimeMillis = metadataLastModificationTimeMillis;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void delete() throws IOException {
        for(DataSource child : getChildren()) {
            child.delete();
        }
        s3Client.deleteObject(bucket, key);
    }

    @Override
    public String getPath() {
        if (path == null) {
            return path;
        }
        return path.startsWith(DataSource.PATH_SEPARATOR) ? path : DataSource.PATH_SEPARATOR + path;
    }

    @Override
    public List<DataSource> getRoots() {
        return Collections.singletonList(new AmazonS3DataSource(bucket, "", delimiter, ttl, s3Client));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final AmazonS3DataSource that = (AmazonS3DataSource) o;
        return Objects.equals(bucket, that.bucket) &&
                Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bucket, key);
    }

    @Override
    public String toString() {
        return "AmazonS3DataSource{" +
                "bucket='" + bucket + '\'' +
                ", key='" + key + '\'' +
                '}';
    }

}
