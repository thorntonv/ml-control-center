/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  S3ClientManager.java
 */

package dataflow.aws;

import com.amazonaws.services.s3.AmazonS3;
import java.util.Map;
import java.util.Optional;

public class S3ClientManager {

    private final Map<String, AmazonS3> bucketIdClientMap;

    public S3ClientManager(Map<String, AmazonS3> bucketIdClientMap) {
        this.bucketIdClientMap = bucketIdClientMap;
    }

    public Optional<AmazonS3> getClientForBucket(String bucketId) {
        return Optional.ofNullable(bucketIdClientMap.get(bucketId));
    }
}
