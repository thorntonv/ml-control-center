/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryTableKey.java
 */

package dataflow.data;

import java.util.Arrays;

public class DataRecordKey {

    private Object[] components;

    public DataRecordKey(final Object[] components) {
        this.components = components;
    }

    public Object[] getComponents() {
        return components;
    }

    public void setComponents(final Object[] components) {
        this.components = components;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final DataRecordKey that = (DataRecordKey) o;
        return Arrays.equals(components, that.components);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(components);
    }

    @Override
    public String toString() {
        return "DataRecordKey{" +
                "components=" + Arrays.toString(components) +
                '}';
    }
}
