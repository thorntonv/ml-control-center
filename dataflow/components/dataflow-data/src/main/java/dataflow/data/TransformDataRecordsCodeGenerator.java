/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TransformDataRecordsCodeGenerator.java
 */

package dataflow.data;

import dataflow.core.codegen.DefaultComponentCodeGenerator;
import dataflow.core.codegen.DataFlowCodeGenerationContext.ComponentCodeGenerationContext;
import dataflow.core.util.IndentPrintWriter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static dataflow.core.codegen.CodeGenerationUtil.getValueReferenceExpression;
import static dataflow.core.codegen.CodeGenerationUtil.setValueStmt;

public class TransformDataRecordsCodeGenerator extends DefaultComponentCodeGenerator {

    private static final String[] IMPORTS = new String[]{
        DataRecords.class.getName()
    };

    public TransformDataRecordsCodeGenerator() {
        super(null);
    }

    @Override
    public Set<String> getImports() {
        Set<String> imports = new HashSet<>(Arrays.asList(IMPORTS));
        imports.addAll(super.getImports());
        return imports;
    }

    @Override
    public void writeExecuteStatements(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        String transform = (String) context.getComponentConfig().getProperties().get("transform");
        String recordTransform = (String) context.getComponentConfig().getProperties().get("recordTransform");
        if((transform == null || transform.isEmpty()) &&
            (recordTransform == null || recordTransform.isEmpty())) {
            throw new RuntimeException("No transform defined for " + context.getComponentConfig().getId());
        }

        transform = replacePlaceholders(transform, context);
        recordTransform = replacePlaceholders(recordTransform, context);

        String inputRefExpr = getValueReferenceExpression(context.getComponentConfig().getInput().get("records"), context);

        out.println(setValueStmt(context, String.format("new DataRecords((DataRecords) %s)", inputRefExpr), null));

        if(recordTransform != null) {
            out.printf("%s.forEach(r -> {%n", getValueReferenceExpression(context));
            out.indent();
            out.printLines(recordTransform);
            out.unindent();
            out.println("});");
        }

        if(transform != null) {
            out.println("{");
            out.indent();
            out.printf("DataRecords records = %s;%n", getValueReferenceExpression(context));
            out.printLines(transform);
            out.unindent();
            out.println("}");
        }
    }
}
