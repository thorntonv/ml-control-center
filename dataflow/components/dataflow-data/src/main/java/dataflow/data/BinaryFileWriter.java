/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  BinaryFileWriter.java
 */

package dataflow.core.component.data;

import java.io.IOException;
import java.io.OutputStream;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public class BinaryFileWriter {

    @DataFlowConfigurable
    public BinaryFileWriter() {
    }

    @OutputValue
    public Void getValue(@InputValue OutputStream out, @InputValue byte[] data) throws IOException {
        out.write(data);
        return null;
    }
}
