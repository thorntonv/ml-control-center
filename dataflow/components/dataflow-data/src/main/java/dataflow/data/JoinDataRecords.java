/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  JoinDataRecords.java
 */

package dataflow.data;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DataFlowComponent
public class JoinDataRecords {

    private static final Logger logger = LoggerFactory.getLogger(JoinDataRecords.class);

    @DataFlowConfigurable
    public JoinDataRecords() {}

    @OutputValue
    public DataRecords getValue(
            @InputValue List<DataRecords> dataRecordsList) {
        if (dataRecordsList == null || dataRecordsList.isEmpty()) {
            return null;
        }
        if (dataRecordsList.size() <= 1) {
            return dataRecordsList.get(0);
        }

        List<String> joinColumns = new ArrayList<>();

        for(DataRecords dataRecords : dataRecordsList) {
            for (String keyColumn : dataRecords.getKeyColumnNames()) {
                if (!joinColumns.contains(keyColumn)) {
                    joinColumns.add(keyColumn);
                }
            }
        }

        List<String> allColumns = new ArrayList<>();
        for (DataRecords dataRecords : dataRecordsList) {
            for(String columnName : dataRecords.getColumnNames()) {
                if(!allColumns.contains(columnName)) {
                    allColumns.add(columnName);
                }
            }
            if (!dataRecords.getKeyColumnNames().containsAll(joinColumns)) {
                Set<String> missingJoinColumns = new HashSet<>(joinColumns);
                missingJoinColumns.removeAll(dataRecords.getKeyColumnNames());
                throw new IllegalArgumentException(String.format(
                    "DataRecord [%s] is missing key columns: %s",
                    dataRecords.toString(), missingJoinColumns));
            }
        }

        Map<DataRecordKey, List<DataRecord>> keyDataRecordsMap =
            buildKeyDataRecordsMap(dataRecordsList, joinColumns);

        DataRecords joinedRecords = new DataRecords();
        for (List<DataRecord> recordGroup : keyDataRecordsMap.values()) {
            joinedRecords.add(join(recordGroup, joinColumns));
        }

        joinedRecords.setColumnNames(allColumns);
        joinedRecords.setKeyColumnNames(joinColumns);
        return joinedRecords;
    }

    private Map<DataRecordKey, List<DataRecord>> buildKeyDataRecordsMap(
        List<DataRecords> dataRecordsList, List<String> joinColumns) {
        Map<DataRecordKey, List<DataRecord>> keyDataRecordsMap = new LinkedHashMap<>();
        for (DataRecords dataRecords : dataRecordsList) {
            for (DataRecord dataRecord : dataRecords) {
                Object[] key = new Object[joinColumns.size()];
                for (int idx = 0; idx < joinColumns.size(); idx++) {
                    key[idx] = dataRecord.get(joinColumns.get(idx));
                }
                keyDataRecordsMap.computeIfAbsent(new DataRecordKey(key),
                    k -> new ArrayList<>()).add(dataRecord);
            }
        }
        return keyDataRecordsMap;
    }

    private DataRecord join(List<DataRecord> records, List<String> joinColumns) {
        DataRecord joinedRecord = new DataRecord();
        for (DataRecord record : records) {
            for (Map.Entry<String, Object> column : record.entrySet()) {
                Object newValue = column.getValue();
                if (joinColumns.contains(column.getKey())) {
                    joinedRecord.put(column.getKey(), column.getValue());
                } else if (!joinedRecord.containsKey(column.getKey()) ||
                    joinedRecord.get(column.getKey()) == null ||
                    Objects.equals(joinedRecord.get(column.getKey()), newValue)) {
                    joinedRecord.put(column.getKey(), newValue);
                } else if(newValue == null) {
                    // The values don't match but this value is null so we can ignore it.
                } else {
                    // TODO(thorntonv): Rework the handling of this case.
//                    throw new IllegalArgumentException("Non-unique values for column " +
//                        column.getKey() + " joining on " + joinColumns + " in records: " + records);
                    logger.warn("Non-unique values for column " +
                        column.getKey() + " joining on " + joinColumns + " in records: " + records);
                }
            }
        }
        return joinedRecord;
    }
}
