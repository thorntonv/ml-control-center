/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecord.java
 */

package dataflow.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static dataflow.core.util.StringUtil.truncate;

public class DataRecord extends TreeMap<String, Object> {

    private static final int MAX_TO_STRING_COL_VAL_LEN = 25;
    private static final int MAX_TO_STRING_LEN = 100;

    public DataRecord() {
        super(String::compareToIgnoreCase);
    }

    public DataRecord(DataRecord record) {
        super(String::compareToIgnoreCase);
        putAll(record);
    }

    public DataRecord(Map<String, Object> map) {
        super(String::compareToIgnoreCase);
        putAll(map);
    }

    @JsonIgnore
    public <T> T getValueWithPath(DataRecordPath path) {
        if (path == null || path.getColumnName() == null) {
            return null;
        }
        Object columnValue = get(path.getColumnName());
        return DataRecordPathUtil.getValue(columnValue, path.getPath());
    }

    @JsonIgnore
    public <T> List<T> getValuesWithPath(DataRecordPath path) {
        if (path == null || path.getColumnName() == null) {
            return null;
        }
        Object columnValue = get(path.getColumnName());
        try {
            return DataRecordPathUtil.getAllValues(columnValue, path.getPath());
        } catch (Exception ex) {
            throw new IllegalArgumentException("Unable to locate value with path " +
                path.getPath() + " in column " + path.getColumnName());
        }
    }

    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        for (Map.Entry<String, Object> entry : entrySet()) {
            if (strBuilder.length() > MAX_TO_STRING_LEN) {
                break;
            }
            if (strBuilder.length() > 0) {
                strBuilder.append(", ");
            }
            strBuilder.append(String.format("%s=%s", entry.getKey(),
                truncate(String.valueOf(entry.getValue()), MAX_TO_STRING_COL_VAL_LEN)));
        }
        return "{" + truncate(strBuilder.toString(), MAX_TO_STRING_LEN) + "}";
    }
}
