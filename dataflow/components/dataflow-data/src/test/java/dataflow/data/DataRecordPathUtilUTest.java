/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordPathUtilUTest.java
 */

package dataflow.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;
import java.util.List;

import dataflow.data.DataRecordPathUtil;
import org.junit.Test;

public class DataRecordPathUtilUTest {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private static final Object TEST_VALUE_1 = ImmutableMap.builder()
        .put("a1", "A")
        .put("a2", ImmutableMap.builder()
            .put("b1", "B")
            .put("b2", ImmutableMap.builder()
                .put("c1", 30)
                .build())
            .put("b3", ImmutableList.of(3, 5, 7, 9))
            .put("b4", ImmutableList.of(
                ImmutableMap.builder().put("d1", "test1").build(),
                ImmutableMap.builder().put("d1", "test2").build()))
            .build())
        .put("a3", 52)
        .build();
    private static final Object TEST_VALUE_1_JSON;
    private static final String TEST_VALUE_1_VISITED = "[a1=A, a2.b1=B, a2.b2.c1=30, a2.b3=3, a2.b3=5, a2.b3=7, a2.b3=9, a2.b4.d1=test1, a2.b4.d1=test2, a3=52]";

    private static final Object TEST_VALUE_2 = ImmutableList.of(
        ImmutableMap.builder()
            .put("a1", "test1")
            .put("a2", 5)
            .build(),
        ImmutableMap.builder()
            .put("a1", "test2")
            .put("a3", ImmutableList.of("red", "green", "blue"))
            .build()
    );
    private static final Object TEST_VALUE_2_JSON;
    private static final String TEST_VALUE_2_VISITED =
        "[a1=test1, a2=5, a1=test2, a3=red, a3=green, a3=blue]";

    static {
        try {
            TEST_VALUE_1_JSON = objectMapper.writeValueAsString(TEST_VALUE_1);
            TEST_VALUE_2_JSON = objectMapper.writeValueAsString(TEST_VALUE_2);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testGetValue() {
        assertEquals("A", DataRecordPathUtil.getValue(TEST_VALUE_1, "a1"));
        assertEquals("B", DataRecordPathUtil.getValue(TEST_VALUE_1, "a2.b1"));
        assertEquals((Integer) 30, DataRecordPathUtil.getValue(TEST_VALUE_1, "a2.b2.c1"));
    }

    @Test
    public void testVisit_all() {
        List<String> visited = new ArrayList<>();
        DataRecordPathUtil.visit(TEST_VALUE_1, (value, path) ->
            visited.add(path + "=" + value));
        assertEquals(TEST_VALUE_1_VISITED, visited.toString());
    }

    @Test
    public void testVisit_path() {
        List<Object> values = DataRecordPathUtil.getAllValues(TEST_VALUE_1, "a2.b4.d1");
        assertEquals("[test1, test2]", values.toString());
    }

    @Test
    public void testGetAllValues_missingPath() {
        List<Object> values = DataRecordPathUtil.getAllValues(TEST_VALUE_1, "a2.b100");
        assertTrue(values.isEmpty());
    }

    @Test
    public void testVisit_invalidPath() {
        try {
            DataRecordPathUtil.getAllValues(TEST_VALUE_1, "a2.b1.c1.x1");
            fail("expected exception was not thrown");
        } catch(IllegalArgumentException ex) {
            assertEquals("Unable to locate value with path: a2.b1.c1.x1", ex.getMessage());
        }
    }

    @Test
    public void testVisit_list() {
        List<String> visited = new ArrayList<>();
        DataRecordPathUtil.visit(TEST_VALUE_2, (value, path) ->
            visited.add(path + "=" + value));
        assertEquals(TEST_VALUE_2_VISITED, visited.toString());
    }

    @Test
    public void testVisit_list_json() {
        List<String> visited = new ArrayList<>();
        DataRecordPathUtil.visit(TEST_VALUE_2_JSON, (value, path) ->
            visited.add(path + "=" + value));
        assertEquals(TEST_VALUE_2_VISITED, visited.toString());
    }

    @Test
    public void testVisit_withMaxDepth1() {
        List<String> visited = new ArrayList<>();
        DataRecordPathUtil.visit(TEST_VALUE_1, 1, (value, path) ->
            visited.add(path + "=" + value));
        assertEquals("[a1=A, a2={b1=B, b2={c1=30}, b3=[3, 5, 7, 9], b4=[{d1=test1}, {d1=test2}]}, a3=52]", visited.toString());
    }

    @Test
    public void testVisit_withMaxDepth2() {
        List<String> visited = new ArrayList<>();
        DataRecordPathUtil.visit(TEST_VALUE_1, 2, (value, path) ->
            visited.add(path + "=" + value));
        assertEquals("[a1=A, a2.b1=B, a2.b2={c1=30}, a2.b3=[3, 5, 7, 9], a2.b4=[{d1=test1}, {d1=test2}], a3=52]",
            visited.toString());
    }

    @Test
    public void testVisit_all_json() {
        List<String> visited = new ArrayList<>();
        DataRecordPathUtil.visit(TEST_VALUE_1_JSON, (value, path) ->
            visited.add(path + "=" + value));
        assertEquals(TEST_VALUE_1_VISITED, visited.toString());
    }

    @Test
    public void testGetValue_root() {
        assertEquals(TEST_VALUE_1, DataRecordPathUtil.getValue(TEST_VALUE_1, "."));
        assertEquals(TEST_VALUE_2, DataRecordPathUtil.getValue(TEST_VALUE_2, "."));
    }

    @Test
    public void testGetValue_json() {
        assertEquals("A", DataRecordPathUtil.getValue(TEST_VALUE_1_JSON, "a1"));
        assertEquals("B", DataRecordPathUtil.getValue(TEST_VALUE_1_JSON, "a2.b1"));
        assertEquals((Integer) 30, DataRecordPathUtil.getValue(TEST_VALUE_1_JSON, "a2.b2.c1"));
    }

    @Test
    public void testGetValue_invalidJson() {
        try {
            DataRecordPathUtil.getValue("abc", "a.b");
        } catch (Exception ex) {
            assertEquals("Unable to locate value with path: a.b", ex.getMessage());
        }
    }

    @Test
    public void testGetAllValues_list() {
        assertEquals(ImmutableList.of("test1", "test2"),
            DataRecordPathUtil.getAllValues(TEST_VALUE_2, "a1"));
    }

    @Test
    public void testGetValue_missingValuePath() {
        assertNull(DataRecordPathUtil.getValue(TEST_VALUE_1, "a100"));
        assertNull(DataRecordPathUtil.getValue(TEST_VALUE_1, "a2.b100"));
    }

    @Test
    public void testGetValue_missingValuePath_json() {
        assertNull(DataRecordPathUtil.getValue(TEST_VALUE_1_JSON, "a100"));
        assertNull(DataRecordPathUtil.getValue(TEST_VALUE_1_JSON, "a2.b100"));
    }

    @Test
    public void testGetValue_invalidValuePath() {
        try {
            assertNull(DataRecordPathUtil.getValue(TEST_VALUE_1, "a3.b1"));
        } catch(Exception ex) {
            assertEquals("Unable to locate value with path: a3.b1", ex.getMessage());
        }
    }

    @Test
    public void testGetValue_invalidValuePath_json() {
        try {
            assertNull(DataRecordPathUtil.getValue(TEST_VALUE_1_JSON, "a3.b1"));
        } catch(Exception ex) {
            assertEquals("Unable to locate value with path: a3.b1", ex.getMessage());
        }
    }

    @Test
    public void testGetValue_multipleValues() {
        try {
            assertNull(DataRecordPathUtil.getValue(TEST_VALUE_1, "a2.b4.d1"));
        } catch(Exception ex) {
            assertEquals("Found multiple values with path: a2.b4.d1", ex.getMessage());
        }
    }

    @Test
    public void testGetValue_multipleValues_json() {
        try {
            assertNull(DataRecordPathUtil.getValue(TEST_VALUE_1_JSON, "a2.b4.d1"));
        } catch(Exception ex) {
            assertEquals("Found multiple values with path: a2.b4.d1", ex.getMessage());
        }
    }
}