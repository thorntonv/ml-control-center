/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ElasticsearchCreateIndex.java
 */

package dataflow.elasticsearch;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.datasource.DataSource;
import dataflow.core.datasource.DataSourceUtil;
import dataflow.core.exception.DataFlowConfigurationException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DataFlowComponent
public class ElasticsearchCreateIndex extends AbstractElasticsearchAction {

    private static final Logger logger = LoggerFactory.getLogger(ElasticsearchCreateIndex.class);

    private final int schemaVersion;
    private final String schemaDefinitionJson;
    private final boolean disableReplicas;
    private final boolean cleanupFailedIndexes;
    private final Boolean transactionLogAsync;
    private final Integer numPriorSchemaVersionsToKeep;
    private final Integer numActiveBackupsForCurrentVersion;
    private final Integer minIndexAgeForCleanupHours;

    @DataFlowConfigurable
    public ElasticsearchCreateIndex(
        @DataFlowConfigProperty String clusterId,
        @DataFlowConfigProperty String type,
        @DataFlowConfigProperty String name,
        @DataFlowConfigProperty(required = false) String version,
        @DataFlowConfigProperty DataSource schema,
        @DataFlowConfigProperty int schemaVersion,
        @DataFlowConfigProperty(required = false) Integer numPriorSchemaVersionsToKeep,
        @DataFlowConfigProperty(required = false) Integer numActiveBackupsForCurrentVersion,
        @DataFlowConfigProperty(required = false) Integer minIndexAgeForCleanupHours,
        @DataFlowConfigProperty(required = false) Boolean disableReplicas,
        @DataFlowConfigProperty(required = false) Boolean transactionLogAsync,
        @DataFlowConfigProperty(required = false) Boolean cleanupFailedIndexes,
        @DataFlowConfigProperty(required = false) String metricNamePrefix,
        @DataFlowConfigProperty(required = false) Map<String, String> metricTags) {
        super(clusterId, type, name, version != null ? version : createTimeBasedVersionString(),
            metricNamePrefix, metricTags);
        try {
            this.schemaDefinitionJson = DataSourceUtil.toString(schema.getInputStream());
        } catch (IOException e) {
            throw new DataFlowConfigurationException(
                "Unable to read index schema definition from " + schema);
        }
        this.schemaVersion = schemaVersion;
        this.numPriorSchemaVersionsToKeep = numPriorSchemaVersionsToKeep != null ?
            numPriorSchemaVersionsToKeep : indexManager.getDefaultNumPriorSchemaVersionsToKeep();
        this.numActiveBackupsForCurrentVersion = numActiveBackupsForCurrentVersion != null ?
            numActiveBackupsForCurrentVersion : indexManager.getDefaultNumActiveBackupsForCurrentVersion();
        this.minIndexAgeForCleanupHours = minIndexAgeForCleanupHours != null ?
            minIndexAgeForCleanupHours : indexManager.getDefaultMinIndexAgeForCleanupHours();
        this.disableReplicas = disableReplicas != null ? disableReplicas : false;
        this.transactionLogAsync = transactionLogAsync;
        this.cleanupFailedIndexes = cleanupFailedIndexes != null ? cleanupFailedIndexes : false;
    }

    @OutputValue
    public IndexMetadata getValue() throws IOException, IndexerException {
        IndexMetadata metadata = indexManager.createIndex(indexType, indexName, indexVersion,
            schemaDefinitionJson, schemaVersion);
        if (disableReplicas) {
            indexManager.setNumberOfReplicas(metadata, 0);
        }
        if (transactionLogAsync != null) {
            indexManager.setTransLogAsync(metadata, transactionLogAsync);
        }
        try {
            indexManager.cleanupOldIndexes(indexType, numPriorSchemaVersionsToKeep,
                numActiveBackupsForCurrentVersion, minIndexAgeForCleanupHours, cleanupFailedIndexes);
        } catch (Exception ex) {
            logger.warn("Error performing cleanup of old indexes", ex);
        }
        return metadata;
    }

    private static String createTimeBasedVersionString() {
        return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
    }
}
