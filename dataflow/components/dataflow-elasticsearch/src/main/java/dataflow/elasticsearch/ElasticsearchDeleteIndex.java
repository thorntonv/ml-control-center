/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ElasticsearchDeleteIndex.java
 */

package dataflow.elasticsearch;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.OutputValue;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DataFlowComponent
public class ElasticsearchDeleteIndex extends AbstractElasticsearchAction {

    private static final Logger logger = LoggerFactory.getLogger(ElasticsearchDeleteIndex.class);

    @DataFlowConfigurable
    public ElasticsearchDeleteIndex(@DataFlowConfigProperty String clusterId,
        @DataFlowConfigProperty String type,
        @DataFlowConfigProperty String name,
        @DataFlowConfigProperty String version,
        @DataFlowConfigProperty(required = false) String metricNamePrefix,
        @DataFlowConfigProperty(required = false) Map<String, String> metricTags) {
        super(clusterId, type, name, version, metricNamePrefix, metricTags);
    }

    @OutputValue
    public IndexMetadata getValue() throws IOException, IndexerException {
        Optional<IndexMetadata> metadata = indexManager.getIndexMetadata(
            indexType, indexName, indexVersion);
        if(metadata.isPresent()) {
            indexManager.removeIndex(metadata.get());
        } else {
            logger.warn(String.format("%s index %s:%s does not exist",
                indexType, indexName, indexVersion));
        }
        return metadata.orElse(null);
    }
}
