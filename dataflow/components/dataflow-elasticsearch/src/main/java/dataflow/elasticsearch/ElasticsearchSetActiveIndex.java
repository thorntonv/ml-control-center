/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ElasticsearchSetActiveIndex.java
 */

package dataflow.elasticsearch;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

@DataFlowComponent
public class ElasticsearchSetActiveIndex extends AbstractElasticsearchAction implements AutoCloseable {

    public static final Logger logger = LoggerFactory.getLogger(ElasticsearchSetActiveIndex.class);

    private static final int DEFAULT_MAX_NUM_SEGMENTS = 5;

    private final Integer replicaCount;
    private final Boolean transactionLogAsync;
    private final Integer switchDurationMinutes;
    private final boolean forceMerge;
    private final int maxNumSegments;
    private final boolean waitForHealthyCluster;

    private final ExecutorService executor = Executors.newCachedThreadPool();

    @DataFlowConfigurable
    public ElasticsearchSetActiveIndex(
            @DataFlowConfigProperty String clusterId,
            @DataFlowConfigProperty String type,
            @DataFlowConfigProperty String name,
            @DataFlowConfigProperty String version,
            @DataFlowConfigProperty(required = false) Integer switchDurationMinutes,
            @DataFlowConfigProperty(required = false) Integer replicaCount,
            @DataFlowConfigProperty(required = false) Boolean forceMergeSegments,
            @DataFlowConfigProperty(required = false) Boolean transactionLogAsync,
            @DataFlowConfigProperty(required = false,
                description = "Wait for the cluster to be healthy before switching") Boolean waitForHealthyCluster,
            @DataFlowConfigProperty(required = false) Integer maxNumSegments,
            @DataFlowConfigProperty(required = false) String metricNamePrefix,
            @DataFlowConfigProperty(required = false) Map<String, String> metricTags) {
        super(clusterId, type, name, version, metricNamePrefix, metricTags);
        this.switchDurationMinutes = switchDurationMinutes != null ? switchDurationMinutes : 0;
        this.replicaCount = replicaCount;
        this.transactionLogAsync = transactionLogAsync;
        if (forceMergeSegments != null && !forceMergeSegments && maxNumSegments != null) {
            throw new DataFlowConfigurationException(
                "Maximum number of segments is set, but force merge segments is disabled");
        }
        if (maxNumSegments != null && maxNumSegments <= 0) {
            throw new DataFlowConfigurationException("Maximum number of segments must be > 0");
        }
        this.forceMerge = forceMergeSegments != null ? forceMergeSegments : true;
        this.maxNumSegments = maxNumSegments != null ? maxNumSegments : DEFAULT_MAX_NUM_SEGMENTS;
        this.waitForHealthyCluster = waitForHealthyCluster != null ? waitForHealthyCluster : true;
    }

    @OutputValue
    public CompletableFuture<IndexMetadata> getValue() {
        CompletableFuture<IndexMetadata> futureResult = new CompletableFuture<>();
        DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        Map<String, String> mdcContext = MDC.getCopyOfContextMap();
        executor.submit(() -> executionContext.execute(() -> {
            if(mdcContext != null) {
                MDC.setContextMap(mdcContext);
            }
            setActiveIndex(futureResult);
        }));
        return futureResult;
    }

    private void setActiveIndex(CompletableFuture<IndexMetadata> futureResult) {
        try {
            IndexMetadata metadata = getIndexMetadata();
            if (forceMerge) {
                indexManager.forceMergeSegments(metadata, maxNumSegments);
            }
            if (replicaCount != null && replicaCount > 0) {
                indexManager.setNumberOfReplicas(metadata, replicaCount);
            }
            if (transactionLogAsync != null) {
                indexManager.setTransLogAsync(metadata, transactionLogAsync);
            }

            if (waitForHealthyCluster) {
                // Wait for cluster to be healthy before switching.
                while (!indexManager.checkClusterHealth()) {
                    try {
                        logger.warn(
                            "Waiting for cluster {} to be healthy before switching the active index",
                            indexManager.getClusterId());
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }

            indexManager.switchIndex(metadata, switchDurationMinutes, TimeUnit.MINUTES, this::isCanceled);

            metadata.setIndexingStatus(IndexingStatusEnum.COMPLETE);
            indexManager.updateMetadataSync(metadata);
            indexManager.makeActiveIndex(metadata);
            futureResult.complete(metadata);
        } catch (Exception ex) {
            futureResult.completeExceptionally(ex);
        }
    }

    private boolean isCanceled() {
        return DataFlowExecutionContext.getCurrentExecutionContext().getInstance().isCanceled();
    }

    @Override
    public void close() throws Exception {
        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.MINUTES);
    }
}
