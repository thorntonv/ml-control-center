/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ElasticsearchWriteDocs.java
 */

package dataflow.elasticsearch;

import static dataflow.core.util.MetricsUtil.getMetricName;
import static dataflow.core.util.MetricsUtil.tagsToStringArray;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import io.micrometer.core.instrument.Counter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DataFlowComponent
public class ElasticsearchWriteDocs extends AbstractElasticsearchAction {

    private static final int DEFAULT_TIMEOUT_MILLIS = 1000;

    private static final Logger logger = LoggerFactory.getLogger(ElasticsearchWriteDocs.class);

    private final boolean waitForIndexingToFinish;
    private final int timeoutMillis;
    private final int retryCount;
    private final int retryIntervalMillis;
    private Counter docWriteCounter;

    @DataFlowConfigurable
    public ElasticsearchWriteDocs(
        @DataFlowConfigProperty String clusterId,
        @DataFlowConfigProperty String type,
        @DataFlowConfigProperty String name,
        @DataFlowConfigProperty(required = false) String version,
        @DataFlowConfigProperty(required = false) Boolean waitForIndexingToFinish,
        @DataFlowConfigProperty(required = false) Integer timeoutMillis,
        @DataFlowConfigProperty(required = false) Integer retryCount,
        @DataFlowConfigProperty(required = false) Integer retryIntervalMillis,
        @DataFlowConfigProperty(required = false) String metricNamePrefix,
        @DataFlowConfigProperty(required = false) Map<String, String> metricTags) {
        super(clusterId, type, name, version, metricNamePrefix, metricTags);

        this.waitForIndexingToFinish = waitForIndexingToFinish != null ? waitForIndexingToFinish : false;
        this.timeoutMillis = timeoutMillis != null ? timeoutMillis : DEFAULT_TIMEOUT_MILLIS;
        this.retryCount = retryCount != null ? retryCount : 1;
        this.retryIntervalMillis = retryIntervalMillis != null ? retryIntervalMillis : this.timeoutMillis;
    }

    @OutputValue(description = "Returns a map of document id string to document")
    public CompletableFuture<Map<String, Object>> getValue(
        @InputValue(description = "The documents to write to the index. The documents must be " +
            "json serializable. Can be a map of doc id string to document or a list of documents. " +
            "If a list is provided then the documents will be added with auto generated ids.") Object documents)
            throws IOException, IndexerException {
        IndexMetadata metadata = getIndexMetadata();
        long startTime = System.currentTimeMillis();

        Map<String, Object> docIdDocMap;
        if(documents instanceof Map) {
            docIdDocMap = (Map<String, Object>) documents;
        } else if(documents instanceof Iterable) {
            docIdDocMap = buildDocIdDocMapWithGeneratedIds((Iterable) documents);
        } else {
            throw new IllegalArgumentException("Documents must be a map or a list");
        }

        logger.debug("Writing {} docs to index {}", docIdDocMap.size(), metadata.getName());

        CompletableFuture<Map<String, Object>> writeFuture = indexManager.writeDocuments(docIdDocMap, metadata,
            waitForIndexingToFinish, timeoutMillis, retryCount, retryIntervalMillis,
            (retries) -> incrementRetryCounter());
        incrementRequestCounter();
        return writeFuture.whenComplete((docMap, ex) -> {
            logger.debug("Finished writing {} docs to index {}",
                docIdDocMap.size(), metadata.getName());
            long runTimeMillis = System.currentTimeMillis() - startTime;
            recordExecutionTime(runTimeMillis);
            if (ex != null) {
                recordError(ex);
            } else {
                incrementDocWriteCounter(docMap.size());
            }
        });
    }

    private Map<String, Object> buildDocIdDocMapWithGeneratedIds(Iterable<Object> documents) {
        Random random = new Random();
        Map<String, Object> docIdDocMap = new HashMap<>();
        if (documents != null) {
            documents.forEach(doc -> {
                String id = String.valueOf(Math.abs(random.nextLong()));
                docIdDocMap.put(id, doc);
            });
        }
        return docIdDocMap;
    }

    private void incrementDocWriteCounter(int count) {
        if (docWriteCounter == null) {
            initMetrics();
        }
        if (docWriteCounter != null) {
            docWriteCounter.increment(count);
        }
    }

    @Override
    protected void initMetrics() {
        super.initMetrics();
        if (meterRegistry == null) {
            return;
        }
        DataFlowInstance instance = DataFlowExecutionContext.getCurrentExecutionContext()
            .getInstance();
        Map<String, String> tags = getMetricTags();
        docWriteCounter = meterRegistry.counter(
            getMetricName(metricNamePrefix, "docWriteCount", instance),
            tagsToStringArray(tags));
    }
}
