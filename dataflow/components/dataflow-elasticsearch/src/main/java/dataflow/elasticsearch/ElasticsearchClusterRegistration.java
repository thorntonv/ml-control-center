/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ElasticsearchClusterRegistration.java
 */

package dataflow.elasticsearch;

import java.util.Map;
import org.elasticsearch.client.RestHighLevelClient;

public class ElasticsearchClusterRegistration {

    private String endpoint;
    private RestHighLevelClient restHighLevelClient;
    private ElasticsearchClient elasticSearchClient;
    private Map<String, String> indexTypeActiveIndexAliasNameMap;
    private IndexManager indexManager;

    public ElasticsearchClusterRegistration(String endpoint,
        ElasticsearchClient elasticSearchClient,
        RestHighLevelClient restHighLevelClient,
        Map<String, String> indexTypeActiveIndexAliasNameMap,
        IndexManager indexManager) {
        this.endpoint = endpoint;
        this.elasticSearchClient = elasticSearchClient;
        this.restHighLevelClient = restHighLevelClient;
        this.indexTypeActiveIndexAliasNameMap = indexTypeActiveIndexAliasNameMap;
        this.indexManager = indexManager;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public ElasticsearchClient getElasticSearchClient() {
        return elasticSearchClient;
    }

    public IndexManager getIndexManager() {
        return indexManager;
    }

    public Map<String, String> getIndexTypeActiveIndexAliasNameMap() {
        return indexTypeActiveIndexAliasNameMap;
    }

    public RestHighLevelClient getRestHighLevelClient() {
        return restHighLevelClient;
    }
}
