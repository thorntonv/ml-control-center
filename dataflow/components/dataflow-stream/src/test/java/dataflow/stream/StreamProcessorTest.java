package dataflow.stream;

import com.google.common.collect.ImmutableList;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StreamProcessorTest {

    private DataFlowEnvironment env;
    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);
        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig = discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }
    @Test
    public void test() throws Exception {
        List<String> inputList = ImmutableList.of("a", "b", "c");
        DataFlowInstance instance = env.getRegistry().getDataFlowRegistration("streamProcessorTest").getInstanceFactory().newInstance(i ->{});
        instance.setValue("stream", inputList.stream());
        List<String> out = Collections.synchronizedList(new ArrayList<>());
        instance.setValue("list", out);
        instance.execute();
        assertEquals(5, out.size());
        assertEquals("before", out.get(0));
        // Items from input list could be in any order since they are added in multiple threads.
        assertTrue(out.containsAll(inputList));
        assertEquals("after", out.get(out.size() - 1));
    }

    @Test
    public void test_beforeStartValueRef() throws Exception {
        List<String> inputList = ImmutableList.of("a", "b", "c");
        DataFlowInstance instance = env.getRegistry().getDataFlowRegistration(
            "streamProcessorTest_beforeStartValueRef").getInstanceFactory().newInstance(i ->{});
        instance.setValue("stream", inputList.stream());
        instance.execute();
        List<String> out = (List<String>) instance.getOutput();
        assertEquals(5, out.size());
        assertEquals("before", out.get(0));
        // Items from input list could be in any order since they are added in multiple threads.
        assertTrue(out.containsAll(inputList));
        assertEquals("after", out.get(out.size() - 1));
    }
}
