/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StreamForEach.java
 */

package dataflow.stream;

import dataflow.batch.config.TaskConfig;
import dataflow.core.component.annotation.*;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.ThreadLocalDataFlowInstance;
import dataflow.retry.Retry;
import io.micrometer.core.instrument.MeterRegistry;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import static dataflow.core.config.DataFlowConfig.DEFAULT_ITEM_VALUE_NAME;

/**
 * A component that processes a stream of items in parallel.
 */
@DataFlowComponent
public class StreamProcessor {

    private final int batchSize;
    private final int parallelism;
    private final int prefetchCount;
    private final boolean prefetchAllItems;
    private final boolean dryRun;
    private final int dryRunRecordCount;
    private final Map<String, Object> properties;
    private final Map<String, Object> dryRunProperties;
    private final Retry retry;
    private final String metricNamePrefix;
    private final String itemValueName;
    private final Long timeoutSeconds;

    @DataFlowConfigurable
    public StreamProcessor(
            @DataFlowConfigProperty(required = false) Integer batchSize,
            @DataFlowConfigProperty(required = false) Integer parallelism,
            @DataFlowConfigProperty(required = false) Integer prefetchCount,
            @DataFlowConfigProperty(required = false) Boolean prefetchAllItems,
            @DataFlowConfigProperty(required = false) Boolean dryRun,
            @DataFlowConfigProperty(required = false) Integer dryRunRecordCount,
            @DataFlowConfigProperty(required = false) Map<String, Object> properties,
            @DataFlowConfigProperty(required = false) Map<String, Object> dryRunProperties,
            @DataFlowConfigProperty(required = false) Long timeoutSeconds,
            @DataFlowConfigProperty(required = false) Retry retry,
            @DataFlowConfigProperty(required = false) String metricNamePrefix,
            @DataFlowConfigProperty(required = false, description = "The name of the item value in the mapping DataFlow")
                    String itemValueName) {
        this.batchSize = batchSize != null ? batchSize : 1;
        this.parallelism = parallelism != null ? parallelism : Runtime.getRuntime().availableProcessors() - 1;
        this.prefetchCount = prefetchCount != null ? prefetchCount : 1;
        this.prefetchAllItems = prefetchAllItems != null ? prefetchAllItems : false;
        this.dryRun = dryRun != null ? dryRun : false;
        this.dryRunRecordCount = dryRunRecordCount != null ? dryRunRecordCount : 100;
        this.properties = properties != null ? new HashMap<>(properties) : Collections.emptyMap();
        this.dryRunProperties = dryRunProperties != null ? new HashMap<>(dryRunProperties) : Collections.emptyMap();
        this.timeoutSeconds = timeoutSeconds;

        DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        this.retry = Retry.clone(retry != null ? retry : executionContext.getDependencyInjector().getInstance(Retry.class));
        this.metricNamePrefix = metricNamePrefix != null ? metricNamePrefix : getClass().getSimpleName();
        this.itemValueName = itemValueName != null ? itemValueName : DEFAULT_ITEM_VALUE_NAME;
    }

    @OutputValue
    public CompletableFuture<Object> getValue(@InputValue Stream<?> stream,
                                            @InputValue DataFlowInstance pipeline,
                                            @InputValue(required = false) DataFlowInstance beforeStart,
                                            @InputValue(required = false) DataFlowInstance afterComplete) {
        final DataFlowExecutionContext executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        if (executionContext == null) {
            throw new RuntimeException("Execution context is not set");
        }
        MeterRegistry meterRegistry = executionContext.getDependencyInjector()
                .getInstance(MeterRegistry.class);

        TaskConfig taskConfig = new TaskConfig();
        taskConfig.setBatchSize(batchSize);
        taskConfig.setParallelism(parallelism);
        taskConfig.setPrefetchCount(prefetchCount);
        taskConfig.setPrefetchAllItems(prefetchAllItems);
        if (beforeStart != null) {
            taskConfig.setBeforeStartFlow(beforeStart.getConfig());
        }
        if (afterComplete != null) {
            taskConfig.setAfterCompleteFlow(afterComplete.getConfig());
        }
        taskConfig.setRetry(retry);
        taskConfig.setProperties(properties);
        taskConfig.setDryRunProperties(dryRunProperties);
        taskConfig.setTimeoutSeconds(timeoutSeconds);

        StreamProcessorTask task = new StreamProcessorTask(stream,
                ThreadLocalDataFlowInstance.create(pipeline.getConfig().getId(), executionContext.getInstance()),
                beforeStart != null ? ThreadLocalDataFlowInstance.create(beforeStart.getConfig().getId(), executionContext.getInstance()) : null,
                afterComplete != null ? ThreadLocalDataFlowInstance.create(afterComplete.getConfig().getId(), executionContext.getInstance()) : null,
                dryRun, dryRunRecordCount, taskConfig, metricNamePrefix, itemValueName, meterRegistry, executionContext.getEnvironment());
        return task.start();
    }
}
