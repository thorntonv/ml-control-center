/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigProperty.java
 */

package dataflow.core.component.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation for configuration properties. This annotation should be used on each parameter of all constructors that
 * are annotated with {@link DataFlowConfigurable}.
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.PARAMETER)
public @interface DataFlowConfigProperty {

    /**
     * The name of the property. If not specified then the name of parameter in the constructor is used.
     */
    String name() default "";

    /**
     * A list of supported types for this property. This is used for validation when loading a DataFlow. If no types are
     * specified then this constraint is not used for validation.
     */
    String[] supportedTypes() default {};

    String description() default "";

    /**
     * Indicates that this property value is required. The DataFlow engine will verify that the property value is
     * specified before constructing the component.
     */
    boolean required() default true;

}
