/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurablePropertyMetadata.java
 */

package dataflow.core.component.metadata;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Holds information about a constructor property for classes that are {@link dataflow.core.component.annotation.DataFlowConfigurable}.
 */
public class DataFlowConfigurableObjectPropertyMetadata {

    private final String name;
    private final String rawType;
    private final boolean isEnum;
    private final Set<String> supportedTypes;
    private final boolean required;
    private final String description;

    public DataFlowConfigurableObjectPropertyMetadata(final String name, final String rawType, final boolean isEnum,
                                                      final String[] supportedTypes, final boolean required, final String description) {
        this.name = name;
        this.rawType = rawType;
        this.isEnum = isEnum;
        this.supportedTypes = new HashSet<>(Arrays.asList(supportedTypes));
        this.required = required;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getRawType() {
        return rawType;
    }

    public Set<String> getSupportedTypes() {
        return supportedTypes;
    }

    public boolean isRequired() {
        return required;
    }

    public String getDescription() {
        return description;
    }

    public boolean isEnum() {
        return isEnum;
    }

    @Override
    public String toString() {
        return "DataFlowConfigurablePropertyMetadata{" +
                "name='" + name + '\'' +
                ", rawType='" + rawType + '\'' +
                ", isEnum=" + isEnum +
                ", supportedTypes=" + supportedTypes +
                ", required=" + required +
                ", description='" + description + '\'' +
                '}';
    }
}
