/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueTypeConverterMetadata.java
 */

package dataflow.core.component.metadata;

public class ValueTypeConverterMetadata {

    private final boolean lossy;
    private final String fromType;
    private final String toType;

    public ValueTypeConverterMetadata(final boolean lossy, final String fromType, final String toType) {
        this.lossy = lossy;
        this.fromType = fromType;
        this.toType = toType;
    }

    public boolean isLossy() {
        return lossy;
    }

    public String getFromType() {
        return fromType;
    }

    public String getToType() {
        return toType;
    }

    @Override
    public String toString() {
        return "ValueTypeConverterMetadata{" +
                "lossy=" + lossy +
                ", fromType=" + fromType +
                ", toType=" + toType +
                '}';
    }
}
