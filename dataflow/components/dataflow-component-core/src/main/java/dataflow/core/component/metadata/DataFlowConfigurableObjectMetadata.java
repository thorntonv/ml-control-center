/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurablePropertyMetadata.java
 */

package dataflow.core.component.metadata;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Holds information about a constructor property for classes that are {@link dataflow.core.component.annotation.DataFlowConfigurable}.
 */
public class DataFlowConfigurableObjectMetadata {

    private final String className;
    private final String description;
    private final List<DataFlowConfigurableObjectPropertyMetadata> propertyMetadata;

    public DataFlowConfigurableObjectMetadata(String className, String description,
                                              DataFlowConfigurableObjectPropertyMetadata...propertyMetadata) {
        this.className = className;
        this.description = description;
        this.propertyMetadata = Arrays.asList(propertyMetadata);
    }

    public String getClassName() {
        return className;
    }

    public String getDescription() {
        return description;
    }

    public List<DataFlowConfigurableObjectPropertyMetadata> getPropertyMetadata() {
        return Collections.unmodifiableList(propertyMetadata);
    }

    public DataFlowConfigurableObjectPropertyMetadata getPropertyMetadata(String name) {
        return propertyMetadata.stream().filter(m -> m.getName().equals(name)).findAny().orElse(null);
    }

    @Override
    public String toString() {
        return "DataFlowConfigurableObjectMetadata{" +
                "className='" + className + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
