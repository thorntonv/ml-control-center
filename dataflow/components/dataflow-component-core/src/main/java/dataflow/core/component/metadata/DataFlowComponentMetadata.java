/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowComponentMetadata.java
 */

package dataflow.core.component.metadata;

import java.util.List;

/**
 * Holds information about a DataFlow component.
 */
public class DataFlowComponentMetadata {

    private final String typeName;
    private final String description;
    private final String componentPackageName;
    private final String componentClassName;
    private final DataFlowConfigurableObjectMetadata configurableObjectMetadata;
    private final List<DataFlowComponentInputMetadata> inputMetadata;
    private final DataFlowComponentOutputMetadata outputMetadata;
    private final String builderClass;
    private final String codeGeneratorClass;

    /**
     * If not null the component is a ValueConverter that can be used to
     * convert values from one type to another, null otherwise.
     */
    private final ValueTypeConverterMetadata typeConverterMetadata;

    // The component has async set in the annotation
    private final boolean isAsync;
    // The component has a method that returns a CompletableFuture
    private final boolean hasAsyncGet;
    // The component takes all inputs from the config as a map. This means that new inputs can be added dynamically
    // in configuration without being known in advance.
    private final boolean hasDynamicInput;

    public DataFlowComponentMetadata(final String typeName, final String description, final String componentPackageName,
                                     final String componentClassName, final DataFlowConfigurableObjectMetadata configurableObjectMetadata,
                                     final List<DataFlowComponentInputMetadata> inputMetadata, final DataFlowComponentOutputMetadata outputMetadata,
                                     String builderClass, String codeGeneratorClass, final ValueTypeConverterMetadata typeConverterMetadata, final boolean isAsync, final boolean hasAsyncGet,
                                     final boolean hasDynamicInput) {
        this.typeName = typeName;
        this.description = description;
        this.componentPackageName = componentPackageName;
        this.componentClassName = componentClassName;
        this.configurableObjectMetadata = configurableObjectMetadata;
        this.inputMetadata = inputMetadata;
        this.outputMetadata = outputMetadata;
        this.builderClass = builderClass;
        this.codeGeneratorClass = codeGeneratorClass;
        this.typeConverterMetadata = typeConverterMetadata;
        this.isAsync = isAsync;
        this.hasAsyncGet = hasAsyncGet;
        this.hasDynamicInput = hasDynamicInput;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getDescription() {
        return description;
    }

    public String getComponentPackageName() {
        return componentPackageName;
    }

    public String getComponentClassName() {
        return componentClassName;
    }

    public DataFlowConfigurableObjectMetadata getConfigurableObjectMetadata() {
        return configurableObjectMetadata;
    }

    public List<DataFlowComponentInputMetadata> getInputMetadata() {
        return inputMetadata;
    }

    public DataFlowComponentOutputMetadata getOutputMetadata() {
        return outputMetadata;
    }

    public boolean isAsync() {
        return isAsync;
    }

    public boolean hasAsyncGet() {
        return hasAsyncGet;
    }

    /**
     * See {@link #hasDynamicInput}
     */
    public boolean hasDynamicInput() {
        return hasDynamicInput;
    }

    public boolean isValueConverter() {
        return typeConverterMetadata != null;
    }

    public ValueTypeConverterMetadata getValueTypeConverterMetadata() {
        return typeConverterMetadata;
    }

    public String getBuilderClass() {
        return builderClass;
    }

    @Override
    public String toString() {
        return "DataFlowComponentMetadata{" +
                "typeName='" + typeName + '\'' +
                ", description='" + description + '\'' +
                ", componentPackageName='" + componentPackageName + '\'' +
                ", componentClassName='" + componentClassName + '\'' +
                ", configurableObjectMetadata=" + configurableObjectMetadata +
                ", inputMetadata=" + inputMetadata +
                ", outputMetadata=" + outputMetadata +
                ", isAsync=" + isAsync +
                ", hasAsyncGet=" + hasAsyncGet +
                ", hasDynamicInput=" + hasDynamicInput +
                ", isValueConverter=" + (typeConverterMetadata != null) +
                ", valueTypeConverterMetadata=" + typeConverterMetadata +
                ", builderClass=" + builderClass +
                ", codeGeneratorClass=" + codeGeneratorClass +
                '}';
    }


    public String getCodeGeneratorClass() {
        return codeGeneratorClass;
    }
}
