/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AvroDataRecordReader.java
 */

package dataflow.avro;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.data.DataRecord;
import dataflow.data.DataRecordReader;
import dataflow.core.datasource.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;
import org.apache.avro.Schema.Type;
import org.apache.avro.file.DataFileStream;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;

public class AvroDataRecordReader implements DataRecordReader {

    private final Schema schema;
    private final DataFileStream<GenericRecord> stream;

    @DataFlowConfigurable
    public AvroDataRecordReader(@DataFlowConfigProperty String schema,
        @DataFlowConfigProperty DataSource dataSource) {
        this.schema = new Schema.Parser().parse(schema);
        try {
            this.stream = new DataFileStream<>(dataSource.getInputStream(), new GenericDatumReader<>());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public AvroDataRecordReader(Schema schema, InputStream in) throws IOException {
        this.schema = schema;
        this.stream = new DataFileStream<>(in, new GenericDatumReader<>());
    }

    @Override
    public List<String> getColumnNames() {
        return schema.getFields().stream().map(Field::name).collect(Collectors.toList());
    }

    @Override
    public List<String> getKeyColumns() {
        return null;
    }

    @Override
    public void close() throws Exception {
        stream.close();
    }

    @Override
    public boolean hasNext() {
        return stream.hasNext();
    }

    @Override
    public DataRecord next() {
        GenericRecord recordData = stream.next();
        DataRecord record = new DataRecord();
        schema.getFields().forEach(field -> {
            Object value = recordData.get(field.name());
            if(field.schema().getType() == Type.STRING) {
                value = String.valueOf(value);
            }
            record.put(field.name(), value);
        });
        return record;
    }
}
