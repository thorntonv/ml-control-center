/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TypeUtil.java
 */

package dataflow.codegen.util;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings({"WeakerAccess", "unused"})
public class TypeUtil {

    private static final Map<String, String> PRIMITIVE_TYPE_OBJECT_TYPE_MAP = new HashMap<>();

    private static final Pattern GENERIC_PATTERN = Pattern.compile(".*?<(.*)>");

    static {
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(boolean.class.getName(), Boolean.class.getName());
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(int.class.getName(), Integer.class.getName());
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(long.class.getName(), Long.class.getName());
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(float.class.getName(), Float.class.getName());
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(double.class.getName(), Double.class.getName());
    }

    public static String getPackageName(String className) {
        int lastDot = className.lastIndexOf('.');
        return lastDot > 0 ? className.substring(0, lastDot) : "";
    }

    public static boolean isPrimitiveType(final String type) {
        return PRIMITIVE_TYPE_OBJECT_TYPE_MAP.containsKey(type);
    }

    public static String primitiveTypeToObjectType(String type) {
        return PRIMITIVE_TYPE_OBJECT_TYPE_MAP.getOrDefault(type, type);
    }

    public static String getSimpleClassName(String className) {
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            className = className.substring(lastDot + 1);
        }
        className = removeGenericTypes(className);
        return className;
    }

    public static String removeGenericTypes(String type) {
        int genericTypeStart = type.indexOf('<');
        int genericTypeEnd = type.lastIndexOf('>');
        if (genericTypeStart > 0 && genericTypeEnd > genericTypeStart) {
            type = type.substring(0, genericTypeStart) + type.substring(genericTypeEnd + 1);
        }
        return type;
    }

    public static String[] getGenericTypes(String type) {
        Matcher matcher = GENERIC_PATTERN.matcher(type);
        if (matcher.matches()) {
            String genericTypes = matcher.group(1);
            if (!genericTypes.trim().isEmpty()) {
                List<String> typesList = new ArrayList<>();
                int depth = 0;
                StringBuilder typeBuilder = new StringBuilder();
                for (char ch : genericTypes.toCharArray()) {
                    if (ch == '<') {
                        depth++;
                    } else if (ch == '>') {
                        depth--;
                    } else if (depth == 0 && (ch == ',')) {
                        typesList.add(typeBuilder.toString().trim());
                        typeBuilder.setLength(0);
                        continue;
                    }
                    typeBuilder.append(ch);
                }
                if (typeBuilder.length() > 0) {
                    typesList.add(typeBuilder.toString().trim());
                }
                return typesList.toArray(new String[]{});
            }
        }
        return new String[0];
    }
}
