/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowComponentMetadataBuilder.java
 */

package dataflow.codegen.util.annotation;

import dataflow.core.component.annotation.*;
import dataflow.core.component.metadata.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.*;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static dataflow.codegen.util.TypeUtil.getSimpleClassName;

/**
 * Builds {@link DataFlowComponentMetadata} for a component using introspection.
 */
public class DataFlowComponentMetadataBuilder {

    private static class InternalMetadata {

        String metadataSimpleClassName;
        String packageName;
        String componentSimpleClassName;
        boolean hasAsyncGet;
        boolean hasDynamicInput;
        String getValueMethodReturnType;
    }

    private final ProcessingEnvironment processingEnv;
    private final DataFlowConfigurableObjectMetadataBuilder configurableObjectMetadataBuilder;

    public DataFlowComponentMetadataBuilder(ProcessingEnvironment processingEnv) {
        this.processingEnv = processingEnv;
        this.configurableObjectMetadataBuilder = new DataFlowConfigurableObjectMetadataBuilder(processingEnv);
    }

    public DataFlowComponentMetadata buildMetadata(final TypeElement componentType) {
        InternalMetadata metadata = new InternalMetadata();

        String componentQualifiedName = componentType.getQualifiedName().toString();
        String componentClassName = processingEnv.getElementUtils().getBinaryName(componentType).toString();
        metadata.componentSimpleClassName = getSimpleClassName(componentQualifiedName);
        String metadataClassName = getComponentMetadataClassName(componentType, processingEnv);
        metadata.metadataSimpleClassName = getSimpleClassName(metadataClassName);
        metadata.packageName = processingEnv.getElementUtils().getPackageOf(componentType).getQualifiedName().toString();

        DataFlowComponent componentAnnotation = componentType.getAnnotation(DataFlowComponent.class);

        String typeName = metadata.componentSimpleClassName;
        if (!componentAnnotation.typeName().isEmpty()) {
            typeName = componentAnnotation.typeName();
        }
        DataFlowConfigurableObjectMetadata configurableObjectMetadata =
                configurableObjectMetadataBuilder.buildMetadata(componentType);

        List<DataFlowComponentInputMetadata> inputMetadata = buildInputMetadata(componentType, metadata);
        DataFlowComponentOutputMetadata outputMetadata = buildOutputMetadata(componentType, metadata);
        ValueTypeConverterMetadata typeConverterMetadata = buildValueTypeConverterMetadata(componentType,
                inputMetadata, outputMetadata, metadata);
        String builderClass = getComponentBuilderClassName(componentType, processingEnv);

        String codeGeneratorClass = null;
        AnnotationValue codeGeneratorClassValue = getAnnotationValue(componentType, DataFlowComponent.class, "codeGenerator");
        if (codeGeneratorClassValue != null) {
            codeGeneratorClass = ((TypeElement)this.processingEnv.getTypeUtils().asElement((TypeMirror) codeGeneratorClassValue.getValue())).getQualifiedName().toString();
        }
        boolean async = componentAnnotation.executeAsync() | metadata.hasAsyncGet;
        return new DataFlowComponentMetadata(typeName, componentAnnotation.description(), metadata.packageName,
                componentClassName, configurableObjectMetadata, inputMetadata, outputMetadata, builderClass,
                codeGeneratorClass, typeConverterMetadata, async,
                metadata.hasAsyncGet, metadata.hasDynamicInput);
    }

    public static String getComponentBuilderClassName(TypeElement componentElement, ProcessingEnvironment processingEnv) {
        return getComponentClassNamePrefix(componentElement, processingEnv) + "Builder";
    }

    public static String getComponentMetadataClassName(TypeElement componentElement, ProcessingEnvironment processingEnv) {
        return getComponentClassNamePrefix(componentElement, processingEnv) + "Metadata";
    }

    private static String getComponentClassNamePrefix(TypeElement componentElement, ProcessingEnvironment processingEnv) {
        String packageName = processingEnv.getElementUtils().getPackageOf(componentElement).getQualifiedName().toString();
        if(packageName == null || packageName.trim().isEmpty()) {
            throw new RuntimeException("No package name specified for " + componentElement.getQualifiedName().toString());
        }

        String className = componentElement.getQualifiedName().toString().substring(packageName.length());
        className = className.startsWith(".") ? className.substring(1) : className;
        className = className.replace(".", "_");
        className = className.replace("$", "_");
        String simpleClassName = getSimpleClassName(className);
        return packageName + "." + simpleClassName;
    }

    private List<DataFlowComponentInputMetadata> buildInputMetadata(
            final TypeElement componentType, final InternalMetadata metadata) {
        ExecutableElement getValueMethod = findGetValueMethod(componentType);

        List<DataFlowComponentInputMetadata> inputMetadata = new ArrayList<>();

        Set<String> inputNames = new HashSet<>();
        List<? extends VariableElement> parameters = getValueMethod.getParameters();
        for (Element param : parameters) {
            String name = param.getSimpleName().toString();
            String rawType = param.asType().toString();
            InputValue inputValue = param.getAnnotation(InputValue.class);
            InputValues inputValues = param.getAnnotation(InputValues.class);
            if (inputValue != null) {
                // A single input.

                if (metadata.hasDynamicInput) {
                    printAndThrowError(InputValue.class.getSimpleName() + " can not be used with " +
                            InputValues.class.getSimpleName(), componentType);
                }
                if (!inputValue.name().isEmpty()) {
                    // Use the name specified in the annotation instead of the getValue method param name.
                    name = inputValue.name();
                }
                if (inputNames.contains(name)) {
                    printAndThrowError("Duplicate input name " + name, componentType);
                }
                inputNames.add(name);

                inputMetadata.add(new DataFlowComponentInputMetadata(
                        name, rawType, inputValue.supportedTypes(), inputValue.required(), inputValue.description()));
            } else if (inputValues != null) {
                // All input values as a map.

                if (parameters.size() > 1) {
                    printAndThrowError(InputValues.class.getSimpleName() +
                            " annotated parameter must be the only method parameter", componentType);
                }

                inputMetadata.add(new DataFlowComponentInputMetadata(
                        name, rawType, new String[]{}, false, inputValues.description()));
                metadata.hasDynamicInput = true;
            } else {
                printAndThrowError("getValue parameters must be annotated with " + InputValue.class.getSimpleName() +
                        " or " + InputValues.class.getSimpleName(), componentType);
            }
        }
        return inputMetadata;
    }

    private DataFlowComponentOutputMetadata buildOutputMetadata(final TypeElement componentType,
            final InternalMetadata metadata) {
        ExecutableElement getValueMethod = findGetValueMethod(componentType);

        OutputValue outputValue = getValueMethod.getAnnotation(OutputValue.class);
        if (outputValue == null) {
            printAndThrowError("The getValue method must be annotated with " +
                    OutputValue.class.getSimpleName(), componentType);
            return null;
        }

        metadata.getValueMethodReturnType = getValueMethod.getReturnType().toString();

        // Check if the return type is a CompletableFuture.
        String rawType = metadata.getValueMethodReturnType;
        if (getValueMethod.getReturnType() instanceof DeclaredType) {
            DeclaredType returnType = (DeclaredType) getValueMethod.getReturnType();
            if (returnType.toString().contains(CompletableFuture.class.getName())) {
                metadata.hasAsyncGet = true;
                rawType = returnType.getTypeArguments().iterator().next().toString();
            }
        }
        return new DataFlowComponentOutputMetadata(rawType, outputValue.supportedTypes(), outputValue.description());
    }

    private ValueTypeConverterMetadata buildValueTypeConverterMetadata(final TypeElement componentType,
                                                                       List<DataFlowComponentInputMetadata> inputMetadata, DataFlowComponentOutputMetadata outputMetadata,
                                                                       InternalMetadata metadata) {
        ValueConverter valueConverter = componentType.getAnnotation(ValueConverter.class);
        if (valueConverter == null) {
            return null;
        }
        if (metadata.hasDynamicInput) {
            printAndThrowError("Value converter may not have dynamic input", componentType);
        }
        if (metadata.hasAsyncGet) {
            printAndThrowError("Value converter may not be async", componentType);
        }
        if (inputMetadata.isEmpty()) {
            printAndThrowError("Value converter getValue method must have at least 1 param", componentType);
        }
        if (inputMetadata.size() > 2) {
            printAndThrowError("Value converter getValue method can have at most 2 params", componentType);
        }
        if (!inputMetadata.get(0).isRequired()) {
            printAndThrowError("Value converter getValue method first param must be required", componentType);
        }
        if (inputMetadata.size() > 1 && !"java.lang.Class<?>[]".equals(inputMetadata.get(1).getRawType())) {
            printAndThrowError("Value converter getValue method second parameter must be of type Class<?>[] but was " +
                    inputMetadata.get(1).getRawType(), componentType);
        }
        if (Void.class.getName().equalsIgnoreCase(outputMetadata.getRawType())) {
            printAndThrowError("Value converter getValue method must return a value", componentType);
        }

        String fromType = inputMetadata.get(0).getRawType();
        String toType = outputMetadata.getRawType();
        return new ValueTypeConverterMetadata(valueConverter.lossy(), fromType, toType);
    }

    private ExecutableElement findGetValueMethod(final TypeElement componentType) {
        List<Element> getMethods = componentType.getEnclosedElements().stream()
                .filter(e -> e.getKind() == ElementKind.METHOD &&
                        e.getModifiers().contains(Modifier.PUBLIC) &&
                        !e.getModifiers().contains(Modifier.STATIC) &&
                        !e.getModifiers().contains(Modifier.ABSTRACT) &&
                        "getValue".equals(e.getSimpleName().toString()) &&
                        e.getAnnotation(OutputValue.class) != null)
                .collect(Collectors.toList());
        if (getMethods.size() != 1) {
            printAndThrowError("ValueProvider must have a single public getValue method that is annotated with the " +
                    OutputValue.class.getSimpleName() + " annotation", componentType);
        }
        return (ExecutableElement) getMethods.get(0);
    }

    private static AnnotationValue getAnnotationValue(TypeElement element, Class<?> annotationClass, String key) {
        String className = annotationClass.getName();
        AnnotationMirror annotationMirror = element.getAnnotationMirrors().stream()
                .filter(m -> m.getAnnotationType().toString().equals(className))
                .findAny().orElse(null);
        if(annotationMirror != null) {
            return annotationMirror.getElementValues().entrySet().stream()
                    .filter(entry -> entry.getKey().getSimpleName().toString().equals(key))
                    .map(Map.Entry::getValue).findAny().orElse(null);
        }
        return null;
    }

    private void printAndThrowError(final String msg, final TypeElement element) {
        AnnotationProcessorUtil.printAndThrowError(msg, element, processingEnv);
    }
}
