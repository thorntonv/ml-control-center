package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_staticGetValueMethod {

    @DataFlowConfigurable
    public InvalidValueProvider_staticGetValueMethod() {
    }

    @OutputValue
    public static String getValue(@InputValue String in1) {
        return null;
    }
}
