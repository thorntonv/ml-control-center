package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.InputValues;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_inputValuesAndOtherParam {

    @DataFlowConfigurable
    public InvalidValueProvider_inputValuesAndOtherParam() {
    }

    @OutputValue
    public String getValue(@InputValues Map<String, Object> values, @InputValue String in1) {
        return null;
    }
}
