package dataflow.test;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_missingConstructorAnnotation {

    public InvalidValueProvider_missingConstructorAnnotation() {
    }

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}
