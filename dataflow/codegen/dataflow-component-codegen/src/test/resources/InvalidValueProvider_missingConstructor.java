package dataflow.test;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_missingConstructor {

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}
