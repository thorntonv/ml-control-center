/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider3Test.java
 */

package dataflow.test;

import com.google.common.collect.ImmutableMap;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.registry.DataFlowRegistry;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestValueProvider3Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider3Metadata.INSTANCE;

    private DataFlowRegistry registry;
    private DataFlowEnvironment env;
    private TestValueProvider3Builder builder;

    @Before
    public void setUp() {
        registry = new DataFlowRegistry();
        env = new DataFlowEnvironment();
        builder = new TestValueProvider3Builder(env);
    }

    @Test
    public void testMetadata() {
        assertFalse(METADATA.hasDynamicInput());
        assertFalse(METADATA.isAsync());
        assertFalse(METADATA.hasAsyncGet());

        assertEquals("float", METADATA.getConfigurableObjectMetadata().getPropertyMetadata("prop1").getRawType());

        assertEquals("count", METADATA.getInputMetadata().get(0).getName());
        assertEquals("int", METADATA.getInputMetadata().get(0).getRawType());
        assertEquals("str", METADATA.getInputMetadata().get(1).getName());
        assertEquals(String.class.getName(), METADATA.getInputMetadata().get(1).getRawType());

        assertEquals(String.class.getName(), METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder_primitives() {
        TestValueProvider3 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("prop1", 3.14f)
                .build(), ImmutableMap.<String, Object>builder()
                .build());
        assertEquals("*****", valueProvider.getValue(5, "*"));
        assertEquals(3.14f, valueProvider.getProp1(), .001);
    }

    @Test
    public void testBuilder_primitives_missing() {
        try {
            builder.build(ImmutableMap.<String, Object>builder()
                    .build(), ImmutableMap.<String, Object>builder()
                    .build());
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            // Expected.
        }
    }
}
