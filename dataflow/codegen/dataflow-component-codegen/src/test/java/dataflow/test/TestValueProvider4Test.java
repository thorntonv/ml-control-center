/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider4Test.java
 */

package dataflow.test;

import com.google.common.collect.ImmutableMap;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.environment.DataFlowEnvironment;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestValueProvider4Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider4Metadata.INSTANCE;

    private TestValueProvider4Builder builder;

    @Before
    public void setUp() {
        final DataFlowEnvironment env = new DataFlowEnvironment();
        builder = new TestValueProvider4Builder(env);
    }

    @Test
    public void testMetadata() {
        assertFalse(METADATA.hasDynamicInput());
        assertTrue(METADATA.isAsync());
        assertFalse(METADATA.hasAsyncGet());

        assertTrue(METADATA.getConfigurableObjectMetadata().getPropertyMetadata().isEmpty());
        assertTrue(METADATA.getInputMetadata().isEmpty());

        assertEquals("int", METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder_primitives() {
        TestValueProvider4 valueProvider = builder.build(ImmutableMap.<String, Object>builder()
                .put("prop1", 3.14f)
                .build(), ImmutableMap.<String, Object>builder()
                .build());
        assertEquals(10, valueProvider.getValue());
    }
}
