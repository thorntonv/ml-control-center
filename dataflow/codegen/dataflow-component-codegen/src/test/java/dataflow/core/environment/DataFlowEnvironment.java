package dataflow.core.environment;

import dataflow.core.registry.DataFlowRegistry;

public class DataFlowEnvironment {
    private DataFlowRegistry registry = new DataFlowRegistry();
    public DataFlowRegistry getRegistry() {
        return registry;
    }
}
