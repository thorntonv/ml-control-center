package dataflow.core.util;

import dataflow.core.config.RawConfigurableObjectConfig;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.test.TestPropertyBuilder;

import java.util.List;
import java.util.Map;

public class BuilderUtil {
    public static Object buildListObject(List testList, String s, Map<String, Object> instanceValues, DataFlowRegistry registry) {
        return testList;
    }

    public static Object buildObject(Object type, RawConfigurableObjectConfig rawConfigurableObjectConfig, Map<String, Object> instanceValues, DataFlowRegistry registry) {
        if(rawConfigurableObjectConfig.getType().equals("TestProperty")) {
            return new TestPropertyBuilder(new DataFlowEnvironment()).build(rawConfigurableObjectConfig, instanceValues);
        }
        return rawConfigurableObjectConfig;
    }
}
