package dataflow.core.engine;

public class ValueReference {

    private final String componentId;

    public ValueReference(String componentId) {
        this.componentId = componentId;
    }

    public String getComponentId() {
        return componentId;
    }
}
