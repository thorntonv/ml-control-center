package dataflow.core.config;

public interface DataFlowConfigurableObjectBuilder {

    default <T> T injectDependencies(T obj) {
        return obj;
    }
}
