package dataflow.core.config;

import com.google.common.collect.ImmutableMap;

import java.util.LinkedHashMap;

public class RawConfigurableObjectConfig extends LinkedHashMap {

    private final String type;

    public <V, K> RawConfigurableObjectConfig(String type, ImmutableMap<K,V> of) {
        super(of);
        this.type = type;
    }

    public Object getType() {
        return type;
    }
}
