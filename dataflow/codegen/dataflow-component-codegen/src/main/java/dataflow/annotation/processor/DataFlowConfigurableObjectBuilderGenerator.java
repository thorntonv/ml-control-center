/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurableObjectBuilderGenerator.java
 */

package dataflow.annotation.processor;

import com.google.auto.service.AutoService;
import dataflow.codegen.util.annotation.DataFlowConfigurableObjectMetadataBuilder;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.metadata.DataFlowConfigurableObjectMetadata;
import dataflow.core.component.metadata.DataFlowConfigurableObjectPropertyMetadata;
import dataflow.core.util.IndentPrintWriter;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

import static dataflow.codegen.util.annotation.AnnotationProcessorUtil.*;
import static dataflow.codegen.util.annotation.DataFlowComponentMetadataBuilder.getComponentBuilderClassName;
import static dataflow.codegen.util.annotation.DataFlowConfigurableObjectMetadataBuilder.SIMPLE_PROPERTY_TYPES;
import static dataflow.codegen.util.TypeUtil.*;


/**
 * Generates the source code for builders of objects that are {@link dataflow.core.component.annotation.DataFlowConfigurable}.
 */
@SupportedAnnotationTypes("dataflow.core.component.annotation.DataFlowConfigurable")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
@AutoService(Processor.class)
@SuppressWarnings("unused")
public class DataFlowConfigurableObjectBuilderGenerator extends AbstractProcessor {

    private static final String[] IMPORT_CLASSES = new String[]{
            Set.class.getName(),
            HashSet.class.getName(),
            Map.class.getName(),
            "java.util.Map.Entry",
            HashMap.class.getName(),
            List.class.getName(),
            ArrayList.class.getName(),
            BiFunction.class.getName(),
            Function.class.getName(),
            Optional.class.getName(),
            DataFlowConfigurableObjectMetadata.class.getName(),
            DataFlowConfigurableObjectPropertyMetadata.class.getName(),
            "dataflow.core.environment.DataFlowEnvironment",
            "dataflow.core.engine.DataFlowExecutionContext",
            "dataflow.core.config.DataFlowConfigurableObjectBuilder",
            "dataflow.core.engine.ValueReference",
            "dataflow.core.type.ValueType",
            "dataflow.core.config.RawConfigurableObjectConfig",
            "dataflow.core.util.BuilderUtil",
            "static dataflow.core.type.TypeUtil.convert",
            "static dataflow.core.type.TypeUtil.valueTypeFromString"

};

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        DataFlowConfigurableObjectMetadataBuilder metadataBuilder = new DataFlowConfigurableObjectMetadataBuilder(processingEnv);

        Map<TypeElement, DataFlowConfigurableObjectMetadata> classMetadataMap = new HashMap<>();
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            // For each constructor annotated with DataFlowConfigurable
            for (Element constructorElement : annotatedElements) {
                try {
                    if (constructorElement.getKind() != ElementKind.CONSTRUCTOR) {
                        printAndThrowError(DataFlowConfigurable.class.getSimpleName() +
                                " can only be used on a constructor", constructorElement, processingEnv);
                    }
                    if (!constructorElement.getModifiers().contains(Modifier.PUBLIC)) {
                        printAndThrowError(DataFlowConfigurable.class.getSimpleName() +
                                " annotated constructor should be public", constructorElement, processingEnv);
                    }
                    // Get the class that has this constructor.
                    TypeElement classElement = (TypeElement) constructorElement.getEnclosingElement();
                    String classType = classElement.asType().toString();

                    if (!classElement.getModifiers().contains(Modifier.ABSTRACT) &&
                            classElement.getKind() == ElementKind.CLASS) {
                        if (classElement.getEnclosingElement().getKind() == ElementKind.CLASS &&
                                !classElement.getModifiers().contains(Modifier.STATIC)) {
                            printAndThrowError(DataFlowConfigurable.class.getSimpleName() +
                                    " class should be static when nested in another class", constructorElement, processingEnv);
                        }
                        DataFlowConfigurableObjectMetadata metadata = metadataBuilder.buildMetadata(classElement);
                        classMetadataMap.put(classElement, metadata);
                    }
                } catch (Exception e) {
                    printAndThrowError("Error generating builder metadata: " + e.getMessage(), constructorElement, processingEnv);
                }
            }
        }

        for (Map.Entry<TypeElement, DataFlowConfigurableObjectMetadata> entry : classMetadataMap.entrySet()) {
            try {
                writePropertyBuilderFile(entry.getKey(), entry.getValue());
            } catch (IOException e) {
                throw new RuntimeException("Error generating builder", e);
            }
        }
        return true;
    }

    private void writePropertyBuilderFile(TypeElement configurableObjectClass,
            DataFlowConfigurableObjectMetadata metadata) throws IOException {
        String packageName = processingEnv.getElementUtils().getPackageOf(configurableObjectClass).getQualifiedName().toString();
        if(packageName == null || packageName.trim().isEmpty()) {
            throw new RuntimeException("No package name specified for " + configurableObjectClass.getQualifiedName().toString());
        }

        String builderClassName = getComponentBuilderClassName(configurableObjectClass, processingEnv);
        String builderSimpleClassName = getSimpleClassName(builderClassName);
        String objectClassName = getSimpleClassName(configurableObjectClass.getSimpleName().toString());

        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(builderClassName);
        try (PrintWriter fileWriter = new PrintWriter(builderFile.openWriter())) {

            IndentPrintWriter out = new IndentPrintWriter(fileWriter);

            out.printf("package %s;%n", packageName);
            out.println();

            List<String> additionalImports = new ArrayList<>();
            additionalImports.add(configurableObjectClass.getQualifiedName().toString().replace("$", "."));
            writeImports(additionalImports, out);

            out.printf("public class %s implements DataFlowConfigurableObjectBuilder {%n", builderSimpleClassName);
            out.println();
            out.indent();

            writeFields(metadata, out);
            writeConstructor(builderSimpleClassName, out);
            writeBuildMethod(objectClassName, metadata, out);

            out.unindent();
            out.println("}");
        }
    }

    private void writeFields(DataFlowConfigurableObjectMetadata metadata, IndentPrintWriter out) {
        out.printf("public static final DataFlowConfigurableObjectMetadata METADATA = %s;\n",
                newDataFlowConfigurableObjectMetadataStmt(metadata));
        out.println("private DataFlowEnvironment env;");
        out.println();
    }

    public static String newDataFlowConfigurableObjectMetadataStmt(DataFlowConfigurableObjectMetadata metadata) {
        StringWriter stringWriter = new StringWriter();
        IndentPrintWriter out = new IndentPrintWriter(new PrintWriter(stringWriter));
        out.printf("new DataFlowConfigurableObjectMetadata(%s, %s",
                quote(metadata.getClassName()),
                quote(metadata.getDescription()));
        out.indent();
        boolean first = true;
        for (DataFlowConfigurableObjectPropertyMetadata propertyMetadata : metadata.getPropertyMetadata()) {
            out.printf(",\nnew DataFlowConfigurableObjectPropertyMetadata(%s, %s, %s, %s, %s, %s)\n",
                    quote(propertyMetadata.getName()),
                    quote(propertyMetadata.getRawType()),
                    String.valueOf(propertyMetadata.isEnum()),
                    buildArrayString(propertyMetadata.getSupportedTypes()),
                    String.valueOf(propertyMetadata.isRequired()),
                    quote(javaEscape(propertyMetadata.getDescription())));
        }

        out.unindent();
        out.println(")");

        return stringWriter.toString();
    }

    private void writeConstructor(String simpleClassName, IndentPrintWriter out) {
        out.printf("public %s(DataFlowEnvironment env) {%n", simpleClassName);
        out.indent();
        out.println("this.env = env;");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeBuildMethod(String objectType, DataFlowConfigurableObjectMetadata metadata, IndentPrintWriter out) {
        out.printf("public %s build(Map<String, Object> propertyValues, Map<String, Object> instanceValues) {%n", objectType);
        out.indent();

        StringBuilder params = new StringBuilder();
        for (DataFlowConfigurableObjectPropertyMetadata propertyMetadata : metadata.getPropertyMetadata()) {
            String paramName = propertyMetadata.getName();
            if (params.length() > 0) {
                params.append(", ");
            }
            params.append(paramName);

            String typeWithoutGenerics = removeGenericTypes(propertyMetadata.getRawType());

            String propertyExpression = String.format("propertyValues.get(\"%s\")", paramName);
            String rawPropertyExpression = propertyExpression;

            if (propertyMetadata.isRequired() || isPrimitiveType(propertyMetadata.getRawType())) {
                out.printf("if(%s == null) {%n", propertyExpression);
                out.indent();
                out.printf("throw new IllegalArgumentException(\"%s %s property is not defined\");%n",
                        getSimpleClassName(objectType), paramName);
                out.unindent();
                out.println("}");
                out.println();
            }

            if (propertyMetadata.isEnum()) {
                propertyExpression = String.format("(%s != null) ? %s.valueOf((String)%s) : null",
                        propertyExpression, propertyMetadata.getRawType(), propertyExpression);
            } else if (List.class.getName().equals(typeWithoutGenerics)) {
                String[] genericTypes = getGenericTypes(propertyMetadata.getRawType());
                if (genericTypes.length == 1) {
                    String genericType = genericTypes[0];
                    if (!SIMPLE_PROPERTY_TYPES.contains(genericType)) {
                        propertyExpression = String.format("BuilderUtil.buildListObject((List) %s, \"%s\", instanceValues, env.getRegistry())",
                                propertyExpression, genericType);
                    }
                }
            } else if (Map.class.getName().equals(typeWithoutGenerics)) {
                String[] genericTypes = getGenericTypes(propertyMetadata.getRawType());
                if (genericTypes.length == 2) {
                    String keyGenericType = genericTypes[0];
                    String valueGenericType = genericTypes[1];
                    if (!SIMPLE_PROPERTY_TYPES.contains(keyGenericType) ||
                        !SIMPLE_PROPERTY_TYPES.contains(valueGenericType)) {
                        propertyExpression = String.format(
                            "BuilderUtil.buildMapObject((Map<?,?>) %s, \"%s\", \"%s\", instanceValues, env.getRegistry())",
                            propertyExpression, keyGenericType, valueGenericType);
                    }
                }
            } else if (!SIMPLE_PROPERTY_TYPES.contains(typeWithoutGenerics)) {
                out.printf("Optional<RawConfigurableObjectConfig> %sObj = (%s != null && %s instanceof RawConfigurableObjectConfig) ? Optional.of((RawConfigurableObjectConfig)%s) : Optional.empty();%n",
                        paramName, propertyExpression, propertyExpression, propertyExpression);
                propertyExpression = String.format(
                        "%sObj.isPresent() ? BuilderUtil.buildObject(%sObj.get().getType(), %sObj.get(), instanceValues, env.getRegistry()) : %s",
                        paramName, paramName, paramName, propertyExpression);
            }
            propertyExpression = String.format("((%s instanceof ValueReference) ? " +
                    "instanceValues.get(((ValueReference) %s).getComponentId()) : %s)",
                    rawPropertyExpression, rawPropertyExpression, propertyExpression);

            out.printf("%s %s = (%s) convert(%s, valueTypeFromString(\"%s\"), env.getRegistry().getTypeRegistry());%n",
                propertyMetadata.getRawType(), paramName, propertyMetadata.getRawType(),
                propertyExpression, propertyMetadata.getRawType());

            if (propertyMetadata.isRequired() && !isPrimitiveType(propertyMetadata.getRawType())) {
                out.printf("if (%s == null) {%n", paramName);
                out.indent();
                out.printf("throw new IllegalArgumentException(\"%s %s property is not defined\");%n",
                        getSimpleClassName(objectType), paramName);
                out.unindent();
                out.println("}");
            }
            out.println();
        }
        out.printf("%s obj = new %s(%s);%n", objectType, objectType, params);

        out.println("return injectDependencies(obj);");
        out.unindent();
        out.println("}");
    }

    private void writeImports(List<String> additionalImports, IndentPrintWriter out) {
        for (String importClass : IMPORT_CLASSES) {
            out.printf("import %s;%n", importClass);
        }
        for (String importClass : additionalImports) {
            out.printf("import %s;%n", importClass);
        }
        out.println();
    }
}
