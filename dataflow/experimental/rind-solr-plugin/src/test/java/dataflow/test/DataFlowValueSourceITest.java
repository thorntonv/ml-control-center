/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowValueSourceITest.java
 */

package dataflow.test;

import org.apache.solr.SolrTestCaseJ4;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.carrotsearch.randomizedtesting.annotations.ThreadLeakFilters;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;


@ThreadLeakFilters(filters = {IgnoreThreadFilter.class})
public class DataFlowValueSourceITest extends SolrTestCaseJ4 {

    @BeforeClass
    public static void beforeClass() throws Exception {
        System.setProperty("ENVIRONMENT", "unit_test");
        initCore("solrconfig.xml", "schema.xml", getFile("collection1").getParentFile().getAbsolutePath());
    }

    @Before
    public void loadData() {
        clearIndex();
        ImmutableList.Builder<String> builder;

        builder = new ImmutableList.Builder<>();
        builder.add("uniqueId").add("0");
        builder.add("jobListingId").add("0");
        builder.add("employerId").add("883614");
        builder.add("jobTitleHashCode").add("980638789");
        builder.add("cityId").add("1140231");
        builder.add("stateId").add("CA");
        builder.add("cpc").add(".017");
        builder.add("rating").add("5.3");
        builder.add("hidden").add("false");
        assertU(adoc(Iterables.toArray(builder.build(), String.class)));

        builder = new ImmutableList.Builder<>();
        builder.add("uniqueId").add("1");
        builder.add("jobListingId").add("1");
        builder.add("employerId").add("866893");
        builder.add("jobTitleHashCode").add("962999239");
        builder.add("cityId").add("1144859");
        builder.add("stateId").add("FL");
        builder.add("cpc").add(".107");
        builder.add("rating").add("2.1");
        builder.add("hidden").add("true");
        assertU(adoc(Iterables.toArray(builder.build(), String.class)));

        assertU(commit());
    }

    @Test(timeout = 600000)
    public void testIntDocValue() {
        assertQ(req("qt", "select", "q", "*:*", "fq", "",
                "fl", "uniqueId,jobListingId,$score",
                "pclickScore", ".15",
                "testSignal", ".05",
                "score",
                "dataflow(testIntDocValue,2,query,home_health_care_nurse,string,userId,44782864,long,1,pclick,$pclickScore)",
                "sort", "uniqueId asc"
                ),
                "//*[@numFound='2']",
                "//result/doc[1]/str[@name='uniqueId'][.='0']",
                "//result/doc[2]/str[@name='uniqueId'][.='1']",
                "//result/doc[1]/int[@name='$score'][.=883614]",
                "//result/doc[2]/int[@name='$score'][.=866893]"
        );
    }

    @Test(timeout = 600000)
    public void testLongDocValue() {
        assertQ(req("qt", "select", "q", "*:*", "fq", "",
                "fl", "uniqueId,jobListingId,$score",
                "pclickScore", ".15",
                "testSignal", ".05",
                "score",
                "dataflow(testLongDocValue,2,query,home_health_care_nurse,string,userId,44782864,long,1,pclick,$pclickScore)",
                "sort", "uniqueId asc"
                ),
                "//*[@numFound='2']",
                "//result/doc[1]/str[@name='uniqueId'][.='0']",
                "//result/doc[2]/str[@name='uniqueId'][.='1']",
                "//result/doc[1]/long[@name='$score'][.=980638789]",
                "//result/doc[2]/long[@name='$score'][.=962999239]"
        );
    }

    @Test(timeout = 600000)
    public void testFloatDocValue() {
        assertQ(req("qt", "select", "q", "*:*", "fq", "",
                "fl", "uniqueId,jobListingId,$score",
                "pclickScore", ".15",
                "testSignal", ".05",
                "score",
                "dataflow(testFloatDocValue,2,query,home_health_care_nurse,string,userId,44782864,long,1,pclick,$pclickScore)",
                "sort", "uniqueId asc"
                ),
                "//*[@numFound='2']",
                "//result/doc[1]/str[@name='uniqueId'][.='0']",
                "//result/doc[2]/str[@name='uniqueId'][.='1']",
                "//result/doc[1]/float[@name='$score'][.=.017]",
                "//result/doc[2]/float[@name='$score'][.=.107]"
        );
    }

    @Test(timeout = 600000)
    public void testDoubleDocValue() {
        assertQ(req("qt", "select", "q", "*:*", "fq", "",
                "fl", "uniqueId,jobListingId,$score",
                "pclickScore", ".15",
                "testSignal", ".05",
                "score",
                "dataflow(testDoubleDocValue,2,query,home_health_care_nurse,string,userId,44782864,long,1,pclick,$pclickScore)",
                "sort", "uniqueId asc"
                ),
                "//*[@numFound='2']",
                "//result/doc[1]/str[@name='uniqueId'][.='0']",
                "//result/doc[2]/str[@name='uniqueId'][.='1']",
                "//result/doc[1]/double[@name='$score'][.=5.3]",
                "//result/doc[2]/double[@name='$score'][.=2.1]"
        );
    }

    @Test(timeout = 600000)
    public void testBooleanDocValue() {
        assertQ(req("qt", "select", "q", "*:*", "fq", "",
                "fl", "uniqueId,jobListingId,$score",
                "pclickScore", ".15",
                "testSignal", ".05",
                "score",
                "dataflow(testBooleanDocValue,2,query,home_health_care_nurse,string,userId,44782864,long,1,pclick,$pclickScore)",
                "sort", "uniqueId asc"
                ),
                "//*[@numFound='2']",
                "//result/doc[1]/str[@name='uniqueId'][.='0']",
                "//result/doc[2]/str[@name='uniqueId'][.='1']",
                "//result/doc[1]/bool[@name='$score'][.='false']",
                "//result/doc[2]/bool[@name='$score'][.='true']"
        );
    }

    @Test(timeout = 600000)
    public void testStringDocValue() {
        assertQ(req("qt", "select", "q", "*:*", "fq", "",
                "fl", "uniqueId,jobListingId,$score",
                "pclickScore", ".15",
                "testSignal", ".05",
                "score",
                "dataflow(testStringDocValue,2,query,home_health_care_nurse,string,userId,44782864,long,1,pclick,$pclickScore)",
                "sort", "uniqueId asc"
                ),
                "//*[@numFound='2']",
                "//result/doc[1]/str[@name='uniqueId'][.='0']",
                "//result/doc[2]/str[@name='uniqueId'][.='1']",
                "//result/doc[1]/str[@name='$score'][.='CA']",
                "//result/doc[2]/str[@name='$score'][.='FL']"
        );
    }

    @Test(timeout = 600000)
    public void testFunctionValue() {
        assertQ(req("qt", "select", "q", "*:*", "fq", "",
                "fl", "uniqueId,jobListingId,$score",
                "pclickScore", ".15",
                "testSignal", ".05",
                "score",
                "dataflow(testFunctionValue,2,query,home_health_care_nurse,string,userId,44782864,long,1,pclick,$pclickScore)",
                "sort", "uniqueId asc"
                ),
                "//*[@numFound='2']",
                "//result/doc[1]/str[@name='uniqueId'][.='0']",
                "//result/doc[2]/str[@name='uniqueId'][.='1']",
                "//result/doc[1]/double[@name='$score'][.=.15]",
                "//result/doc[2]/double[@name='$score'][.=.15]"
        );
    }

    @Test(timeout = 600000)
    public void testProvidedValue() {
        assertQ(req("qt", "select", "q", "*:*", "fq", "",
                "fl", "uniqueId,jobListingId,$score",
                "pclickScore", ".15",
                "testSignal", ".05",
                "score",
                "dataflow(testProvidedValue,2,query,home_health_care_nurse,string,userId,10,long,1,pclick,$pclickScore)",
                "sort", "uniqueId asc"
                ),
                "//*[@numFound='2']",
                "//result/doc[1]/str[@name='uniqueId'][.='0']",
                "//result/doc[2]/str[@name='uniqueId'][.='1']",
                "//result/doc[1]/long[@name='$score'][.=883624]",
                "//result/doc[2]/long[@name='$score'][.=866903]"
        );
    }

    @Test(timeout = 600000)
    public void testWrongFieldType() {
        try {
            assertQ(req("qt", "select", "q", "*:*", "fq", "",
                    "fl", "uniqueId,jobListingId,$score",
                    "pclickScore", ".15",
                    "testSignal", ".05",
                    "score",
                    "dataflow(testWrongFieldType,2,query,home_health_care_nurse,string,userId,44782864,long,1,pclick,$pclickScore)",
                    "sort", "uniqueId asc"
                    ),
                    "//*[@numFound='2']",
                    "//result/doc[1]/str[@name='uniqueId'][.='0']",
                    "//result/doc[2]/str[@name='uniqueId'][.='1']",
                    "//result/doc[1]/int[@name='$score'][.=883614]",
                    "//result/doc[2]/int[@name='$score'][.=866893]"
            );
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Field jobListingId type should be an instance of IntValueFieldType, but was TrieLongField",
                    ex.getCause().getMessage());
        }
    }
}



