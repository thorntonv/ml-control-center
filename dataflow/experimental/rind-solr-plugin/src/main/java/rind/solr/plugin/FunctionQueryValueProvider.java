/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FunctionQueryValueProvider.java
 */

package rind.solr.plugin;

import java.io.IOException;
import java.util.Map;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.queries.function.FunctionValues;
import org.apache.lucene.queries.function.ValueSource;
import org.apache.lucene.queries.function.docvalues.BoolDocValues;
import org.apache.lucene.queries.function.docvalues.DoubleDocValues;
import org.apache.lucene.queries.function.docvalues.FloatDocValues;
import org.apache.lucene.queries.function.docvalues.IntDocValues;
import org.apache.lucene.queries.function.docvalues.LongDocValues;
import org.apache.lucene.queries.function.docvalues.StrDocValues;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.type.BooleanValue;
import dataflow.core.type.DoubleValue;
import dataflow.core.type.FloatValue;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.LongValue;

@DataFlowComponent
public class FunctionQueryValueProvider {

    interface DocValueSupplier {

        Object getValue(int docId);
    }

    private String functionQuery;
    private DocValueSupplier docValueSupplier;

    @DataFlowConfigurable
    public FunctionQueryValueProvider(@DataFlowConfigProperty String functionQuery) {
        this.functionQuery = functionQuery;
    }

    @OutputValue
    public Object getValue(@InputValue int docId) {
        return docValueSupplier.getValue(docId);
    }

    public void init(ValueSource valueSource, Map map, LeafReaderContext readerContext) throws IOException {
        FunctionValues functionValues = valueSource.getValues(map, readerContext);
        if (functionValues instanceof IntDocValues) {
            IntegerValue docValue = new IntegerValue();
            docValueSupplier = docId -> docValue.setValue(functionValues.intVal(docId));
        } else if (functionValues instanceof LongDocValues) {
            LongValue docValue = new LongValue();
            docValueSupplier = docId -> docValue.setValue(functionValues.longVal(docId));
        } else if (functionValues instanceof FloatDocValues) {
            FloatValue docValue = new FloatValue();
            docValueSupplier = docId -> docValue.setValue(functionValues.floatVal(docId));
        } else if (functionValues instanceof DoubleDocValues) {
            DoubleValue docValue = new DoubleValue();
            docValueSupplier = docId -> docValue.setValue(functionValues.doubleVal(docId));
        } else if (functionValues instanceof BoolDocValues) {
            BooleanValue docValue = new BooleanValue();
            docValueSupplier = docId -> docValue.setValue(functionValues.boolVal(docId));
        } else if (functionValues instanceof StrDocValues) {
            docValueSupplier = functionValues::strVal;
        } else {
            docValueSupplier = functionValues::objectVal;
        }
    }

    public boolean isAvailable() {
        return docValueSupplier != null;
    }
}
