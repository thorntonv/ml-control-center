/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfiguration.java
 */

package dataflow.spring;

import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.registry.DataFlowRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class DataFlowConfiguration {

    @Autowired
    private ApplicationContext appContext;

    @Bean
    public SpringDependencyInjector springDependencyInjector() {
        return new SpringDependencyInjector(appContext);
    }

    @Bean
    public DataFlowEnvironment dataFlowEnvironment() {
        return new DataFlowEnvironment("core", springDependencyInjector(), null);
    }

    @Bean
    public DataFlowRegistry dataFlowRegistry() throws IOException {
        DataFlowEnvironment env = dataFlowEnvironment();
        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig = discoveryConfigParser.parse(getClass().getResourceAsStream("/dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
        return env.getRegistry();
    }

}
