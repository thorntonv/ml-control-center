/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowCodeGenerationContext.java
 */

package dataflow.core.codegen;

import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.config.ComponentConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.registry.ComponentRegistry;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.registry.TypeRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static dataflow.core.codegen.CodeGenerationUtil.*;


/**
 * A class that holds objects/data that are useful for code generation.
 */
@SuppressWarnings("WeakerAccess")
public class DataFlowCodeGenerationContext {

    private static final Logger logger = LoggerFactory.getLogger(DataFlowCodeGenerationContext.class);
    public class ComponentCodeGenerationContext {

        private String id;
        private ComponentConfig componentConfig;
        private DataFlowComponentMetadata componentMetadata;

        public ComponentCodeGenerationContext(final ComponentConfig componentConfig) {
            this.id = componentConfig.getId();
            this.componentConfig = componentConfig;
            if(componentConfig.getType() != null) {
                this.componentMetadata = getComponentRegistry().getComponentMetadata(componentConfig.getType());
            }
        }

        public String getId() {
            return id;
        }

        public String getVarNamePrefix() {
            return getVarName(id);
        }

        public ComponentConfig getComponentConfig() {
            return componentConfig;
        }

        public String getRawOutputType() {
            return DataFlowCodeGenerationContext.this.getRawOutputType(id);
        }

        public String getOutputType() {
            return DataFlowCodeGenerationContext.this.getOutputType(id);
        }

        public String getFieldType()  {
            return DataFlowCodeGenerationContext.this.getFieldType(id);
        }

        public boolean hasPrimitiveOutputType() {
            return DataFlowCodeGenerationContext.this.hasPrimitiveOutputType(id);
        }

        public DataFlowComponentMetadata getComponentMetadata() {
            return componentMetadata;
        }

        public DataFlowCodeGenerationContext getDataFlowContext() {
            return DataFlowCodeGenerationContext.this;
        }

        public boolean isAsync() {
            return componentMetadata != null && componentMetadata.isAsync();
        }

        /**
         * Returns true if events are enabled for this component. Events are enabled if the flag is set in the
         * component config and events are enabled for the DataFlow.
         */
        public boolean isEventsEnabled() {
            return DataFlowCodeGenerationContext.this.isEventsEnabled() && componentConfig.isEventsEnabled();
        }

        public ComponentRegistry getEngine() {
            return DataFlowCodeGenerationContext.this.getComponentRegistry();
        }
    }

    private DataFlowConfig dataFlowConfig;
    private Map<String, ComponentCodeGenerationContext> componentContextMap = new HashMap<>();
    private List<ComponentConfig> sortedConfigs;
    private Map<String, Object> constValueMap;

    /**
     * See {@link #getRawOutputType(String)}
     */
    private Map<String, String> rawComponentOutputTypeMap;

    /**
     * See {@link #getOutputType(String)}
     */
    private Map<String, String> componentOutputTypeMap;

    private Set<String> inProgressAsyncComponentIds = new HashSet<>();
    private DataFlowInstanceCodeGenConfig codeGenConfig;
    private DataFlowRegistry registry;

    public DataFlowCodeGenerationContext(final DataFlowConfig dataFlowConfig,
                                         final List<ComponentConfig> sortedConfigs, final DataFlowInstanceCodeGenConfig codeGenConfig,
                                         final DataFlowRegistry registry) {
        this.dataFlowConfig = dataFlowConfig;
        this.codeGenConfig = codeGenConfig;
        this.sortedConfigs = sortedConfigs;
        this.registry = registry;
        sortedConfigs.forEach(
                v -> componentContextMap.put(v.getId(), new ComponentCodeGenerationContext(v)));
        this.constValueMap = buildConstValueMap(sortedConfigs);
        this.rawComponentOutputTypeMap = buildRawTypeMap(sortedConfigs);
        this.componentOutputTypeMap = buildOutputTypeMap(sortedConfigs);
    }

    public DataFlowConfig getDataFlowConfig() {
        return dataFlowConfig;
    }

    public String getInstanceClassName() {
        return codeGenConfig.getClassName();
    }

    public ComponentCodeGenerationContext getComponentContext(final ComponentConfig componentConfig) {
        return componentContextMap.get(componentConfig.getId());
    }

    public List<ComponentConfig> getSortedConfigs() {
        return sortedConfigs;
    }

    public Object getConstValue(String id) {
        return constValueMap.get(id);
    }

    /**
     * Returns the type of the field for this component value in the DataFlow instance class. This can be
     * different from {@link #getOutputType(String)} because components with a primitive output type
     * are stored in fields with an object type (eg. {@link dataflow.core.type.IntegerValue},
     * {@link dataflow.core.type.FloatValue}, etc.)
     */
    public String getFieldType(String id) {
        String outputType = getOutputType(id);
        if (PRIMITIVE_TYPE_MAP.containsKey(outputType)) {
            return PRIMITIVE_TYPE_MAP.get(outputType);
        }
        String type = getTypeRegistry().getValueTypeClassName(outputType).orElse(null);
        if (type == null) {
            logger.warn(String.format("%s type is not registered", outputType));
            type = outputType;
        }
        return type.replace("$", ".");
    }

    /**
     * Returns the output type of the component with the given id. If a type is explicitly specified in the
     * configuration then that type is returned. If there is no explicit type specified then the type is the raw output
     * type of the getValue/execute method. If there is not explicit type specified and the value is a provided
     * value (ie. not obtained from a component) then the type will be {@link Object}.
     * See {@link #buildOutputTypeMap(List)}
     */
    public String getOutputType(String id) {
        String type = componentOutputTypeMap.get(id);
        type = type == null ? Object.class.getName() : type;
        return type;
    }

    /**
     * Returns the raw output type of the component with the given id. The raw output type is the
     * return value of the component getValue/execute method. See {@link #buildRawTypeMap(List)}
     */
    public String getRawOutputType(String id) {
        return rawComponentOutputTypeMap.get(id);
    }

    /**
     * Returns true if the output type of the component is a primitive type, false otherwise.
     */
    public boolean hasPrimitiveOutputType(String id) {
        return PRIMITIVE_TYPE_MAP.containsKey(getOutputType(id));
    }

    /**
     * Returns a set that is used to keep track of the ids of the async componentss that could potentially be in
     * progress at the current point in the DataFlow instance execute method code. This is used for an optimization
     * which avoids waiting for async components which must be finished because a synchronous wait for the result
     * has already occurred at an earlier point in the method.
     */
    public Set<String> getInProgressAsyncComponentIds() {
        return inProgressAsyncComponentIds;
    }

    public boolean isIncrementalEnabled() {
        return codeGenConfig.isIncrementalEnabled();
    }

    public boolean isEventsEnabled() {
        return codeGenConfig.isEventsEnabled();
    }

    public boolean isTimeoutEnabled() {
        return codeGenConfig.isTimeoutEnabled();
    }

    public DataFlowRegistry getRegistry() {
        return registry;
    }

    public TypeRegistry getTypeRegistry() {
        return registry.getTypeRegistry();
    }

    public ComponentRegistry getComponentRegistry() {
        return registry.getComponentRegistry();
    }

    private Map<String, Object> buildConstValueMap(List<ComponentConfig> componentConfigs) {
        Map<String, Object> constValueMap = new HashMap<>();
        for (ComponentConfig componentConfig : componentConfigs) {
            if (isSimpleConstant(componentConfig)) {
                Object value = componentConfig.getProperties().get("value");
                constValueMap.put(componentConfig.getId(), value);
            }
        }
        return constValueMap;
    }

    private Map<String, String> buildRawTypeMap(List<ComponentConfig> componentConfigs) {
        Map<String, String> rawTypeMap = new HashMap<>();
        for (ComponentConfig componentConfig : componentConfigs) {
            String id = componentConfig.getId();
            ComponentCodeGenerationContext componentContext = getComponentContext(
                componentConfig);
            String type = null;
            if (!isConstant(componentConfig)) {
                if (componentContext != null && componentContext.getComponentMetadata() != null) {
                    type = componentContext.getComponentMetadata().getOutputMetadata().getRawType();
                }
            }
            if (type != null) {
                rawTypeMap.put(id, type);
            }
        }
        return rawTypeMap;
    }

    private Map<String, String> buildOutputTypeMap(List<ComponentConfig> componentConfigs) {
        Map<String, String> typeMap = new HashMap<>();
        for (ComponentConfig componentConfig : componentConfigs) {
            String id = componentConfig.getId();

            if (!isConstant(componentConfig)) {
                if (componentConfig.getOutputType() != null) {
                    // The component has an output type that is explicitly specified in the configuration.
                    String type = componentConfig.getOutputType();
                    String valueTypeClassName = getTypeRegistry().getValueTypeClassName(type).orElse(null);
                    if(valueTypeClassName == null) {
                        logger.warn(String.format("Output type %s of component %s is not defined",
                                componentConfig.getOutputType(), id));
                        valueTypeClassName = type;
                    }

                    String rawType = (rawComponentOutputTypeMap.containsKey(id)) ?
                            getTypeRegistry().getValueTypeClassName(rawComponentOutputTypeMap.get(id))
                                    .orElse(rawComponentOutputTypeMap.get(id)) : valueTypeClassName;
                    try {
                        if (!getTypeRegistry().canConvert(rawType, valueTypeClassName)) {
                            throw new DataFlowConfigurationException(String.format(
                                    "Output of component %s is not convertible from %s to %s",
                                    id, rawType, valueTypeClassName));
                        }
                    } catch (Throwable t) {
                        // Unable to check if conversion is possible. Proceed anyway, but it may not be possible to
                        // compile the resulting code.
                    }

                    typeMap.put(id, type);
                } else {
                    // The component output type is not explicitly specified in the configuration. Use the
                    // raw output type of the component getValue/execute method.
                    String type = getRawOutputType(id);
                    if (type == null) {
                        type = Object.class.getName();
                    }
                    // TODO(thorntonv): Rework this so that we don't need to get an instance of the class.
//                    if(getTypeRegistry().getValueTypeClass(type) == null) {
//                        throw new DataFlowConfigurationException(String.format(
//                                "Output type %s of component %s is not defined", type, id));
//                    }
                    typeMap.put(id, type);
                }
            }
        }
        return typeMap;
    }
}
