/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MapValueProviderCodeGenerator.java
 */

package dataflow.core.codegen;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import dataflow.core.codegen.CodeGenerationUtil;
import dataflow.core.codegen.ComponentCodeGenerator;
import dataflow.core.codegen.DataFlowCodeGenerationContext;
import dataflow.core.codegen.DataFlowCodeGenerationContext.ComponentCodeGenerationContext;
import dataflow.core.config.ComponentConfig;
import dataflow.core.util.IndentPrintWriter;

import static dataflow.core.codegen.CodeGenerationUtil.*;

public class MapValueProviderCodeGenerator implements ComponentCodeGenerator {

    private static final String[] IMPORTS = new String[]{
            Map.class.getName(),
            LinkedHashMap.class.getName(),
            "dataflow.core.util.MapUtil",
    };

    @Override
    public Set<String> getImports() {
        return new HashSet<>(Arrays.asList(IMPORTS));
    }

    @Override
    public void writeFields(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        String id = context.getId();
        out.printf("private Map<String, Object> %s_inputMap = new LinkedHashMap<>();%n", getVarName(id));
        for (Map.Entry<String, ComponentConfig> inputEntry : context.getComponentConfig().getInput().entrySet()) {
            if (!isConstant(inputEntry.getValue())) {
                out.printf("private Map.Entry %s_inputMapEntry_%s;%n", getVarName(id), inputEntry.getKey());
            }
        }
    }

    @Override
    public void writeConstructorStatements(final IndentPrintWriter out,
            final ComponentCodeGenerationContext context) {
        String id = context.getId();
        ComponentConfig config = context.getComponentConfig();
        for (Map.Entry<String, ComponentConfig> inputEntry : config.getInput().entrySet()) {
            boolean isConst = isConstant(inputEntry.getValue());
            if (isConst) {
                Object simpleConstValue = context.getDataFlowContext().getConstValue(inputEntry.getValue().getId());
                String constValue = (simpleConstValue != null) ?
                        simpleConstValueToString(simpleConstValue) :
                        getVarName(inputEntry.getValue().getId() + "_value");
                out.printf("this.%s_inputMap.put(\"%s\", %s);%n", getVarName(id), inputEntry.getKey(), constValue);
            } else {
                out.printf("this.%s_inputMap.put(\"%s\", null);%n", getVarName(id), inputEntry.getKey());
            }
        }
        for (Map.Entry<String, ComponentConfig> inputEntry : config.getInput().entrySet()) {
            if (!isConstant(inputEntry.getValue())) {
                out.printf("this.%s_inputMapEntry_%s = MapUtil.getEntry(\"%s\", %s_inputMap);%n", getVarName(id),
                        inputEntry.getKey(), inputEntry.getKey(), getVarName(id));
            }
        }
    }

    @Override
    public void writeExecuteStatements(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
        DataFlowCodeGenerationContext dataFlowContext = context.getDataFlowContext();
        ComponentConfig config = context.getComponentConfig();
        for (Map.Entry<String, ComponentConfig> inputEntry : config.getInput().entrySet()) {
            ComponentConfig inputComponentConfig = inputEntry.getValue();
            if (!isConstant(inputComponentConfig)) {
                String valueExpr = getValueReferenceExpression(inputComponentConfig, context);
                String fromType = dataFlowContext.getFieldType(inputComponentConfig.getId());
                String toType = config.getInputTypes().get(inputEntry.getKey());
                if(toType != null) {
                    valueExpr = CodeGenerationUtil.valueConvertExpr(fromType, toType, valueExpr, dataFlowContext);
                }
                out.printf("%s_inputMapEntry_%s.setValue(%s);%n", context.getVarNamePrefix(),
                        inputEntry.getKey(), valueExpr);
            }
        }
    }

    @Override
    public void writeCloseStatements(final IndentPrintWriter out, final ComponentCodeGenerationContext context) {
    }
}
