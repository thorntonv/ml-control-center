/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueType.java
 */

package dataflow.core.type;


import java.util.*;

public class ValueType {

    private final String type;
    private final List<ValueType> typeGenericParams;
    private final String typeWithGenericParams;
    private ValueType typeWithoutGenericParams;

    public ValueType(final String type, final List<ValueType> typeGenericParams) {
        this.type = type;
        this.typeGenericParams = typeGenericParams;
        this.typeWithGenericParams = toString();
    }

    public String getType() {
        return type;
    }

    public List<ValueType> getTypeGenericParams() {
        return typeGenericParams;
    }

    public ValueType getTypeWithoutGenericParams() {
        if (typeWithoutGenericParams == null) {
            typeWithoutGenericParams = new ValueType(type, Collections.emptyList());
        }
        return typeWithoutGenericParams;
    }

    public static ValueType of(final Object obj) {
        return new ValueType(obj.getClass().getName(), Collections.emptyList());
    }

    public static ValueType of(final Class<?> clazz) {
        return new ValueType(clazz.getName(), Collections.emptyList());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final ValueType valueType = (ValueType) o;
        return Objects.equals(typeWithGenericParams, valueType.typeWithGenericParams);
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeWithGenericParams);
    }

    @Override
    public String toString() {
        if (typeWithGenericParams != null) {
            return typeWithGenericParams;
        }
        if (typeGenericParams == null || typeGenericParams.isEmpty()) {
            return type;
        }
        StringJoiner joiner = new StringJoiner(",");
        typeGenericParams.forEach(v -> joiner.add(v.toString()));
        return String.format("%s<%s>", type, joiner);
    }
}
