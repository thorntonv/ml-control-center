/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  WrapperUtil.java
 */

package dataflow.core.type;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility class to unwrap primitive wrapper classes.
 * eg. {@link IntegerValue}, {@link DoubleValue}, etc.
 */
public class WrapperUtil {

    public static Object unwrap(Object obj) {
        if (obj.getClass() == BooleanValue.class) {
            BooleanValue value = (BooleanValue) obj;
            return value.isSet() ? value.toBoolean() : null;
        } else if (obj.getClass() == DoubleValue.class) {
            DoubleValue value = (DoubleValue) obj;
            return value.isSet() ? value.toDouble() : null;
        } else if (obj.getClass() == FloatValue.class) {
            FloatValue value = (FloatValue) obj;
            return value.isSet() ? value.toFloat() : null;
        } else if (obj.getClass() == IntegerValue.class) {
            IntegerValue value = (IntegerValue) obj;
            return value.isSet() ? value.toInt() : null;
        } else if (obj.getClass() == LongValue.class) {
            LongValue value = (LongValue) obj;
            return value.isSet() ? value.toLong() : null;
        }
        return obj;
    }

    public static <K, V> Map<K, V> unwrapMapValues(Map<K, V> map) {
        if (map == null) {
            return null;
        }
        Map<K, V> replacements = null;

        for (Map.Entry<K, V> entry : map.entrySet()) {
            V value = entry.getValue();
            V replacement = (V) unwrap(value);
            if (value != replacement) {
                if (replacements == null) {
                    // Only allocate replacements if at least one replacement is found.
                    replacements = new HashMap<>();
                }
                replacements.put(entry.getKey(), replacement);
            }
        }
        if (replacements != null && !replacements.isEmpty()) {
            map = new HashMap<>(map);
            map.putAll(replacements);
        }

        return map;
    }
}
