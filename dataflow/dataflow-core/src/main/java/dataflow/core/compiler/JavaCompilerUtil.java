/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  JavaCompilerUtil.java
 */

package dataflow.core.compiler;

import javax.tools.*;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject.Kind;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.security.SecureClassLoader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility methods for compiling Java source code.
 */
public final class JavaCompilerUtil {

    private JavaCompilerUtil() {}

    /**
     * Java source code that is stored as a String in memory.
     */
    private static class JavaSourceFromString extends SimpleJavaFileObject {

        final String code;

        public JavaSourceFromString(String name, String code) {
            super(URI.create("string:///" + name.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
            this.code = code;
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            return code;
        }
    }

    /**
     * A {@link JavaFileObject} that holds the compiled class in memory.
     */
    public static class ClassOutputJavaFileObject extends SimpleJavaFileObject {

        protected final ByteArrayOutputStream bos = new ByteArrayOutputStream();

        public ClassOutputJavaFileObject(String name, Kind kind) {
            super(URI.create("string:///" + name.replace('.', '/') + kind.extension), kind);
        }

        public byte[] getBytes() {
            return bos.toByteArray();
        }

        @Override
        public OutputStream openOutputStream() {
            return bos;
        }
    }

    /**
     * A {@link JavaFileManager} that holds the compiled class in memory.
     */
    private static class ClassOutputJavaFileManager extends ClassloaderJavaFileManager {

        private final Map<String, ClassOutputJavaFileObject> compiledClassFileObjects = new HashMap<>();

        public ClassOutputJavaFileManager(ClassLoader classLoader,
            StandardJavaFileManager standardFileManager) {
            super(classLoader, standardFileManager);
        }

        public ClassOutputJavaFileObject getClassOutputFileObject(String name) {
            return compiledClassFileObjects.get(name);
        }

        @Override
        public JavaFileObject getJavaFileForOutput(
                Location location, String className, Kind kind, FileObject sibling) {
            ClassOutputJavaFileObject classOutputFileObject =
                new ClassOutputJavaFileObject(className, kind);
            compiledClassFileObjects.put(className, classOutputFileObject);
            return classOutputFileObject;
        }
    }

    /**
     * Compiles the Java source code contained in a String.
     *
     * @param implClass the fully qualified package and name of the class that will be compiled
     * @param sourceStr the source code as a String
     * @param classLoader the class loader to use to find any classes not discoverable using the
     * {@link StandardJavaFileManager}
     *
     * @return an instance of the compiled class
     * @throws CompileException if an error occurs during compilation
     */
    @SuppressWarnings("unchecked")
    public static <T> Class<T> compile(String implClass, String sourceStr, ClassLoader classLoader) throws CompileException {
        JavaFileObject file = new JavaSourceFromString(implClass, sourceStr);

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();

        Iterable<? extends JavaFileObject> compilationUnits = Collections.singletonList(file);
        ClassOutputJavaFileManager fileManager = new ClassOutputJavaFileManager(classLoader,
            compiler.getStandardFileManager(diagnostics, null, null));

        CompilationTask task = compiler.getTask(
            null, fileManager, diagnostics, null, null, compilationUnits);

        boolean success = task.call();
        if (success) {
            try {
                return (Class<T>) new SecureClassLoader(classLoader) {
                    @Override
                    protected Class<?> findClass(String name) {
                        byte[] b = fileManager.getClassOutputFileObject(name).getBytes();
                        return super.defineClass(name, b, 0, b.length);
                    }
                }.loadClass(implClass);
            } catch (Throwable e) {
                throw new CompileException(diagnostics.getDiagnostics(), e);
            }
        } else {
            throw new CompileException(diagnostics.getDiagnostics());
        }
    }
}