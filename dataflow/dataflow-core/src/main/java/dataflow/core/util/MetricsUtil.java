/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MetricsUtil.java
 */

package dataflow.core.util;

import dataflow.core.engine.DataFlowInstance;
import java.util.Map;

public class MetricsUtil {

    public static String getMetricName(String metricNamePrefix, String metricName,
        DataFlowInstance instance) {
        StringBuilder finalName = new StringBuilder();
        if (instance != null) {
            appendToMetricName(instance.getMetricNamePrefix(), finalName);
        }
        appendToMetricName(metricNamePrefix, finalName);
        appendToMetricName(metricName, finalName);
        return finalName.toString();
    }

    public static void appendToMetricName(String name, StringBuilder metricNameBuilder) {
        if (name != null && !name.trim().isEmpty()) {
            if (metricNameBuilder.length() > 0) {
                metricNameBuilder.append(".");
            }
            metricNameBuilder.append(name);
        }
    }

    public static String[] tagsToStringArray(Map<String, String> tags) {
        String[] array = new String[tags.size() * 2];
        int idx = 0;
        for (Map.Entry<String, String> tag : tags.entrySet()) {
            array[idx++] = tag.getKey();
            array[idx++] = tag.getValue();
        }
        return array;
    }
}
