package dataflow.core.util;

import dataflow.core.config.ComponentConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.exception.DataFlowConfigurationException;

import java.util.*;
import java.util.stream.Collectors;

public class DataFlowConfigUtil {

    public static List<ComponentConfig> getSortedConfigs(DataFlowConfig dataFlowConfig) {
        // TODO(thorntonv): This can be optimized by re-ordering to start executing async components
        //  as early as possible.
        ComponentConfig output = new ComponentConfig();
        output.setId("finalOutput-" + UUID.randomUUID().toString());

        List<ComponentConfig> outputDependencies = new ArrayList<>();
        outputDependencies.add(dataFlowConfig.getOutput());
        // Add the sinks as dependencies of the output so they are included in the topological sort.
        dataFlowConfig.getSinks().forEach(id -> outputDependencies.add(dataFlowConfig.getComponents().get(id)));

        List<ComponentConfig> sortedConfigs = GraphUtil.topologicalSort(output, v -> {
            if(v == output) {
                return outputDependencies;
            }
            Map<String, ComponentConfig> deps = v.getDependencies();
            return deps.values();
        });
        sortedConfigs.remove(output);
        List<String> abstractConfigIds = sortedConfigs.stream()
                .filter(ComponentConfig::isAbstract)
                .map(ComponentConfig::getId)
                .collect(Collectors.toList());
        if (!abstractConfigIds.isEmpty()) {
            throw new DataFlowConfigurationException(abstractConfigIds +
                    " components are abstract and should not be used as an input or output");
        }
        Map<String, ComponentConfig> unusedConfigs = new HashMap<>(dataFlowConfig.getComponents());
        sortedConfigs.forEach(v -> unusedConfigs.remove(v.getId()));

        List<String> unusedNotAbstractConfigIds = unusedConfigs.values().stream()
                .filter(v -> !v.isAbstract())
                .filter(v -> v.getType() != null)
                .map(ComponentConfig::getId)
                .collect(Collectors.toList());
        if (!unusedNotAbstractConfigIds.isEmpty()) {
            throw new DataFlowConfigurationException(unusedNotAbstractConfigIds +
                    " components are not used and are not abstract");
        }
        return sortedConfigs;
    }
}
