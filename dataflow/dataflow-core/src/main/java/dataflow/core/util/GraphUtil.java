/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  GraphUtil.java
 */

package dataflow.core.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

/**
 * Graph utility methods.
 */
public class GraphUtil {

    /**
     * Returns a list of topologically sorted nodes from the graph starting at the given node.
     *
     * @param node               the starting point for the topological sort
     * @param dependenciesGetter a function that will get the dependencies of a given node
     * @param <N>                the node type
     * @return the topologically sorted nodes
     */
    public static <N> List<N> topologicalSort(N node, Function<N, Iterable<N>> dependenciesGetter) {
        List<N> sortedNodes = new ArrayList<>();
        Set<N> visited = new HashSet<>();
        topologicalSort(node, visited, sortedNodes, new HashSet<>(), dependenciesGetter);
        return sortedNodes;
    }

    private static <N> void topologicalSort(N node, Set<N> visited, List<N> sortedNodes, Set<N> stackNodes,
            Function<N, Iterable<N>> dependenciesGetter) {
        visited.add(node);
        stackNodes.add(node);
        for (N dependency : dependenciesGetter.apply(node)) {
            if (stackNodes.contains(dependency)) {
                throw new IllegalArgumentException("cycle detected: " + stackNodes);
            }
            if (!visited.contains(dependency)) {
                topologicalSort(dependency, visited, sortedNodes, stackNodes, dependenciesGetter);
            }
        }
        sortedNodes.add(node);
        stackNodes.remove(node);
    }
}
