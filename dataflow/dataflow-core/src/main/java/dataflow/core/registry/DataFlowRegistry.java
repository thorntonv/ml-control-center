package dataflow.core.registry;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.engine.DataFlowInstanceFactory;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DataFlowRegistry {

    private final TypeRegistry typeRegistry = new TypeRegistry();
    private final ComponentRegistry componentRegistry = new ComponentRegistry();
    private final Map<String, DataFlowRegistration> dataFlowRegistrationMap = new ConcurrentHashMap<>();

    public DataFlowRegistration getDataFlowRegistration(String dataFlowId) {
        if (dataFlowId == null) {
            throw new IllegalArgumentException(
                    "Attempted to get dataflow registration with a null id");
        }
        DataFlowRegistration registration = dataFlowRegistrationMap.get(dataFlowId);
        if(registration == null) {
            // Check for an id with timestamp match.
            registration = dataFlowRegistrationMap.values().stream()
                    .filter(reg -> reg.getDataFlowConfig().getId().equals(dataFlowId))
                    .findAny().orElse(null);
            if(registration == null) {
                throw new IllegalArgumentException("DataFlow " + dataFlowId + " is not registered");
            }
        }
        return registration;
    }

    public void registerDataFlow(DataFlowConfig dataFlowConfig, DataFlowInstanceFactory instanceFactory) {
        dataFlowRegistrationMap.put(dataFlowConfig.getId(), new DataFlowRegistration(dataFlowConfig.getId(), dataFlowConfig, instanceFactory));
    }

    public Map<String, DataFlowRegistration> getRegisteredDataFlows() {
        return Collections.unmodifiableMap(dataFlowRegistrationMap);
    }
    
    public ComponentRegistry getComponentRegistry() {
        return componentRegistry;
    }

    public TypeRegistry getTypeRegistry() {
        return typeRegistry;
    }
}
