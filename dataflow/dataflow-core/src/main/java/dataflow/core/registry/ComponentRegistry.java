package dataflow.core.registry;

import dataflow.core.codegen.ComponentCodeGenerator;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.config.DataFlowConfigurableObjectBuilder;
import dataflow.core.type.TypeUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComponentRegistry {

    private final Map<String, ComponentRegistration> componentRegistrationMap = new HashMap<>();
    private final Map<String, DataFlowConfigurableObjectBuilder> objectTypeBuilderMap = new HashMap<>();

    private static class ComponentRegistration {
        private DataFlowComponentMetadata metadata;
        private DataFlowConfigurableObjectBuilder builder;
        private ComponentCodeGenerator codeGenerator;
    }

    public void registerComponent(DataFlowComponentMetadata metadata,
                                  DataFlowConfigurableObjectBuilder propertyObjectBuilder) {
        registerComponent(metadata, propertyObjectBuilder, null);
    }

    public void registerComponent(DataFlowComponentMetadata metadata,
                                  DataFlowConfigurableObjectBuilder propertyObjectBuilder,
                                  ComponentCodeGenerator codeGenerator) {
        String type = metadata.getTypeName();
        ComponentRegistration registration = new ComponentRegistration();
        registration.metadata = metadata;
        registration.builder = propertyObjectBuilder;
        registration.codeGenerator = codeGenerator;
        componentRegistrationMap.put(type, registration);

        objectTypeBuilderMap.put(type, propertyObjectBuilder);
    }

    public void registerPropertyObjectBuilder(String className, DataFlowConfigurableObjectBuilder builder) {
        objectTypeBuilderMap.put(TypeUtil.getSimpleClassName(className), builder);
    }

    public void registerPropertyObjectBuilder(Class classType, DataFlowConfigurableObjectBuilder builder) {
        objectTypeBuilderMap.put(classType.getSimpleName(), builder);
    }

    public Map<String, DataFlowConfigurableObjectBuilder> getPropertyObjectBuilders() {
        return new HashMap<>(objectTypeBuilderMap);
    }

    public DataFlowConfigurableObjectBuilder getPropertyObjectBuilder(String type) {
        DataFlowConfigurableObjectBuilder builder = objectTypeBuilderMap.get(type);
        if(builder == null) {
            throw new RuntimeException("No builder of type " + type + " is registered");
        }
        return builder;
    }

    public List<DataFlowComponentMetadata> getAllComponentMetadata() {
        List<DataFlowComponentMetadata> metadataList = new ArrayList<>();
        componentRegistrationMap.forEach((type, registration) ->
                metadataList.add(registration.metadata));
        return metadataList;
    }

    public Class<?> getComponentClass(final String type) {
        ComponentRegistration registration = componentRegistrationMap.get(type);
        if (registration == null) {
            throw new IllegalArgumentException("No class available for type " + type);
        }
        try {
            return Class.forName(registration.metadata.getComponentClassName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public DataFlowComponentMetadata getComponentMetadata(final String type) {
        ComponentRegistration registration = componentRegistrationMap.get(type);
        if (registration == null) {
            throw new IllegalArgumentException("No metadata available for type " + type);
        }
        return registration.metadata;
    }

    public ComponentCodeGenerator getComponentCodeGenerator(final String type) {
        ComponentRegistration registration = componentRegistrationMap.get(type);
        return registration != null ? registration.codeGenerator : null;
    }
}
