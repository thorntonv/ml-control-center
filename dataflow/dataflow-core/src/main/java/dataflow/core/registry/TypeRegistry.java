/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueRegistry.java
 */

package dataflow.core.registry;

import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.component.metadata.ValueTypeConverterMetadata;
import dataflow.core.type.*;

import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Contains information about the set of types that are available for use and provides type conversion functions.
 */
public class TypeRegistry {

    public static final String STRING_TYPE = "string";
    public static final ValueType STRING_VALUE_TYPE = new ValueType(String.class.getName(), Collections.EMPTY_LIST);

    private static final Pattern CLASS_ARRAY_PATTERN = Pattern.compile("\\[+L(.*);");
    private static final Pattern PRIMITIVE_ARRAY_PATTERN = Pattern.compile("\\[+([BCDFIJSZ])");

    private final Map<String, String> valueTypeClassNameMap = new HashMap<>();
    private final Map<String, Class<?>> primitiveClassNameMap = new HashMap<>();
    private final Map<ValueConverterRegistration.ValueConverterRegistrationKey, ValueConverterRegistration> converters = new HashMap<>();

    public TypeRegistry() {
        // Classes that can be referenced using their simple name.
        Class<?>[] simpleNameClasses = new Class<?>[]{
                Object.class, Integer.class, IntegerValue.class, Long.class, LongValue.class, Float.class,
                FloatValue.class, Double.class, DoubleValue.class, Boolean.class, BooleanValue.class, String.class,
                List.class, Stream.class
        };
        Arrays.stream(simpleNameClasses).forEach(simpleNameClass -> {
            valueTypeClassNameMap.put(simpleNameClass.getSimpleName(), simpleNameClass.getName());
            valueTypeClassNameMap.put(simpleNameClass.getSimpleName().toLowerCase(), simpleNameClass.getName());
        });
        valueTypeClassNameMap.put("int", int.class.getName());
        primitiveClassNameMap.put("int", int.class);
        valueTypeClassNameMap.put("long", long.class.getName());
        primitiveClassNameMap.put("long", long.class);
        valueTypeClassNameMap.put("float", float.class.getName());
        primitiveClassNameMap.put("float", float.class);
        valueTypeClassNameMap.put("double",double.class.getName());
        primitiveClassNameMap.put("double", double.class);
        valueTypeClassNameMap.put("bool", boolean.class.getName());
        primitiveClassNameMap.put("bool", boolean.class);
        valueTypeClassNameMap.put("boolean", boolean.class.getName());
        primitiveClassNameMap.put("boolean", boolean.class);
        valueTypeClassNameMap.put("byte", byte.class.getName());
        primitiveClassNameMap.put("byte", byte.class);
        valueTypeClassNameMap.put("char", char.class.getName());
        primitiveClassNameMap.put("char", char.class);
    }

    public <FROM_TYPE, TO_TYPE> void register(
        DataFlowComponentMetadata converterValueProviderMetadata,
            ValueTypeConverterFunction<FROM_TYPE, TO_TYPE> converterFunction) {
        ValueTypeConverterMetadata typeConverterMetadata = converterValueProviderMetadata.getValueTypeConverterMetadata();
        ValueType fromType = resolveClasses(TypeUtil.valueTypeFromString(typeConverterMetadata.getFromType()));
        ValueType toType = resolveClasses(TypeUtil.valueTypeFromString(typeConverterMetadata.getToType()));
        ValueConverterRegistration.ValueConverterRegistrationKey key =
                new ValueConverterRegistration.ValueConverterRegistrationKey(fromType, toType);
        converters.put(key, new ValueConverterRegistration(key, converterValueProviderMetadata, converterFunction));
    }

    public boolean canConvert(final String fromType, final String toType) {
        if (fromType.equals(toType)) {
            return true;
        }
        return getConverterRegistration(fromType, toType) != null;
    }

    public <T, R> boolean canConvert(Class<T> fromType, Class<R> toType) {
        return canConvert(fromType.getName(), toType.getName());
    }

    public Class<?> getValueTypeClass(final String valueTypeParam) {
        try {
            if(primitiveClassNameMap.containsKey(valueTypeParam)) {
                return primitiveClassNameMap.get(valueTypeParam);
            }
            return Class.forName(getValueTypeClassName(valueTypeParam).orElse(valueTypeParam));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public Optional<String> getValueTypeClassName(final String valueTypeParam) {
        if("?".equalsIgnoreCase(valueTypeParam)) {
            return Optional.of(Object.class.getName());
        }

        String type = valueTypeParam;
        // Check for an entry that exactly matches the given type.
        String valueTypeClassName = valueTypeClassNameMap.get(type);
        if (valueTypeClassName != null) {
            return Optional.of(valueTypeClassName);
        }
        // If the type is an array then try to find the type of the elements.
        boolean isArray = false;
        Matcher matcher = CLASS_ARRAY_PATTERN.matcher(type);
        if(type.contains("]")) {
            type = type.substring(0, type.indexOf('['));
            isArray = true;
        } else if(matcher.matches()) {
            // Drop [L and ;
            type = matcher.group(1);
            isArray = true;
        } else if(PRIMITIVE_ARRAY_PATTERN.matcher(type).matches()) {
            // The type is already a valid class name.
            return Optional.of(type);
        }

        type = TypeUtil.removeGenericTypes(type);
        valueTypeClassName = valueTypeClassNameMap.get(type);
        if (valueTypeClassName != null) {
            if(!isArray) {
                return Optional.of(valueTypeClassName);
            } else if(primitiveClassNameMap.containsKey(valueTypeClassName)) {
                return Optional.of(Array.newInstance(primitiveClassNameMap.get(valueTypeClassName), 0).getClass().getName());
            } else if((matcher = CLASS_ARRAY_PATTERN.matcher(valueTypeParam)).matches()) {
                return Optional.of(valueTypeParam.replace(matcher.group(1), valueTypeClassName));
            }
        }
        try {
            Class<?> valueTypeClass = Class.forName(type);
            if (isArray) {
                valueTypeClass = Array.newInstance(valueTypeClass, 0).getClass();
                return Optional.of(valueTypeClass.getName());
            }
        } catch (ClassNotFoundException e) {
        }
        return Optional.empty();
    }


    public Class<?>[] getGenericTypeClasses(final String valueType) {
        String[] genericTypes = TypeUtil.getGenericTypes(valueType);
        Class<?>[] genericTypeClasses = new Class<?>[genericTypes.length];
        for (int idx = 0; idx < genericTypes.length; idx++) {
            genericTypeClasses[idx] = getValueTypeClass(genericTypes[idx]);
        }
        return genericTypeClasses;
    }

    private ValueConverterRegistration getConverterRegistration(ValueType fromType, ValueType toType) {
        ValueConverterRegistration.ValueConverterRegistrationKey key =
                new ValueConverterRegistration.ValueConverterRegistrationKey(fromType, toType);
        ValueConverterRegistration registration = converters.get(key);
        if (registration != null) {
            return registration;
        }

        // Resolve type classes.
        fromType = resolveClasses(fromType);
        toType = resolveClasses(toType);

        key = new ValueConverterRegistration.ValueConverterRegistrationKey(fromType, toType);
        registration = converters.get(key);
        if (registration != null) {
            // Cache the converter so that it can be found more quickly in the future.
            converters.put(key, registration);
            return registration;
        }

        // Try to find generic converter that is not specific to the generic type params.
        key = new ValueConverterRegistration.ValueConverterRegistrationKey(
                fromType.getTypeWithoutGenericParams(), toType.getTypeWithoutGenericParams());
        registration = converters.get(key);
        if (registration != null) {
            // Cache the converter so that it can be found more quickly in the future.
            converters.put(key, registration);
            return registration;
        }

        try {
            // Try looking for a converter using the fromType parent classes.
            Class<?> fromTypeClass = getValueTypeClass(fromType.getType());
            if (fromTypeClass != Object.class) {
                Set<Class<?>> fromTypeParentClasses = new HashSet<>();
                fromTypeParentClasses.add(fromTypeClass.getSuperclass());
                fromTypeParentClasses.addAll(Arrays.asList(fromTypeClass.getInterfaces()));
                fromTypeParentClasses.remove(null);
                for (Class<?> fromTypeParentClass : fromTypeParentClasses) {
                    registration = getConverterRegistration(
                            ValueType.of(fromTypeParentClass), toType);
                    if (registration != null) {
                        // Cache the converter so that it can be found more quickly in the future.
                        converters.put(key, registration);
                        return registration;
                    }
                }
            }

            // Try looking for a converter using the toType super class.
            Class<?> toTypeClass = getValueTypeClass(toType.getType());
            if (toTypeClass != Object.class) {
                Set<Class<?>> toTypeParentClasses = new HashSet<>();
                toTypeParentClasses.add(toTypeClass.getSuperclass());
                toTypeParentClasses.addAll(Arrays.asList(toTypeClass.getInterfaces()));
                toTypeParentClasses.remove(null);
                for (Class<?> toTypeParentClass : toTypeParentClasses) {
                    registration = getConverterRegistration(fromType, ValueType.of(toTypeParentClass));
                    if (registration != null) {
                        // Cache the converter so that it can be found more quickly in the future.
                        converters.put(key, registration);
                        return registration;
                    }
                }
            }
        } catch(Throwable t) {
        }
        return null;
    }

    public ValueTypeConverterFunction<?, ?> getConverter(ValueType fromType, ValueType toType) {
        ValueConverterRegistration registration = getConverterRegistration(fromType, toType);
        return registration != null ? registration.getConverterFunction() : null;
    }

    public ValueTypeConverterFunction<?, ?> getConverter(String fromType, String toType) {
        ValueConverterRegistration registration = getConverterRegistration(fromType, toType);
        return registration != null ? registration.getConverterFunction() : null;
    }

    public DataFlowComponentMetadata getConverterValueProviderMetadata(String fromType, String toType) {
        ValueConverterRegistration registration = getConverterRegistration(fromType, toType);
        return registration != null ? registration.getValueProviderMetadata() : null;
    }
//
//    public boolean isAssignable(ValueType fromType, ValueType toType) throws ClassNotFoundException {
//        fromType = resolveClasses(fromType);
//        toType = resolveClasses(toType);
//        Class<?> fromTypeClass = Class.forName(fromType.getType());
//        Class<?> toTypeClass = Class.forName(toType.getType());
//        if (toTypeClass.isAssignableFrom(fromTypeClass) &&
//                fromType.getTypeGenericParams().size() == toType.getTypeGenericParams().size()) {
//
//        }
//
//        return false;
//    }

    public ValueType resolveClasses(ValueType valueType) {
        String typeClassName = getValueTypeClassName(valueType.getType()).orElse(valueType.getType());
        List<ValueType> genericTypeParams = valueType.getTypeGenericParams().stream()
                .map(this::resolveClasses)
                .collect(Collectors.toList());
        return new ValueType(typeClassName, genericTypeParams);
    }

    ValueConverterRegistration getConverterRegistration(String fromType, String toType) {
        return getConverterRegistration(TypeUtil.valueTypeFromString(fromType), TypeUtil.valueTypeFromString(toType));
    }
}
