package dataflow.core.registry;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.engine.DataFlowInstanceFactory;

public class DataFlowRegistration {
    private String id;
    private DataFlowConfig dataFlowConfig;
    private DataFlowInstanceFactory instanceFactory;

    public DataFlowRegistration(String id, DataFlowConfig dataFlowConfig, DataFlowInstanceFactory instanceFactory) {
        this.id = id;
        this.dataFlowConfig = dataFlowConfig;
        this.instanceFactory = instanceFactory;
    }

    public String getId() {
        return id;
    }

    public DataFlowConfig getDataFlowConfig() {
        return dataFlowConfig;
    }

    public DataFlowInstanceFactory getInstanceFactory() {
        return instanceFactory;
    }
}
