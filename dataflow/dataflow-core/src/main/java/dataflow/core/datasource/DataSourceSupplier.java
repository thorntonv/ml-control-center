package dataflow.core.datasource;

import java.util.List;
import java.util.function.Supplier;

public interface DataSourceSupplier<T extends DataSource> extends Supplier<List<T>> {
}
