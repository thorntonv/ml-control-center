package dataflow.core.datasource;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.datasource.DataSource;
import dataflow.core.datasource.DataSourceSupplier;

import java.util.ArrayList;
import java.util.List;

import static dataflow.core.datasource.ClassPathFileDataSource.getClassPathDataSourcesMatchingGlobPattern;


public class ClassPathDataSourceSupplier implements DataSourceSupplier<DataSource> {

    private List<String> paths;

    @DataFlowConfigurable
    public ClassPathDataSourceSupplier(@DataFlowConfigProperty List<String> paths) {
        this.paths = paths;
    }

    @Override
    public List<DataSource> get() {
        List<DataSource> dataSources = new ArrayList<>();
        for(String path : paths) {
            try {
                dataSources.addAll(getClassPathDataSourcesMatchingGlobPattern(path));
            } catch (Exception e) {
                throw new RuntimeException("Error loading classpath resources with path " + path);
            }
        }
        return dataSources;
    }
}
