/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataSourceManager.java
 */

package dataflow.core.datasource;

import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Manages {@link DataSource} instances. The managed DataSources are obtained using suppliers that are registered with
 * this class. A DataSource supplier is a function that returns a list of DataSources. After a supplier is registered
 * the list of supplied DataSources is checked periodically for changes. Each supplied DataSource is also checked for
 * changes periodically using the {@link DataSource#hasChanged()} method. Whenever a change is detected an event is
 * fired to the registered {@link DataSourceEventListener}s.
 *
 * @param <D> the type of DataSource. it must implement equals and hashCode
 */
public class DataSourceManager<D extends DataSource> implements AutoCloseable {

    /**
     * Holds registration information for a DataSource supplier.
     */
    public final class DataSourceSupplierRegistration {

        private DataSourceSupplier<D> dataSourceSupplier;
        private long refreshIntervalSeconds;
        private ScheduledFuture<?> refreshTaskFuture;
        // The current set of DataSources supplied by the supplier.
        private Map<String, D> pathDataSourceMap = new HashMap<>();
    }

    /**
     * Holds registration information for a DataSource.
     */
    private class DataSourceRegistration {

        private D dataSource;
        private ScheduledFuture<?> refreshTaskFuture;
        private long refreshIntervalSeconds;
        // The registrations for the suppliers that have supplied this DataSource. It is possible that the same
        // DataSource could be supplied by different suppliers.
        private Set<DataSourceSupplierRegistration> supplierRegistrations = new HashSet<>();
    }

    /**
     * A task to check the list of DataSources supplied by a supplier for changes.
     */
    private class DataSourceSupplierRefreshTask implements Runnable {

        private final DataSourceSupplierRegistration supplierRegistration;

        DataSourceSupplierRefreshTask(final DataSourceSupplierRegistration supplierRegistration) {
            this.supplierRegistration = supplierRegistration;
        }

        @Override
        public void run() {
            synchronized (DataSourceManager.this) {
                doRefresh();
            }
        }

        private void doRefresh() {
            long refreshIntervalSeconds = supplierRegistration.refreshIntervalSeconds;
            try {
                List<D> newDataSourceList = supplierRegistration.dataSourceSupplier.get();
                // Build a new path to DataSource map. Already existing DataSources will be moved from the old map
                // to the new map.
                Map<String, D> newPathDataSourceMap = new HashMap<>();
                for (D newDataSource : newDataSourceList) {
                    D oldDataSource = supplierRegistration.pathDataSourceMap.remove(newDataSource.getPath());
                    if (oldDataSource != null) {
                        // Use the already existing DataSource.
                        newDataSource = oldDataSource;
                    }
                    DataSourceRegistration dataSourceRegistration = createOrUpdateDataSourceRegistration(
                            newDataSource, refreshIntervalSeconds);
                    dataSourceRegistration.supplierRegistrations.add(supplierRegistration);
                    newPathDataSourceMap.put(newDataSource.getPath(), newDataSource);
                }
                // Any remaining entries in supplierRegistration.pathDataSourceMap are no longer active for this
                // supplier so remove the supplier registration from them.
                supplierRegistration.pathDataSourceMap.values().stream()
                        .map(dataSourceRegistrationMap::get)
                        .filter(Objects::nonNull)
                        .forEach(dataSourceReg -> removeSupplierRegistration(dataSourceReg, supplierRegistration));

                // Switch to the new map.
                supplierRegistration.pathDataSourceMap = newPathDataSourceMap;
            } catch (Exception e) {
                eventListeners.forEach(listener -> listener.onError(e));
            }
        }
    }

    /**
     * A task for refreshing a single {@link DataSource}.
     */
    private class DataSourceRefreshTask implements Runnable {

        private DataSourceRegistration dataSourceRegistration;

        DataSourceRefreshTask(final DataSourceRegistration dataSourceRegistration) {
            this.dataSourceRegistration = dataSourceRegistration;
            try {
                dataSourceRegistration.dataSource.hasChanged();
            } catch (Exception e) {
                eventListeners.forEach(listener -> listener.onError(e));
            }
        }

        @Override
        public void run() {
            try {
                if (dataSourceRegistration.dataSource.hasChanged()) {
                    eventListeners.forEach(listener -> listener.onChange(dataSourceRegistration.dataSource));
                }
            } catch (Exception e) {
                eventListeners.forEach(listener -> listener.onError(e));
            }
        }
    }

    private final Map<D, DataSourceRegistration> dataSourceRegistrationMap = new HashMap<>();
    private final Set<DataSourceEventListener<D>> eventListeners = new HashSet<>();
    private final ScheduledThreadPoolExecutor scheduledExecutor = new ScheduledThreadPoolExecutor(1);

    public DataSourceManager() {
        scheduledExecutor.setRemoveOnCancelPolicy(true);
    }

    public void addEventListener(DataSourceEventListener<D> eventListener) {
        eventListeners.add(eventListener);
    }

    public int getRegisteredDataSourceCount() {
        return dataSourceRegistrationMap.size();
    }

    public synchronized DataSourceSupplierRegistration register(D dataSource, long refreshIntervalSeconds) {
        return register(() -> Collections.singletonList(dataSource), refreshIntervalSeconds);
    }

    public synchronized DataSourceSupplierRegistration register(DataSourceSupplier<D> dataSourceSupplier,
            long refreshIntervalSeconds) {
        DataSourceSupplierRegistration dataSourceSupplierRegistration = new DataSourceSupplierRegistration();
        dataSourceSupplierRegistration.dataSourceSupplier = dataSourceSupplier;
        dataSourceSupplierRegistration.refreshIntervalSeconds = refreshIntervalSeconds;
        Runnable refreshTask = new DataSourceSupplierRefreshTask(dataSourceSupplierRegistration);
        refreshTask.run();
        dataSourceSupplierRegistration.refreshTaskFuture = schedule(refreshTask, refreshIntervalSeconds);
        return dataSourceSupplierRegistration;
    }

    public synchronized void unregister(DataSourceSupplierRegistration dataSourceSupplierRegistration) {
        if (dataSourceSupplierRegistration != null && dataSourceSupplierRegistration.refreshTaskFuture != null) {
            dataSourceSupplierRegistration.refreshTaskFuture.cancel(false);
            dataSourceSupplierRegistration.refreshTaskFuture = null;
            dataSourceSupplierRegistration.pathDataSourceMap.values().stream()
                    .map(dataSourceRegistrationMap::get)
                    .filter(Objects::nonNull)
                    .forEach(
                            dataSourceReg -> removeSupplierRegistration(dataSourceReg, dataSourceSupplierRegistration));
        }
    }

    @Override
    public void close() throws Exception {
        scheduledExecutor.shutdownNow();
        while (!scheduledExecutor.isTerminated()) {
            scheduledExecutor.awaitTermination(1, TimeUnit.SECONDS);
        }
    }

    private DataSourceRegistration createOrUpdateDataSourceRegistration(D dataSource, long refreshIntervalSeconds) {
        DataSourceRegistration dataSourceRegistration = dataSourceRegistrationMap.get(dataSource);
        if (dataSourceRegistration == null) {
            // The DataSource was not already registered.
            dataSourceRegistration = new DataSourceRegistration();
            dataSourceRegistration.dataSource = dataSource;
            dataSourceRegistration.refreshIntervalSeconds = refreshIntervalSeconds;
            dataSourceRegistration.refreshTaskFuture = schedule(
                    new DataSourceRefreshTask(dataSourceRegistration), refreshIntervalSeconds);
            dataSourceRegistrationMap.put(dataSource, dataSourceRegistration);
            eventListeners.forEach(listener -> listener.onAdd(dataSource));
        } else if (dataSourceRegistration.refreshIntervalSeconds > refreshIntervalSeconds) {
            // The DataSource was already registered, but the refresh interval was greater. The existing registration
            // will be kept, but the refresh interval will be reduced.
            dataSourceRegistration.refreshIntervalSeconds = refreshIntervalSeconds;
            dataSourceRegistration.refreshTaskFuture.cancel(false);
            dataSourceRegistration.refreshTaskFuture = schedule(
                    new DataSourceRefreshTask(dataSourceRegistration), refreshIntervalSeconds);
        }
        return dataSourceRegistration;
    }

    private void removeSupplierRegistration(DataSourceRegistration dataSourceRegistration,
            DataSourceSupplierRegistration supplierRegistration) {
            dataSourceRegistration.supplierRegistrations.remove(supplierRegistration);
            if (dataSourceRegistration.supplierRegistrations.isEmpty()) {
                // There are no more suppliers supplying this DataSource so unregister it.
                dataSourceRegistration.refreshTaskFuture.cancel(false);
                dataSourceRegistration.refreshTaskFuture = null;
                dataSourceRegistrationMap.remove(dataSourceRegistration.dataSource);
                eventListeners.forEach(listener -> listener.onRemove(dataSourceRegistration.dataSource));
        }
    }

    private ScheduledFuture<?> schedule(Runnable task, long refreshIntervalSeconds) {
        return (refreshIntervalSeconds > 0) ?
                scheduledExecutor.scheduleWithFixedDelay(task, 0, refreshIntervalSeconds, TimeUnit.SECONDS) :
                scheduledExecutor.schedule(task, 0, TimeUnit.SECONDS);
    }
}
