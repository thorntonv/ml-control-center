package dataflow.core.cache;

import dataflow.core.exception.DataFlowConfigurationException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CacheManager {

    private final Map<String, Cache<?, ?>> idCacheMap = new ConcurrentHashMap<>();

    public Cache<?, ?> getCache(String id) {
        Cache<?, ?> cache = idCacheMap.get(id);
        if(cache == null) {
            throw new DataFlowConfigurationException("No cache with id " + id + " is registered");
        }
        return cache;
    }

    public void register(String id, Cache<?, ?> cache) {
        idCacheMap.put(id, cache);
    }
}
