package dataflow.core.cache;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

/**
 * A {@link Cache} implementation that uses a hash map in memory.
 */
public class MemoryCache<K, V> implements Cache<K, V> {

    private final Map<K, V> cache = new ConcurrentHashMap<>();

    @Override
    public V get(K key, Callable<? extends V> loader) throws ExecutionException {
        V value = cache.get(key);
        if(value == null) {
            try {
                value = loader.call();
                cache.put(key, value);
            } catch (Throwable t) {
                throw new ExecutionException("Unable to load value into cache", t);
            }
        }
        return value;
    }
}
