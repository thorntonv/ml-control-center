package dataflow.core.cache;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public interface Cache<K, V> {

    V get(K key, Callable<? extends V> loader) throws ExecutionException;
}
