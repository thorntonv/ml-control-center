/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowInstance.java
 */

package dataflow.core.engine;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowExecutionException;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * An instance of a DataFlow that can be used to set values, execute the flow, and get output values. Implementations of
 * this interface are NOT thread safe. Separate instances should be created if a DataFlow needs to be executed
 * concurrently by multiple threads.
 */
public interface DataFlowInstance extends AutoCloseable {

    /**
     * Get a map of all values.
     */
    Map<String, Object> getValues();

    /**
     * Get the value with the given id.
     */
    Object getValue(String id);

    default Object getValue(ValueReference valueReference) {
        return getValue(valueReference.getComponentId());
    }

    void setValue(String id, Object value);

    String getOutputType();

    String getValueType(String id);

    /**
     * Executes this DataFlow instance.
     *
     * @throws DataFlowExecutionException if an error occurred while executing the DataFlow
     */
    void execute() throws DataFlowExecutionException;

    Object getOutput();

    void setMaxExecutionTimeMillis(final long maxExecutionTimeMillis);

    DataFlowEnvironment getEnvironment();

    Map<String, Object> getComponents();

    void reset();

    void updateAvailability();

    void setMetricNamePrefix(String metricNamePrefix);
    String getMetricNamePrefix();

    void setMetricTags(Map<String, String> tags);
    Map<String, String> getMetricTags();

    Map<String, String> getMetricTagsForComponent(Object component);

    /**
     * Execute the given consumer function for each component instance.
     *
     * @param function the consumer function
     */
    void forEachComponent(BiConsumer<String, Object> function);

    /**
     * Execute the given consumer function for each component instance of the given class type.
     *
     * @param function the consumer function
     */
    <T> void forEachComponent(Class<T> componentClass, Consumer<T> function);

    String getInstanceId();

    DataFlowConfig getConfig();

    boolean isCanceled();
    void cancel();
}
