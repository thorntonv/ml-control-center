/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ThreadLocalDataFlowInstance.java
 */

package dataflow.core.engine;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowExecutionException;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * A {@link DataFlowInstance} wrapper that creates a separate thread-local instance of the specified DataFlow as needed.
 */
public class ThreadLocalDataFlowInstance implements DataFlowInstance {

    private final String dataFlowId;
    private final ThreadLocal<DataFlowInstance> threadLocalInstance = new ThreadLocal<>();
    private final Queue<DataFlowInstance> allocatedInstances = new ConcurrentLinkedQueue<>();
    private final DataFlowExecutionContext parentExecutionContext;

    private final Consumer<DataFlowInstance> instanceInitFn;

    public ThreadLocalDataFlowInstance(final String dataFlowId, Consumer<DataFlowInstance> instanceInitFn) {
        this.dataFlowId = dataFlowId;
        this.instanceInitFn = instanceInitFn != null ? instanceInitFn : i -> {};
        this.parentExecutionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        if (parentExecutionContext == null) {
            throw new RuntimeException("Execution context is not set");
        }
    }

    /**
     * Creates a new {@link ThreadLocalDataFlowInstance} for the given instance.
     */
    public static ThreadLocalDataFlowInstance create(DataFlowInstance instance,
        Consumer<DataFlowInstance> initFn) {
        if(instance instanceof ThreadLocalDataFlowInstance) {
            return (ThreadLocalDataFlowInstance) instance;
        }
        return new ThreadLocalDataFlowInstance(instance.getConfig().getId(), initFn);
    }

    /**
     * Creates a new {@link ThreadLocalDataFlowInstance} for the given data flow id. The provided
     * values of newly created instances will be initialized with values from the given parent
     * instance.
     */
    public static ThreadLocalDataFlowInstance create(String dataFlowId,
        DataFlowInstance parentInstance) {
        return new ThreadLocalDataFlowInstance(dataFlowId, newInstance ->
            newInstance.getConfig().getProvidedValueIds().stream()
                .filter(id -> !id.startsWith("_"))
                .forEach(id -> newInstance.setValue(id, parentInstance.getValue(id))));
    }

    @Override
    public Map<String, Object> getValues() {
        return getInstance().getValues();
    }

    @Override
    public Object getValue(final String id) {
        return getInstance().getValue(id);
    }

    @Override
    public void setValue(final String id, final Object value) {
        getInstance().setValue(id, value);
    }

    @Override
    public String getOutputType() {
        return getInstance().getOutputType();
    }

    @Override
    public String getValueType(final String id) {
        return getInstance().getValueType(id);
    }

    @Override
    public void execute() throws DataFlowExecutionException {
        getInstance().execute();
    }

    @Override
    public Object getOutput() {
        return getInstance().getOutput();
    }

    @Override
    public void setMaxExecutionTimeMillis(final long maxExecutionTimeMillis) {
        getInstance().setMaxExecutionTimeMillis(maxExecutionTimeMillis);
    }

    @Override
    public DataFlowEnvironment getEnvironment() {
        return getInstance().getEnvironment();
    }

    @Override
    public Map<String, Object> getComponents() {
        return getInstance().getComponents();
    }

    @Override
    public void reset() {
        getInstance().reset();
    }

    @Override
    public void updateAvailability() {
        getInstance().updateAvailability();
    }

    @Override
    public void setMetricNamePrefix(String metricNamePrefix) {
        getInstance().setMetricNamePrefix(metricNamePrefix);
    }

    @Override
    public String getMetricNamePrefix() {
        return getInstance().getMetricNamePrefix();
    }

    @Override
    public void setMetricTags(Map<String, String> tags) {
        getInstance().setMetricTags(tags);
    }

    @Override
    public Map<String, String> getMetricTags() {
        return getInstance().getMetricTags();
    }

    @Override
    public Map<String, String> getMetricTagsForComponent(Object component) {
        return getInstance().getMetricTagsForComponent(component);
    }

    @Override
    public void forEachComponent(final BiConsumer<String, Object> function) {
        getInstance().forEachComponent(function);
    }

    @Override
    public <T> void forEachComponent(final Class<T> componentClass, final Consumer<T> function) {
        getInstance().forEachComponent(componentClass, function);
    }

    @Override
    public String getInstanceId() {
        return getInstance().getInstanceId();
    }

    @Override
    public DataFlowConfig getConfig() {
        return getInstance().getConfig();
    }

    @Override
    public boolean isCanceled() {
        return getInstance().isCanceled();
    }

    @Override
    public void cancel() {
        getInstance().cancel();
    }

    @Override
    public void close() throws Exception {
        DataFlowInstance instance;
        while ((instance = allocatedInstances.poll()) != null) {
            instance.close();
        }
    }

    private DataFlowInstance getInstance() {
        try {
            DataFlowInstance instance = threadLocalInstance.get();
            if (instance == null) {
                instance = parentExecutionContext.getEnvironment().getRegistry()
                    .getDataFlowRegistration(dataFlowId).getInstanceFactory()
                    .newInstance(instanceInitFn);
                if (instance == null) {
                    throw new RuntimeException("New DataFlow instance was NULL");
                }
                allocatedInstances.add(instance);
                threadLocalInstance.set(instance);
            }
            return instance;
        } catch (Exception ex) {
            throw new RuntimeException("Failed to allocate instance of DataFlow " + dataFlowId, ex);
        }
    }
}
