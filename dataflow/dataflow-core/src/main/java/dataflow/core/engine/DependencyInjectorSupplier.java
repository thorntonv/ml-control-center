package dataflow.core.engine;

import java.util.function.Supplier;

public interface DependencyInjectorSupplier extends Supplier<DependencyInjector> {
}
