package dataflow.core.engine;

import java.util.function.Consumer;

public interface DataFlowInstanceFactory {

    DataFlowInstance newInstance(Consumer<DataFlowInstance> instanceInitializationFunction) throws Exception;

    default DataFlowInstance newInstance() throws Exception {
        return newInstance(initFn -> {});
    }
}