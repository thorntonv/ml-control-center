package dataflow.core.engine;

/**
 * An interface for the dependency injection framework. Typically this will be implemented using Spring one of the
 * other commonly used dependency injection frameworks.
 */
public interface DependencyInjector {

    /**
     * Get an instance of the specified class.
     */
    <T> T getInstance(Class<T> instanceClass);

    /**
     * Injects dependencies into the given object.
     */
    <T> T injectDependencies(T obj);
}