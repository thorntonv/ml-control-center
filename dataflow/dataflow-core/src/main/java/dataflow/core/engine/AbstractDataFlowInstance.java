/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AbstractDataFlowInstance.java
 */

package dataflow.core.engine;

import dataflow.core.config.ComponentConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.RawConfigurableObjectConfig;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.util.BuilderUtil;
import dataflow.core.parser.DataFlowParser;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * An abstract base class for dynamically generated {@link DataFlowInstance} implementations.
 */
@SuppressWarnings("unchecked")
public abstract class AbstractDataFlowInstance implements DataFlowInstance {

    protected static final class CompletableFutureHolder {

        private final String componentId;
        private long startTime;
        private CompletableFuture<?> future;

        public CompletableFutureHolder(String componentId) {
            this.componentId = componentId;
        }

        public String getComponentId() {
            return componentId;
        }

        public long getStartTime() {
            return startTime;
        }

        public <T> CompletableFuture<T> getFuture() {
            return (CompletableFuture<T>) future;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public void setFuture(CompletableFuture<?> future) {
            this.future = future;
        }
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    protected final String instanceId = UUID.randomUUID().toString();
    protected final DataFlowEnvironment environment;
    protected final Executor executor;
    protected final DataFlowConfig dataFlowConfig;
    protected long startTimeMillis;
    protected long endTimeMillis;
    protected long maxExecutionTimeMillis;
    protected final Map<String, CompletableFutureHolder> futureHolders = new HashMap<>();
    protected DataFlowInstance parentInstance;

    private String metricNamePrefix;
    private Map<String, String> metricTags;
    private final Map<Object, Map<String, String>> componentMetricTags = new IdentityHashMap<>();
    private final Map<Object, String> componentIdMap = new IdentityHashMap<>();
    private final AtomicBoolean canceled = new AtomicBoolean(false);
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public AbstractDataFlowInstance(String id, String yamlConfig, final DataFlowEnvironment environment,
            final Executor executor) {
        try {
            DataFlowConfig cfg = new DataFlowParser(environment.getRegistry()).parse(new ByteArrayInputStream(yamlConfig.getBytes(StandardCharsets.UTF_8)));
            if(id.equals(cfg.getId())) {
                this.dataFlowConfig = cfg;
            } else {
                this.dataFlowConfig = cfg.getChildDataFlows().values().stream().filter(child -> id.equals(child.getId())).findAny()
                        .orElseThrow(() -> new DataFlowParseException("No dataflow with id " + id + " is defined"));
            }
        } catch (DataFlowParseException e) {
            throw new RuntimeException("Failed to parse dataflow config yaml " + yamlConfig, e);
        }
        this.environment = environment;
        this.executor = executor;
        this.maxExecutionTimeMillis = environment.getMaxExecutionTimeMillis();
    }

    public String getInstanceId() {
        return instanceId;
    }

    public long getMaxExecutionTimeMillis() {
        return maxExecutionTimeMillis;
    }

    public void setMaxExecutionTimeMillis(final long maxExecutionTimeMillis) {
        this.maxExecutionTimeMillis = maxExecutionTimeMillis;
    }

    public DataFlowConfig getDataFlowConfig() {
        return dataFlowConfig;
    }

    public DataFlowEnvironment getEnvironment() {
        return environment;
    }

    public <T> void forEachComponent(Class<T> componentClass, Consumer<T> function) {
        getComponents().values().stream().filter(v -> componentClass.isAssignableFrom(v.getClass()))
                .map(v -> (T) v)
                .forEach(function);
        updateAvailability();
    }

    public void forEachComponent(BiConsumer<String, Object> function) {
        getComponents().forEach(function);
        updateAvailability();
    }

    public void updateAvailability() {
    }

    @Override
    public void setMetricNamePrefix(String metricNamePrefix) {
        this.metricNamePrefix = metricNamePrefix;
    }

    @Override
    public String getMetricNamePrefix() {
        if (metricNamePrefix != null) {
            return metricNamePrefix;
        }
        return parentInstance != null ? parentInstance.getMetricNamePrefix() : null;
    }

    @Override
    public void setMetricTags(Map<String, String> tags) {
        this.metricTags = tags != null ? new HashMap<>(tags) : new HashMap<>();
    }

    @Override
    public Map<String, String> getMetricTags() {
        Map<String, String> mergedTags = new HashMap<>();
        if (parentInstance != null && parentInstance.getMetricTags() != null) {
            mergedTags.putAll(parentInstance.getMetricTags());
        }
        if (metricTags != null) {
            mergedTags.putAll(metricTags);
        }
        return mergedTags;
    }

    @Override
    public void reset() {
        init();
    }

    public DataFlowConfig getConfig() {
        return dataFlowConfig;
    }

    @Override
    public void cancel() {
        canceled.set(true);
    }

    @Override
    public boolean isCanceled() {
        return canceled.get();
    }

    @Override
    public void close() {
        closed.set(true);
    }

    @Override
    protected void finalize() throws Throwable {
        // TODO(thorntonv): Finalize is now deprecated so this check should be done another way.
        try {
            if (!closed.get()) {
                logger.warn("Instance {} for DataFlow {} was not closed",
                    getInstanceId(), getDataFlowConfig().getId());
                close();
            }
        } finally {
            super.finalize();
        }
    }

    protected DataFlowExecutionContext createExecutionContext() {
        DataFlowExecutionContext context = DataFlowExecutionContext.createExecutionContext(environment);
        context.setInstance(this);

        DataFlowExecutionContext parentContext = context.getParentContext();
        if (parentContext != null) {
            this.parentInstance = parentContext.getInstance();
        }
        return context;
    }

    public abstract Map<String, Object> getComponents();

    protected abstract void init();

    public synchronized Map<String, String> getMetricTagsForComponent(Object component) {
        Map<String, String> tags = componentMetricTags.get(component);
        if (tags == null) {
            tags = new HashMap<>();
            tags.put("dataFlowId", dataFlowConfig.getId());
            String componentId = getComponentId(component);
            tags.put("componentId", componentId != null ? componentId : "unknown");

            String componentType = null;
            if (componentId != null) {
                ComponentConfig componentConfig = dataFlowConfig.getComponents().get(componentId);
                if (componentConfig != null) {
                    componentType = componentConfig.getType();
                }
            }
            tags.put("componentType", componentType != null ? componentType : "unknown");
            tags.putAll(getMetricTags());
            componentMetricTags.put(component, tags);
        }
        return tags;
    }

    protected synchronized String getComponentId(Object component) {
        String componentId = componentIdMap.get(component);
        if (componentId != null) {
            return componentId;
        }
        for (Map.Entry<String, Object> entry : getComponents().entrySet()) {
            if (entry.getValue() == component) {
                componentIdMap.put(component, entry.getKey());
                return entry.getKey();
            }
        }
        return null;
    }

    protected void setComponentFuture(String componentId, CompletableFuture<?> future) {
        CompletableFutureHolder holder = futureHolders.get(componentId);
        if(holder == null) {
            throw new RuntimeException("Invalid component id " + componentId);
        }
        holder.future = future;
    }

    protected void clearExecutedFutures() {
        futureHolders.values().forEach(f -> {f.future = null; f.startTime = 0; });
    }

    protected void cancelExecutedFutures() {
        futureHolders.values().stream().filter(f -> f.future != null)
            .map(CompletableFutureHolder::getFuture).filter(f -> !f.isDone())
                .forEach(f -> f.cancel(true));
    }

    protected void waitForExecutedFutures() {
        futureHolders.values().forEach(futureHolder -> {
            try {
                if(futureHolder != null && futureHolder.future != null) {
                    waitFor(futureHolder);
                }
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                logger.warn("Error while waiting for result from component " + futureHolder.getComponentId(), e);
                throw new RuntimeException(e);
            }
        });
    }

    protected CompletableFuture<?> allOf(String... componentIds) {
        return CompletableFuture.allOf(Arrays.stream(componentIds).map(futureHolders::get)
            .filter(holder -> holder != null && holder.getFuture() != null)
            .map(holder -> holder.future).toArray(CompletableFuture[]::new));
    }

    protected void waitFor(String componentId)
        throws ExecutionException, InterruptedException, TimeoutException {
        waitFor(futureHolders.get(componentId));
    }

    protected <T> T waitFor(CompletableFutureHolder futureHolder)
            throws InterruptedException, ExecutionException, TimeoutException {
        if(futureHolder == null || futureHolder.getFuture() == null) {
            return null;
        }
        if(startTimeMillis == 0) {
            startTimeMillis = System.currentTimeMillis();
        }
        long waitStartTime = System.currentTimeMillis();
        while(true) {
            long millisRemaining =
                maxExecutionTimeMillis - (System.currentTimeMillis() - startTimeMillis);
            if (millisRemaining <= 0) {
                throw new TimeoutException("Timeout waiting for result for component " +
                    futureHolder.getComponentId());
            }

            try {
                return futureHolder.<T>getFuture().get(1, TimeUnit.MINUTES);
            } catch(TimeoutException ex) {
                String elapsedTime = Duration.ofMillis(System.currentTimeMillis() - waitStartTime).toString();
                String remainingTime = Duration.ofMillis(millisRemaining).toString();

                logger.warn("Waiting for result from component " + futureHolder.getComponentId() +
                    "; elapsed=" + elapsedTime +
                    "; remaining=" + remainingTime);
            } catch(InterruptedException | ExecutionException ex) {
                logger.warn("Error while waiting for result from component " + futureHolder.getComponentId(), ex);
                throw ex;
            }
        }
    }

    protected Object buildConstant(Object value) {
        if (value instanceof RawConfigurableObjectConfig) {
            RawConfigurableObjectConfig rawConfig = (RawConfigurableObjectConfig) value;
            return buildObject(rawConfig.getType(), rawConfig, getValues());
        } else {
            return value;
        }
    }

    protected Object buildObject(String type, Map<String, Object> map, Map<String, Object> instanceValues) {
        return BuilderUtil.buildObject(type, map, instanceValues, environment.getRegistry());
    }

    protected void fireDataFlowExecutionSuccessEvent() {
        // TODO(thorntonv): Determine if this method should be implemented or removed.
    }

    protected void fireDataFlowExecutionErrorEvent(Throwable error) {
        // TODO(thorntonv): Determine if this method should be implemented or removed.
    }

    protected void fireComponentExecutionSuccessEvent(String componentId, long startTimeMillis) {
        // TODO(thorntonv): Determine if this method should be implemented or removed.
    }

    protected void fireComponentExecutionErrorEvent(String componentId, long startTimeMillis, Throwable error) {
        // TODO(thorntonv): Determine if this method should be implemented or removed.
    }

    protected void fireComponentFallbackEvent(String componentId, String fallbackComponentId) {
        // TODO(thorntonv): Determine if this method should be implemented or removed.
    }

    protected void fireComponentErrorFallbackEvent(String componentId, String fallbackComponentId,
            Exception error) {
        // TODO(thorntonv): Determine if this method should be implemented or removed.
    }
}
