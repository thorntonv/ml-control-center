package dataflow.core.config;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DataflowDiscoveryConfig {

    private Set<String> componentPackages;
    private Set<String> excludeComponentPackages;
    private Set<String> includeComponentClasses;
    private Set<String> excludeComponentClasses;
    private Set<String> dataFlowConfigPaths;
    private Set<String> environmentConfigPaths;


    public Set<String> getComponentPackages() {
        return componentPackages != null ? Collections.unmodifiableSet(componentPackages) : Collections.emptySet();
    }

    public Set<String> getExcludeComponentPackages() {
        return excludeComponentPackages != null ? Collections.unmodifiableSet(excludeComponentPackages) : Collections.emptySet();
    }

    public Set<String> getIncludeComponentClasses() {
        return includeComponentClasses != null ? Collections.unmodifiableSet(includeComponentClasses) : Collections.emptySet();
    }

    public Set<String> getExcludeComponentClasses() {
        return excludeComponentClasses != null ? Collections.unmodifiableSet(excludeComponentClasses) : Collections.emptySet();
    }

    public Set<String> getDataFlowConfigPaths() {
        return dataFlowConfigPaths != null ? Collections.unmodifiableSet(dataFlowConfigPaths) : Collections.emptySet();
    }

    public Set<String> getEnvironmentConfigPaths() {
        return environmentConfigPaths != null ? Collections.unmodifiableSet(environmentConfigPaths) : Collections.emptySet();
    }

    public void setComponentPackages(Set<String> componentPackages) {
        this.componentPackages = new HashSet<>(componentPackages);
    }

    public void setExcludeComponentPackages(Set<String> excludeComponentPackages) {
        this.excludeComponentPackages = new HashSet<>(excludeComponentPackages);
    }

    public void setIncludeComponentClasses(Set<String> includeComponentClasses) {
        this.includeComponentClasses = new HashSet<>(includeComponentClasses);
    }

    public void setExcludeComponentClasses(Set<String> excludeComponentClasses) {
        this.excludeComponentClasses = new HashSet<>(excludeComponentClasses);
    }

    public void setDataFlowConfigPaths(Set<String> dataFlowConfigPaths) {
        this.dataFlowConfigPaths = new HashSet<>(dataFlowConfigPaths);
    }

    public void setEnvironmentConfigPaths(Set<String> environmentConfigPaths) {
        this.environmentConfigPaths = new HashSet<>(environmentConfigPaths);
    }
}
