/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ComponentConfig.java
 */

package dataflow.core.config;

import dataflow.core.exception.DataFlowConfigurationException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings({"WeakerAccess", "unchecked", "unused"})
public class ComponentConfig {

    /**
     * If any fields are changed here check if {@link #setExtendedConfig(ComponentConfig)} needs to be updated.
     */

    private String id;
    private String type;
    private String extendsId;
    private Map<String, Object> properties = new HashMap<>();
    private final Map<String, ComponentConfig> propertyValueProviders = new HashMap<>();
    private Map<String, ComponentConfig> input = new HashMap<>();
    private final Map<String, ComponentConfig> output = new HashMap<>();
    private Map<String, String> inputTypes = new HashMap<>();
    private String outputType;
    private Boolean failOnError = true;
    private Map<String, Object> rawConfig = new HashMap<>();
    private boolean internal = false;
    private boolean sink = false;
    private boolean _abstract = false;
    private Boolean eventsEnabled = false;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public String getId() {
        return id;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public Map<String, ComponentConfig> getInput() {
        if (input == null) {
            return Collections.emptyMap();
        }
        return input;
    }

    public Map<String, ComponentConfig> getDependencies() {
        Map<String, ComponentConfig> inputs = new HashMap<>(getInput());
        if (propertyValueProviders != null) {
            inputs.putAll(propertyValueProviders);
        }
        return inputs;
    }

    public boolean shouldFailOnError() {
        return failOnError != null && failOnError;
    }

    public void setFailOnError(Boolean failOnError) {
        this.failOnError = failOnError;
    }

    public Map<String, Object> getRawConfig() {
        return rawConfig;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setProperties(final Map<String, Object> properties) {
        this.properties = properties;
    }

    public Map<String, ComponentConfig> getPropertyValueProviders() {
        return propertyValueProviders;
    }

    public void addPropertyValueProvider(ComponentConfig componentConfig) {
        propertyValueProviders.put(componentConfig.getId(), componentConfig);
        componentConfig.output.put(getId(), this);
    }

    public void setInput(final Map<String, ComponentConfig> input) {
        this.input = input;
        input.values().forEach(v -> v.output.put(getId(), this));
    }

    public void setInput(String id, ComponentConfig input) {
        this.input.put(id, input);
        input.output.put(getId(), this);
    }

    /**
     * Returns the components that have this component as a direct input.
     */
    public Map<String, ComponentConfig> getOutputComponents() {
        return output;
    }

    public void setRawConfig(final Map<String, Object> rawConfig) {
        this.rawConfig = rawConfig;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setAbstract(boolean _abstract) {
        this._abstract = _abstract;
    }

    public boolean isAbstract() {
        return _abstract;
    }

    public void setSink(boolean sink) {
        this.sink = sink;
    }

    public boolean isSink() {
        return sink;
    }

    public void setExtendedConfig(ComponentConfig extendedConfig) throws DataFlowConfigurationException {
        if (this.type != null && !this.type.equalsIgnoreCase(extendedConfig.type) &&
                !this.type.equalsIgnoreCase("Component") &&
                !this.type.equalsIgnoreCase("ValueProvider") &&
                !this.type.equalsIgnoreCase("Action")) {
            throw new DataFlowConfigurationException(String.format(
                    "Invalid type %s for component %s. It should be %s or Component",
                    this.type, id, extendedConfig.type));
        }
        if(extendedConfig == null) {
            throw new DataFlowConfigurationException("Invalid extended config id for component " + getId());
        }
        this.type = extendedConfig.getType();

        if (extendedConfig.properties != null) {
            extendMapRecursive(properties, extendedConfig.properties);
        }
        if(extendedConfig.propertyValueProviders != null) {
            extendedConfig.propertyValueProviders.forEach((k, v) -> {
                propertyValueProviders.putIfAbsent(k, v);
                v.getOutputComponents().putIfAbsent(getId(), this);
            });
        }
        if (extendedConfig.input != null) {
            extendedConfig.input.forEach((k, v) -> {
                input.putIfAbsent(k, v);
                v.getOutputComponents().put(getId(), this);
            });
        }
        if (extendedConfig.inputTypes != null) {
            extendedConfig.inputTypes.forEach((k, v) -> {
                inputTypes.putIfAbsent(k, v);
            });
        }
        // Skip extending output because this config will be connected independently to other nodes in the graph.

        if (outputType == null) {
            outputType = extendedConfig.outputType;
        }
        if (failOnError == null) {
            failOnError = extendedConfig.failOnError;
        }
        extendMapRecursive(rawConfig, extendedConfig.rawConfig);
        if (eventsEnabled == null) {
            eventsEnabled = extendedConfig.eventsEnabled;
        }
    }

    private void extendMapRecursive(Map map, Map extendedMap) {
        Set<Map.Entry> entrySet = extendedMap.entrySet();
        for (Map.Entry entry : entrySet) {
            Object key = entry.getKey();
            Object extendedValue = entry.getValue();
            boolean containsKey = map.containsKey(key);
            if(extendedValue instanceof RawComponentConfig) {
                RawComponentConfig innerConfig = containsKey ? (RawComponentConfig) map.get(key)
                    : new RawComponentConfig(((RawComponentConfig) extendedValue).getType(),
                        new HashMap<>());
                extendMapRecursive(innerConfig, (Map) extendedValue);
                map.put(key, innerConfig);
            } else if(extendedValue instanceof RawConfigurableObjectConfig) {
                RawConfigurableObjectConfig innerConfig = containsKey ? (RawConfigurableObjectConfig) map.get(key)
                    : new RawConfigurableObjectConfig(((RawConfigurableObjectConfig) extendedValue).getType(),
                        new HashMap<>());
                extendMapRecursive(innerConfig, (Map) extendedValue);
                map.put(key, innerConfig);
            } else if (extendedValue instanceof Map) {
                Map innerMap = containsKey ? (Map) map.get(key) : new HashMap();
                extendMapRecursive(innerMap, (Map) extendedValue);
                map.put(key, innerMap);
            } else if (!containsKey && extendedValue instanceof List) {
                List innerList = new ArrayList();
                extendListRecursive(innerList, (List) extendedValue);
                map.put(key, innerList);
            } else if (!containsKey) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
    }

    private void extendListRecursive(List list, List extendedList) {
        for (Object extendedValue : extendedList) {
            if (extendedValue instanceof Map) {
                Map innerMap = new HashMap();
                extendMapRecursive(innerMap, (Map) extendedValue);
                list.add(innerMap);
            } else if (extendedValue instanceof List) {
                List innerList = new ArrayList();
                extendListRecursive(innerList, (List) extendedValue);
                list.add(innerList);
            } else {
                list.add(extendedValue);
            }
        }
    }

    public String getExtendsId() {
        return extendsId;
    }

    public void setExtendsId(final String extendsId) {
        this.extendsId = extendsId;
    }

    public boolean isEventsEnabled() {
        return eventsEnabled;
    }

    public void setEventsEnabled(final boolean eventsEnabled) {
        this.eventsEnabled = eventsEnabled;
    }

    public Map<String, String> getInputTypes() {
        return inputTypes;
    }

    public void setInputTypes(final Map<String, String> inputTypes) {
        this.inputTypes = inputTypes;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(final String outputType) {
        this.outputType = outputType;
    }

    @Override
    public String toString() {
        return "ComponentConfig {" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", extendsId='" + extendsId + '\'' +
                ", properties=" + properties +
                ", propertyValueProviders=" + emptyIfNull(propertyValueProviders).keySet() +
                ", input=" + emptyIfNull(input).keySet() +
                ", output=" + emptyIfNull(output).keySet() +
                ", inputTypes=" + inputTypes.keySet() +
                ", outputType='" + outputType + '\'' +
                ", failOnError=" + failOnError +
                ", abstract=" + _abstract +
                ", internal=" + internal +
                ", eventsEnabled=" + eventsEnabled +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ComponentConfig that = (ComponentConfig) o;
        return isInternal() == that.isInternal() && isSink() == that.isSink()
            && _abstract == that._abstract && Objects.equals(getId(), that.getId())
            && Objects.equals(getType(), that.getType()) && Objects
            .equals(getExtendsId(), that.getExtendsId()) && Objects
            .equals(getProperties(), that.getProperties()) && Objects
            .equals(emptyIfNull(getPropertyValueProviders()).keySet(), emptyIfNull(that.getPropertyValueProviders()).keySet()) && Objects
            .equals(emptyIfNull(getInput()).keySet(), emptyIfNull(that.getInput()).keySet()) &&
            Objects.equals(emptyIfNull(output).keySet(), emptyIfNull(that.output).keySet())
            && Objects.equals(getInputTypes(), that.getInputTypes()) && Objects
            .equals(getOutputType(), that.getOutputType()) && Objects
            .equals(failOnError, that.failOnError) && Objects
            .equals(isEventsEnabled(), that.isEventsEnabled());
    }

    @Override
    public int hashCode() {
        return Objects
            .hash(getId(), getType(), getExtendsId(), getProperties(),
                emptyIfNull(getPropertyValueProviders()).keySet(),
                emptyIfNull(getInput()).keySet(), emptyIfNull(output).keySet(), getInputTypes(),
                getOutputType(), failOnError, getRawConfig(),
                isInternal(), isSink(), _abstract, isEventsEnabled());
    }

    private <K, V> Map<K, V> emptyIfNull(Map<K, V> map) {
        return map != null ? map : Collections.emptyMap();
    }
}
