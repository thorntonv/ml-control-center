/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowEnvironment.java
 */

package dataflow.core.environment;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.engine.DependencyInjector;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.registry.DataFlowRegistry;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Contains the environment used for executing a dataflow.
 */
@SuppressWarnings({"unused"})
public class DataFlowEnvironment {

    private final String id;
    private final DataFlowRegistry registry;
    private final long maxExecutionTimeMillis;
    private final DependencyInjector dependencyInjector;
    private ExecutorService executor = Executors.newCachedThreadPool();

    private static final class DefaultDependencyInjector extends SimpleDependencyInjector {

        public DefaultDependencyInjector() {
            super();
        }
    }

    @DataFlowConfigurable
    public DataFlowEnvironment(
            @DataFlowConfigProperty String id,
            @DataFlowConfigProperty(required = false) DependencyInjector dependencyInjector,
            @DataFlowConfigProperty(required = false) Long maxExecutionTimeMillis) {
        this.id = id;
        this.dependencyInjector = dependencyInjector != null ? dependencyInjector : new DefaultDependencyInjector();
        this.maxExecutionTimeMillis = maxExecutionTimeMillis != null ? maxExecutionTimeMillis : TimeUnit.DAYS.toMillis(1);

        this.registry = new DataFlowRegistry();
    }

    public String getId() {
        return id;
    }

    public DataFlowRegistry getRegistry() {
        return registry;
    }

    public DependencyInjector getDependencyInjector() {
        return dependencyInjector;
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public long getMaxExecutionTimeMillis() {
        return maxExecutionTimeMillis;
    }
}
