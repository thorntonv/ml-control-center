package dataflow.core.environment;

import dataflow.core.codegen.ComponentCodeGenerator;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.component.metadata.DataFlowConfigurableObjectMetadata;
import dataflow.core.component.metadata.DataFlowConfigurableObjectPropertyMetadata;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.DataFlowConfigurableObjectBuilder;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.parser.DataFlowParser;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.type.ValueType;
import dataflow.core.type.ValueTypeConverterFunction;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import static dataflow.core.codegen.DataFlowInstanceCodeGenerator.getDefaultClassName;

public class DataFlowDiscoveryService {

    private static final Logger logger = LoggerFactory.getLogger(DataFlowDiscoveryService.class);

    public void register(DataflowDiscoveryConfig discoveryConfig, DataFlowEnvironment env) {
        registerComponents(discoveryConfig, env);
        registerDataFlows(discoveryConfig, env);
    }

    public void registerDataFlows(DataflowDiscoveryConfig discoveryConfig, DataFlowEnvironment env) {
        DataFlowParser parser = new DataFlowParser(env.getRegistry());
        for(String configPath : discoveryConfig.getDataFlowConfigPaths()) {
            try {
                DataFlowConfig dataFlowConfig = parser.parse(getClass().getResourceAsStream("/" + configPath));
                register(dataFlowConfig, env);
                for(DataFlowConfig childConfig : dataFlowConfig.getChildDataFlows().values()) {
                    register(childConfig, env);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void register(DataFlowConfig dataFlowConfig, DataFlowEnvironment env) throws NoSuchMethodException, ClassNotFoundException {
        Class<? extends DataFlowInstance> instanceClass = (Class<? extends DataFlowInstance>) Class.forName("dataflow.core.environment." + getDefaultClassName(dataFlowConfig));
        Constructor<? extends DataFlowInstance> constructor = instanceClass.getDeclaredConstructor(DataFlowEnvironment.class, Executor.class, Consumer.class);
        constructor.setAccessible(true);
        env.getRegistry().registerDataFlow(dataFlowConfig, initFn ->
                constructor.newInstance(env, Executors.newCachedThreadPool(), initFn));
        logger.info("Registered dataflow " + dataFlowConfig.getId());
    }

    public void registerComponents(DataflowDiscoveryConfig discoveryConfig, DataFlowEnvironment env) {
        final DataFlowRegistry registry = env.getRegistry();
        Set<URL> urls = new HashSet<>();
        urls.addAll(ClasspathHelper.forJavaClassPath());
        urls.addAll(ClasspathHelper.forClassLoader(ClassLoader.getSystemClassLoader(), getClass().getClassLoader()));
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner())
                .setUrls(urls)
        );
        DataFlowExecutionContext.createExecutionContext(env);
        reflections.getSubTypesOf(DataFlowComponentMetadata.class).stream()
                .map(metadataClass -> {
                    try {
                        return (DataFlowComponentMetadata) metadataClass.getField("INSTANCE").get(null);
                    } catch (IllegalAccessException | NoSuchFieldException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .filter(metadata -> matches(metadata, discoveryConfig))
                .forEach(metadata -> {
                            try {
                                Class<?> builderClass = Class.forName(metadata.getBuilderClass());
                                DataFlowConfigurableObjectBuilder builder = (DataFlowConfigurableObjectBuilder) builderClass.getConstructor(DataFlowEnvironment.class).newInstance(new Object[]{env});
                                ComponentCodeGenerator codeGenerator = null;
                                if (metadata.getCodeGeneratorClass() != null) {
                                    Class<?> codeGeneratorClass = Class.forName(metadata.getCodeGeneratorClass());
                                    codeGenerator = (ComponentCodeGenerator) codeGeneratorClass.getConstructor().newInstance();
                                }
                                registry.getComponentRegistry().registerComponent(metadata, builder, codeGenerator);
                                logger.info("Registered component " + metadata.getComponentClassName());
                                if(metadata.getValueTypeConverterMetadata() != null) {
                                    Class<?> converterClass = Class.forName(metadata.getConfigurableObjectMetadata().getClassName());
                                    Constructor constructor = Arrays.stream(converterClass.getConstructors()).filter(c -> c.isAnnotationPresent(DataFlowConfigurable.class)).findAny().orElse(null);

                                    if(metadata.getConfigurableObjectMetadata().getPropertyMetadata().stream().noneMatch(DataFlowConfigurableObjectPropertyMetadata::isRequired)) {
                                        Object converterInstance = constructor.newInstance(new Object[constructor.getParameterCount()]);

                                        Method getValueMethod = Arrays.stream(converterClass.getMethods())
                                                .filter(method -> method.getName().equals("getValue") && method.getParameterCount() == 1)
                                                .findAny().orElse(null);
                                        Method getValueToTypeMethod = Arrays.stream(converterClass.getMethods())
                                                .filter(method -> method.getName().equals("getValue") && method.getParameterCount() == 2)
                                                .findAny().orElse(null);

                                        registry.getTypeRegistry().register(metadata, new ValueTypeConverterFunction<>() {
                                            @Override
                                            public Object getValue(Object value, ValueType toValueType) {
                                                try {
                                                    if(getValueToTypeMethod == null) {
                                                        return getValueMethod.invoke(converterInstance, value);
                                                    }
                                                    return getValueToTypeMethod.invoke(converterInstance, value, toValueType);
                                                } catch (IllegalAccessException | InvocationTargetException e) {
                                                    throw new RuntimeException(e);
                                                }
                                            }

                                            @Override
                                            public Object getValue(Object value) {
                                                try {
                                                    return getValueMethod.invoke(converterInstance, value);
                                                } catch (IllegalAccessException | InvocationTargetException e) {
                                                    throw new RuntimeException(e);
                                                }
                                            }
                                        });
                                    }
                                }
                            } catch (Throwable t) {
                                throw new RuntimeException(metadata.toString(), t);
                            }
                        }
                );
        reflections.getSubTypesOf(DataFlowConfigurableObjectBuilder.class).stream()
                .map(builderClass -> {
                    try {
                        return builderClass.getConstructor(DataFlowEnvironment.class).newInstance(env);
                    } catch (InvocationTargetException | InstantiationException | NoSuchMethodException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                })
                .forEach(builder -> {
                            try {
                                DataFlowConfigurableObjectMetadata metadata = (DataFlowConfigurableObjectMetadata)
                                        builder.getClass().getField("METADATA").get(null);
                                registry.getComponentRegistry().registerPropertyObjectBuilder(Class.forName(metadata.getClassName()), builder);
                            } catch (Throwable t) {
                                throw new RuntimeException(t);
                            }
                        }
                );
    }

    private boolean matches(DataFlowComponentMetadata metadata, DataflowDiscoveryConfig componentDiscoveryConfig) {
        String className = metadata.getClass().getName();
        if (componentDiscoveryConfig.getExcludeComponentClasses().stream()
                .map(Pattern::compile)
                .anyMatch(pattern -> pattern.matcher(className).matches())) {
            return false;
        }
        if (componentDiscoveryConfig.getIncludeComponentClasses().stream()
                .map(Pattern::compile)
                .anyMatch(pattern -> pattern.matcher(className).matches())) {
            return true;
        }
        String packageName = metadata.getClass().getPackageName();
        if (componentDiscoveryConfig.getExcludeComponentPackages().stream()
                .map(Pattern::compile)
                .anyMatch(pattern -> pattern.matcher(packageName).matches())) {
            return false;
        }
        return componentDiscoveryConfig.getComponentPackages().stream()
                .map(Pattern::compile)
                .anyMatch(pattern -> pattern.matcher(packageName).matches());
    }
}
