package dataflow.core.parser;

import dataflow.core.config.DataflowDiscoveryConfig;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;

public class DataFlowDiscoveryConfigParser {

    public DataflowDiscoveryConfig parse(InputStream in) throws IOException {
        Yaml yaml = new Yaml();
        if (in == null) {
            return null;
        }
        try (in) {
            return yaml.loadAs(in, DataflowDiscoveryConfig.class);
        }
    }
}
