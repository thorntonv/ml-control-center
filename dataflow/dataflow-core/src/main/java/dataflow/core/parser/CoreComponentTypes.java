package dataflow.core.parser;

public class CoreComponentTypes {

    static final String CONST_VALUE_PROVIDER = "ConstValueProvider";
    static final String DATAFLOW_INSTANCE_VALUE_PROVIDER = "DataFlowInstanceValueProvider";
    static final String PLACEHOLDER_STRING_VALUE_PROVIDER = "PlaceholderStringValueProvider";
    static final String MAP_ENTRY_VALUE_PROVIDER = "MapEntryValueProvider";
    static final String LIST_VALUE_PROVIDER = "ListValueProvider";
    static final String EXPRESSION_VALUE_PROVIDER = "ExpressionValueProvider";
}
