/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ListValueProvider.java
 */

package dataflow.core.component.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dataflow.core.component.annotation.InputValues;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;

@DataFlowComponent
public class ListValueProvider {

    private String[] itemKeys;
    private List<Object> list;

    @DataFlowConfigurable
    public ListValueProvider(@DataFlowConfigProperty List<String> itemKeys) {
        this.itemKeys = itemKeys.toArray(new String[0]);
        // Pre-allocate the return value.
        list = new ArrayList<>(itemKeys.size());
        for (int cnt = 1; cnt <= itemKeys.size(); cnt++) {
            list.add(null);
        }
    }

    @OutputValue
    public List<Object> getValue(@InputValues Map<String, Object> input) {
        for (int idx = 0; idx < itemKeys.length; idx++) {
            list.set(idx, input.get(itemKeys[idx]));
        }
        return list;
    }
}
