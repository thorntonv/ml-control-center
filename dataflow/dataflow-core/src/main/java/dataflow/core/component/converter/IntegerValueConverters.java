/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IntegerValueConverters.java
 */

package dataflow.core.component.converter;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.component.annotation.ValueConverter;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.ValueTypeConverterFunction;

public class IntegerValueConverters {

    @DataFlowComponent
    @ValueConverter(lossy = true)
    public static class IntegerValue_to_int_converter {

        @DataFlowConfigurable
        public IntegerValue_to_int_converter() {}

        @OutputValue
        public int getValue(@InputValue final IntegerValue value) {
            return value.toInt();
        }
    }

    @DataFlowComponent
    @ValueConverter
    public static final class IntegerValue_to_Integer_converter implements ValueTypeConverterFunction<IntegerValue, Integer> {

        @DataFlowConfigurable
        public IntegerValue_to_Integer_converter() {}

        @Override
        @OutputValue
        public Integer getValue(@InputValue final IntegerValue value) {
            return value != null ? value.toInt() : null;
        }
    }

    @DataFlowComponent
    @ValueConverter
    public static final class IntegerValue_to_String_converter implements ValueTypeConverterFunction<IntegerValue, String> {

        @DataFlowConfigurable
        public IntegerValue_to_String_converter() {}

        @Override
        @OutputValue
        public String getValue(@InputValue final IntegerValue value) {
            return value != null ? value.toString() : null;
        }
    }
}
