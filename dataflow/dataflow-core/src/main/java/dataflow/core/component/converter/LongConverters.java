/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LongConverters.java
 */

package dataflow.core.component.converter;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.component.annotation.ValueConverter;
import dataflow.core.type.LongValue;
import dataflow.core.type.ValueTypeConverterFunction;

public class LongConverters {

    private static final ValueTypeConverterFunction<Long, Long> IDENTITY_CONVERTER = new ValueTypeConverterFunction<Long, Long>() {
        @Override
        public Long getValue(final Long value) {
            return value;
        }
    };

    @DataFlowComponent
    @ValueConverter
    public static final class Long_to_long_converter {

        @DataFlowConfigurable
        public Long_to_long_converter() {}

        @OutputValue
        public long getValue(@InputValue Long input) {
            return input;
        }
    }

    @DataFlowComponent
    @ValueConverter
    public static final class Long_to_LongValue_converter implements ValueTypeConverterFunction<Long, LongValue> {

        @DataFlowConfigurable
        public Long_to_LongValue_converter() {}

        @OutputValue
        public LongValue getValue(@InputValue Long input) {
            return input != null ? new LongValue(input) : new LongValue();
        }
    }

    @DataFlowComponent
    @ValueConverter
    public static final class Long_to_String_converter implements ValueTypeConverterFunction<Long, String> {

        @DataFlowConfigurable
        public Long_to_String_converter() {}

        @OutputValue
        public String getValue(@InputValue Long input) {
            return String.valueOf(input);
        }
    }
}
