/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ZipFileDataSourceITest.java
 */

package dataflow.core.datasource;

import com.google.common.io.ByteStreams;
import dataflow.core.datasource.DataSource;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Integration test for {@link ZipFileDataSource}.
 */
public class ZipFileDataSourceITest {

    private static final String TEST_ENTRY_NAME1 = "test-1.txt";
    private static final String TEST_ENTRY_NAME2 = "test-2.txt";
    private static final String TEST_DIR1 = "dir1/";

    @Test
    public void testReadEntry() throws IOException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource = new ZipFileDataSource(temp.getAbsolutePath(), TEST_ENTRY_NAME1);
            byte[] buffer = new byte[1024];
            try (InputStream in = dataSource.getInputStream()) {
                int len = in.read(buffer);
                assertEquals("test1", new String(buffer, 0, len));
            }
        } finally {
            temp.delete();
        }
    }

    @Test
    public void testGetChildren() throws IOException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource = new ZipFileDataSource(temp.getAbsolutePath(), null);
            Map<String, DataSource> children = dataSource.getChildren().stream()
                    .collect(Collectors.toMap(DataSource::getPath, child -> child));
            assertEquals(5, children.size());
            assertTrue(children.containsKey("/" + TEST_DIR1));
        } finally {
            temp.delete();
        }
    }

    @Test
    public void testGetParent() throws IOException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource = new ZipFileDataSource(temp.getAbsolutePath(), TEST_ENTRY_NAME1);
            ZipFileDataSource parent = dataSource.getParent();
            assertTrue(parent.getChild(TEST_ENTRY_NAME1).isPresent());
        } finally {
            temp.delete();
        }
    }

    @Test
    public void testGetChildren_fileEntry() throws IOException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource = new ZipFileDataSource(temp.getAbsolutePath(), TEST_ENTRY_NAME1);
            // Single file should have no children.
            assertEquals(0, dataSource.getChildren().size());
        } finally {
            temp.delete();
        }
    }

    @Test
    public void testGetChildren_dirEntry() throws IOException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource = new ZipFileDataSource(temp.getAbsolutePath(), TEST_DIR1);
            assertEquals(2, dataSource.getChildren().size());
        } finally {
            temp.delete();
        }
    }

    @Test
    public void testHasChanged() throws IOException, InterruptedException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource = new ZipFileDataSource(temp.getAbsolutePath(), null);
            assertTrue(dataSource.hasChanged());
            assertFalse(dataSource.hasChanged());

            Thread.sleep(2000);
            ByteStreams.copy(ClassLoader.getSystemResourceAsStream("test.zip"), new FileOutputStream(temp));

            assertTrue(dataSource.hasChanged());
        } finally {
            temp.delete();
        }
    }

    @Test
    public void testHasChanged_updateEntry() throws IOException, InterruptedException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource = new ZipFileDataSource(temp.getAbsolutePath(), TEST_ENTRY_NAME1);
            assertTrue(dataSource.hasChanged());
            assertFalse(dataSource.hasChanged());

            Thread.sleep(2000);
            try (FileSystem zipfs = getZipFileSystem(temp)) {
                Files.setLastModifiedTime(zipfs.getPath(TEST_ENTRY_NAME1), FileTime.fromMillis(1005));
            }

            assertTrue(dataSource.hasChanged());
        } finally {
            temp.delete();
        }
    }

    @Test
    public void testHasChanged_deleteEntry() throws IOException, InterruptedException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource = new ZipFileDataSource(temp.getAbsolutePath(), TEST_ENTRY_NAME1);
            assertTrue(dataSource.hasChanged());
            assertFalse(dataSource.hasChanged());

            Thread.sleep(2000);
            try (FileSystem zipfs = getZipFileSystem(temp)) {
                Files.delete(zipfs.getPath(TEST_ENTRY_NAME1));
            }

            assertTrue(dataSource.hasChanged());
        } finally {
            temp.delete();
        }
    }

    @Test
    public void testGetRoots() throws IOException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource = new ZipFileDataSource(temp.getAbsolutePath(), TEST_ENTRY_NAME1);
            List<DataSource> roots = dataSource.getRoots();
            assertEquals(1, roots.size());
            DataSource root = roots.get(0);
            assertEquals(DataSource.PATH_SEPARATOR, root.getPath());
            assertTrue(root.getChildren().size() > 0);
        } finally {
            temp.delete();
        }
    }

    @Test
    public void testEqualsAndHashCode() throws IOException {
        File temp = createTempFile();
        try {
            ZipFileDataSource dataSource1 = new ZipFileDataSource(temp.getAbsolutePath(), TEST_ENTRY_NAME1);
            ZipFileDataSource dataSource2 = new ZipFileDataSource(temp.getAbsolutePath(), TEST_ENTRY_NAME1);
            assertEquals(dataSource1, dataSource1);
            assertEquals(dataSource1, dataSource2);
            assertEquals(dataSource1.hashCode(), dataSource2.hashCode());
            ZipFileDataSource dataSource3 = new ZipFileDataSource(temp.getAbsolutePath(), TEST_ENTRY_NAME2);
            assertNotEquals(dataSource1, dataSource3);
        } finally {
            temp.delete();
        }
    }

    private static FileSystem getZipFileSystem(File zipFile) throws IOException {
        URI zipUri = URI.create("jar:file:" + zipFile.getAbsolutePath());
        Map<String, String> zip_properties = new HashMap<>();
        zip_properties.put("create", "false");
        zip_properties.put("encoding", "UTF-8");
        return FileSystems.newFileSystem(zipUri, zip_properties);
    }

    private File createTempFile() throws IOException {
        File temp = File.createTempFile("test", ".zip");
        ByteStreams.copy(ClassLoader.getSystemResourceAsStream("test.zip"), new FileOutputStream(temp));
        return temp;
    }
}