/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LocalFileDataSourceITest.java
 */

package dataflow.core.datasource;

import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class LocalFileDataSourceITest {

    @Test
    public void getSourcesMatchingPattern() throws IOException {
        File tempFile = File.createTempFile("LocalFileSource", ".tmp");
        try (OutputStream out = new FileOutputStream(tempFile)) {
            out.write(1);
        }
        assertTrue(tempFile.exists());
        try {
            List<DataSource> sources = new ArrayList<>();
            String globPattern = tempFile.getParent() + File.separator + "Local*Source*.tmp";
            for (DataSource root : new LocalFileDataSource(tempFile.getAbsolutePath()).getRoots()) {
                sources.addAll(DataSourceUtil.getDataSourcesMatchingGlobPattern(root, globPattern));
            }
            assertTrue(sources.size() > 0);
            for (DataSource dataSource : sources) {
                assertTrue(dataSource instanceof LocalFileDataSource);
                assertTrue(dataSource.getInputStream().read() > 0);
            }
        } finally {
            tempFile.delete();
        }
    }

    @Test
    public void testHasChanged() throws IOException, InterruptedException {
        File tempFile = File.createTempFile("LocalFileSource", ".tmp");
        try {
            DataSource dataSource = new LocalFileDataSource(tempFile.getAbsolutePath());
            assertTrue(dataSource.hasChanged());
            assertFalse(dataSource.hasChanged());

            Thread.sleep(2000);
            try(OutputStream out = new FileOutputStream(tempFile)) {
                out.write("this is a test".getBytes());
                out.flush();
            }
            assertTrue(dataSource.hasChanged());
            assertFalse(dataSource.hasChanged());
        } finally {
            tempFile.delete();
        }
    }

    @Test
    public void testEqualsAndHashCode() throws IOException, InterruptedException {
        File tempFile1 = File.createTempFile("LocalFileSource", ".tmp");
        File tempFile2 = File.createTempFile("LocalFileSource", ".tmp");
        try {
            DataSource dataSource1 = new LocalFileDataSource(tempFile1.getAbsolutePath());
            DataSource dataSource2 = new LocalFileDataSource(tempFile1.getAbsolutePath());
            DataSource dataSource3 = new LocalFileDataSource(tempFile2.getAbsolutePath());
            assertEquals(dataSource1, dataSource1);
            assertEquals(dataSource1, dataSource2);
            assertEquals(dataSource1.hashCode(), dataSource2.hashCode());
            assertNotEquals(dataSource1, dataSource3);
        } finally {
            tempFile1.delete();
            tempFile2.delete();
        }
    }

    @Test
    public void testGetParent() throws IOException {
        File tempFile = File.createTempFile("LocalFileSource", ".tmp");
        try {
            DataSource dataSource = new LocalFileDataSource(tempFile.getAbsolutePath());
            DataSource parent = dataSource.getParent();
            assertTrue(parent.getChild(tempFile.getName()).isPresent());
        } finally {
            tempFile.delete();
        }
    }

    @Test
    public void testPath_noTrailingSeparator() {
        DataSource dataSource1 = new LocalFileDataSource("/abcd/");
        DataSource dataSource2 = new LocalFileDataSource("/abcd");
        assertEquals(dataSource1, dataSource2);
    }
}