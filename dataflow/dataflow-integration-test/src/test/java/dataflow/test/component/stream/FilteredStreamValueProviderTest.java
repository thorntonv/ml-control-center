/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FilteredStreamValueProviderTest.java
 */

package dataflow.test.component.stream;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class FilteredStreamValueProviderTest {

    private DataFlowEnvironment env;

    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig = discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void getValue() throws Exception {
        Stream<String> stream = ImmutableList.of("a", "xyz", "ab", "abc").stream();
        Stream<String> mappedStream = (Stream<String>) testFlow("filteredStreamTest",
                ImmutableMap.<String, Object>builder()
                        .put("stream", stream)
                        .build());
        assertEquals(ImmutableList.of("xyz", "abc"), mappedStream.collect(Collectors.toList()));
    }

    private Object testFlow(String id, Map<String, Object> input) throws Exception {
        try (DataFlowInstance instance = env.getRegistry().getDataFlowRegistration(id).getInstanceFactory().newInstance(i -> {})) {
            input.forEach(instance::setValue);
            instance.execute();
            return instance.getOutput();
        }
    }
}