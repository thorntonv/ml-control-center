/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FailingValueProvider.java
 */

package dataflow.test.provider;

import java.io.IOException;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public class FailingValueProvider {

    @DataFlowConfigurable
    public FailingValueProvider() {
    }

    @OutputValue
    public String getValue(@InputValue(required = false) Boolean shouldFail) throws IOException {
        if (shouldFail == null || shouldFail) {
            throw new IOException();
        }
        return "OK";
    }
}
