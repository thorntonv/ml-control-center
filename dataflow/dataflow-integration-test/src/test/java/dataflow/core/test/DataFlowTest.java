/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowTest.java
 */

package dataflow.core.test;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.codegen.DataFlowInstanceCodeGenConfig;
import dataflow.core.codegen.DataFlowInstanceCodeGenerator;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.DependencyInjector;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.parser.DataFlowParser;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.LongValue;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class DataFlowTest {

    private DataFlowEnvironment env;
    private DependencyInjector dependencyInjector;

    @Before
    public void setUp() throws IOException {
        this.dependencyInjector = new SimpleDependencyInjector("myTestVal");
        this.env = new DataFlowEnvironment("test", dependencyInjector, null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig = discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void test_singleValueProvider() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_singleValueProvider");
        instance.execute();
        assertEquals(1L, ((LongValue) instance.getOutput()).toLong());
    }

    @Test
    public void test_multipleInputValues() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_multipleInputValues");
        instance.setValue("in1", 35);
        instance.execute();
        assertEquals(57, ((IntegerValue) instance.getOutput()).toInt());
    }

    @Test
    public void test_placeholderString() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_placeholderStringProperty");
        instance.setValue("sep1", "%");
        instance.setValue("sep2", "@");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("in3", "C");
        instance.execute();
        assertEquals("A_%_@B_%_@C", instance.getOutput());
    }

    @Test
    public void test_placeholderString_multipleValueRefs() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_placeholderString_multipleValueRefs");
        instance.setValue("sep1", "%");
        instance.setValue("sep2", "@");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("in3", "C");
        instance.execute();
        assertEquals("A%_@B%_@C", instance.getOutput());
    }

    @Test
    public void test_placeholderStringInInput() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_placeholderStringInInput");
        instance.setValue("in1", ",");
        instance.setValue("in2", "A");
        instance.setValue("in3", "B");
        instance.execute();
        assertEquals("A_test2,test3_B", instance.getOutput());
    }

    @Test
    public void test_buildMap() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_buildMap");
        instance.setValue("in1", "X");
        instance.execute();
        Map outputMap = (Map) instance.getOutput();
        assertEquals(3, outputMap.size());
        assertEquals("X", outputMap.get("val1"));
        assertEquals("someValue2", outputMap.get("val2"));
        assertEquals("X,Y", outputMap.get("val3"));
    }

    @Test
    public void test_mapInput() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_mapInput");
        instance.execute();
        Map outputMap = (Map) instance.getOutput();
        assertNotNull(outputMap);
    }

    @Test
    public void test_mapEntry() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_mapEntry");
        instance.setValue("in1", "A");
        instance.setValue("in2", ImmutableMap.of("value", "B"));
        instance.execute();
        assertEquals("B", instance.getOutput());
    }

    @Test
    public void test_mapInvalidEntry() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_mapInvalidEntry");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        try {
            instance.execute();
            fail("Expected exception was not thrown");
        } catch (DataFlowExecutionException ex) {
            assertEquals("java.lang.IllegalArgumentException: Map must have an entry with key in2: {v2=B, v1=A}", ex.getMessage());
        }
    }

    @Test
    public void test_incremental() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_incremental");

        instance.execute();
        assertNull(instance.getValue("provider1"));
        assertNull(instance.getOutput());

        instance.setValue("in1", "A");
        instance.execute();
        assertNull(instance.getValue("provider1"));
        assertNull(instance.getOutput());

        instance.setValue("in2", "B");
        instance.execute();
        // Inputs 1 and 2 are set so the value of provider1 is available, but input 3 is not set so provider 2 value
        // is not available.
        assertEquals("A,B", instance.getValue("provider1"));
        assertNull(instance.getOutput());

        // All inputs are available so the value of provider2 is available.
        instance.setValue("in3", "C");
        instance.execute();
        assertEquals("A,B", instance.getValue("provider1"));
        assertEquals("A,B,C", instance.getOutput());

        // Update the value of input 2. this should cause provider1 and provider2 values to be recomputed.
        instance.setValue("in2", "X");
        instance.execute();
        assertEquals("A,X", instance.getValue("provider1"));
        assertEquals("A,X,C", instance.getOutput());
    }

    @Test
    public void test_asyncValueProvider1() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncValueProvider1");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{input={input=test}A}B", instance.getOutput());
    }

    @Test
    public void test_asyncValueProvider2() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncValueProvider2");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{in1={input=test}A, in2={input=test}B}C", instance.getOutput());
    }

    @Test
    public void test_asyncThreadValueProvider1() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncThreadValueProvider1");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{input={input=test}A}B", instance.getOutput());
    }

    @Test
    public void test_asyncThreadValueProvider2() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncThreadValueProvider2");
        instance.setValue("in", "test");
        instance.execute();
        Stopwatch stopwatch = Stopwatch.createStarted();
        assertEquals("{in1={input=test}A, in2={input=test}B, in3={input=test}C, in4={input=test}D}E", instance.getOutput());

        // If the four inputs execute concurrently then this test should complete in about 10 seconds.
        assertTrue(stopwatch.elapsed(TimeUnit.SECONDS) < 15);
    }

    @Test
    public void test_syncValueProviderAfterAsync() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_syncValueProviderAfterAsync");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{input=test}A:{input=test}B", instance.getOutput());
    }

    @Test
    public void test_errorNoFallback() {
        try {
            DataFlowInstance instance = getInstance("testFlow_errorNoFallback");
            instance.execute();
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("java.io.IOException", ex.getMessage());
        }
    }

    @Test
    public void test_syncErrorFallback() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_syncErrorFallback");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("ABC", instance.getOutput());
    }

    @Test
    public void test_asyncErrorFallback_const() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorFallback_const");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("ABC", instance.getOutput());
    }

    @Test
    public void test_asyncErrorFallback_sync() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorFallback_sync");
        instance.setValue("in1", "test");
        instance.execute();
        assertEquals("test", instance.getOutput());
    }

    @Test
    public void test_asyncErrorNoFallback() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorNoFallback");
        instance.setValue("in", "test");
        try {
            instance.execute();
            fail("Expected exception was not thrown");
        } catch(Exception ex) {
            assertEquals(null, instance.getOutput());
        }
    }

    @Test
    public void test_asyncErrorNoFallbackNoFail() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorNoFallbackNoFail");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals(null, instance.getOutput());
    }

    @Test
    public void test_syncMissingValueFallback() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_syncMissingValueFallback");
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("ABC", instance.getOutput());
    }

    @Test
    public void test_joinList() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_joinList");
        instance.setValue("in1", "X");
        instance.setValue("in3", "Z");
        instance.execute();
        assertEquals("X:Y:Z", instance.getOutput());
    }

    @Test
    public void test_transformDataRecords() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_transformDataRecords");
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("count", 5);
        records.add(record);
        instance.setValue("in", records);
        instance.execute();
        DataRecords output = (DataRecords) instance.getOutput();
        assertEquals(1, output.size());
        assertEquals(2, output.get(0).size());
        assertEquals(6, output.get(0).get("count"));
        assertEquals("abc", output.get(0).get("test"));
        assertEquals(1, records.get(0).size());
        assertEquals(ImmutableList.of("count", "test"), output.getColumnNames());
    }

    @Test
    public void test_dynamicProperty() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_dynamicProperty");
        instance.setValue("in3", "X");
        instance.setValue("in4", "Y");
        instance.execute();
        assertEquals("BA:XY", instance.getOutput());
    }

    @Test
    public void test_customProperties() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_customProperties");
        instance.execute();
        assertEquals("EDCBA", instance.getOutput());
    }

    @Test
    public void test_dynamicProvidedProperty() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_dynamicProvidedProperty");
        instance.setValue("separator", "@");
        instance.setValue("in1", "X");
        instance.setValue("in2", "Y");
        instance.execute();
        assertEquals("X@Y", instance.getOutput());
    }

    @Test
    public void test_missingInput() {
        try {
            DataFlowParser parser = new DataFlowParser(env.getRegistry());
            DataFlowConfig dataFlowConfig = parser.parse(ClassLoader.getSystemResourceAsStream("dataflow/core/test/TestFlow-missingInput.yaml"));
            DataFlowInstanceCodeGenerator codeGenerator = new DataFlowInstanceCodeGenerator(env.getRegistry());
            DataFlowInstanceCodeGenConfig codeGenConfig = new DataFlowInstanceCodeGenConfig("dataflow.environment", "TestFlow_missingInputDataFlow", true, true, true);
            codeGenerator.generateSource(dataFlowConfig, "", codeGenConfig);
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Input values not defined for component provider1", ex.getMessage());
        }
    }

    @Test
    public void test_intConstToStringConversion() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_intConstToStringConversion");
        instance.execute();
        assertEquals("54321", instance.getOutput());
    }

    @Test
    public void test_intToStringConversion() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_intToStringConversion");
        instance.setValue("in1", 12345);
        instance.setValue("in2", 56789);
        instance.execute();
        assertEquals("98765", instance.getOutput());
    }

    @Test
    public void test_dependencyInjection() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_dependencyInjection");
        instance.execute();
        assertEquals("myTestVal", instance.getOutput());
    }

    @Test
    public void test_constOutput() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_constOutput");
        instance.execute();
        assertEquals("result", instance.getOutput());
    }

    @Test
    public void test_asyncError_valueCleared() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorValueCleared");
        instance.setValue("shouldFail", false);
        instance.execute();
        assertEquals("OK", instance.getOutput());
        instance.setValue("shouldFail", true);
        instance.execute();
        assertNull(instance.getOutput());
    }

    @Test
    public void test_syncError_valueCleared() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_syncErrorValueCleared");
        instance.setValue("shouldFail", false);
        instance.execute();
        assertEquals("OK", instance.getOutput());
        instance.setValue("shouldFail", true);
        instance.execute();
        assertNull(instance.getOutput());
    }

    @Test
    public void test_optionalInput_missing() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_optionalInputMissing");
        instance.execute();
        assertEquals("EMPTY", instance.getOutput());
    }

    @Test
    public void test_optionalInput_present() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_optionalInputPresent");
        instance.execute();
        assertEquals("test", instance.getOutput());
    }

    @Test
    public void test_sink() throws Exception {
        List<String> list = new ArrayList<>();
        DataFlowInstance instance = getInstance("testFlow_sink");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("list", list);
        assertEquals(0, list.size());
        instance.execute();
        assertEquals(ImmutableMap.of("v1", "A", "v2", "B"), instance.getOutput());
        assertEquals(1, list.size());
        assertEquals("test", list.get(0));
    }

    @Test
    public void test_sinkWithOutputAsDependency() throws Exception {
        List<String> list = new ArrayList<>();
        DataFlowInstance instance = getInstance("testFlow_sinkWithOutputAsDependency");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("list", list);
        assertEquals(0, list.size());
        instance.execute();
        assertEquals("A:B", instance.getOutput());
        assertEquals(1, list.size());
        assertEquals("A:B", list.get(0));
    }

    @Test
    public void test_sink_sinkPropertyNotSet() {
        try {
            DataFlowParser parser = new DataFlowParser(env.getRegistry());
            DataFlowConfig dataFlowConfig = parser.parse(ClassLoader.getSystemResourceAsStream("dataflow/core/test/TestFlow-sink-propNotSet.yaml"));
            DataFlowInstanceCodeGenerator codeGenerator = new DataFlowInstanceCodeGenerator(env.getRegistry());
            DataFlowInstanceCodeGenConfig codeGenConfig = new DataFlowInstanceCodeGenConfig("dataflow.environment", "TestFlow_sink_propNotSet", true, true, true);
            codeGenerator.generateSource(dataFlowConfig, "", codeGenConfig);
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("[_TestSinkValueProvider0] components are not used and are not abstract", ex.getMessage());
        }
    }

    private DataFlowInstance getInstance(String id) throws Exception {
        return env.getRegistry().getDataFlowRegistration(id).getInstanceFactory().newInstance();
    }
}
