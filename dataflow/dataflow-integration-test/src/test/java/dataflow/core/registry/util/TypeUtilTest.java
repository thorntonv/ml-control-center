/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TypeUtilTest.java
 */

package dataflow.core.registry.util;

import java.util.ArrayList;

import dataflow.core.type.TypeUtil;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TypeUtilTest {

    @Test
    public void getPackageName() {
        assertEquals(ArrayList.class.getPackage().getName(), TypeUtil.getPackageName(ArrayList.class.getName()));
        assertEquals(ArrayList.class.getPackage().getName(), TypeUtil.getPackageName("java.util.ArrayList<String>"));
        assertEquals("", TypeUtil.getPackageName("Test"));
    }

    @Test
    public void getSimpleClassName() {
        assertEquals(ArrayList.class.getSimpleName(), TypeUtil.getSimpleClassName(ArrayList.class.getName()));
        assertEquals(ArrayList.class.getSimpleName(), TypeUtil.getSimpleClassName("java.util.ArrayList<String>"));
        assertEquals("Test", TypeUtil.getSimpleClassName("Test"));
        assertEquals("Test", TypeUtil.getSimpleClassName("Test<String>"));
    }

    @Test
    public void removeGenericTypes() {
        assertEquals("java.util.Map", TypeUtil.removeGenericTypes("java.util.Map"));
        assertEquals("java.util.Map", TypeUtil.removeGenericTypes("java.util.Map<>"));
        assertEquals("java.util.ArrayList", TypeUtil.removeGenericTypes("java.util.ArrayList<String>"));
        assertEquals("java.util.Map", TypeUtil.removeGenericTypes("java.util.Map<String, Object>"));
        assertEquals("java.util.ArrayList[]", TypeUtil.removeGenericTypes("java.util.ArrayList<String>[]"));
    }

    @Test
    public void getGenericTypes() {
        assertArrayEquals(new String[]{}, TypeUtil.getGenericTypes("java.util.Map"));
        assertArrayEquals(new String[]{}, TypeUtil.getGenericTypes("java.util.Map<>"));
        assertArrayEquals(new String[]{"String"}, TypeUtil.getGenericTypes("java.util.ArrayList<String>"));
        assertArrayEquals(new String[]{"String", "Object"}, TypeUtil.getGenericTypes("java.util.Map<String, Object>"));
        assertArrayEquals(new String[]{"java.util.Map<java.lang.String,java.lang.Object>"}, TypeUtil.getGenericTypes("java.util.List<java.util.Map<java.lang.String,java.lang.Object>>"));
    }
}