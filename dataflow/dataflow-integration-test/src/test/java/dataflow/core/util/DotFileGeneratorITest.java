/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DotFileGeneratorITest.java
 */

package dataflow.core.util;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.parser.DataFlowParser;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class DotFileGeneratorITest {

    @Test
    public void generateDOTFile() throws DataFlowParseException, IOException {
        DataFlowEnvironment env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);
        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig = discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);

        DataFlowParser parser = new DataFlowParser(env.getRegistry());
        DataFlowConfig config = parser.parse(
                ClassLoader.getSystemResourceAsStream("dataflow/core/parser/test-flow-2.yaml"));
        assertEquals("digraph test_2 {\n" +
                "    _list1 -> provider3[ label=\"fallbackValues\" ];\n" +
                "    provider2 -> provider3[ label=\"value\" ];\n" +
                "    _ConstValueProvider0 -> _list1[ label=\"\" ];\n" +
                "    _list0 -> provider2[ label=\"values\" ];\n" +
                "    _const0 -> _list0[ label=\"\" ];\n" +
                "    _const1 -> _list0[ label=\"\" ];\n" +
                "    _const1 [label=< <b>b</b><br/><i>ConstValueProvider</i> >];\n" +
                "    _const0 [label=< <b>a</b><br/><i>ConstValueProvider</i> >];\n" +
                "    _list1 [label=< <i>ListValueProvider</i> >];\n" +
                "    _ConstValueProvider0 [label=< <b>a/b</b><br/><i>ConstValueProvider</i> >];\n" +
                "    _list0 [label=< <i>ListValueProvider</i> >];\n" +
                "    provider2 [label=< <b>provider2</b><br/><i>StringJoinValueProvider</i> >];\n" +
                "    provider3 [shape=diamond, label=< <b>provider3</b><br/><i>FallbackValueProvider</i> >];\n" +
                "}\n", DotFileGenerator.generateDOTFile(config));
    }
}