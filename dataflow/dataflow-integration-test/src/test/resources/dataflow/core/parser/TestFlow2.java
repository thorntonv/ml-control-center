package dataflow.test;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.engine.AbstractDataFlowInstance;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.type.BooleanValue;
import dataflow.core.type.DoubleValue;
import dataflow.core.type.FloatValue;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.LongValue;
import dataflow.core.type.ValueType;
import dataflow.core.util.MapUtil;
import java.lang.AutoCloseable;
import java.util.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;

public class TestFlow2 extends AbstractDataFlowInstance {
    private static final String YAML_CONFIG = "DataFlow:\n  id: test_2\n  ValueProviders:\n    - !StringJoinValueProvider\n      abstract: true\n      id: provider1\n      separator: ':'\n    - !ValueProvider\n      id: provider2\n      extends: provider1\n      input: { values: [ 'a', 'b' ] }\n    - !FallbackValueProvider\n      id: provider3\n      input:\n        value: $(provider2)\n        fallbackValues:\n          - !ConstValueProvider\n            value: 'a/b'\n\n  output: $(provider3)\n";
    private final Map<String, Runnable> componentInitRunnables = new HashMap<>();
    private dataflow.core.component.collection.ListValueProvider __list1_component;
    private volatile java.util.List<java.lang.Object> __list1_value;
    private Map<String, Object> __list1_inputMap = new LinkedHashMap<>();
    private boolean __list1_available = true;
    private volatile boolean __list1_finished = false;
    private dataflow.core.component.collection.ListValueProvider __list0_component;
    private volatile java.util.List<java.lang.Object> __list0_value;
    private Map<String, Object> __list0_inputMap = new LinkedHashMap<>();
    private boolean __list0_available = true;
    private volatile boolean __list0_finished = false;
    private dataflow.test.provider.StringJoinValueProvider _provider2_component;
    private volatile java.lang.String _provider2_value;
    private boolean _provider2_available = true;
    private volatile boolean _provider2_finished = false;
    private dataflow.core.component.FallbackValueProvider _provider3_component;
    private volatile java.lang.Object _provider3_value;
    private boolean _provider3_available = true;
    private volatile boolean _provider3_finished = false;

    TestFlow2(DataFlowEnvironment env, Executor executor, Consumer<DataFlowInstance> initFn) {
        super("test_2", YAML_CONFIG, env, executor);
        initFn.accept(this);
        init();
    }

    public void init() {
        createExecutionContext();
        try {
            __list1_init();
            __list0_init();
            _provider2_init();
            _provider3_init();
            updateAvailability();
        } finally { DataFlowExecutionContext.popExecutionContext(); }
    }

    private void __list1_init() {
        this.__list1_component = (dataflow.core.component.collection.ListValueProvider) buildObject("ListValueProvider", dataFlowConfig.getComponents().get("_list1").getProperties(), this.getValues());
        this.__list1_inputMap.put("0", "a/b");
    }

    private void __list0_init() {
        this.__list0_component = (dataflow.core.component.collection.ListValueProvider) buildObject("ListValueProvider", dataFlowConfig.getComponents().get("_list0").getProperties(), this.getValues());
        this.__list0_inputMap.put("0", "a");
        this.__list0_inputMap.put("1", "b");
    }

    private void _provider2_init() {
        this._provider2_component = (dataflow.test.provider.StringJoinValueProvider) buildObject("StringJoinValueProvider", dataFlowConfig.getComponents().get("provider2").getProperties(), this.getValues());
    }

    private void _provider3_init() {
        this._provider3_component = (dataflow.core.component.FallbackValueProvider) buildObject("FallbackValueProvider", dataFlowConfig.getComponents().get("provider3").getProperties(), this.getValues());
    }

    public Map<String, Object> getComponents() {
        Map<String, Object> components = new LinkedHashMap<>();
        components.put("_list1", __list1_component);
        components.put("_list0", __list0_component);
        components.put("provider2", _provider2_component);
        components.put("provider3", _provider3_component);
        return components;
    }

    public void setValue(String id, Object value) {
        switch(id) {
            case "provider2":
                _provider2_value = (java.lang.String)value;
                _provider2_finished = true;
                _provider3_finished = false;
                break;
            case "provider3":
                _provider3_value = value;
                _provider3_finished = true;
                break;
        }
        updateAvailability();
    }

    public Map<String, Object> getValues() {
        Map<String, Object> values = new HashMap<>();
        values.put("_list1", __list1_value);
        values.put("_list0", __list0_value);
        values.put("provider2", _provider2_value);
        values.put("provider3", _provider3_value);
        return values;
    }

    public Object getValue(String id) {
        switch(id) {
            case "_list1":
                return __list1_value;
            case "_list0":
                return __list0_value;
            case "provider2":
                return _provider2_value;
            case "provider3":
                return _provider3_value;
        }
        return null;
    }

    public String getOutputType() {
        return "java.lang.Object";
    }

    public String getValueType(String id) {
        switch(id) {
            case "_list1":
                return "java.util.List<java.lang.Object>";
            case "_list0":
                return "java.util.List<java.lang.Object>";
            case "provider2":
                return "java.lang.String";
            case "provider3":
                return "java.lang.Object";
        }
        return null;
    }

    public void execute() throws DataFlowExecutionException {
        DataFlowExecutionContext executionContext = createExecutionContext();
        this.startTimeMillis = System.currentTimeMillis();
        try {
            componentInitRunnables.values().forEach(runnable -> runnable.run());
            if(!componentInitRunnables.isEmpty()) updateAvailability();
            componentInitRunnables.clear();
            clearExecutedFutures();
            if(__list1_available && !__list1_finished) {
                __list1_value = ((java.util.List<java.lang.Object>)__list1_component.getValue(__list1_inputMap));
                __list1_finished = true;
            }
            if(__list0_available && !__list0_finished) {
                __list0_value = ((java.util.List<java.lang.Object>)__list0_component.getValue(__list0_inputMap));
                __list0_finished = true;
            }
            if(_provider2_available && !_provider2_finished) {
                _provider2_value = ((java.lang.String)_provider2_component.getValue(((java.util.List<java.lang.String>) dataflow.core.type.TypeUtil.convert(__list0_value, dataflow.core.type.TypeUtil.valueTypeFromString("java.util.List<java.lang.Object>"), dataflow.core.type.TypeUtil.valueTypeFromString("java.util.List<java.lang.String>"), environment.getRegistry().getTypeRegistry()))));
                _provider2_finished = true;
            }
            if(_provider3_available && !_provider3_finished) {
                _provider3_value = ((java.lang.Object)_provider3_component.getValue(_provider2_value, ((java.util.List) dataflow.core.type.TypeUtil.convert(__list1_value, dataflow.core.type.TypeUtil.valueTypeFromString("java.util.List<java.lang.Object>"), dataflow.core.type.TypeUtil.valueTypeFromString("java.util.List"), environment.getRegistry().getTypeRegistry()))));
                _provider3_finished = true;
            }
            waitForExecutedFutures();
            this.endTimeMillis = System.currentTimeMillis();
            fireDataFlowExecutionSuccessEvent();
        } catch(Throwable t) {
            this.endTimeMillis = System.currentTimeMillis();
            fireDataFlowExecutionErrorEvent(t);
            cancelExecutedFutures();
            throw new DataFlowExecutionException(t);
        } finally {
            DataFlowExecutionContext.popExecutionContext();
        }
    }

    public java.lang.Object getOutput() {
        return _provider3_value;
    }

    public void updateAvailability() {
        __list1_available = true;
        __list0_available = true;
        _provider2_available = __list0_available;
        _provider3_available = __list1_available && _provider2_available;
    }

    public void close() {
        super.close();
        try {
            if(__list1_component != null && (__list1_component instanceof AutoCloseable)) ((AutoCloseable) __list1_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
        try {
            if(__list0_component != null && (__list0_component instanceof AutoCloseable)) ((AutoCloseable) __list0_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
        try {
            if(_provider2_component != null && (_provider2_component instanceof AutoCloseable)) ((AutoCloseable) _provider2_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
        try {
            if(_provider3_component != null && (_provider3_component instanceof AutoCloseable)) ((AutoCloseable) _provider3_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
    }
}
