package dataflow.test.component;

import dataflow.core.component.annotation.*;
import dataflow.core.type.DoubleValue;
import dataflow.core.type.FloatValue;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.LongValue;

import java.util.List;

@DataFlowComponent(description = "A ValueProvider that outputs the bin for the given input value.")
public class BinnedValueProvider {

    private List<Double> bins;

    @DataFlowConfigurable
    public BinnedValueProvider(@DataFlowConfigProperty(
            description = "A list of bin boundaries. Values less than the first boundary will be assigned bin 0. " +
                    "Values at the boundary will be assigned to the next higher bin."
    ) List<Double> bins) {
        this.bins = bins;
    }

    @OutputValue(description = "The bin for the specific value")
    public int getValue(@InputValue(
            supportedTypes = {IntegerValue.TYPE, LongValue.TYPE, FloatValue.TYPE, DoubleValue.TYPE},
            description = "The value that will be used to determine the bin to provide")
                                double value) {
        for (int idx = 0; idx < bins.size(); idx++) {
            if (bins.get(idx) > value) {
                return idx;
            }
        }
        return bins.size();
    }
}
